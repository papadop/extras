///-------------------------------------------------------------------------------------------------
/// 
/// \file ovpCBoxAlgorithmCovarianceMatrixToFeatureVector.h
/// \brief Class of the box computing the Feature vector with the covariance matrix.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 17/10/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <Eigen/Dense>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector (0x7c265dba, 0x202c1f70)
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVectorDesc (0xc0fb0445, 0x0d1cd546)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)

namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/// <summary>	The class CBoxAlgorithmCovarianceMatrixToFeatureVector describes the box Covariance Matrix To Feature Vector. </summary>
		class CBoxAlgorithmCovarianceMatrixToFeatureVector : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t index) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmCovarianceMatrixToFeatureVector> m_i0MatrixCodec;	// Input Matrix Codec
			OpenViBEToolkit::TFeatureVectorEncoder<CBoxAlgorithmCovarianceMatrixToFeatureVector> m_o0FeatureCodec;	// Output Feature Codec
			//***** Matrices *****
			OpenViBE::IMatrix* m_iMatrix = nullptr;		// Input Matrix pointer
			OpenViBE::IMatrix* m_oMatrix = nullptr;		// Output Matrix pointer
			//***** Settings *****
			bool m_tangentSpace = true;					// Method to use (only tangent or squeeze now)
			Eigen::MatrixXd m_ref;						// Reference matrix for tangent space compute
			bool initRef();
			OpenViBE::Kernel::ELogLevel m_logLevel = OpenViBE::Kernel::LogLevel_Info;	// Log Level
		};

		/// <summary>	Descriptor of the box Covariance Matrix To Feature Vector. </summary>
		class CBoxAlgorithmCovarianceMatrixToFeatureVectorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Covariance Matrix To Feature Vector"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Transforms a covariance matrix into a feature vector"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Transforms a covariance matrix (size : NxN) into a feature vector (size : N(N+1)/2).\nThe Setting Tangent Space define if the transformation into vector is done in the tanget space or if it is a squeeze of the upper triangular matrix"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-jump-to"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmCovarianceMatrixToFeatureVector; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Covariance Matrix",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput("Output Feature Vector",OV_TypeId_FeatureVector);

				rBoxAlgorithmPrototype.addSetting("Tangent Space", OV_TypeId_Boolean, "true");
				rBoxAlgorithmPrototype.addSetting("Filename to Reference Matrix (CSV, empty for Identity)", OV_TypeId_Filename, "${Player_ScenarioDirectory}/Mean.csv");
				rBoxAlgorithmPrototype.addSetting("Log Level", OV_TypeId_LogLevel, "Information");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVectorDesc);
		};
	}  // namespace RiemannianGeometry
}  // namespace OpenViBEPlugins
