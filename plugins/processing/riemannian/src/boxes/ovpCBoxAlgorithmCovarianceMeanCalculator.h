///-------------------------------------------------------------------------------------------------
/// 
/// \file ovpCBoxAlgorithmCovarianceMeanCalculator.h
/// \brief Class of the box computing the mean of the covariance matrix.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/11/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <Eigen/Dense>
#include "utils/Metrics.hpp"

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_CovarianceMeanCalculator (0x67955ea4, 0x7c643c0f)
#define OVP_ClassId_BoxAlgorithm_CovarianceMeanCalculatorDesc (0x62e8f759, 0xd59d82a9)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)

namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/// <summary>	The class CBoxAlgorithmCovarianceMeanCalculator describes the box Covariance Mean Calculator. </summary>
		class CBoxAlgorithmCovarianceMeanCalculator : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t index) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CovarianceMeanCalculator);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmCovarianceMeanCalculator> m_i0StimulationCodec;				// Input Stimulation Codec
			std::vector<OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmCovarianceMeanCalculator>> m_i1MatrixCodec;	// Input Signal Codec
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCovarianceMeanCalculator> m_o0MatrixCodec;					// Output Codec
			//***** Matrices *****
			size_t m_nbClass = 1;											// Number of input classes
			std::vector<OpenViBE::IMatrix*> m_iMatrix;						// Input Matrix pointer
			OpenViBE::IMatrix* m_oMatrix = nullptr;							// Output Matrix pointer
			std::vector<Eigen::MatrixXd> m_covs;							// List of Covariance Matrix
			Eigen::MatrixXd m_mean;											// Mean
			EMetrics m_metric = Metric_Euclidian;							// Metric Used

			//***** Settings *****
			OpenViBE::IStimulationSet* m_iStimulation = nullptr;			// Stimulation receiver
			uint64_t m_stimulationName = OVTK_StimulationId_TrainCompleted;	// Name of stimulation to check
			OpenViBE::Kernel::ELogLevel m_logLevel = OpenViBE::Kernel::LogLevel_Info;	// Log Level

			// File
			OpenViBE::CString m_filename;
			bool saveCSV();
		};


		/// <summary>	Listener of the box Covariance Mean Calculator. </summary>
		class CBoxAlgorithmCovarianceMeanCalculatorListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t index) override;
			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t index) override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		/// <summary>	Descriptor of the box Covariance Mean Calculator. </summary>
		class CBoxAlgorithmCovarianceMeanCalculatorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Covariance Mean Calculator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Calculation of the mean of covariance matrix."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Calculation of the mean of covariance matrix.\nThe Calculation is done when \"OVTK_StimulationId_TrainCompleted\" is received.\nThe Mean is saved in a CSV File."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CovarianceMeanCalculator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmCovarianceMeanCalculator; }

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmCovarianceMeanCalculatorListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Stimulation", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Input Covariance Matrix 1", OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				rBoxAlgorithmPrototype.addOutput("Output Mean Matrix", OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Metric", OVP_TypeId_Metric, NAME_RIEM);
				rBoxAlgorithmPrototype.addSetting("Filename to save Matrix (CSV, empty to not save)",OV_TypeId_Filename, "${Player_ScenarioDirectory}/Mean.csv");
				rBoxAlgorithmPrototype.addSetting("Stimulation name that triggers the compute",OV_TypeId_Stimulation, "OVTK_StimulationId_TrainCompleted");
				rBoxAlgorithmPrototype.addSetting("Log Level", OV_TypeId_LogLevel, "Information");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CovarianceMeanCalculatorDesc);
		};
	} // namespace RiemannianGeometry
} // namespace OpenViBEPlugins
