#include "ovpCBoxAlgorithmCovarianceMeanCalculator.h"
#include "utils/Covariance.hpp"
#include "utils/Mean.hpp"
#include "utils/Basics.hpp"
#include "utils/ovpMisc.hpp"
#include <fstream>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculator::initialize()
{
	// Stimulations
	m_i0StimulationCodec.initialize(*this, 0);
	m_iStimulation = m_i0StimulationCodec.getOutputStimulationSet();

	// Classes
	const IBox& boxContext = this->getStaticBoxContext();
	m_nbClass = size_t(boxContext.getInputCount() - 1);
	m_i1MatrixCodec.resize(m_nbClass);
	m_iMatrix.resize(m_nbClass);
	for (size_t k = 0; k < m_nbClass; ++k)
	{
		m_i1MatrixCodec[k].initialize(*this, uint32_t(k + 1));
		m_iMatrix[k] = m_i1MatrixCodec[k].getOutputMatrix();
	}

	m_o0MatrixCodec.initialize(*this, 0);
	m_oMatrix = m_o0MatrixCodec.getInputMatrix();

	// Settings
	const CIdentifier metric(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0)));
	m_filename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_stimulationName = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	m_logLevel = ELogLevel(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3)));

	if (metric == OVP_TypeId_Metric_EUCL) { m_metric = Metric_Euclidian; }
	else if (metric == OVP_TypeId_Metric_RIEM) { m_metric = Metric_Riemann; }
	else if (metric == OVP_TypeId_Metric_LEUC) { m_metric = Metric_LogEuclidian; }
	else if (metric == OVP_TypeId_Metric_LDET) { m_metric = Metric_LogDet; }
	else if (metric == OVP_TypeId_Metric_KULL) { m_metric = Metric_Kullback; }
	else if (metric == OVP_TypeId_Metric_HARM) { m_metric = Metric_Harmonic; }
		//else if (Metric == OVP_TypeId_Metric_ALE) {	m_Metric = Metric_ALE; }
		//else if (Metric == OVP_TypeId_Metric_WASS) {	m_Metric = Metric_Wasserstein; }
	else if (metric == OVP_TypeId_Metric_IDEN) { m_metric = Metric_Identity; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Selected Metric", ErrorType::BadSetting); }

	this->getLogManager() << m_logLevel << MetricToString(m_metric).c_str() << " Metric\n";

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculator::uninitialize()
{
	this->getLogManager() << m_logLevel << m_covs.size() << " Matrices Registered, Mean Matrix : \n" << MatrixPrint(m_mean).c_str() << "\n";

	m_i0StimulationCodec.uninitialize();
	for (auto& codec : m_i1MatrixCodec) { codec.uninitialize(); }
	m_i1MatrixCodec.clear();
	m_iMatrix.clear();
	m_covs.clear();

	m_o0MatrixCodec.uninitialize();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculator::processInput(uint32_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculator::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	//***** Stimulations *****
	for (uint32_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_i0StimulationCodec.decode(i);													// Decode the chunk
		if (m_i0StimulationCodec.isBufferReceived())									// Buffer received
		{
			for (uint64_t j = 0; j < m_iStimulation->getStimulationCount(); ++j)
			{
				if (m_iStimulation->getStimulationIdentifier(j) == m_stimulationName)
				{
					OV_ERROR_UNLESS_KRF(Mean(m_covs, m_mean, m_metric), "Mean Compute Error", ErrorType::BadProcessing);	// Compute the mean
					MatrixConvert(m_mean, *m_oMatrix);
					const uint64_t tStart = boxContext.getInputChunkStartTime(0, i),	// Time Code Chunk Start
								   tEnd = boxContext.getInputChunkEndTime(0, i);		// Time Code Chunk End
					m_o0MatrixCodec.encodeBuffer();										// Buffer encoded
					boxContext.markOutputAsReadyToSend(0, tStart, tEnd);				// Makes the output available
					OV_ERROR_UNLESS_KRF(saveCSV(), "CSV Writing Error", ErrorType::BadFileWrite);
				}
			}
		}
	}
	
	//***** Matrix *****
	for (size_t k = 0; k < m_nbClass; ++k)
	{
		for (uint32_t i = 0; i < boxContext.getInputChunkCount(uint32_t(k + 1)); ++i)
		{
			m_i1MatrixCodec[k].decode(i);											// Decode the chunk
			OV_ERROR_UNLESS_KRF(m_iMatrix[k]->getDimensionCount() == 2, "Invalid Input Signal", ErrorType::BadInput);

			if (m_i1MatrixCodec[k].isHeaderReceived() && k == 0)					// First Header received
			{
				const uint64_t tStart = boxContext.getInputChunkStartTime(1, i),	// Time Code Chunk Start
							   tEnd = boxContext.getInputChunkEndTime(1, i);		// Time Code Chunk End
				const uint32_t n = m_iMatrix[0]->getDimensionSize(0);
				MatrixInit(*m_oMatrix, size_t(n));									// Update Size and set to 0
				m_o0MatrixCodec.encodeHeader();										// Header encoded
				boxContext.markOutputAsReadyToSend(0, tStart, tEnd);				// Makes the output available
			}
			else if (m_i1MatrixCodec[k].isBufferReceived())							// Buffer received
			{
				Eigen::MatrixXd cov;
				MatrixConvert(*m_iMatrix[k], cov);
				m_covs.push_back(cov);
			}
			else if (m_i1MatrixCodec[k].isEndReceived() && k == 0)					// First End received
			{
				const uint64_t tStart = boxContext.getInputChunkStartTime(1, i),	// Time Code Chunk Start
							   tEnd = boxContext.getInputChunkEndTime(1, i);		// Time Code Chunk End
				m_o0MatrixCodec.encodeEnd();
				boxContext.markOutputAsReadyToSend(0, tStart, tEnd);				// Makes the output available
			}
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculator::saveCSV()
{
	if (m_filename.length() == 0) { return true; }

	std::ofstream file;
	file.open(m_filename.toASCIIString(), std::ios::trunc);
	OV_ERROR_UNLESS_KRF(file.is_open(),
		"Error opening file [" << m_filename << "] for writing", ErrorType::BadFileWrite);

	// Header
	const size_t s = m_mean.rows();
	file << "Time:" << s << "x" << s << ",End Time,";
	for (size_t i = 1; i <= s; ++i)
	{
		for (size_t j = 0; j < s; ++j)
		{
			file << i << ":,";
		}
	}
	file << "Event Id,Event Date,Event Duration\n";

	// Matrix
	file << "0.0000000000,0.0000000000,";	// Time
	const Eigen::IOFormat fmt(Eigen::FullPrecision, 0, ", ", ", ", "", "", "", ",,,\n");
	file << m_mean.format(fmt);
	file.close();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculatorListener::onInputAdded(IBox& rBox, const uint32_t index)
{
	rBox.setInputType(index, OV_TypeId_StreamedMatrix);
	char name[1024];
	sprintf(name, "Input Covariance Matrix %u", index);
	rBox.setInputName(index, name);
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMeanCalculatorListener::onInputRemoved(IBox& /*rBox*/, const uint32_t /*index*/) { return true; }
//---------------------------------------------------------------------------------------------------
