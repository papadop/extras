///-------------------------------------------------------------------------------------------------
/// 
/// \file ovpCBoxAlgorithmCovarianceMatrixCalculator.h
/// \brief Class of the box computing the covariance matrix
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 16/10/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------
# pragma once

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "utils/Covariance.hpp"

#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculator (0x9a93af80, 0x6449c826)
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculatorDesc (0x12fcd91f, 0xd1d8f678)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)

namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/// <summary>	 The class CBoxAlgorithmCovarianceMatrixCalculator describes the box Covariance Matrix Calculator. </summary>
		class CBoxAlgorithmCovarianceMatrixCalculator : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t index) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculator);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmCovarianceMatrixCalculator> m_i0SignalCodec;			// Input Signal Codec
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCovarianceMatrixCalculator> m_o0MatrixCodec;	// Output Matrix Codec
			//***** Matrices *****
			OpenViBE::IMatrix* m_iMatrix = nullptr;		// Input Matrix pointer
			OpenViBE::IMatrix* m_oMatrix = nullptr;		// Output Matrix pointer
			//***** Settings *****
			EEstimator m_est = Estimator_COV;			// Covariance Estimator
			bool m_center = true;						// Center data
			OpenViBE::Kernel::ELogLevel m_logLevel = OpenViBE::Kernel::LogLevel_Info;	// Log Level
		};

		/// <summary>	 Descriptor of the box Covariance Matrix Calculator. </summary>
		class CBoxAlgorithmCovarianceMatrixCalculatorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Covariance Matrix Calculator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Calculation of the covariance matrix of the input signal."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Calculation of the covariance matrix of the input signal.\nReturns a covariance matrix of size NxN per each input chunk. Where N is the number of channels."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmCovarianceMatrixCalculator; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Output Covariance Matrix", OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Estimator", OVP_TypeId_Estimator, NAME_COV);
				rBoxAlgorithmPrototype.addSetting("Center Data", OV_TypeId_Boolean, "true");
				rBoxAlgorithmPrototype.addSetting("Log Level", OV_TypeId_LogLevel, "Information");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculatorDesc);
		};
	} // namespace RiemannianGeometry
}  // namespace OpenViBEPlugins
