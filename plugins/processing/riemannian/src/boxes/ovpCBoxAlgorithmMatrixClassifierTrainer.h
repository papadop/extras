///-------------------------------------------------------------------------------------------------
/// 
/// \file ovpCBoxAlgorithmMatrixClassifierProcessor.h
/// \brief Class of the box Train a Matrix Classifier.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 17/01/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <Eigen/Dense>
#include "classifier/IMatrixClassifier.hpp"

#define OVP_ClassId_BoxAlgorithm_MatrixClassifierTrainer (0xc0b79b42, 0x4150c837)
#define OVP_ClassId_BoxAlgorithm_MatrixClassifierTrainerDesc (0x26f4fa93, 0x704d39dd)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)

namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/// <summary>	The class CBoxAlgorithmMatrixClassifierTrainer describes the box Matrix Classifier Trainer. </summary>
		class CBoxAlgorithmMatrixClassifierTrainer : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t index) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_MatrixClassifierTrainer);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmMatrixClassifierTrainer> m_i0StimulationCodec;
			std::vector<OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmMatrixClassifierTrainer>> m_i1MatrixCodec;	// Input Signal Codec
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmMatrixClassifierTrainer> m_o0StimulationCodec;
			//***** Matrices *****
			size_t m_nbClass = 2;											// Number of input classes
			std::vector<OpenViBE::IMatrix*> m_iMatrix;						// Input Matrix pointer
			std::vector<std::vector<Eigen::MatrixXd>> m_covs;				// List of Covariance Matrix one class by row

			//***** Stimulations *****
			OpenViBE::IStimulationSet *m_iStimulation = nullptr,			// Stimulation receiver
									  *m_oStimulation = nullptr;			// Stimulation sender
			uint64_t m_stimulationName = OVTK_StimulationId_Train;			// Name of stimulation to check for train lunch
			std::vector<uint64_t> m_stimulationClassName;					// Name of stimulation to check for each class
			bool m_isTrain = false;
			//***** Settings *****
			OpenViBE::Kernel::ELogLevel m_logLevel = OpenViBE::Kernel::LogLevel_Info;	// Log Level
			OpenViBE::CIdentifier m_method = OVP_TypeId_Matrix_Classifier_MDM;

			//***** File *****
			OpenViBE::CString m_filename;

			bool train();
			bool saveXML(IMatrixClassifier* classifier);
		};

		/// <summary>	Listener of the box Matrix Classifier Trainer. </summary>
		class CBoxAlgorithmMatrixClassifierTrainerListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t index) override;;
			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t index) override;;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};
		
		/// <summary>	Descriptor of the box Matrix Classifier Trainer. </summary>
		class CBoxAlgorithmMatrixClassifierTrainerDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Matrix Classifier Trainer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Matrix classifier trainer."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Matrix classifier trainer."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_MatrixClassifierTrainer; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmMatrixClassifierTrainer; }

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmMatrixClassifierTrainerListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Stimulations",OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Matrix for class 1",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addInput("Matrix for class 2",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);

				rBoxAlgorithmPrototype.addOutput("Tran-completed Flag",OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Train trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Train");
				rBoxAlgorithmPrototype.addSetting("Filename to save classifier model",OV_TypeId_Filename, "${Player_ScenarioDirectory}/my-classifier.xml");
				rBoxAlgorithmPrototype.addSetting("Method", OVP_TypeId_Matrix_Classifier, NAME_MDM);
				rBoxAlgorithmPrototype.addSetting("Log Level", OV_TypeId_LogLevel, "Information");
				rBoxAlgorithmPrototype.addSetting("Class 1 label", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Class 2 label", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_02");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_MatrixClassifierTrainerDesc);
		};
	}  // namespace RiemannianGeometry
}  // namespace OpenViBEPlugins
