#include "ovpCBoxAlgorithmMatrixClassifierTrainer.h"
#include "3rd-party/tinyxml2.h"
#include "classifier/CMatrixClassifierMDM.hpp"
#include "classifier/CMatrixClassifierMDMRebias.hpp"
#include "classifier/CMatrixClassifierFgMDMRT.hpp"
#include "utils/ovpMisc.hpp"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

using namespace tinyxml2;

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainer::initialize()
{
	// Stimulations
	m_i0StimulationCodec.initialize(*this, 0);
	m_iStimulation = m_i0StimulationCodec.getOutputStimulationSet();

	m_o0StimulationCodec.initialize(*this, 0);
	m_oStimulation = m_o0StimulationCodec.getInputStimulationSet();

	// Classes
	const IBox& boxContext = this->getStaticBoxContext();
	m_nbClass = size_t(boxContext.getInputCount() - 1);
	m_i1MatrixCodec.resize(m_nbClass);
	m_iMatrix.resize(m_nbClass);
	m_covs.resize(m_nbClass);
	m_stimulationClassName.resize(m_nbClass);
	for (size_t k = 0; k < m_nbClass; ++k)
	{
		m_i1MatrixCodec[k].initialize(*this, uint32_t(k + 1));
		m_iMatrix[k] = m_i1MatrixCodec[k].getOutputMatrix();
		m_stimulationClassName[k] = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), uint32_t(k + 4)));
	}

	// Settings
	m_stimulationName = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_filename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_method = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2));
	m_logLevel = ELogLevel(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3)));

	OV_ERROR_UNLESS_KRF(m_filename.length() != 0, "Invalid empty model filename", ErrorType::BadSetting);

	// Printing info
	std::stringstream msg;
	msg << std::endl << "Number of classes : " << m_nbClass << std::endl
		<< "Filename : " << m_filename << std::endl
		<< "Method : ";
	if (m_method == OVP_TypeId_Matrix_Classifier_MDM) { msg << NAME_MDM; }
	else if (m_method == OVP_TypeId_Matrix_Classifier_MDM_REBIAS) { msg << NAME_MDM_REBIAS; }
	else if (m_method == OVP_TypeId_Matrix_Classifier_FGMDM_RT) { msg << NAME_FGMDM_RT; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Selected Method", ErrorType::BadSetting); }
	msg << std::endl;
	this->getLogManager() << m_logLevel << msg.str().c_str();

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainer::uninitialize()
{
	m_i0StimulationCodec.uninitialize();
	for (auto& codec : m_i1MatrixCodec) { codec.uninitialize(); }
	m_i1MatrixCodec.clear();
	m_iMatrix.clear();
	for (auto& cov : m_covs) { cov.clear(); }
	m_covs.clear();
	m_stimulationClassName.clear();

	m_o0StimulationCodec.uninitialize();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainer::processInput(uint32_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainer::process()
{
	if (!m_isTrain)
	{
		IBoxIO& boxContext = this->getDynamicBoxContext();

		//***** Stimulations *****
		for (uint32_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
		{
			m_i0StimulationCodec.decode(i);										// Decode the chunk
			const uint64_t tStart = boxContext.getInputChunkStartTime(0, i),	// Time Code Chunk Start
						   tEnd = boxContext.getInputChunkEndTime(0, i);		// Time Code Chunk End

			if (m_i0StimulationCodec.isHeaderReceived())
			{
				m_o0StimulationCodec.encodeHeader();
				boxContext.markOutputAsReadyToSend(0, 0, 0);
			}
			else if (m_i0StimulationCodec.isBufferReceived())					// Buffer received
			{
				for (uint64_t j = 0; j < m_iStimulation->getStimulationCount(); ++j)
				{
					if (m_iStimulation->getStimulationIdentifier(j) == m_stimulationName)
					{
						OV_ERROR_UNLESS_KRF(train(), "Train failed", ErrorType::BadProcessing);
						const uint64_t stim = this->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, "OVTK_StimulationId_TrainCompleted");
						m_oStimulation->appendStimulation(stim, m_iStimulation->getStimulationDate(j), 0);
						m_isTrain = true;
					}
				}
				m_o0StimulationCodec.encodeBuffer();
				boxContext.markOutputAsReadyToSend(0, tStart, tEnd);
			}
			else if (m_i0StimulationCodec.isEndReceived())
			{
				m_o0StimulationCodec.encodeEnd();
				boxContext.markOutputAsReadyToSend(0, tStart, tEnd);
			}
		}

		//***** Matrix *****
		for (size_t k = 0; k < m_nbClass; ++k)
		{
			for (uint32_t i = 0; i < boxContext.getInputChunkCount(uint32_t(k + 1)); ++i)
			{
				m_i1MatrixCodec[k].decode(i);									// Decode the chunk
				OV_ERROR_UNLESS_KRF(m_iMatrix[k]->getDimensionCount() == 2, "Invalid Input Signal", ErrorType::BadInput);

				if (m_i1MatrixCodec[k].isBufferReceived()) 						// Buffer received
				{
					Eigen::MatrixXd cov;
					MatrixConvert(*m_iMatrix[k], cov);
					m_covs[k].push_back(cov);
				}
			}
		}
	}
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainer::train()
{
	IMatrixClassifier* matrixClassifier;
	if (m_method == OVP_TypeId_Matrix_Classifier_MDM) { matrixClassifier = new CMatrixClassifierMDM; }
	else if (m_method == OVP_TypeId_Matrix_Classifier_MDM_REBIAS) { matrixClassifier = new CMatrixClassifierMDMRebias; }
	else if (m_method == OVP_TypeId_Matrix_Classifier_FGMDM_RT) { matrixClassifier = new CMatrixClassifierFgMDMRT; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Selected Method", ErrorType::BadSetting); }

	OV_ERROR_UNLESS_KRF(matrixClassifier->train(m_covs), "Train failed", ErrorType::BadProcessing);
	OV_ERROR_UNLESS_KRF(saveXML(matrixClassifier), "Save failed", ErrorType::BadProcessing);

	this->getLogManager() << m_logLevel << matrixClassifier->print().str().c_str();
	delete matrixClassifier;
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainer::saveXML(IMatrixClassifier* classifier)
{
	OV_ERROR_UNLESS_KRF(classifier->saveXML(m_filename.toASCIIString()), "Save failed", ErrorType::BadFileWrite);

	// Add Stimulation to XML
	XMLDocument xmlDoc;
	// Load File
	OV_ERROR_UNLESS_KRF(xmlDoc.LoadFile(m_filename.toASCIIString()) == 0, "Unable to load xml file : " << m_filename.toASCIIString(), ErrorType::BadFileRead);

	// Load Root
	XMLNode* root = xmlDoc.FirstChild();
	OV_ERROR_UNLESS_KRF(root != nullptr, "Unable to get xml root node", ErrorType::BadFileParsing);

	// Load Data
	XMLElement* data = root->FirstChildElement("Classifier-data");
	OV_ERROR_UNLESS_KRF(data != nullptr, "Unable to get xml classifier node", ErrorType::BadFileParsing);

	XMLElement* element = data->FirstChildElement("Class");					// Get Fist Class Node
	for (size_t k = 0; k < classifier->getClassCount(); ++k)				// for each class
	{
		OV_ERROR_UNLESS_KRF(element != nullptr, "Invalid class node", ErrorType::BadFileParsing);
		const size_t idx = element->IntAttribute("class-id");				// Get Id (normally idx = k)
		OV_ERROR_UNLESS_KRF(idx == k, "Invalid Class id", ErrorType::BadFileParsing);
		const CString stimulationName = this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, m_stimulationClassName[k]);
		element->SetAttribute("stimulation", stimulationName.toASCIIString());
		element = element->NextSiblingElement("Class");						// Next Class
	}
	return xmlDoc.SaveFile(m_filename.toASCIIString()) == 0;				// save XML (if != 0 it means error)
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainerListener::onInputAdded(IBox& rBox, const uint32_t index)
{
	rBox.setInputType(index, OV_TypeId_StreamedMatrix);
	char name[1024], stimulation[1024];
	sprintf(name, "Matrix for class %u", index);
	rBox.setInputName(index, name);

	sprintf(name, "Class %u label", index);
	sprintf(stimulation, "OVTK_StimulationId_Label_%02u", index);
	rBox.addSetting(name, OV_TypeId_Stimulation, stimulation);

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierTrainerListener::onInputRemoved(IBox& rBox, const uint32_t index)
{
	rBox.removeSetting(index + 4);	// (avoid the 4 first setting)
	//check if the removed class is not the last
	if (index != rBox.getInputCount())
	{
		char name[1024], stimulation[1024];
		for (uint32_t k = index; k < rBox.getInputCount(); ++k)
		{
			sprintf(name, "Matrix for class %u", k);
			rBox.setInputName(k, name);
			this->getLogManager() << LogLevel_Info << name << "\n";

			sprintf(name, "Class %u label", k);
			sprintf(stimulation, "OVTK_StimulationId_Label_%02u", k);
			rBox.setSettingName(uint32_t(k + 3), name);					// +3 because one is removed
			rBox.setSettingDefaultValue(uint32_t(k + 3), stimulation);
			rBox.setSettingValue(uint32_t(k + 3), stimulation);
		}
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
