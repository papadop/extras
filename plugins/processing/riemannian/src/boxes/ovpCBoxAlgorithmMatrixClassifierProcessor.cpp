#include "ovpCBoxAlgorithmMatrixClassifierProcessor.h"
#include "classifier/CMatrixClassifierMDM.hpp"
#include "classifier/CMatrixClassifierMDMRebias.hpp"
#include "classifier/CMatrixClassifierFgMDMRT.hpp"
#include "utils/ovpMisc.hpp"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

using namespace tinyxml2;

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierProcessor::initialize()
{
	//***** Codecs *****
	m_i0StimulationCodec.initialize(*this, 0);
	m_i1MatrixCodec.initialize(*this, 1);
	m_o0StimulationCodec.initialize(*this, 0);
	m_o1MatrixCodec.initialize(*this, 1);
	m_o2MatrixCodec.initialize(*this, 2);

	//***** Pointers *****
	m_i0Stimulation = m_i0StimulationCodec.getOutputStimulationSet();
	m_i1Matrix = m_i1MatrixCodec.getOutputMatrix();
	m_o0Stimulation = m_o0StimulationCodec.getInputStimulationSet();
	m_o1Matrix = m_o1MatrixCodec.getInputMatrix();
	m_o2Matrix = m_o2MatrixCodec.getInputMatrix();

	// Settings
	m_filename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	const uint64_t adatpation = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1));
	if (adatpation == OVP_TypeId_Classifier_Adaptation_NONE) { m_adaptation = Adaptation_None; }
	else if (adatpation == OVP_TypeId_Classifier_Adaptation_SUPERVISED) { m_adaptation = Adaptation_Supervised; }
	else if (adatpation == OVP_TypeId_Classifier_Adaptation_UNSUPERVISED) { m_adaptation = Adaptation_Unsupervised; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Adaptation Method", ErrorType::BadSetting); }

	m_logLevel = ELogLevel(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2)));

	OV_ERROR_UNLESS_KRF(m_filename.length() != 0, "Invalid empty model filename", ErrorType::BadSetting);

	OV_ERROR_UNLESS_KRF(loadXML(), "Loading XML Error", ErrorType::BadFileRead);
	// Change matrix size
	VectorInit(*m_o1Matrix, m_classifier->getClassCount());
	VectorInit(*m_o2Matrix, m_classifier->getClassCount());

	// Printing info
	std::stringstream msg;
	msg << std::endl << "Filename : " << m_filename << std::endl
		<< "Method : " << m_classifier->getType() << " with " << IMatrixClassifier::getAdaptation(m_adaptation) << " adaptation" << std::endl
		<< "Number of classes : " << m_classifier->getClassCount() << std::endl;
	for (size_t k = 0; k < m_classifier->getClassCount(); ++k)
	{
		msg << "Stimulation for class " << k << " : " << m_stimulationClassName[k] << " => [" << this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, m_stimulationClassName[k]) << "]\n";
	}
	this->getLogManager() << m_logLevel << msg.str().c_str();

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierProcessor::uninitialize()
{
	m_i0StimulationCodec.uninitialize();
	m_i1MatrixCodec.uninitialize();
	m_o0StimulationCodec.uninitialize();
	m_o1MatrixCodec.uninitialize();
	m_o2MatrixCodec.uninitialize();

	delete m_classifier;	// check if pointeur is null is useless now.
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierProcessor::processInput(uint32_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierProcessor::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();
	//**** Stimulation *****
	if (m_adaptation != Adaptation_None)
	{
		for (uint32_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
		{
			m_i0StimulationCodec.decode(i);
			if (m_i0StimulationCodec.isBufferReceived())					// Buffer received
			{
				bool finish = false;
				for (uint64_t j = 0; j < m_i0Stimulation->getStimulationCount() && !finish; ++j)
				{
					const uint64_t stim = m_i0Stimulation->getStimulationIdentifier(j);
					for (size_t k = 0; k < m_stimulationClassName.size() && !finish; ++k)
					{
						if (stim == this->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial"))
						{
							m_lastLabelReceived = std::numeric_limits<std::size_t>::max();
							finish = true;
						}
						else if (stim == m_stimulationClassName[k])
						{
							m_lastLabelReceived = k;
							finish = true;
						}
					}
				}
			}
		}
	}

	//***** Matrix *****
	if (m_adaptation == Adaptation_None || m_lastLabelReceived < m_stimulationClassName.size()) 
	{
		for (uint32_t i = 0; i < boxContext.getInputChunkCount(1); ++i)
		{
			m_i1MatrixCodec.decode(i);											// Decode the chunk
			OV_ERROR_UNLESS_KRF(m_i1Matrix->getDimensionCount() == 2, "Invalid Input Signal", ErrorType::BadInput);
			const uint64_t tStart = boxContext.getInputChunkStartTime(1, i),	// Time Code Chunk Start
						   tEnd = boxContext.getInputChunkEndTime(1, i);		// Time Code Chunk End

			if (m_i1MatrixCodec.isHeaderReceived()) 							// Header received
			{
				m_o0StimulationCodec.encodeHeader();
				m_o1MatrixCodec.encodeHeader();
				m_o2MatrixCodec.encodeHeader();
			}
			else if (m_i1MatrixCodec.isBufferReceived()) 						// Buffer received
			{
				OV_ERROR_UNLESS_KRF(classify(tEnd), "Classify Error", ErrorType::BadProcessing);
				m_o0StimulationCodec.encodeBuffer();
				m_o1MatrixCodec.encodeBuffer();
				m_o2MatrixCodec.encodeBuffer();
			}
			else if (m_i1MatrixCodec.isEndReceived()) 							// End received
			{
				m_o0StimulationCodec.encodeEnd();
				m_o1MatrixCodec.encodeEnd();
				m_o2MatrixCodec.encodeEnd();
			}
			for (uint32_t j = 0; j < 3; ++j) { boxContext.markOutputAsReadyToSend(j, tStart, tEnd); }
		}
	}
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierProcessor::classify(const uint64_t tEnd)
{
	std::vector<double> distance, probability;
	Eigen::MatrixXd cov;
	size_t classId;
	MatrixConvert(*m_i1Matrix, cov);
	OV_ERROR_UNLESS_KRF(m_classifier->classify(cov, classId, distance, probability, m_adaptation, m_lastLabelReceived), "Classify Error", ErrorType::BadProcessing);

	//Fill Output
	m_o0Stimulation->setStimulationCount(1);									//No append stimulation only one is used
	m_o0Stimulation->setStimulationIdentifier(0, m_stimulationClassName[classId]);
	m_o0Stimulation->setStimulationDate(0, tEnd);
	m_o0Stimulation->setStimulationDuration(0, 0);
	MatrixConvert(distance, *m_o1Matrix);
	MatrixConvert(probability, *m_o2Matrix);

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmMatrixClassifierProcessor::loadXML()
{
	delete m_classifier;	// if (m_classifer != nullptr) useless now

	XMLDocument xmlDoc;
	// Load File
	OV_ERROR_UNLESS_KRF(xmlDoc.LoadFile(m_filename.toASCIIString()) == 0, "Unable to load xml file : " << m_filename.toASCIIString(), ErrorType::BadFileRead);

	// Load Root
	XMLNode* root = xmlDoc.FirstChild();
	OV_ERROR_UNLESS_KRF(root != nullptr, "Unable to get xml root node", ErrorType::BadFileParsing);

	// Load Data
	XMLElement* data = root->FirstChildElement("Classifier-data");
	OV_ERROR_UNLESS_KRF(data != nullptr, "Unable to get xml classifier node", ErrorType::BadFileParsing);

	const std::string classifierType = data->Attribute("type");

	// Check Type
	if (classifierType == IMatrixClassifier::getType(Matrix_Classifier_MDM)) { m_classifier = new CMatrixClassifierMDM; }
	else if (classifierType == IMatrixClassifier::getType(Matrix_Classifier_MDM_Rebias)) { m_classifier = new CMatrixClassifierMDMRebias; }
	else if (classifierType == IMatrixClassifier::getType(Matrix_Classifier_FgMDM_RT)) { m_classifier = new CMatrixClassifierFgMDMRT; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Classifier", ErrorType::BadFileParsing); }

	// Object Load
	m_classifier->loadXML(m_filename.toASCIIString());

	// Load Stimulation
	m_stimulationClassName.resize(m_classifier->getClassCount());
	XMLElement* element = data->FirstChildElement("Class");						// Get Fist Class Node
	for (size_t k = 0; k < m_classifier->getClassCount(); ++k)					// for each class
	{
		OV_ERROR_UNLESS_KRF(element != nullptr, "Invalid class node", ErrorType::BadFileParsing);
		const size_t idx = element->IntAttribute("class-id");					// Get Id (normally idx = k)
		OV_ERROR_UNLESS_KRF(idx == k, "Invalid Class id", ErrorType::BadFileParsing);
		m_stimulationClassName[k] = this->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, element->Attribute("stimulation"));
		element = element->NextSiblingElement("Class");							// Next Class
	}

	return true;
}
//---------------------------------------------------------------------------------------------------
