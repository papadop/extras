///-------------------------------------------------------------------------------------------------
/// 
/// \file ovpCBoxAlgorithmMatrixClassifierProcessor.h
/// \brief Class of the box Process a Matrix Classifier.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 17/01/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "classifier/IMatrixClassifier.hpp"
#include "ovp_defines.h"

#define OVP_ClassId_BoxAlgorithm_MatrixClassifierProcessor (0x918f6952, 0xb22ddf0d)
#define OVP_ClassId_BoxAlgorithm_MatrixClassifierProcessorDesc (0x8cf29eec, 0x223fbfc5)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)

namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/// <summary>	The class CBoxAlgorithmMatrixClassifierProcessor describes the box Matrix Classifier Processor. </summary>
		class CBoxAlgorithmMatrixClassifierProcessor : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t index) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_MatrixClassifierProcessor);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmMatrixClassifierProcessor> m_i0StimulationCodec;
			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmMatrixClassifierProcessor> m_i1MatrixCodec;
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmMatrixClassifierProcessor> m_o0StimulationCodec;
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmMatrixClassifierProcessor> m_o1MatrixCodec, m_o2MatrixCodec;

			//***** Matrices *****
			OpenViBE::IMatrix *m_i1Matrix = nullptr, *m_o1Matrix = nullptr, *m_o2Matrix = nullptr;	// Matrix Pointer
			Eigen::MatrixXd m_distance, m_probability;												// Eigen Matrix

			//***** Stimulations *****
			OpenViBE::IStimulationSet *m_i0Stimulation = nullptr,						// Stimulation receiver
									  *m_o0Stimulation = nullptr;						// Stimulation sender
			std::vector<uint64_t> m_stimulationClassName;								// Name of stimulation to check for each class

			IMatrixClassifier* m_classifier = nullptr;									// Classifier
			size_t m_lastLabelReceived = std::numeric_limits<std::size_t>::max();		// Last label received for Supervised Adaptation
			EAdaptations m_adaptation = Adaptation_None;								// Adaptation Method
			//***** Setting *****
			OpenViBE::CString m_filename;
			OpenViBE::Kernel::ELogLevel m_logLevel = OpenViBE::Kernel::LogLevel_Info;	// Log Level

			bool classify(uint64_t tEnd);
			bool loadXML();
		};

		/// <summary>	Descriptor of the box Matrix Classifier Processor. </summary>
		class CBoxAlgorithmMatrixClassifierProcessorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Matrix Classifier Processor"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Matrix classifier Processor."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Matrix classifier Processor."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_MatrixClassifierProcessor; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmMatrixClassifierProcessor; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Expected Label", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Input Matrix",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addOutput("Label",OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Distance",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput("Probability",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Filename to load classifier model",OV_TypeId_Filename, "${Player_ScenarioDirectory}/my-classifier.xml");
				rBoxAlgorithmPrototype.addSetting("Adaptation", OVP_TypeId_Classifier_Adaptation, "None");
				rBoxAlgorithmPrototype.addSetting("Log Level", OV_TypeId_LogLevel, "Information");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_MatrixClassifierProcessorDesc);
		};
	}  // namespace RiemannianGeometry
}  // namespace OpenViBEPlugins