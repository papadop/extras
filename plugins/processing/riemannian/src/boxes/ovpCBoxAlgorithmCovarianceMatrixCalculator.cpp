﻿#include "ovpCBoxAlgorithmCovarianceMatrixCalculator.h"
#include "utils/ovpMisc.hpp"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMatrixCalculator::initialize()
{
	m_i0SignalCodec.initialize(*this, 0);
	m_o0MatrixCodec.initialize(*this, 0);
	m_iMatrix = m_i0SignalCodec.getOutputMatrix();
	m_oMatrix = m_o0MatrixCodec.getInputMatrix();

	//***** Settings *****
	const CIdentifier method(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0)));
	m_center = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_logLevel = ELogLevel(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2)));

	if (method == OVP_TypeId_Estimator_COV) { m_est = Estimator_COV; }
	else if (method == OVP_TypeId_Estimator_COR) { m_est = Estimator_COR; }
	else if (method == OVP_TypeId_Estimator_LWF) { m_est = Estimator_LWF; }
	else if (method == OVP_TypeId_Estimator_SCM) { m_est = Estimator_SCM; }
	else if (method == OVP_TypeId_Estimator_OAS) { m_est = Estimator_OAS; }
		//else if (est == OVP_TypeId_Estimator_MCD) {	m_est = Estimator_MCD; }
	else if (method == OVP_TypeId_Estimator_IDE) { m_est = Estimator_IDE; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Selected Estimator", ErrorType::BadSetting); }

	this->getLogManager() << m_logLevel << EstimatorToString(m_est).c_str() << " Estimator" << (m_center ? ", Center Data " : "") << "\n";
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMatrixCalculator::uninitialize()
{
	m_i0SignalCodec.uninitialize();
	m_o0MatrixCodec.uninitialize();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMatrixCalculator::processInput(uint32_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmCovarianceMatrixCalculator::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	for (uint32_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_i0SignalCodec.decode(i);											// Decode the chunk
		OV_ERROR_UNLESS_KRF(m_iMatrix->getDimensionCount() == 2, "Invalid Input Signal", ErrorType::BadInput);

		const uint64_t tStart = boxContext.getInputChunkStartTime(0, i),	// Time Code Chunk Start
					   tEnd = boxContext.getInputChunkEndTime(0, i);		// Time Code Chunk End
		const auto nChannels = size_t(m_iMatrix->getDimensionSize(0));

		if (m_i0SignalCodec.isHeaderReceived())								// Header received
		{
			MatrixInit(*m_oMatrix, nChannels);								// Update Size and set to 0
			m_o0MatrixCodec.encodeHeader();									// Header encoded
		}
		else if (m_i0SignalCodec.isBufferReceived())						// Buffer received
		{
			OV_ERROR_UNLESS_KRF(
				CovarianceMatrix(*m_iMatrix, *m_oMatrix, m_center, m_est),	// Compute Covariance
				"Covariance Matrix Processing Error", ErrorType::BadProcessing);
			m_o0MatrixCodec.encodeBuffer();									// Buffer encoded
		}
		else if (m_i0SignalCodec.isEndReceived()) { m_o0MatrixCodec.encodeEnd(); }	// End receivded and encoded

		boxContext.markOutputAsReadyToSend(0, tStart, tEnd);				// Makes the output available
	}
	return true;
}
//---------------------------------------------------------------------------------------------------
