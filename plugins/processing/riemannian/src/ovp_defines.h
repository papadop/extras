///-------------------------------------------------------------------------------------------------
/// 
/// \file ovp_defines.h
/// \brief Defines list for Setting, Shortcut Macro and const.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 26/10/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// \remarks 
/// - List of Estimator inspired by the work of Alexandre Barachant : <a href="https://github.com/alexandrebarachant/pyRiemann">pyRiemann</a> (<a href="https://github.com/alexandrebarachant/pyRiemann/blob/master/LICENSE">License</a>).
/// - List of Metrics inspired by the work of Alexandre Barachant : <a href="https://github.com/alexandrebarachant/pyRiemann">pyRiemann</a> (<a href="https://github.com/alexandrebarachant/pyRiemann/blob/master/LICENSE">License</a>).
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

// Covariance Estimator List
//---------------------------------------------------------------------------------------------------
#define OVP_TypeId_Estimator							OpenViBE::CIdentifier(0x5261636B, 0x45535449)
#define OVP_TypeId_Estimator_COV						OpenViBE::CIdentifier(0x5261636B, 0x45434F56)
#define OVP_TypeId_Estimator_COR						OpenViBE::CIdentifier(0x5261636B, 0x45434F52)
#define OVP_TypeId_Estimator_LWF		 				OpenViBE::CIdentifier(0x5261636B, 0x454C5746)
#define OVP_TypeId_Estimator_SCM						OpenViBE::CIdentifier(0x5261636B, 0x4553434D)
#define OVP_TypeId_Estimator_OAS						OpenViBE::CIdentifier(0x5261636B, 0x45414F53)
#define OVP_TypeId_Estimator_MCD						OpenViBE::CIdentifier(0x5261636B, 0x454D4344)
#define OVP_TypeId_Estimator_IDE						OpenViBE::CIdentifier(0x5261636B, 0x45494445)
//---------------------------------------------------------------------------------------------------

// Metric List
//---------------------------------------------------------------------------------------------------
#define OVP_TypeId_Metric								OpenViBE::CIdentifier(0x5261636B, 0x4D455452)
#define OVP_TypeId_Metric_RIEM							OpenViBE::CIdentifier(0x5261636B, 0x4D524945)
#define OVP_TypeId_Metric_EUCL							OpenViBE::CIdentifier(0x5261636B, 0x4D455543)
#define OVP_TypeId_Metric_LEUC		 					OpenViBE::CIdentifier(0x5261636B, 0x4D4C4745)
#define OVP_TypeId_Metric_LDET							OpenViBE::CIdentifier(0x5261636B, 0x4D4C4744)
#define OVP_TypeId_Metric_KULL							OpenViBE::CIdentifier(0x5261636B, 0x4D4B554C)
#define OVP_TypeId_Metric_HARM							OpenViBE::CIdentifier(0x5261636B, 0x4D484152)
#define OVP_TypeId_Metric_ALE							OpenViBE::CIdentifier(0x5261636B, 0x4D414C45)
#define OVP_TypeId_Metric_WASS							OpenViBE::CIdentifier(0x5261636B, 0x4D574153)
#define OVP_TypeId_Metric_IDEN							OpenViBE::CIdentifier(0x5261636B, 0x4D494445)
//---------------------------------------------------------------------------------------------------

// Classifier List
//---------------------------------------------------------------------------------------------------
#define OVP_TypeId_Matrix_Classifier					OpenViBE::CIdentifier(0x5261636B, 0x436C6173)
#define OVP_TypeId_Matrix_Classifier_MDM				OpenViBE::CIdentifier(0x5261636B, 0x434D444D)
#define OVP_TypeId_Matrix_Classifier_MDM_REBIAS			OpenViBE::CIdentifier(0x5261636B, 0x434d4452)
#define OVP_TypeId_Matrix_Classifier_FGMDM_RT			OpenViBE::CIdentifier(0x5261636B, 0x43464d52)
//---------------------------------------------------------------------------------------------------

// Adaptation List
//---------------------------------------------------------------------------------------------------
#define OVP_TypeId_Classifier_Adaptation				OpenViBE::CIdentifier(0x5261636B, 0x41646170)
#define OVP_TypeId_Classifier_Adaptation_NONE			OpenViBE::CIdentifier(0x5261636B, 0x414E4F4E)
#define OVP_TypeId_Classifier_Adaptation_SUPERVISED		OpenViBE::CIdentifier(0x5261636B, 0x41535550)
#define OVP_TypeId_Classifier_Adaptation_UNSUPERVISED	OpenViBE::CIdentifier(0x5261636B, 0x41554E53)
//---------------------------------------------------------------------------------------------------

// Name Of Methods
//---------------------------------------------------------------------------------------------------
#define NAME_COV	"Covariance"
#define NAME_COR	"Pearson Correlation"
#define NAME_LWF	"Ledoit and Wolf"
#define NAME_SCM	"Sample Covariance Matrix (SCM)"
#define NAME_OAS	"Oracle Approximating Shrinkage (OAS)"
#define NAME_MCD	"Minimum Covariance Determinant (MCD)"
#define NAME_IDE	"Identity"
#define NAME_RIEM	"Riemann"
#define NAME_EUCL	"Euclidian"
#define NAME_LEUC	"Log-Euclidian"
#define NAME_LDET	"Log-Det"
#define NAME_KULL	"Kullback"
#define NAME_HARM	"Harmonic"
#define NAME_ALE	"Approximate joint diagonalization based log-Euclidean (ALE)"
#define NAME_WASS	"Wasserstein"
#define NAME_MDM		"Minimum Distance to Mean (MDM)"
#define NAME_MDM_REBIAS	"Minimum Distance to Mean REBIAS (MDM Rebias)"
#define NAME_FGMDM_RT	"Minimum Distance to Mean with geodesic filtering (FgMDM) (Real Time adaptation assumed)"
//---------------------------------------------------------------------------------------------------
