#include <openvibe/ov_all.h>
#include "ovp_defines.h"

// Boxes Includes
#include "boxes/ovpCBoxAlgorithmCovarianceMatrixCalculator.h"
#include "boxes/ovpCBoxAlgorithmCovarianceMatrixToFeatureVector.h"
#include "boxes/ovpCBoxAlgorithmCovarianceMeanCalculator.h"
#include "boxes/ovpCBoxAlgorithmMatrixClassifierTrainer.h"
#include "boxes/ovpCBoxAlgorithmMatrixClassifierProcessor.h"

OVP_Declare_Begin();

		// Register boxes
		OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmCovarianceMatrixCalculatorDesc);
		OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmCovarianceMatrixToFeatureVectorDesc);
		OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmCovarianceMeanCalculatorDesc);
		OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmMatrixClassifierTrainerDesc);
		OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmMatrixClassifierProcessorDesc);

		// Enumeration Estimator
		rPluginModuleContext.getTypeManager().registerEnumerationType(OVP_TypeId_Estimator, "Estimator");
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_COV, OVP_TypeId_Estimator_COV.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_COR, OVP_TypeId_Estimator_COR.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_LWF, OVP_TypeId_Estimator_LWF.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_SCM, OVP_TypeId_Estimator_SCM.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_OAS, OVP_TypeId_Estimator_OAS.toUInteger());
		//rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_MCD, OVP_TypeId_Estimator_MCD.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, NAME_IDE, OVP_TypeId_Estimator_IDE.toUInteger());

		// Enumeration Metric
		rPluginModuleContext.getTypeManager().registerEnumerationType(OVP_TypeId_Metric, "Metric");
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_RIEM, OVP_TypeId_Metric_RIEM.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_EUCL, OVP_TypeId_Metric_EUCL.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_LEUC, OVP_TypeId_Metric_LEUC.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_LDET, OVP_TypeId_Metric_LDET.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_KULL, OVP_TypeId_Metric_KULL.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_HARM, OVP_TypeId_Metric_HARM.toUInteger());
		//rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_ALE, OVP_TypeId_Metric_ALE.toUInteger());
		//rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_WASS, OVP_TypeId_Metric_WASS.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, NAME_IDE, OVP_TypeId_Metric_IDEN.toUInteger());

		// Enumeration Classifier
		rPluginModuleContext.getTypeManager().registerEnumerationType(OVP_TypeId_Matrix_Classifier, "Matrix Classifier");
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Matrix_Classifier, NAME_MDM, OVP_TypeId_Matrix_Classifier_MDM.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Matrix_Classifier, NAME_MDM_REBIAS, OVP_TypeId_Matrix_Classifier_MDM_REBIAS.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Matrix_Classifier, NAME_FGMDM_RT, OVP_TypeId_Matrix_Classifier_FGMDM_RT.toUInteger());

		// Enumeration Classifier Adaptater
		rPluginModuleContext.getTypeManager().registerEnumerationType(OVP_TypeId_Classifier_Adaptation, "Classifier Adaptation");
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Classifier_Adaptation, "None", OVP_TypeId_Classifier_Adaptation_NONE.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Classifier_Adaptation, "Supervised", OVP_TypeId_Classifier_Adaptation_SUPERVISED.toUInteger());
		rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Classifier_Adaptation, "Unsupervised", OVP_TypeId_Classifier_Adaptation_UNSUPERVISED.toUInteger());

OVP_Declare_End();
