#include "ovpCConnectivityAlgorithm.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEToolkit;
using namespace OpenViBEPlugins;

bool CConnectivityAlgorithm::initialize()
{
	ip_pSignal1.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1));
	ip_pSignal2.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2));
	ip_ui64SamplingRate1.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_ui64SamplingRate1));
	ip_ui64SamplingRate2.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_ui64SamplingRate2));
	ip_pChannelPairs.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix));
	op_pMatrix.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix));

	return true;
}


bool CConnectivityAlgorithm::uninitialize()
{
	ip_pSignal1.uninitialize();
	ip_pSignal2.uninitialize();
	ip_ui64SamplingRate1.uninitialize();
	ip_ui64SamplingRate2.uninitialize();
	ip_pChannelPairs.uninitialize();
	op_pMatrix.uninitialize();

	return true;
}

bool CConnectivityAlgorithm::process()
{
	if (this->isInputTriggerActive(OVP_Algorithm_Connectivity_InputTriggerId_Initialize))
	{
		IMatrix* l_pChannelPairs = ip_pChannelPairs;
		if (!l_pChannelPairs)
		{
			this->getLogManager() << LogLevel_ImportantWarning << "Channel lookup matrix is NULL\n Channel pairs selection failed\n";
			return false;
		}
		return true;
	}

	return true;
}
