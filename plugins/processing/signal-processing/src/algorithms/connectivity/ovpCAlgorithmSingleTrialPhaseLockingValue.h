#if defined(TARGET_HAS_ThirdPartyEIGEN)

#ifndef __OpenViBEPlugins_Algorithm_SingleTrialPhaseLockingValue_H__
#define __OpenViBEPlugins_Algorithm_SingleTrialPhaseLockingValue_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include "ovpCConnectivityAlgorithm.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/FFT>

#include "../basic/ovpCHilbertTransform.h"

/*
#define OVP_TypeId_Algorithm_SingleTrialPhaseLockingValue									OpenViBE::CIdentifier(0x344B79DE, 0x89EAAABB)
#define OVP_TypeId_Algorithm_SingleTrialPhaseLockingValueDesc								OpenViBE::CIdentifier(0x8CAB236A, 0xA789800D)
*/

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CAlgorithmSingleTrialPhaseLockingValue : public CConnectivityAlgorithm
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;


			_IsDerivedFromClass_Final_(OpenViBEPlugins::CConnectivityAlgorithm, OVP_TypeId_Algorithm_SingleTrialPhaseLockingValue);

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pHilbertTransform;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pHilbertInput;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pInstantaneousPhase;
		};

		class CAlgorithmSingleTrialPhaseLockingValueDesc : public CConnectivityAlgorithmDesc
		{
		public:
			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Phase Locking algorithm"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Alison Cellard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Computes the Phase-Locking Value algorithm"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Connectivity"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			virtual OpenViBE::CString getStockItemName() const { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_TypeId_Algorithm_SingleTrialPhaseLockingValue; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmSingleTrialPhaseLockingValue; }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1, "Signal 1", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2, "Signal 2", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix, "Pairs of channel", OpenViBE::Kernel::ParameterType_Matrix);

				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix, "S-PLV signal", OpenViBE::Kernel::ParameterType_Matrix);

				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Initialize, "Initialize");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Process, "Process");
				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone, "Process done");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEPlugins::CConnectivityAlgorithmDesc, OVP_TypeId_Algorithm_SingleTrialPhaseLockingValueDesc);
		};
	};  // namespace SignalProcessing
}; // namespace OpenViBEPlugins

#endif //__OpenViBEPlugins_Algorithm_SingleTrialPhaseLockingValue_H__
#endif //TARGET_HAS_ThirdPartyEIGEN
