#if defined(TARGET_HAS_ThirdPartyEIGEN)

#ifndef __WindowFunctions_H__
#define __WindowFunctions_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Eigen/Dense>
#include <cmath>

class WindowFunctions
{
public:

	bool bartlett(Eigen::VectorXd& vecXdWindow, uint32_t ui32WinSize);
	bool hamming(Eigen::VectorXd& vecXdWindow, uint32_t ui32WinSize);
	bool hann(Eigen::VectorXd& vecXdWindow, uint32_t ui32WinSize);
	bool parzen(Eigen::VectorXd& vecXdWindow, uint32_t ui32WinSize);
	bool welch(Eigen::VectorXd& vecXdWindow, uint32_t ui32WinSize);
};


#endif //__WindowFunctions_H__
#endif //TARGET_HAS_ThirdPartyEIGEN
