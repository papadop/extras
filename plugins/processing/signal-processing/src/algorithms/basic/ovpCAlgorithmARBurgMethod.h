#if defined(TARGET_HAS_ThirdPartyEIGEN)

#ifndef __OpenViBEPlugins_Algorithm_ARBurgMethod_H__
#define __OpenViBEPlugins_Algorithm_ARBurgMethod_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <Eigen/Dense>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CAlgorithmARBurgMethod : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_ARBurgMethod);

		protected:

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pMatrix; // input matrix
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pMatrix; // output matrix
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64Order;

		private:

			Eigen::VectorXd m_vecXdErrForward; // Error Forward		
			Eigen::VectorXd m_vecXdErrBackward; //Error Backward
			Eigen::VectorXd m_vecXdARCoefs; // AutoRegressive Coefficents

			Eigen::VectorXd m_vecXdErrForwardPrediction; // Error Forward prediction
			Eigen::VectorXd m_vecXdErrBackwardPrediction; //Error Backward prediction

			Eigen::VectorXd m_vecXdError; // Total error vector

			double m_f64K;
			uint32_t m_ui32Order;
		};

		class CAlgorithmARBurgMethodDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("AR Burg's Method algorithm"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Alison Cellard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Extract AR coefficient using Burg's Method"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal Processing"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName() const { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_ARBurgMethod; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmARBurgMethod; }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ARBurgMethod_InputParameterId_Matrix, "Vector", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_ARBurgMethod_OutputParameterId_Matrix, "Coefficents Vector", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ARBurgMethod_InputParameterId_UInteger, "Order", OpenViBE::Kernel::ParameterType_UInteger);

				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_ARBurgMethod_InputTriggerId_Initialize, "Initialize");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_ARBurgMethod_InputTriggerId_Process, "Process");
				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_ARBurgMethod_OutputTriggerId_ProcessDone, "Process done");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_ARBurgMethodDesc);
		};
	};
};

#endif // __OpenViBEPlugins_Algorithm_ARBurgMethod_H__

#endif // TARGET_HAS_ThirdPartyEIGEN
