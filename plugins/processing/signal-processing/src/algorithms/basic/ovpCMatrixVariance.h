#ifndef __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CMatrixVariance_H__
#define __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CMatrixVariance_H__

#if defined(TARGET_HAS_ThirdPartyITPP)

#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <itpp/base/vec.h>
#include <deque>

using namespace itpp;

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CMatrixVariance : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_MatrixVariance);

		protected:

			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64AveragingMethod;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64MatrixCount;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64SignificanceLevel;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pAveragedMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pMatrixVariance;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pConfidenceBound;

			std::deque<Vec<double>*> m_vHistory;

			Vec<double> m_vMean;
			Vec<double> m_vM;
			Vec<double> m_f64Variance;
			uint32_t m_ui32InputCounter;
		};

		class CMatrixVarianceDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Matrix variance"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Dieter Devlaminck"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_MatrixVariance; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CMatrixVariance(); }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmProto) const override
			{
				rAlgorithmProto.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_Matrix, "Matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_MatrixCount, "Matrix count", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_SignificanceLevel, "Significance Level", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_AveragingMethod, "Averaging Method", OpenViBE::Kernel::ParameterType_UInteger);

				rAlgorithmProto.addOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_AveragedMatrix, "Averaged matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_Variance, "Matrix variance", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_ConfidenceBound, "Confidence bound", OpenViBE::Kernel::ParameterType_Matrix);

				rAlgorithmProto.addInputTrigger(OVP_Algorithm_MatrixVariance_InputTriggerId_Reset, "Reset");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_MatrixVariance_InputTriggerId_FeedMatrix, "Feed matrix");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_MatrixVariance_InputTriggerId_ForceAverage, "Force average");

				rAlgorithmProto.addOutputTrigger(OVP_Algorithm_MatrixVariance_OutputTriggerId_AveragePerformed, "Average performed");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_MatrixVarianceDesc);
		};
	};
};

#endif

#endif // __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CMatrixVariance_H__
