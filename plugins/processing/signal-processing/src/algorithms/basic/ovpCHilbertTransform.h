#if defined(TARGET_HAS_ThirdPartyEIGEN)

#ifndef __OpenViBEPlugins_Algorithm_HilbertTransform_H__
#define __OpenViBEPlugins_Algorithm_HilbertTransform_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Eigen/Dense>
#include <unsupported/Eigen/FFT>
#include <complex>

// This class could be in its own file
class HilbertTransform
{
public:

	bool transform(const Eigen::VectorXcd& vecXcdInput, Eigen::VectorXcd& vecXcdOutput);

private:
	Eigen::VectorXcd m_vecXcdSignalFourier;    // Fourier Transform of the input signal
	Eigen::VectorXcd m_vecXcdHilbert;          // Vector h used to apply Hilbert transform

	Eigen::FFT<double, Eigen::internal::kissfft_impl<double>> m_oFFT; // Instance of the fft transform
};

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CAlgorithmHilbertTransform : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>, OVP_ClassId_Algorithm_HilbertTransform)


		protected:

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pMatrix; //input matrix
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pHilbertMatrix; //output matrix 1 : Hilbert transform of the signal
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pEnvelopeMatrix; //output matrix 2 : Envelope of the signal
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pPhaseMatrix; //output matrix 3 : Phase of the signal


		private:

			HilbertTransform m_oHilbert; // Instance of the Hilbert transform doing the actual computation
		};

		class CAlgorithmHilbertTransformDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:
			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Hilbert Transform"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Alison Cellard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Computes the Hilbert transform of a signal"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Give the analytic signal ua(t) = u(t) + iH(u(t)) of the input signal u(t) using Hilbert transform"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }
			virtual OpenViBE::CString getStockItemName() const { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_HilbertTransform; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmHilbertTransform; }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_HilbertTransform_InputParameterId_Matrix, "Matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_HilbertTransform_OutputParameterId_HilbertMatrix, "Hilbert Matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_HilbertTransform_OutputParameterId_EnvelopeMatrix, "Envelope Matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_HilbertTransform_OutputParameterId_PhaseMatrix, "Phase Matrix", OpenViBE::Kernel::ParameterType_Matrix);

				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_HilbertTransform_InputTriggerId_Initialize, "Initialize");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_HilbertTransform_InputTriggerId_Process, "Process");
				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_HilbertTransform_OutputTriggerId_ProcessDone, "Process done");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_HilbertTransformDesc);
		};
	};  // namespace SignalProcessing
}; // namespace OpenViBEPlugins

#endif //__OpenViBEPlugins_Algorithm_HilbertTransform_H__
#endif //TARGET_HAS_ThirdPartyEIGEN
