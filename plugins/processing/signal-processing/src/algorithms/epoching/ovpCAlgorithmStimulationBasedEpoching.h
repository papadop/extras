#ifndef __OpenViBEPlugins_Algorithm_StimulationBasedEpoching_H__
#define __OpenViBEPlugins_Algorithm_StimulationBasedEpoching_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CAlgorithmStimulationBasedEpoching : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_StimulationBasedEpoching);

		protected:

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> m_pInputSignal;
			OpenViBE::Kernel::TParameterHandler<uint64_t> m_ui64OffsetSampleCount;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> m_pOutputSignal;
			OpenViBE::Kernel::TParameterHandler<uint64_t> m_ui64EndTimeChunkToProcess;

			uint64_t m_ui64ReceivedSamples;
			uint64_t m_ui64SamplesToSkip;
			uint64_t m_ui64TimeLastProcessedChunk;
		};

		class CAlgorithmStimulationBasedEpochingDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Stimulation based epoching"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Epoching"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_StimulationBasedEpoching; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmStimulationBasedEpoching; }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_InputSignal, "Input signal", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_OffsetSampleCount, "Offset sample count", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_EndTimeChunkToProcess, "End time of the chunk one wants to process", OpenViBE::Kernel::ParameterType_Integer);

				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_StimulationBasedEpoching_OutputParameterId_OutputSignal, "Output signal", OpenViBE::Kernel::ParameterType_Matrix);

				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_StimulationBasedEpoching_InputTriggerId_Reset, "Reset");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_StimulationBasedEpoching_InputTriggerId_PerformEpoching, "Perform epoching");

				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_StimulationBasedEpoching_OutputTriggerId_EpochingDone, "Epoching done");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_StimulationBasedEpochingDesc);
		};
	};
};

#endif // __OpenViBEPlugins_Algorithm_StimulationBasedEpoching_H__
