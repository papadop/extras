#ifndef __OpenViBEPlugins_BoxAlgorithm_XDAWNSpatialFilterTrainer_H__
#define __OpenViBEPlugins_BoxAlgorithm_XDAWNSpatialFilterTrainer_H__

#if defined TARGET_HAS_ThirdPartyITPP

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainer     OpenViBE::CIdentifier(0xAE241F9F, 0x599FAD88)
#define OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainerDesc OpenViBE::CIdentifier(0x46FFAD13, 0x5F5C68CE)


namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmXDAWNSpatialFilterTrainer : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainer);

		protected:

			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_pStimulationDecoder;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_pSignalDecoder;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_pEvokedPotentialDecoder;

			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_oStimulationEncoder;

			uint64_t m_ui64StimulationIdentifier;
			OpenViBE::CString m_sSpatialFilterConfigurationFilename;
			uint64_t m_ui64FilterDimension;
			bool m_bSaveAsBoxConfig;
		};

		class CBoxAlgorithmXDAWNSpatialFilterTrainerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("xDAWN Trainer (Deprecated)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Computes spatial filter coeffcients in order to get better evoked potential classification (typically used for P300 detection)"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Filtering"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainer; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmXDAWNSpatialFilterTrainer; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Session signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Evoked potential epochs", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Train-completed Flag", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Train stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Train");
				rBoxAlgorithmPrototype.addSetting("Spatial filter configuration", OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Filter dimension", OV_TypeId_Integer, "4");
				rBoxAlgorithmPrototype.addSetting("Save as box config", OV_TypeId_Boolean, "true");
				// rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_IsDeprecated);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainerDesc);
		};
	};
};

#endif // TARGET_HAS_ThirdPartyITPP

#endif // __OpenViBEPlugins_BoxAlgorithm_XDAWNSpatialFilterTrainer_H__
