#include "ovpCBoxAlgorithmNull.h"

#include <iostream>

using namespace OpenViBE;
using namespace Kernel;
using namespace OpenViBEPlugins::SignalProcessing;

void CBoxAlgorithmNull::release()
{
	delete this;
}

bool CBoxAlgorithmNull::processInput(uint32_t inputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmNull::process()
{
	const IBox* l_pStaticBoxContext = getBoxAlgorithmContext()->getStaticBoxContext();
	IBoxIO* l_pDynamicBoxContext = getBoxAlgorithmContext()->getDynamicBoxContext();

	for (uint32_t i = 0; i < l_pStaticBoxContext->getInputCount(); i++)
	{
		for (uint32_t j = 0; j < l_pDynamicBoxContext->getInputChunkCount(i); j++)
		{
			l_pDynamicBoxContext->markInputAsDeprecated(i, j);
		}
	}

	return true;
}
