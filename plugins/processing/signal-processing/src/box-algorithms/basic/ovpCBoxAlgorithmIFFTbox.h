#ifndef __OpenViBEPlugins_BoxAlgorithm_IFFTbox_H__
#define __OpenViBEPlugins_BoxAlgorithm_IFFTbox_H__

#if defined TARGET_HAS_ThirdPartyITPP

//You may have to change this path to match your folder organisation
#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_IFFTbox OpenViBE::CIdentifier(0xD533E997, 0x4AFD2423)
#define OVP_ClassId_BoxAlgorithm_IFFTboxDesc OpenViBE::CIdentifier(0xD533E997, 0x4AFD2423)
#include <complex>

#include <itpp/itbase.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessingBasic
	{
		/**
		 * \class CBoxAlgorithmIFFTbox
		 * \author Guillermo Andrade B. (INRIA)
		 * \date Fri Jan 20 15:35:05 2012
		 * \brief The class CBoxAlgorithmIFFTbox describes the box IFFT box.
		 *
		 */
		class CBoxAlgorithmIFFTbox : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
				
			//Here is the different process callbacks possible
			// - On clock ticks :
			//virtual bool processClock(OpenViBE::CMessageClock& rMessageClock);
			// - On new input received (the most common behaviour for signal processing) :
			bool processInput(uint32_t inputIndex) override;
			
			// If you want to use processClock, you must provide the clock frequency.
			//virtual OpenViBE::uint64 getClockFrequency();

			bool process() override;

			// As we do with any class in openvibe, we use the macro below 
			// to associate this box to an unique identifier. 
			// The inheritance information is also made available, 
			// as we provide the superclass OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_IFFTbox);

		protected:
			// Codec algorithms specified in the skeleton-generator:
			// Spectrum real and imaginary parts stream decoders
			OpenViBEToolkit::TSpectrumDecoder<CBoxAlgorithmIFFTbox> m_oAlgo0_SpectrumDecoder[2];
			// Signal stream encoder
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmIFFTbox> m_oAlgo1_SignalEncoder;
		private:
			itpp::Vec<std::complex<double>> m_frequencyBuffer;
			itpp::Vec<double> m_signalBuffer;
			uint32_t m_bufferStartTime;
			uint32_t m_bufferEndTime;
			uint32_t m_ui32SampleCount;
			uint32_t m_channelsNumber;
			bool m_bHeaderSent;
		};

		/**
		 * \class CBoxAlgorithmIFFTboxDesc
		 * \author Guillermo Andrade B. (INRIA)
		 * \date Fri Jan 20 15:35:05 2012
		 * \brief Descriptor of the box IFFT box.
		 *
		 */
		class CBoxAlgorithmIFFTboxDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("IFFT"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillermo Andrade B."); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Compute Inverse Fast Fourier Transformation"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Compute Inverse Fast Fourier Transformation (depends on ITPP/fftw)"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Spectral Analysis"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_IFFTbox; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmIFFTbox; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("real part",OV_TypeId_Spectrum);
				rBoxAlgorithmPrototype.addInput("imaginary part",OV_TypeId_Spectrum);

				rBoxAlgorithmPrototype.addOutput("Signal output",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_IFFTboxDesc);
		};
	};
};
#endif //TARGET_HAS_ThirdPartyITPP

#endif // __OpenViBEPlugins_BoxAlgorithm_IFFTbox_H__
