#ifndef __OpenViBEPlugins_BoxAlgorithm_Null_H__
#define __OpenViBEPlugins_BoxAlgorithm_Null_H__

#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_Null                                              OpenViBE::CIdentifier(0x601118A8, 0x14BF700F)
#define OVP_ClassId_BoxAlgorithm_NullDesc                                          OpenViBE::CIdentifier(0x6BD21A21, 0x0A5E685A)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmNull : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_BoxAlgorithm_Null)
		};

		class CBoxAlgorithmNullDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Null"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Consumes input and produces nothing. It can be used to show scenario design intent."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Directing to Null instead of leaving a box output unconnected may add a tiny overhead."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getSoftwareComponent() const override { return OpenViBE::CString("openvibe-extras"); }
			OpenViBE::CString getAddedSoftwareVersion() const override { return OpenViBE::CString("0.0.0"); }
			OpenViBE::CString getUpdatedSoftwareVersion() const override { return OpenViBE::CString("0.0.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_Null; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmNull(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input stream", OV_TypeId_EBMLStream);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_NullDesc)
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_Null_H__
