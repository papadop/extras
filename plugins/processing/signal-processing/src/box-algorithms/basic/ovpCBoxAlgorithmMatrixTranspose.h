#ifndef __OpenViBEPlugins_BoxAlgorithm_MatrixTranspose_H__
#define __OpenViBEPlugins_BoxAlgorithm_MatrixTranspose_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>


namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmMatrixTranspose : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_MatrixTranspose);

		protected:

			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmMatrixTranspose> m_oDecoder;
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmMatrixTranspose> m_oEncoder;
		};


		class CBoxAlgorithmMatrixTransposeDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Matrix Transpose"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Transposes each matrix of the input stream"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Only works for 1 and 2 dimensional matrices. One-dimensional matrixes will be upgraded to two dimensions: [N x 1]"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-sort-ascending"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_MatrixTranspose; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmMatrixTranspose; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input matrix", OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput("Output matrix", OV_TypeId_StreamedMatrix);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_MatrixTransposeDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_MatrixTranspose_H__
