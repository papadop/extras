#ifndef __OpenViBEPlugins_BoxAlgorithm_Hilbert_H__
#define __OpenViBEPlugins_BoxAlgorithm_Hilbert_H__


#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
//#define OVP_ClassId_BoxAlgorithm_Hilbert OpenViBE::CIdentifier(0x7878A47F, 0x9A8FE349)
//#define OVP_ClassId_BoxAlgorithm_HilbertDesc OpenViBE::CIdentifier(0x2DB54E2F, 0x435675EF)

namespace OpenViBEPlugins
{
	namespace SignalProcessingBasic
	{
		/**
		 * \class CBoxAlgorithmHilbert
		 * \author Alison Cellard (Inria)
		 * \date Thu Jun  6 13:47:53 2013
		 * \brief The class CBoxAlgorithmHilbert describes the box Phase and Envelope.
		 *
		 */
		class CBoxAlgorithmHilbert : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;

			bool process() override;

			// As we do with any class in openvibe, we use the macro below 
			// to associate this box to an unique identifier. 
			// The inheritance information is also made available, 
			// as we provide the superclass OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_Hilbert);

		protected:

			// Signal stream decoder
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmHilbert> m_oAlgo0_SignalDecoder;
			// Signal stream encoder
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmHilbert> m_oAlgo1_SignalEncoder;
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmHilbert> m_oAlgo2_SignalEncoder;
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmHilbert> m_oAlgo3_SignalEncoder;

			OpenViBE::Kernel::IAlgorithmProxy* m_pHilbertAlgo;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pSignal_Matrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pHilbert_Matrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pEnvelope_Matrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pPhase_Matrix;
		};

		

		/**
		 * \class CBoxAlgorithmHilbertDesc
		 * \author Alison Cellard (Inria)
		 * \date Thu Jun  6 13:47:53 2013
		 * \brief Descriptor of the box Phase and Envelope.
		 *
		 */
		class CBoxAlgorithmHilbertDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Hilbert Transform"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Alison Cellard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Hilbert transform, Phase and Envelope from discrete-time analytic signal using Hilbert"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Return Hilbert transform, phase and envelope of the input signal using analytic signal computation"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-new"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_Hilbert; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmHilbert; }


			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Signal",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addOutput("Hilbert Transform", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Envelope",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Phase",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_HilbertDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_Hilbert_H__
