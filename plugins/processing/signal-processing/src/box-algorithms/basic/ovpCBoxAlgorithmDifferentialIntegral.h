#ifndef __OpenViBEPlugins_BoxAlgorithm_DifferentialIntegral_H__
#define __OpenViBEPlugins_BoxAlgorithm_DifferentialIntegral_H__

//You may have to change this path to match your folder organisation
#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <vector>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/**
		 * \class CBoxAlgorithmDifferentialIntegral
		 * \author Jozef Legeny (INRIA)
		 * \date Thu Oct 27 15:24:05 2011
		 * \brief The class CBoxAlgorithmDifferentialIntegral describes the box DifferentialIntegral.
		 *
		 */
		class CBoxAlgorithmDifferentialIntegral : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;


			bool processInput(uint32_t inputIndex) override;


			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_DifferentialIntegral);

		protected:
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmDifferentialIntegral> m_oSignalDecoder;
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmDifferentialIntegral> m_oSignalEncoder;

		private:
			OpenViBE::float64 operation(double a, double b);
			uint64_t m_ui64Operation;
			uint64_t m_ui64FilterOrder;

			/// Holds the differentials/integrals of all orders from the previous step
			double** m_pPastData;
			double** m_pTmpData;

			/// Is true when the filter is stabilized
			bool* m_pStabilized;
			/// Counts the samples up to the filter order, used to stabilize the filter
			uint32_t* m_pStep;
		};


		/**
		 * \class CBoxAlgorithmDifferentialIntegralDesc
		 * \author Jozef Legeny (INRIA)
		 * \date Thu Oct 27 15:24:05 2011
		 * \brief Descriptor of the box DifferentialIntegral.
		 *
		 */
		class CBoxAlgorithmDifferentialIntegralDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Signal Differential/Integral"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jozef Legeny"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Calculates a differential or an integral of a signal"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Calculates a differential or an integral of a signal."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_DifferentialIntegral; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmDifferentialIntegral; }
			
			/*
			virtual OpenViBE::Plugins::IBoxListener* createBoxListener() const               { return new CBoxAlgorithmDifferentialIntegralListener; }
			virtual void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const { delete pBoxListener; }
			*/
			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Signal",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addOutput("Output Signal",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addSetting("Operation", OVP_TypeId_DifferentialIntegralOperation, OVP_TypeId_DifferentialIntegralOperation_Differential.toString());
				rBoxAlgorithmPrototype.addSetting("Order", OV_TypeId_Integer,
												  "1");


				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_DifferentialIntegralDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_DifferentialIntegral_H__
