#if defined(TARGET_HAS_ThirdPartyEIGEN)

#ifndef __OpenViBEPlugins_BoxAlgorithm_ARCoefficients_H__
#define __OpenViBEPlugins_BoxAlgorithm_ARCoefficients_H__

//You may have to change this path to match your folder organisation
#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_ARCoefficients OpenViBE::CIdentifier(0xBAADC2F3, 0xB556A07B)
#define OVP_ClassId_BoxAlgorithm_ARCoefficientsDesc OpenViBE::CIdentifier(0xBAADC2F3, 0xB556A07B)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/**
		 * \class CBoxAlgorithmARCoefficients
		 * \author Alison Cellard (Inria)
		 * \date Wed Nov 28 10:40:52 2012
		 * \brief The class CBoxAlgorithmARCoefficients describes the box AR Features.
		 *
		 */
		class CBoxAlgorithmARCoefficients : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
				
			// Process callbacks on new input received
			bool processInput(uint32_t inputIndex) override;


			bool process() override;

			// As we do with any class in openvibe, we use the macro below 
			// to associate this box to an unique identifier. 
			// The inheritance information is also made available, 
			// as we provide the superclass OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_ARCoefficients);

		protected:
			
			// Signal stream decoder
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmARCoefficients> m_oAlgo0_SignalDecoder;

			OpenViBE::Kernel::IAlgorithmProxy* m_pARBurgMethodAlgorithm;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pARBurgMethodAlgorithm_Matrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pARBurgMethodAlgorithm_Matrix;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64ARBurgMethodAlgorithm_Order;
			
			// Feature vector stream encoder
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmARCoefficients> m_oAlgo1_Encoder;
		};


		/**
		 * \class CBoxAlgorithmARCoefficientsDesc
		 * \author Alison Cellard (Inria)
		 * \date Wed Nov 28 10:40:52 2012
		 * \brief Descriptor of the box AR Features.
		 *
		 */
		class CBoxAlgorithmARCoefficientsDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("AutoRegressive Coefficients"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Alison Cellard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Estimates autoregressive (AR) coefficients from a set of signals"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Estimates autoregressive (AR) linear model coefficients using Burg's method"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-convert"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ARCoefficients; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmARCoefficients; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("EEG Signal",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("AR Features",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addSetting("Order",OV_TypeId_Integer, "1");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ARCoefficientsDesc);
		};
	};
};


#endif // __OpenViBEPlugins_BoxAlgorithm_ARCoefficients_H__

#endif  // TARGET_HAS_ThirdPartyEIGEN
