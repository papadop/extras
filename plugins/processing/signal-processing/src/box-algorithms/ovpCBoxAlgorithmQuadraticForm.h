#ifndef __OpenViBEPlugins_BoxAlgorithm_QuadraticForm_H__
#define __OpenViBEPlugins_BoxAlgorithm_QuadraticForm_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmQuadraticForm : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processEvent(OpenViBE::CMessageEvent& rMessageEvent) override;
			bool processSignal(OpenViBE::CMessageSignal& rMessageSignal) override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_QuadraticForm);

		protected:

			//algorithms for encoding and decoding EBML stream
			OpenViBE::Kernel::IAlgorithmProxy* m_pEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pDecoder;

			//input and output buffers
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> m_oEBMLMemoryBufferHandleInput;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> m_oEBMLMemoryBufferHandleOutput;

			//the signal matrices (input and output)
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> m_oMatrixInputHandle;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> m_oMatrixOutputHandle;

			//start and end times
			uint64_t m_ui64StartTime;
			uint64_t m_ui64EndTime;

			//The matrix used in the quadratic form: the quadratic operator
			OpenViBE::CMatrix m_oQuadraticOperator;

			//dimensions (number of input channels and number of samples) of the input buffer
			uint32_t m_ui32NbChannels;
			uint32_t m_ui32NbSamplesPerBuffer;
		};

		class CBoxAlgorithmQuadraticFormDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Quadratic Form"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Fabien Lotte"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("IRISA-INSA Rennes"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Perform a quadratic matrix operation on the input signals m (result = m^T * A * m)"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("a square matrix A (which can be seen as a spatial filter) is applied to the input signals m (a vector). Then the transpose m^T of the input signals is multiplied to the resulting vector. In other words the output o is such as: o = m^T * A * m."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_QuadraticForm; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmQuadraticForm; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("input signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("output signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addSetting("Matrix values", OV_TypeId_String, "1 0 0 1");
				rBoxAlgorithmPrototype.addSetting("Number of rows/columns (square matrix)", OV_TypeId_Integer, "2");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_QuadraticFormDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_QuadraticForm_H__
