#if defined(TARGET_HAS_ThirdPartyFFTW3) // required by wavelet2s

#ifndef __OpenViBEPlugins_BoxAlgorithm_DiscreteWaveletTransform_H__
#define __OpenViBEPlugins_BoxAlgorithm_DiscreteWaveletTransform_H__

//You may have to change this path to match your folder organisation
#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <iostream>
#include <string>
#include <sstream>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_DiscreteWaveletTransform OpenViBE::CIdentifier(0x824194C5, 0x46D7FDE9)
#define OVP_ClassId_BoxAlgorithm_DiscreteWaveletTransformDesc OpenViBE::CIdentifier(0x6744711B, 0xF21B59EC)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/**
		 * \class CBoxAlgorithmDiscreteWaveletTransform
		 * \author Joao-Pedro Berti-Ligabo / Inria
		 * \date Wed Jul 16 15:05:16 2014
		 * \brief The class CBoxAlgorithmDiscreteWaveletTransform describes the box DiscreteWaveletTransform.
		 *
		 */
		class CBoxAlgorithmDiscreteWaveletTransform : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;


			bool processInput(uint32_t inputIndex) override;


			bool process() override;


			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_DiscreteWaveletTransform);

		protected:
			// Codec algorithms specified in the skeleton-generator:
			// Signal stream decoder
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmDiscreteWaveletTransform> m_oAlgo0_SignalDecoder;

			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmDiscreteWaveletTransform> m_oAlgoInfo_SignalEncoder;
			std::vector<OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmDiscreteWaveletTransform>*> m_vAlgoX_SignalEncoder;

			OpenViBE::CString m_sWaveletType;
			OpenViBE::CString m_sDecompositionLevel;

			uint32_t m_ui32Infolength;
			std::vector<std::vector<double>> m_sig;
		};



		// The box listener can be used to call specific callbacks whenever the box structure changes : input added, name changed, etc.
		// Please uncomment below the callbacks you want to use.
		class CBoxAlgorithmDiscreteWaveletTransformListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onSettingValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				if (ui32Index == 0)
				{
					return true;
				}

				if (ui32Index == 1)
				{
					uint32_t l_ui32OutputsCount = rBox.getOutputCount();

					OpenViBE::CIdentifier l_oLevelsIdentifier;
					OpenViBE::CString l_sNumberDecompositionLevels;

					rBox.getSettingValue(1, l_sNumberDecompositionLevels);

					uint32_t l_ui32NbDecompositionLevels;

					l_ui32NbDecompositionLevels = atoi(l_sNumberDecompositionLevels);

					if (l_ui32OutputsCount != l_ui32NbDecompositionLevels + 2)
					{
						for (uint32_t i = 0; i < l_ui32OutputsCount; i++)
						{
							rBox.removeOutput(l_ui32OutputsCount - i - 1);
						}

						rBox.addOutput("Info",OV_TypeId_Signal);
						rBox.addOutput("A",OV_TypeId_Signal);
						std::string l_sLevel;
						std::string l_sLevelName;
						for (uint32_t i = l_ui32NbDecompositionLevels; i > 0; i--)
						{
							std::ostringstream l_ostringstreamConvert;
							l_ostringstreamConvert << i;
							l_sLevel = l_ostringstreamConvert.str();
							l_sLevelName = "D";
							l_sLevelName = l_sLevelName + l_sLevel;
							rBox.addOutput(l_sLevelName.c_str(),OV_TypeId_Signal);
						}
					}
				}

				return true;
			};

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};


		/**
		 * \class CBoxAlgorithmDiscreteWaveletTransformDesc
		 * \author Joao-Pedro Berti-Ligabo / Inria
		 * \date Wed Jul 16 15:05:16 2014
		 * \brief Descriptor of the box DiscreteWaveletTransform.
		 *
		 */
		class CBoxAlgorithmDiscreteWaveletTransformDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Discrete Wavelet Transform"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Joao-Pedro Berti-Ligabo"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Calculate DiscreteWaveletTransform"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Calculate DiscreteWaveletTransform using different types of wavelets"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Wavelets"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gnome-fs-regular.png"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_DiscreteWaveletTransform; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmDiscreteWaveletTransform; }


			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmDiscreteWaveletTransformListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Signal",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addOutput("Info",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("A",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("D2",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("D1",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addSetting("Wavelet type",OVP_TypeId_WaveletType, "");
				rBoxAlgorithmPrototype.addSetting("Wavelet decomposition levels",OVP_TypeId_WaveletLevel, "");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_DiscreteWaveletTransformDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_DiscreteWaveletTransform_H__

#endif
