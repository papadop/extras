#ifndef __OpenViBEPlugins_CDLLBridge_H__
#define __OpenViBEPlugins_CDLLBridge_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <string>
#include <vector>
#include <queue>
#include <cstdio>

#if defined(TARGET_OS_Windows)
#include <windows.h>
#endif

namespace OpenViBEPlugins
{
	namespace DLLBridge
	{
		class CDLLBridge : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_DLLBridge)

		private:

			OpenViBEToolkit::TDecoder<CDLLBridge>* m_pDecoder;
			OpenViBEToolkit::TEncoder<CDLLBridge>* m_pEncoder;

			OpenViBE::CIdentifier m_oInputType;
			OpenViBE::CString m_sDLLFile;
			OpenViBE::CString m_sParameters;

			// These functions are expected from the DLL library
			// @note the inputs are non-const on purpose to ensure maximum compatibility with non-C++ dlls
			typedef void (* INITFUNC)(OpenViBE::int32* paramsLength, const char* params, int32_t* errorCode);
			typedef void (* UNINITFUNC)(int32_t* errorCode);
			typedef void (* PROCESSHEADERFUNC)(
				OpenViBE::int32* rowsIn, OpenViBE::int32* colsIn, int32_t* samplingRateIn,
				OpenViBE::int32* rowsOut, OpenViBE::int32* colsOut, int32_t* samplingRateOut,
				int32_t* errorCode);
			typedef void (* PROCESSFUNC)(double* matIn, double* matOut, int32_t* errorCode);

			INITFUNC m_pInitialize;
			UNINITFUNC m_pUninitialize;
			PROCESSHEADERFUNC m_pProcessHeader;
			PROCESSFUNC m_pProcess;

#if defined(TARGET_OS_Windows)
			HINSTANCE m_pLibrary;
#elif defined(TARGET_OS_Linux)
			void* m_pLibrary;
#endif
		};

		class CDLLBridgeListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:
			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				OpenViBE::CIdentifier l_oTypeIdentifier;
				rBox.getInputType(ui32Index, l_oTypeIdentifier);
				rBox.setOutputType(0, l_oTypeIdentifier);
				return true;
			}

			bool onOutputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				OpenViBE::CIdentifier l_oTypeIdentifier;
				rBox.getOutputType(ui32Index, l_oTypeIdentifier);
				rBox.setInputType(0, l_oTypeIdentifier);
				return true;
			};
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		/**
		* Plugin's description
		*/
		class CDLLBridgeDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("DLL Bridge"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Process a signal or matrix stream with a DLL"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Scripting"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-convert"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_DLLBridge; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CDLLBridge(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CDLLBridgeListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addSetting("DLL file", OV_TypeId_Filename, "");
				rPrototype.addSetting("Parameters", OV_TypeId_String, "");

				rPrototype.addInput("Input",OV_TypeId_Signal);
				rPrototype.addOutput("Output",OV_TypeId_Signal);

				rPrototype.addInputSupport(OV_TypeId_StreamedMatrix);
				rPrototype.addInputSupport(OV_TypeId_Signal);
				rPrototype.addOutputSupport(OV_TypeId_StreamedMatrix);
				rPrototype.addOutputSupport(OV_TypeId_Signal);

				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyOutput);

				rPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_DLLBridgeDesc);
		};
	};
};

#endif
