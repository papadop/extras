#ifndef __OpenViBEPlugins_BoxAlgorithm_KappaCoefficient_H__
#define __OpenViBEPlugins_BoxAlgorithm_KappaCoefficient_H__

#if defined(TARGET_HAS_ThirdPartyGTK)

//You may have to change this path to match your folder organisation
#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <sstream>
#include <gtk/gtk.h>

#include <visualization-toolkit/ovviz_all.h>

#define OVP_ClassId_BoxAlgorithm_KappaCoefficient OpenViBE::CIdentifier         (0x160D8F1B, 0xD864C5BB)
#define OVP_ClassId_BoxAlgorithm_KappaCoefficientDesc OpenViBE::CIdentifier     (0xD8BA2199, 0xD252BECB)

namespace OpenViBEPlugins
{
	namespace Evaluation
	{

		/**
		 * \class CBoxAlgorithmKappaCoefficient
		 * \author Serrière Guillaume (Inria)
		 * \date Tue May  5 12:45:13 2015
		 * \brief The class CBoxAlgorithmKappaCoefficient describes the box Kappa coefficient.
		 *
		 */
		class CBoxAlgorithmKappaCoefficient : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_KappaCoefficient)

		protected:
			void updateKappaValue();

			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmKappaCoefficient> m_oTargetStimulationDecoder;
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmKappaCoefficient> m_oClassifierStimulationDecoder;

			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmKappaCoefficient> m_oOutputMatrixEncoder;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pConfusionMatrix;

			OpenViBE::Kernel::IAlgorithmProxy* m_pConfusionMatrixAlgorithm;

			uint32_t m_ui32AmountClass;
			uint64_t m_ui64CurrentProcessingTimeLimit;
			double m_f64KappaCoefficient;

			GtkWidget* m_pKappaLabel;
		private:
			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};
		
		
		// The box listener can be used to call specific callbacks whenever the box structure changes : input added, name changed, etc.
		// Please uncomment below the callbacks you want to use.
		class CBoxAlgorithmKappaCoefficientListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onSettingValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				if (ui32Index == 0)
				{
					OpenViBE::CString l_sClassCount;
					rBox.getSettingValue(ui32Index, l_sClassCount);

					if (l_sClassCount.length() == 0)
					{
						return true;
					}

					int32_t l_i32SettingCount;
					std::stringstream l_sStream(l_sClassCount.toASCIIString());
					l_sStream >> l_i32SettingCount;

					//First of all we prevent for the value to goes under 1.
					if (l_i32SettingCount < 1)
					{
						rBox.setSettingValue(ui32Index, "1");
						l_i32SettingCount = 1;
					}
					int32_t l_i32CurrentCount = rBox.getSettingCount() - 1;
					//We have two choice 1/We need to add class, 2/We need to remove some
					if (l_i32CurrentCount < l_i32SettingCount)
					{
						while (l_i32CurrentCount < l_i32SettingCount)
						{
							std::stringstream l_sSettingName;
							l_sSettingName << "Stimulation of class " << (l_i32CurrentCount + 1);
							rBox.addSetting(l_sSettingName.str().c_str(), OVTK_TypeId_Stimulation, "");
							++l_i32CurrentCount;
						}
					}
					else
					{
						while (l_i32CurrentCount > l_i32SettingCount)
						{
							rBox.removeSetting(rBox.getSettingCount() - 1);
							--l_i32CurrentCount;
						}
					}
				}
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier)
		};

		/**
		 * \class CBoxAlgorithmKappaCoefficientDesc
		 * \author Serrière Guillaume (Inria)
		 * \date Tue May  5 12:45:13 2015
		 * \brief Descriptor of the box Kappa coefficient.
		 *
		 */
		class CBoxAlgorithmKappaCoefficientDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Kappa coefficient"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Serrière Guillaume"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Compute the kappa coefficient for the classifier."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("The box computes kappa coefficient for a classifier."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Evaluation/Classification"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-yes"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_KappaCoefficient; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmKappaCoefficient; }


			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmKappaCoefficientListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Expected stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Found stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addOutput("Confusion Matrix", OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Number of classes", OV_TypeId_Integer, "2");
				rBoxAlgorithmPrototype.addSetting("Stimulation of class 1", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Stimulation of class 2", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_02");

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifySetting);
				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_KappaCoefficientDesc)
		};
	}
}

#endif // TARGET_HAS_ThirdPartyGTK

#endif // __OpenViBEPlugins_BoxAlgorithm_KappaCoefficient_H__
