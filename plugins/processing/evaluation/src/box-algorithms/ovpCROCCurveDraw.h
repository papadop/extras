#ifndef __OpenViBEPlugins_BoxAlgorithm_ROCCurveDraw_H__
#define __OpenViBEPlugins_BoxAlgorithm_ROCCurveDraw_H__

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gtk/gtk.h>
#include <vector>

namespace OpenViBEPlugins
{
	namespace Evaluation
	{
		typedef std::pair<double, double> CCoordinate;

		//The aim of the class is to handle the graphical part of a RocCurve
		class CROCCurveDraw
		{
		public:
			CROCCurveDraw(GtkNotebook* pNotebook, uint32_t ui32ClassIndex, OpenViBE::CString& rClassName);
			virtual ~CROCCurveDraw();
			std::vector<CCoordinate>& getCoordinateVector();

			void generateCurve();

			//Callbak functions, should not be called
			void resizeEvent(GdkRectangle* pRectangle);
			void exposeEnvent();

			//This function is called when the cruve should be redraw for an external reason
			void forceRedraw();

		private:
			uint32_t m_ui32Margin;
			uint32_t m_ui32ClassIndex;
			std::vector<GdkPoint> m_oPointList;
			std::vector<CCoordinate> m_oCoordinateList;
			uint64_t m_ui64PixelsPerLeftRulerLabel;

			GtkWidget* m_pDrawableArea;
			bool m_bHasBeenInit;

			//For a mytical reason, gtk says that the DrawableArea is not a DrawableArea unless it's been exposed at least once...
			// So we need to if the DrawableArea as been exposed
			bool m_bHasBeenExposed;

			void redraw();
			void drawLeftMark(uint32_t ui32W, uint32_t ui32H, const char* sLabel);
			void drawBottomMark(uint32_t ui32W, uint32_t ui32H, const char* sLabel);
		};
	}
}


#endif // TARGET_HAS_ThirdPartyGTK

#endif // OVPCROCCURVEDRAW_H
