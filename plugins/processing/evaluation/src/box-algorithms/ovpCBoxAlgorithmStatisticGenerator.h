#ifndef __OpenViBEPlugins_BoxAlgorithm_StatisticGenerator_H__
#define __OpenViBEPlugins_BoxAlgorithm_StatisticGenerator_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>

#define OVP_ClassId_BoxAlgorithm_StatisticGenerator OpenViBE::CIdentifier(0x83EDA40B, 0x425FBFFE)
#define OVP_ClassId_BoxAlgorithm_StatisticGeneratorDesc OpenViBE::CIdentifier(0x35A0CB63, 0x78882C28)

namespace OpenViBEPlugins
{
	namespace Evaluation
	{
		typedef struct
		{
			OpenViBE::CString m_sChannelName;
			double m_f64Min;
			double m_f64Max;
			double m_f64SampleSum;
			uint32_t m_ui32SampleCount;
		} SSignalInfo;

		/**
		 * \class CBoxAlgorithmStatisticGenerator
		 * \author Serrière Guillaume (Inria)
		 * \date Thu Apr 30 15:24:39 2015
		 * \brief The class CBoxAlgorithmStatisticGenerator describes the box Statistic generator.
		 *
		 */
		class CBoxAlgorithmStatisticGenerator : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_StatisticGenerator)

		private:
			XML::IXMLNode* getFloat64Node(const char* const sNodeName, double f64Value);

			// Input decoder:
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmStatisticGenerator> m_oSignalDecoder;
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmStatisticGenerator> m_oStimulationDecoder;

			uint32_t m_ui32AmountChannel;
			OpenViBE::CString m_oFilename;
			std::map<OpenViBE::CIdentifier, uint32_t> m_oStimulationMap;
			std::vector<SSignalInfo> m_oSignalInfoList;

			bool m_bHasBeenStreamed;
		};


		/**
		 * \class CBoxAlgorithmStatisticGeneratorDesc
		 * \author Serrière Guillaume (Inria)
		 * \date Thu Apr 30 15:24:39 2015
		 * \brief Descriptor of the box Statistic generator.
		 *
		 */
		class CBoxAlgorithmStatisticGeneratorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("General statistics generator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Serrière Guillaume"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Generate statistics on signal."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Generate some general purpose statistics on signal and store them in a file."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Evaluation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-yes"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_StatisticGenerator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmStatisticGenerator; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Signal",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Stimulations",OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Filename for saving",OV_TypeId_Filename, "${Path_UserData}/statistics-dump.xml");

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifySetting);

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StatisticGeneratorDesc)
		};
	}
}

#endif // __OpenViBEPlugins_BoxAlgorithm_StatisticGenerator_H__
