#ifndef __OpenViBEPlugins_BoxAlgorithm_ConfusionMatrix_H__
#define __OpenViBEPlugins_BoxAlgorithm_ConfusionMatrix_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <cstdio>

#define OVP_ClassId_BoxAlgorithm_ConfusionMatrix     OpenViBE::CIdentifier(0x1AB625DA, 0x3B2502CE)
#define OVP_ClassId_BoxAlgorithm_ConfusionMatrixDesc OpenViBE::CIdentifier(0x52237A64, 0x63555613)
#define FIRST_CLASS_SETTING_INDEX 2

namespace OpenViBEPlugins
{
	namespace Evaluation
	{
		class CBoxAlgorithmConfusionMatrix : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_ConfusionMatrix)

		protected:

			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmConfusionMatrix> m_oTargetStimulationDecoder;
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmConfusionMatrix> m_oClassifierStimulationDecoder;

			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmConfusionMatrix> m_oOutputMatrixEncoder;

			OpenViBE::Kernel::IAlgorithmProxy* m_pConfusionMatrixAlgorithm;

			uint64_t m_ui64CurrentProcessingTimeLimit;
		};

		class CBoxAlgorithmConfusionMatrixListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				char l_sName[1024];
				char l_sValue[1024];
				sprintf(l_sName, "Class %i", ui32Index - 1);
				sprintf(l_sValue, "OVTK_StimulationId_Label_%02i", ui32Index - 2);
				rBox.setSettingName(ui32Index, l_sName);
				rBox.setSettingType(ui32Index, OV_TypeId_Stimulation);
				rBox.setSettingValue(ui32Index, l_sValue);
				return true;
			}

			bool onSettingRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				uint32_t l_ui32SettingCount = rBox.getSettingCount();
				uint32_t l_ui32ClassCount = l_ui32SettingCount - FIRST_CLASS_SETTING_INDEX;

				for (uint32_t i = 0; i < l_ui32ClassCount; i++)
				{
					char l_sName[1024];
					sprintf(l_sName, "Class %i", i + 1);
					rBox.setSettingName(FIRST_CLASS_SETTING_INDEX + i, l_sName);
				}

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier)
		};

		class CBoxAlgorithmConfusionMatrixDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Confusion Matrix"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bonnet"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Make a confusion matrix out of classification results coming from one classifier."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Evaluation/Classification"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ConfusionMatrix; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmConfusionMatrix; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Targets", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Classification results", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Confusion Matrix", OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Percentages", OV_TypeId_Boolean, "true");
				rBoxAlgorithmPrototype.addSetting("Sums", OV_TypeId_Boolean, "false");

				rBoxAlgorithmPrototype.addSetting("Class 1", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Class 2", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);

				return true;
			}

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmConfusionMatrixListener; }
			virtual void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) { delete pBoxListener; }

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ConfusionMatrixDesc)
		};
	}
}

#endif // __OpenViBEPlugins_BoxAlgorithm_ConfusionMatrix_H__
