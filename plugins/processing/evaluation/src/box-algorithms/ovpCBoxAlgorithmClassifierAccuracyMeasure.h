#ifndef __OpenViBEPlugins_BoxAlgorithm_ClassifierAccuracyMeasure_H__
#define __OpenViBEPlugins_BoxAlgorithm_ClassifierAccuracyMeasure_H__

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <gtk/gtk.h>
#include <map>
#include <vector>

#include <visualization-toolkit/ovviz_all.h>

#define OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasure      OpenViBE::CIdentifier(0x48395CE7, 0x17D62550)
#define OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasureDesc  OpenViBE::CIdentifier(0x067F38CC, 0x084A6ED3)

namespace OpenViBEPlugins
{
	namespace Evaluation
	{
		class CBoxAlgorithmClassifierAccuracyMeasure : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasure);

		protected:

			//codecs
			// for the TARGET
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmClassifierAccuracyMeasure> m_oTargetStimulationDecoder;
			// For the CLASSIFIERS
			std::vector<OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmClassifierAccuracyMeasure>*> m_vpClassifierStimulationDecoder;


			// deduced timeline:
			std::map<OpenViBE::uint64, uint64_t> m_mTargetsTimeLine;
			uint64_t m_ui64CurrentProcessingTimeLimit;


			// Outputs: visualization in a gtk window
			GtkBuilder* m_pMainWidgetInterface;
			GtkBuilder* m_pToolbarWidgetInterface;
			GtkWidget* m_pMainWidget;
			GtkWidget* m_pToolbarWidget;

		public:
			typedef struct
			{
				GtkLabel* m_pLabelClassifier;
				GtkProgressBar* m_pProgressBar;
				uint32_t m_ui32Score;
				uint32_t m_ui32StimulationCount;
			} SProgressBar;

			std::vector<SProgressBar> m_vProgressBar;
			bool m_bShowPercentages;
			bool m_bShowScores;

		private:
			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};

		class CBoxAlgorithmClassifierAccuracyMeasureListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:
			bool onInputNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				if (ui32Index == 0)
				{
					rBox.setInputName(0, "Targets"); // forced
				}
				return true;
			}

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.setInputType(ui32Index, OV_TypeId_Stimulations); // all inputs must be stimulations
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmClassifierAccuracyMeasureDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Classifier Accuracy Measure"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bonnet"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Displays real-time classifier accuracies as vertical progress bars"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Evaluation/Classification"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-sort-ascending"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasure; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmClassifierAccuracyMeasure; }

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmClassifierAccuracyMeasureListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Targets", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Classifier 1", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);

				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_Stimulations);
				// rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasureDesc);
		};
	};
};

#endif // TARGET_HAS_ThirdPartyGTK

#endif // __OpenViBEPlugins_BoxAlgorithm_ClassifierAccuracyMeasure_H__
