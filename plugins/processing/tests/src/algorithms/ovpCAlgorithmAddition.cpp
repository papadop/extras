#include "ovpCAlgorithmAddition.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace Tests;

bool CAlgorithmAddition::initialize()
{
	m_oParameter1.initialize(getInputParameter(CIdentifier(0, 1)));
	m_oParameter2.initialize(getInputParameter(CIdentifier(0, 2)));
	m_oParameter3.initialize(getOutputParameter(CIdentifier(0, 3)));

	return true;
}

bool CAlgorithmAddition::uninitialize()
{
	m_oParameter3.uninitialize();
	m_oParameter2.uninitialize();
	m_oParameter1.uninitialize();

	return true;
}

bool CAlgorithmAddition::process()
{
	m_oParameter3 = m_oParameter1 + m_oParameter2;

	return true;
}
