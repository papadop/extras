#ifndef __SamplePlugin_CAlgorithmAddition_H__
#define __SamplePlugin_CAlgorithmAddition_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace Tests
	{
		class CAlgorithmAddition : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_AlgorithmAddition);

		protected:

			OpenViBE::Kernel::TParameterHandler<int64_t> m_oParameter1;
			OpenViBE::Kernel::TParameterHandler<int64_t> m_oParameter2;
			OpenViBE::Kernel::TParameterHandler<int64_t> m_oParameter3;
		};

		class CAlgorithmAdditionDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Addition"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Computes and outputs the sum of two inputs"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tests"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_AlgorithmAddition; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmAddition(); }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OpenViBE::CIdentifier(0, 1), "First addition operand", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addInputParameter(OpenViBE::CIdentifier(0, 2), "Second addition operand", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OpenViBE::CIdentifier(0, 3), "Addition result", OpenViBE::Kernel::ParameterType_Integer);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_AlgorithmAdditionDesc);
		};
	};
};

#endif // __SamplePlugin_CAlgorithmAddition_H__
