#ifndef __OpenViBEPlugins_Samples_CCrashingBox_H__
#define __OpenViBEPlugins_Samples_CCrashingBox_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <iostream>
#include <vector>
#include <cmath>

namespace OpenViBEPlugins
{
	namespace Tests
	{
		class CCrashingBox : public OpenViBE::Plugins::IBoxAlgorithm
		{
		public:

			void release() override
			{
				delete this;
			}

			bool initialize(
				OpenViBE::Kernel::IBoxAlgorithmContext& rBoxAlgorithmContext) override;

			bool uninitialize(
				OpenViBE::Kernel::IBoxAlgorithmContext& rBoxAlgorithmContext) override;

			bool processInput(
				OpenViBE::Kernel::IBoxAlgorithmContext& rBoxAlgorithmContext,
				uint32_t inputIndex) override;

			bool process(OpenViBE::Kernel::IBoxAlgorithmContext& rBoxAlgorithmContext) override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_CrashingBox)
		};

		class CCrashingBoxDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Crashing box"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("A box which code launches exceptions"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box illustrates the behavior of the platform given a crashing plugin code"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tests"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_CrashingBox; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CCrashingBox(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("an input", OV_UndefinedIdentifier);

				return true;
			}

			OpenViBE::CString getStockItemName() const override
			{
				return OpenViBE::CString("gtk-cancel");
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_CrashingBoxDesc)
		};
	};
};

#endif // __OpenViBEPlugins_Samples_CCrashingBox_H__
