#ifndef __SamplePlugin_CBoxAlgorithmAdditionTest_H__
#define __SamplePlugin_CBoxAlgorithmAdditionTest_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace Tests
	{
		class CBoxAlgorithmAdditionTest : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CBoxAlgorithmAdditionTest();

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::Kernel::IMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithmAdditionTest)

		protected:

			OpenViBE::Kernel::ELogLevel m_eLogLevel;

			int64_t m_iInt64_1;
			int64_t m_iInt64_2;
			int64_t m_iInt64_3;
			int64_t m_iInt64_4;

			OpenViBE::Kernel::IAlgorithmProxy* m_pProxy1;
			OpenViBE::Kernel::IAlgorithmProxy* m_pProxy2;
			OpenViBE::Kernel::IAlgorithmProxy* m_pProxy3;
		};

		class CBoxAlgorithmAdditionTestDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Addition Test"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("This box illustrates how an algorithm can be used in a box"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This specific sample computes 4 random numbers and uses 3 sum operator algorithms in order to get the total"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tests"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithmAdditionTest; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmAdditionTest(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addSetting("Log level to use", OV_TypeId_LogLevel, "Information");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithmAdditionTestDesc);
		};
	};
};

#endif // __SamplePlugin_CBoxAlgorithmAdditionTest_H__
