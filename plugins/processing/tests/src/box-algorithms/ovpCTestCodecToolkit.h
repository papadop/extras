#ifndef __OpenViBEPlugins_TestCodecToolkit_H__
#define __OpenViBEPlugins_TestCodecToolkit_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>

#define OVP_ClassId_TestCodecToolkit     OpenViBE::CIdentifier(0x330E3A87, 0x31565BA6)
#define OVP_ClassId_TestCodecToolkitDesc OpenViBE::CIdentifier(0x376A4712, 0x1AA65567)

namespace OpenViBEPlugins
{
	namespace Tests
	{
		class CTestCodecToolkit : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_TestCodecToolkit);

		protected:

			OpenViBEToolkit::TStreamedMatrixDecoder<CTestCodecToolkit> m_oStreamedMatrixDecoder;
			OpenViBEToolkit::TStreamedMatrixEncoder<CTestCodecToolkit> m_oStreamedMatrixEncoder;

			OpenViBEToolkit::TChannelLocalisationDecoder<CTestCodecToolkit> m_oChannelLocalisationDecoder;
			OpenViBEToolkit::TChannelLocalisationEncoder<CTestCodecToolkit> m_oChannelLocalisationEncoder;

			OpenViBEToolkit::TFeatureVectorDecoder<CTestCodecToolkit> m_oFeatureVectorDecoder;
			OpenViBEToolkit::TFeatureVectorEncoder<CTestCodecToolkit> m_oFeatureVectorEncoder;

			OpenViBEToolkit::TSpectrumDecoder<CTestCodecToolkit> m_oSpectrumDecoder;
			OpenViBEToolkit::TSpectrumEncoder<CTestCodecToolkit> m_oSpectrumEncoder;

			OpenViBEToolkit::TSignalDecoder<CTestCodecToolkit> m_oSignalDecoder;
			OpenViBEToolkit::TSignalEncoder<CTestCodecToolkit> m_oSignalEncoder;

			OpenViBEToolkit::TStimulationDecoder<CTestCodecToolkit> m_oStimDecoder;
			OpenViBEToolkit::TStimulationEncoder<CTestCodecToolkit> m_oStimEncoder;

			OpenViBEToolkit::TExperimentInformationDecoder<CTestCodecToolkit> m_oExperimentInformationDecoder;
			OpenViBEToolkit::TExperimentInformationEncoder<CTestCodecToolkit> m_oExperimentInformationEncoder;

			/* One decoder per input. This vector makes easy the decoding in one iteration over the inputs. */
			std::vector<OpenViBEToolkit::TDecoder<CTestCodecToolkit>*> m_vDecoders;

			/* One encoder per output This vector makes easy the encoding in one iteration over the outputs. */
			std::vector<OpenViBEToolkit::TEncoder<CTestCodecToolkit>*> m_vEncoders;
		};

		class CTestCodecToolkitDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Codec Toolkit testbox"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bonnet"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Sample box to test the codec toolkit. Identity (input = output)."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tests/Algorithms"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_TestCodecToolkit; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CTestCodecToolkit; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Streamed Matrix", OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput("Streamed Matrix", OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addInput("Channel Localisation", OV_TypeId_ChannelLocalisation);
				rBoxAlgorithmPrototype.addOutput("Channel Localisation", OV_TypeId_ChannelLocalisation);

				rBoxAlgorithmPrototype.addInput("Feature Vector", OV_TypeId_FeatureVector);
				rBoxAlgorithmPrototype.addOutput("Feature Vector", OV_TypeId_FeatureVector);

				rBoxAlgorithmPrototype.addInput("Spectrum", OV_TypeId_Spectrum);
				rBoxAlgorithmPrototype.addOutput("Spectrum", OV_TypeId_Spectrum);

				rBoxAlgorithmPrototype.addInput("Signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Signal", OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addInput("XP info", OV_TypeId_ExperimentInformation);
				rBoxAlgorithmPrototype.addOutput("XP info", OV_TypeId_ExperimentInformation);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_TestCodecToolkitDesc);
		};
	};
};

#endif // __OpenViBEPlugins_TestCodecToolkit_H__
