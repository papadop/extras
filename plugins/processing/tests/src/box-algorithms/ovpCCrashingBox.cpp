#include "ovpCCrashingBox.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;
using namespace OpenViBEPlugins;
using namespace Tests;
using namespace std;

bool CCrashingBox::initialize(
	IBoxAlgorithmContext& rBoxAlgorithmContext)
{
	throw 0;
	return true;
}

bool CCrashingBox::uninitialize(
	IBoxAlgorithmContext& rBoxAlgorithmContext)
{
	int one = (int)1.0;
	int zero = (int)sin(0.0);
	int division_by_zero = one / zero;
	return division_by_zero ? true : false;
}

bool CCrashingBox::processInput(
	IBoxAlgorithmContext& rBoxAlgorithmContext,
	uint32_t inputIndex)
{
	rBoxAlgorithmContext.markAlgorithmAsReadyToProcess();
	return true;
}

bool CCrashingBox::process(IBoxAlgorithmContext& rBoxAlgorithmContext)
{
	*((int*)nullptr) = 0;

	return true;
}
