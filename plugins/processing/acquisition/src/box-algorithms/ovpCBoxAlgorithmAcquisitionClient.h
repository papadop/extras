#ifndef __OpenViBEPlugins_BoxAlgorithm_AcquisitionClient_H__
#define __OpenViBEPlugins_BoxAlgorithm_AcquisitionClient_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <socket/IConnectionClient.h>

namespace OpenViBEPlugins
{
	namespace Acquisition
	{
		class CBoxAlgorithmAcquisitionClient : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_AcquisitionClient);

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pAcquisitionStreamDecoder;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> ip_pAcquisitionMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64BufferDuration;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pExperimentInformationMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pSignalMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pStimulationMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pChannelLocalisationMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pChannelUnitsMemoryBuffer;

			Socket::IConnectionClient* m_pConnectionClient;

			uint64_t m_ui64LastChunkStartTime;
			uint64_t m_ui64LastChunkEndTime;
		};

		class CBoxAlgorithmAcquisitionClientDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Acquisition client"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("A generic network based acquisition client"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This algorithm waits for EEG data from the network and distributes it into the scenario"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_AcquisitionClient; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmAcquisitionClient; }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addOutput("Experiment information", OV_TypeId_ExperimentInformation);
				rBoxAlgorithmPrototype.addOutput("Signal stream", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Channel localisation", OV_TypeId_ChannelLocalisation);
				rBoxAlgorithmPrototype.addOutput("Channel units", OV_TypeId_ChannelUnits);
				rBoxAlgorithmPrototype.addSetting("Acquisition server hostname", OV_TypeId_String, "${AcquisitionServer_HostName}");
				rBoxAlgorithmPrototype.addSetting("Acquisition server port", OV_TypeId_Integer, "1024");
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_AcquisitionClientDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_AcquisitionClient_H__
