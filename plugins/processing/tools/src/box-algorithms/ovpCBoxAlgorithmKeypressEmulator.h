#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_KeypressEmulator OpenViBE::CIdentifier(0x38503532, 0x19494145)
#define OVP_ClassId_BoxAlgorithm_KeypressEmulatorDesc OpenViBE::CIdentifier(0x59286224, 0x99423852)

namespace OpenViBEPlugins
{
	namespace Tools
	{
		/**
		 * \class CBoxAlgorithmKeypressEmulator
		 * \author Jussi T. Lindgren / Inria
		 * \date 29.Oct.2019
		 * \brief Emulates keypresses on a keyboard based on input stimulations. Based on a request from Fabien Lotte / POTIOC.
		 *
		 */
		class CBoxAlgorithmKeypressEmulator : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_KeypressEmulator);

			// Register enums to the kernel used by this box
			static void registerEnums(const OpenViBE::Kernel::IPluginModuleContext& rContext);

		protected:
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmKeypressEmulator> m_Decoder;

			// @todo for multiple triggers, use std::map<> 
			uint64_t m_TriggerStimulation = 0;
			uint64_t m_KeyToPress = 0;
			uint64_t m_Modifier = 0;
		};

		class CBoxAlgorithmKeypressEmulatorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Keypress Emulator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Emulates pressing keyboard keys when receiving stimulations"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tools"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-index"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_KeypressEmulator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmKeypressEmulator; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Stimulations",OV_TypeId_Stimulations);
				
				// @todo add support for multiple keys, e.g. look at VRPN boxes for howto
				rBoxAlgorithmPrototype.addSetting("Trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Key to press", OVP_TypeId_Keypress_Key, "A");
				rBoxAlgorithmPrototype.addSetting("Key modifier", OVP_TypeId_Keypress_Modifier, OVP_TypeId_Keypress_Modifier_None.toString());

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_KeypressEmulatorDesc);
		};
	};
};
