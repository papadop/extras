#ifndef __OpenViBEPlugins_BoxAlgorithm_LatencyEvaluation_H__
#define __OpenViBEPlugins_BoxAlgorithm_LatencyEvaluation_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <system/ovCTime.h>

namespace OpenViBEPlugins
{
	namespace Tools
	{
		class CBoxAlgorithmLatencyEvaluation : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_LatencyEvaluation);

			uint64_t m_ui64StartTime;
			OpenViBE::Kernel::ELogLevel m_eLogLevel;
		};

		class CBoxAlgorithmLatencyEvaluationDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Latency evaluation"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Evaluates i/o jittering and outputs values to log manager"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box evaluates i/o jittering comparing input chunk' start and end time against current clock time"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tools"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-info"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LatencyEvaluation; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmLatencyEvaluation; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("input", OV_TypeId_EBMLStream);
				rBoxAlgorithmPrototype.addSetting("Log level to use", OV_TypeId_LogLevel, "Trace");
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LatencyEvaluationDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_LatencyEvaluation_H__
