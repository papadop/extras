#ifndef __OpenViBEPlugins_BoxAlgorithm_MouseTracking_H__
#define __OpenViBEPlugins_BoxAlgorithm_MouseTracking_H__

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_MouseTracking OpenViBE::CIdentifier(0x1E386EE5, 0x203E13C6)
#define OVP_ClassId_BoxAlgorithm_MouseTrackingDesc OpenViBE::CIdentifier(0x7A31C11B, 0xF522262E)

namespace OpenViBEPlugins
{
	namespace Tools
	{
		/**
		 * \class CBoxAlgorithmMouseTracking
		 * \author Alison Cellard (Inria)
		 * \date Mon Mar 10 15:07:21 2014
		 * \brief The class CBoxAlgorithmMouseTracking describes the box Mouse tracking.
		 *
		 */
		class CBoxAlgorithmMouseTracking : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			//			CBoxAlgorithmMouseTracking();
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			uint64_t getClockFrequency() override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_MouseTracking);

		protected:
			// Feature vector stream encoder
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmMouseTracking> m_oAlgo0_SignalEncoder;
			// Feature vector stream encoder
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmMouseTracking> m_oAlgo1_SignalEncoder;

			// To check if the header was sent or not
			bool m_bHeaderSent;

			// Requested Sampling frequency
			uint64_t m_ui64Frequency;
			// Process clock frequency
			uint64_t m_ui64ClockFrequency;

			// Length of output chunks
			uint64_t m_ui64GeneratedEpochSampleCount;
			// Absolute coordinates of the mouse pointer, that is, relative to the window in fullscreen
			OpenViBE::IMatrix* m_pAbsoluteCoordinateBuffer;
			// Relative coordinates of the mouse pointer, the coordinates is relative to the previous point
			OpenViBE::IMatrix* m_pRelativeCoordinateBuffer;

			uint64_t m_ui64SentSampleCount;

			// Gtk window to track mouse position
			GtkWidget* m_pWindow;

			// X coordinate from the previous position (in pixel, reference is upper left corner of window)
			double m_f64Previous_x;
			// Y coordinate from the previous position (in pixel, reference is upper left corner of window)
			double m_f64Previous_y;


		public:

			// X coordinate of mouse current position
			double m_f64Mouse_x;
			// Y coordinate of mouse current position
			double m_f64Mouse_y;
		};


		class CBoxAlgorithmMouseTrackingListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onSettingValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override { return true; };

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};
		

		/**
		 * \class CBoxAlgorithmMouseTrackingDesc
		 * \author Alison Cellard (Inria)
		 * \date Mon Mar 10 15:07:21 2014
		 * \brief Descriptor of the box Mouse tracking.
		 *
		 */
		class CBoxAlgorithmMouseTrackingDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Mouse Tracking"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Alison Cellard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Track mouse position within the screen"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Return absolute and relative to the previous one mouse position"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tools"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-index"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_MouseTracking; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmMouseTracking; }


			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmMouseTrackingListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addOutput("Absolute coordinate",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Previous relative coordinate",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addSetting("Sampling Frequency",OV_TypeId_Integer, "16");
				rBoxAlgorithmPrototype.addSetting("Generated epoch sample count",OV_TypeId_Integer, "1");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_MouseTrackingDesc);
		};
	};
};


#endif

#endif // __OpenViBEPlugins_BoxAlgorithm_MouseTracking_H__
