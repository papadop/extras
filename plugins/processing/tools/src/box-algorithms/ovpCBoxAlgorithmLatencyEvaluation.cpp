#include "ovpCBoxAlgorithmLatencyEvaluation.h"

// @note by just repeatedly printing to the console, this box introduces significant latency by itself.
// @fixme If it makes sense to enable this box at all, it should be reimplemented as printing to a small GUI widget instead.

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace Tools;

bool CBoxAlgorithmLatencyEvaluation::initialize()
{
	CString l_sLogLevel;
	getBoxAlgorithmContext()->getStaticBoxContext()->getSettingValue(0, l_sLogLevel);
	m_eLogLevel = static_cast<ELogLevel>(getBoxAlgorithmContext()->getPlayerContext()->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_LogLevel, l_sLogLevel));

	m_ui64StartTime = System::Time::zgetTime();

	return true;
}

bool CBoxAlgorithmLatencyEvaluation::uninitialize()
{
	return true;
}

bool CBoxAlgorithmLatencyEvaluation::processInput(uint32_t inputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

bool CBoxAlgorithmLatencyEvaluation::process()
{
	// FIXME is it necessary to keep next line uncomment ?
	//IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO& l_rDynamicBoxContext = this->getDynamicBoxContext();

	const uint64_t l_ui64CurrentTime = getPlayerContext().getCurrentTime();

	for (uint32_t i = 0; i < l_rDynamicBoxContext.getInputChunkCount(0); i++)
	{
		const uint64_t l_ui64StartTime = l_rDynamicBoxContext.getInputChunkStartTime(0, i);
		const uint64_t l_ui64EndTime = l_rDynamicBoxContext.getInputChunkEndTime(0, i);

		const double l_f64StartLatencyMilli = (((((int64_t)(l_ui64CurrentTime - l_ui64StartTime)) >> 22) * 1000) / 1024.0);
		const double l_f64EndLatencyMilli = (((((int64_t)(l_ui64CurrentTime - l_ui64EndTime)) >> 22) * 1000) / 1024.0);

		// getLogManager() << LogLevel_Debug << "Timing values [start:end:current]=[" << l_ui64StartTime << ":" << l_ui64EndTime << ":" << l_ui64Time << "]\n";
		getLogManager() << m_eLogLevel << "Current latency at chunk " << i << " [start:end]=[" << l_f64StartLatencyMilli << ":" << l_f64EndLatencyMilli << "] ms\n";

		l_rDynamicBoxContext.markInputAsDeprecated(0, i);
	}

	const uint64_t l_ui64RealTimeElapsed = System::Time::zgetTime() - m_ui64StartTime;
	const double l_f64InnerLatencyMilli = (((int64_t(l_ui64RealTimeElapsed - l_ui64CurrentTime) >> 22) * 1000) / 1024.0);

	getLogManager() << m_eLogLevel << "Inner latency : " << l_f64InnerLatencyMilli << " ms\n";

	return true;
}
