#ifndef __OpenViBEPlugins_BoxAlgorithm_StreamedMatrixSwitch_H__
#define __OpenViBEPlugins_BoxAlgorithm_StreamedMatrixSwitch_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <stdio.h>
#include <map>
#include <vector>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitch OpenViBE::CIdentifier(0x556A2C32, 0x61DF49FC)
#define OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitchDesc OpenViBE::CIdentifier(0x556A2C32, 0x61DF49FC)

namespace OpenViBEPlugins
{
	namespace Streaming
	{
		/**
		 * \class CBoxAlgorithmStreamedMatrixSwitch
		 * \author Laurent Bonnet (INRIA)
		 * \date Thu May 12 18:02:05 2011
		 * \brief The class CBoxAlgorithmStreamedMatrixSwitch describes the box Streamed Matrix Switch.
		 *
		 */
		class CBoxAlgorithmStreamedMatrixSwitch : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitch);

		protected:
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmStreamedMatrixSwitch> m_oStimulationDecoder;
			OpenViBEToolkit::TDecoder<CBoxAlgorithmStreamedMatrixSwitch>* m_pStreamDecoder;

			std::map<uint64_t, uint32_t> m_mStimulationOutputIndexMap;
			int32_t m_i32ActiveOutputIndex;
			uint64_t m_ui64LastStimulationInputChunkEndTime;
		};


		class CBoxAlgorithmStreamedMatrixSwitchListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				if (ui32Index == 0)
				{
					rBox.setInputType(0,OV_TypeId_Stimulations);
					return true;
				}

				OpenViBE::CIdentifier l_oIdentifier;
				rBox.getInputType(1, l_oIdentifier);

				// all output must have the input type
				for (uint32_t i = 0; i < rBox.getOutputCount(); i++)
				{
					rBox.setOutputType(i, l_oIdentifier);
				}
				return true;
			};

			bool onOutputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				// the output must have the same type as the input
				OpenViBE::CIdentifier l_oIdentifier;
				rBox.getInputType(1, l_oIdentifier);
				rBox.setOutputType(ui32Index, l_oIdentifier);

				char l_sName[1024];
				sprintf(l_sName, "Switch stim for output %i", ui32Index + 1);
				rBox.addSetting(l_sName, OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");

				return true;
			};

			bool onOutputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeSetting(1 + ui32Index);		// +1 for the first setting which doesn't correspond to a stream

				// Rename the rest to match the changed indexing
				for (uint32_t i = (1 + ui32Index); i < rBox.getSettingCount(); i++)
				{
					char l_sName[1024];
					sprintf(l_sName, "Switch stim for output %i", i);
					rBox.setSettingName(i, l_sName);
				}
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};
		

		/**
		 * \class CBoxAlgorithmStreamedMatrixSwitchDesc
		 * \author Laurent Bonnet (INRIA)
		 * \date Thu May 12 18:02:05 2011
		 * \brief Descriptor of the box Streamed Matrix Switch.
		 *
		 */
		class CBoxAlgorithmStreamedMatrixSwitchDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Stream Switch"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bonnet"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Redirect its input on a particular output"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box act as a switch between N possible outputs for its Streamed Matrix input. N Stimulation settings trigger the switch."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Streaming"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-sort-ascending"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitch; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmStreamedMatrixSwitch; }


			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmStreamedMatrixSwitchListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Triggers",OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Matrix",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);
				//rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);

				rBoxAlgorithmPrototype.addOutput("Output",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput("Output",OV_TypeId_StreamedMatrix);

				//rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyOutput);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddOutput);

				rBoxAlgorithmPrototype.addSetting("Default to output 1", OV_TypeId_Boolean, "false");
				rBoxAlgorithmPrototype.addSetting("Switch stim for output 1",OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Switch stim for output 2",OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");

				//rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifySetting);
				//rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);

				//rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitchDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_StreamedMatrixSwitch_H__
