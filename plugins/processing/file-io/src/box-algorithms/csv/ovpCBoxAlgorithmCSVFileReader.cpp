#include "ovpCBoxAlgorithmCSVFileReader.h"
#include <iostream>
#include <sstream>
#include <map>
#include <cmath>  // std::ceil() on Linux

#include <openvibe/ovITimeArithmetics.h>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace FileIO;

namespace
{
	std::vector<std::string> Split(const std::string& sString, const std::string& c)
	{
		std::vector<std::string> l_vResult;
		std::string::size_type i = 0;
		std::string::size_type j;
		while ((j = sString.find(c, i)) != std::string::npos)
		{
			l_vResult.emplace_back(sString, i, j - i);
			i = j + c.size();
		}
		//the last element without the \n character
		l_vResult.emplace_back(sString, i, sString.size() - 1 - i);

		return l_vResult;
	}

	void ClearMatrix(std::vector<std::vector<std::string>>& vMatrix)
	{
		for (auto& matrix : vMatrix) { matrix.clear(); }
		vMatrix.clear();
	}
}  // namespace;

CBoxAlgorithmCSVFileReader::CBoxAlgorithmCSVFileReader()
	: m_pFile(nullptr),
	  m_ui64SamplingRate(0),
	  m_fpRealProcess(nullptr),
	  m_pAlgorithmEncoder(nullptr),
	  m_bHeaderSent(false) {}

uint64_t CBoxAlgorithmCSVFileReader::getClockFrequency()
{
	return 128LL << 32; // the box clock frequency
}

bool CBoxAlgorithmCSVFileReader::initialize()
{
	m_ui64SamplingRate = 0;
	m_pAlgorithmEncoder = nullptr;

	this->getStaticBoxContext().getOutputType(0, m_oTypeIdentifier);

	m_sFilename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	CString l_sSeparator = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_sSeparator = l_sSeparator.toASCIIString();
	m_bDoNotUseFileTime = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	m_ui32SamplesPerBuffer = 1;
	if (m_oTypeIdentifier == OV_TypeId_ChannelLocalisation)
	{
		m_ui32ChannelNumberPerBuffer = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);
	}
	else if (m_oTypeIdentifier != OV_TypeId_Stimulations
		&& m_oTypeIdentifier != OV_TypeId_Spectrum)
	{
		m_ui32SamplesPerBuffer = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);
	}
	OV_WARNING_UNLESS_K(!(m_oTypeIdentifier == OV_TypeId_FeatureVector && m_bDoNotUseFileTime), "'Do not use file time' setting does not work with feature vectors\n");
	m_f64NextTime = 0.;

	m_ui64ChunkStartTime = 0;
	m_ui64ChunkEndTime = 0;

	return true;
}

bool CBoxAlgorithmCSVFileReader::uninitialize()
{
	if (m_pFile != nullptr)
	{
		fclose(m_pFile);
		m_pFile = nullptr;
	}
	if (m_pAlgorithmEncoder != nullptr)
	{
		m_pAlgorithmEncoder->uninitialize();
		delete m_pAlgorithmEncoder;
		m_pAlgorithmEncoder = nullptr;
	}
	return true;
}

bool CBoxAlgorithmCSVFileReader::initializeFile()
{
	//open file
	m_pFile = fopen(m_sFilename.toASCIIString(), "re"); // we don't open as binary as that gives us \r\n on Windows as line-endings and leaves a dangling char after split. CSV files should be text.

	OV_ERROR_UNLESS_KRF(m_pFile,
		"Error opening file [" << m_sFilename << "] for reading",
		OpenViBE::Kernel::ErrorType::BadFileRead
	);

	// simulate RAII through closure
	const auto releaseResources = [&]()
	{
		fclose(m_pFile);
		m_pFile = nullptr;
	};

	//read the header
	char l_pLine[m_ui32BufferLen];
	char* l_pResult = fgets(l_pLine, m_ui32BufferLen, m_pFile);
	if (nullptr == l_pResult)
	{
		releaseResources();
		OV_ERROR_KRF("Error reading data from file", ErrorType::BadParsing);
	}

	m_vHeaderFile = Split(std::string(l_pLine), m_sSeparator);
	m_ui32ColumnCount = m_vHeaderFile.size();

	if (m_oTypeIdentifier == OV_TypeId_ChannelLocalisation)
	{
		m_pAlgorithmEncoder = new OpenViBEToolkit::TChannelLocalisationEncoder<CBoxAlgorithmCSVFileReader>(*this, 0);
		//number of column without the column contains the dynamic parameter
		//m_ui32NbColumn-=1;
		m_fpRealProcess = &CBoxAlgorithmCSVFileReader::processChannelLocalisation;
	}
	else if (m_oTypeIdentifier == OV_TypeId_FeatureVector)
	{
		m_pAlgorithmEncoder = new OpenViBEToolkit::TFeatureVectorEncoder<CBoxAlgorithmCSVFileReader>(*this, 0);
		m_fpRealProcess = &CBoxAlgorithmCSVFileReader::processFeatureVector;
		m_ui32SamplesPerBuffer = 1;
	}
	else if (m_oTypeIdentifier == OV_TypeId_Spectrum)
	{
		m_pAlgorithmEncoder = new OpenViBEToolkit::TSpectrumEncoder<CBoxAlgorithmCSVFileReader>(*this, 0);
		m_fpRealProcess = &CBoxAlgorithmCSVFileReader::processSpectrum;

		//number of column without columns contains min max frequency bands parameters
		m_ui32ColumnCount -= 2;
	}
	else if (m_oTypeIdentifier == OV_TypeId_Signal)
	{
		m_pAlgorithmEncoder = new OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmCSVFileReader>(*this, 0);
		m_fpRealProcess = &CBoxAlgorithmCSVFileReader::processSignal;

		//find the sampling rate
		l_pResult = fgets(l_pLine, m_ui32BufferLen, m_pFile);

		if (nullptr == l_pResult)
		{
			releaseResources();
			OV_ERROR_KRF("Error reading sampling rate from file", ErrorType::BadParsing);
		}

		std::vector<std::string> l_vParsed = Split(std::string(l_pLine), m_sSeparator);

		if ((m_ui32ColumnCount - 1) >= l_vParsed.size())
		{
			releaseResources();
			OV_ERROR_KRF("Error reading columns (not enough columns found) from file", ErrorType::BadParsing);
		}

		const auto l_f64SamplingRate = static_cast<double>(atof(l_vParsed[m_ui32ColumnCount - 1].c_str()));
		if (std::ceil(l_f64SamplingRate) != l_f64SamplingRate)
		{
			releaseResources();
			OV_ERROR_KRF("Invalid fractional sampling rate (" << l_f64SamplingRate << ") in file", ErrorType::BadValue);
		}

		m_ui64SamplingRate = static_cast<uint64_t>(l_f64SamplingRate);

		if (m_ui64SamplingRate == 0)
		{
			releaseResources();
			OV_ERROR_KRF("Invalid NULL sampling rate in file", ErrorType::BadValue);
		}

		// Skip the header
		rewind(m_pFile);
		l_pResult = fgets(l_pLine, m_ui32BufferLen, m_pFile);
		if (nullptr == l_pResult)
		{
			releaseResources();
			OV_ERROR_KRF("Error reading data from file", ErrorType::BadParsing);
		}

		//number of column without the column contains the sampling rate parameters
		m_ui32ColumnCount -= 1;
	}
	else if (m_oTypeIdentifier == OV_TypeId_StreamedMatrix)
	{
		m_pAlgorithmEncoder = new OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCSVFileReader>(*this, 0);
		m_fpRealProcess = &CBoxAlgorithmCSVFileReader::processStreamedMatrix;
	}
	else if (m_oTypeIdentifier == OV_TypeId_Stimulations)
	{
		m_pAlgorithmEncoder = new OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmCSVFileReader>(*this, 0);
		m_fpRealProcess = &CBoxAlgorithmCSVFileReader::processStimulation;
	}
	else
	{
		releaseResources();
		OV_ERROR_KRF(
			"Invalid input type identifier " << this->getTypeManager().getTypeName(m_oTypeIdentifier) << " in file ",
			ErrorType::BadValue
		);
	}

	return true;
}

bool CBoxAlgorithmCSVFileReader::processClock(IMessageClock& rMessageClock)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmCSVFileReader::process()
{
	if (m_pFile == nullptr)
	{
		OV_ERROR_UNLESS_KRF(initializeFile(), "Error reading data from csv file " << m_sFilename, ErrorType::Internal);
	}
	//line buffer
	char l_pLine[m_ui32BufferLen];
	const double l_dCurrentTime = ITimeArithmetics::timeToSeconds(getPlayerContext().getCurrentTime());
	//IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

	//if no line was read, read the first data line.
	if (m_vLastLineSplit.empty())
	{
		//next line
		uint32_t l_ui32NbSamples = 0;
		while ((feof(m_pFile) == 0) && l_ui32NbSamples < m_ui32SamplesPerBuffer && fgets(l_pLine, m_ui32BufferLen, m_pFile) != nullptr)
		{
			m_vLastLineSplit = Split(std::string(l_pLine), m_sSeparator);

			l_ui32NbSamples++;

			if (m_oTypeIdentifier != OV_TypeId_Stimulations
				&& m_oTypeIdentifier != OV_TypeId_Spectrum
				&& m_oTypeIdentifier != OV_TypeId_ChannelLocalisation)
			{
				m_vDataMatrix.push_back(m_vLastLineSplit);
			}
		}
		if ((m_oTypeIdentifier == OV_TypeId_StreamedMatrix || m_oTypeIdentifier == OV_TypeId_Signal)
			&& (feof(m_pFile) != 0) && l_ui32NbSamples < m_ui32SamplesPerBuffer)
		{
			// Last chunk will be partial, zero the whole output matrix...
			IMatrix* iMatrix = static_cast<OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();
			OpenViBEToolkit::Tools::Matrix::clearContent(*iMatrix);
		}
	}

	bool l_bSomethingToSend = (!m_vLastLineSplit.empty())
		&& atof(m_vLastLineSplit[0].c_str()) < l_dCurrentTime;
	l_bSomethingToSend |= (m_oTypeIdentifier == OV_TypeId_Stimulations); // we always send a stim chunk, even if empty

	if (m_oTypeIdentifier == OV_TypeId_Stimulations
		|| m_oTypeIdentifier == OV_TypeId_ChannelLocalisation
		|| m_oTypeIdentifier == OV_TypeId_Spectrum)
	{
		while (!m_vLastLineSplit.empty() && atof(m_vLastLineSplit[0].c_str()) < l_dCurrentTime)
		{
			m_vDataMatrix.push_back(m_vLastLineSplit);

			l_bSomethingToSend = true;

			if ((feof(m_pFile) == 0) && fgets(l_pLine, m_ui32BufferLen, m_pFile) != nullptr)
			{
				m_vLastLineSplit = Split(std::string(l_pLine), m_sSeparator);
			}
			else { m_vLastLineSplit.clear(); }
		}
	}

	//convert data to the good output type

	if (l_bSomethingToSend)
	{
		// Encode the data
		OV_ERROR_UNLESS_KRF(
			(this->*m_fpRealProcess)(),
			"Error encoding data from csv file " << m_sFilename << " into the right output format",
			ErrorType::Internal
		);

		//for the stimulation, the line contents in m_vLastLineSplit isn't processed.
		if (m_oTypeIdentifier != OV_TypeId_Stimulations
			&& m_oTypeIdentifier != OV_TypeId_Spectrum
			&& m_oTypeIdentifier != OV_TypeId_ChannelLocalisation)
		{
			m_vLastLineSplit.clear();
		}

		//clear the Data Matrix.
		ClearMatrix(m_vDataMatrix);
	}
	return true;
}

bool CBoxAlgorithmCSVFileReader::processStreamedMatrix()
{
	IMatrix* iMatrix = static_cast<OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();

	//Header
	if (!m_bHeaderSent)
	{
		iMatrix->setDimensionCount(2);
		iMatrix->setDimensionSize(0, m_ui32ColumnCount - 1);
		iMatrix->setDimensionSize(1, m_ui32SamplesPerBuffer);

		for (uint32_t i = 1; i < m_ui32ColumnCount; i++)
		{
			iMatrix->setDimensionLabel(0, i - 1, m_vHeaderFile[i].c_str());
		}
		m_pAlgorithmEncoder->encodeHeader();
		m_bHeaderSent = true;

		this->getDynamicBoxContext().markOutputAsReadyToSend(0, 0, 0);
	}

	OV_ERROR_UNLESS_KRF(
		convertVectorDataToMatrix(iMatrix),
		"Error converting vector data to streamed matrix",
		ErrorType::Internal
	);

	m_pAlgorithmEncoder->encodeBuffer();

	if (m_bDoNotUseFileTime)
	{
		m_ui64ChunkStartTime = m_ui64ChunkEndTime;
		m_ui64ChunkEndTime = this->getPlayerContext().getCurrentTime();
	}
	else
	{
		m_ui64ChunkStartTime = ITimeArithmetics::secondsToTime(atof(m_vDataMatrix[0][0].c_str()));
		m_ui64ChunkEndTime = ITimeArithmetics::secondsToTime(atof(m_vDataMatrix.back()[0].c_str()));
	}

	this->getDynamicBoxContext().markOutputAsReadyToSend(0, m_ui64ChunkStartTime, m_ui64ChunkEndTime);

	return true;
}

bool CBoxAlgorithmCSVFileReader::processStimulation()
{
	//Header
	if (!m_bHeaderSent)
	{
		m_pAlgorithmEncoder->encodeHeader();
		m_bHeaderSent = true;

		this->getDynamicBoxContext().markOutputAsReadyToSend(0, 0, 0);
	}

	IStimulationSet* iStimulationSet = static_cast<OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputStimulationSet();
	iStimulationSet->clear();

	for (auto& matrix : m_vDataMatrix)
	{
		OV_ERROR_UNLESS_KRF(
			matrix.size() == 3,
			"Invalid data row length: must be 3 for stimulation date, index and duration",
			ErrorType::BadParsing
		);

		//stimulation date
		const uint64_t l_ui64StimulationDate = ITimeArithmetics::secondsToTime(atof(matrix[0].c_str()));

		//stimulation indices
		const auto l_ui64Stimulation = uint64_t(atof(matrix[1].c_str()));

		//stimulation duration
		const uint64_t l_ui64StimulationDuration = ITimeArithmetics::secondsToTime(atof(matrix[2].c_str()));

		iStimulationSet->appendStimulation(l_ui64Stimulation, l_ui64StimulationDate, l_ui64StimulationDuration);
	}

	m_pAlgorithmEncoder->encodeBuffer();

	// Never use file time
	m_ui64ChunkStartTime = m_ui64ChunkEndTime;
	m_ui64ChunkEndTime = this->getPlayerContext().getCurrentTime();

	this->getDynamicBoxContext().markOutputAsReadyToSend(0, m_ui64ChunkStartTime, m_ui64ChunkEndTime);

	return true;
}

bool CBoxAlgorithmCSVFileReader::processSignal()
{
	IMatrix* iMatrix = static_cast<OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();

	//Header
	if (!m_bHeaderSent)
	{
		// This is the first chunk, find out the start time from the file
		// (to keep time chunks continuous, start time is previous end time, hence set end time)
		if (!m_bDoNotUseFileTime)
		{
			m_ui64ChunkEndTime = ITimeArithmetics::secondsToTime(atof(m_vDataMatrix[0][0].c_str()));
		}

		iMatrix->setDimensionCount(2);
		iMatrix->setDimensionSize(0, m_ui32ColumnCount - 1);
		iMatrix->setDimensionSize(1, m_ui32SamplesPerBuffer);

		for (uint32_t i = 1; i < m_ui32ColumnCount; i++)
		{
			iMatrix->setDimensionLabel(0, i - 1, m_vHeaderFile[i].c_str());
		}

		static_cast<OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputSamplingRate() = m_ui64SamplingRate;

		m_pAlgorithmEncoder->encodeHeader();
		m_bHeaderSent = true;

		this->getDynamicBoxContext().markOutputAsReadyToSend(0, 0, 0);
	}

	OV_ERROR_UNLESS_KRF(
		convertVectorDataToMatrix(iMatrix),
		"Error converting vector data to signal",
		ErrorType::Internal
	);

	// this->getLogManager() << LogLevel_Info << "Cols from header " << m_ui32NbColumn << "\n";
	// this->getLogManager() << LogLevel_Info << "InMatrix " << (m_vDataMatrix.size() > 0 ? m_vDataMatrix[0].size() : 0) << " outMatrix " << iMatrix->getDimensionSize(0) << "\n";

	m_pAlgorithmEncoder->encodeBuffer();

	if (m_bDoNotUseFileTime)
	{
		// We use time dictated by the sampling rate
		m_ui64ChunkStartTime = m_ui64ChunkEndTime; // previous time end is current time start
		m_ui64ChunkEndTime = m_ui64ChunkStartTime + ITimeArithmetics::sampleCountToTime(m_ui64SamplingRate, m_ui32SamplesPerBuffer);
	}
	else
	{
		// We use time suggested by the last sample of the chunk
		m_ui64ChunkStartTime = ITimeArithmetics::secondsToTime(atof(m_vDataMatrix[0][0].c_str()));
		m_ui64ChunkEndTime = ITimeArithmetics::secondsToTime(atof(m_vDataMatrix.back()[0].c_str()));
	}

	this->getDynamicBoxContext().markOutputAsReadyToSend(0, m_ui64ChunkStartTime, m_ui64ChunkEndTime);

	return true;
}

bool CBoxAlgorithmCSVFileReader::processChannelLocalisation()
{
	IMatrix* iMatrix = static_cast<OpenViBEToolkit::TChannelLocalisationEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();

	if (!m_bHeaderSent)
	{
		iMatrix->setDimensionCount(2);
		iMatrix->setDimensionSize(0, m_ui32ColumnCount - 1);
		iMatrix->setDimensionSize(1, m_ui32SamplesPerBuffer);

		for (uint32_t i = 1; i < m_ui32ColumnCount; i++)
		{
			iMatrix->setDimensionLabel(0, i - 1, m_vHeaderFile[i].c_str());
		}

		static_cast<OpenViBEToolkit::TChannelLocalisationEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputDynamic() = false;//atoi(m_vDataMatrix[0][m_ui32NbColumn].c_str());

		m_pAlgorithmEncoder->encodeHeader();

		this->getDynamicBoxContext().markOutputAsReadyToSend(0, 0, 0);

		m_bHeaderSent = true;
	}

	std::vector<std::vector<std::string>> l_vChannelBloc;
	for (const auto& matrix : m_vDataMatrix) { l_vChannelBloc.push_back(matrix); }

	//clear matrix
	ClearMatrix(m_vDataMatrix);

	for (size_t i = 0; i < l_vChannelBloc.size(); i++)
	{
		m_vDataMatrix.push_back(l_vChannelBloc[i]);

		//send the current bloc if the next data hasn't the same date
		if (i >= l_vChannelBloc.size() - 1 || l_vChannelBloc[i + 1][0] != m_vDataMatrix[0][0])
		{
			OV_ERROR_UNLESS_KRF(
				convertVectorDataToMatrix(iMatrix),
				"Error converting vector data to channel localisation",
				ErrorType::Internal
			);

			m_pAlgorithmEncoder->encodeBuffer();
			const uint64_t l_ui64Date = ITimeArithmetics::secondsToTime(atof(m_vDataMatrix[0][0].c_str()));
			this->getDynamicBoxContext().markOutputAsReadyToSend(0, l_ui64Date, l_ui64Date);

			//clear matrix
			ClearMatrix(m_vDataMatrix);
		}
	}

	//clear matrix
	ClearMatrix(l_vChannelBloc);

	return true;
}

bool CBoxAlgorithmCSVFileReader::processFeatureVector()
{
	IBoxIO& l_rDynamicBoxContext = this->getDynamicBoxContext();
	IMatrix* l_pMatrix = static_cast<OpenViBEToolkit::TFeatureVectorEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();

	//Header
	if (!m_bHeaderSent)
	{
		// in this case we need to transpose it
		IMatrix* iMatrix = static_cast<OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();

		iMatrix->setDimensionCount(1);
		iMatrix->setDimensionSize(0, m_ui32ColumnCount - 1);

		for (uint32_t i = 1; i < m_ui32ColumnCount; i++)
		{
			iMatrix->setDimensionLabel(0, i - 1, m_vHeaderFile[i].c_str());
		}

		m_pAlgorithmEncoder->encodeHeader();

		this->getDynamicBoxContext().markOutputAsReadyToSend(0, 0, 0);

		m_bHeaderSent = true;
	}

	// Each vector has to be sent separately
	for (auto& matrix : m_vDataMatrix)
	{
		OV_ERROR_UNLESS_KRF(matrix.size() == m_ui32ColumnCount,
			"Unexpected number of elements" << "(got " << static_cast<uint64_t>(matrix.size()) << ", expected " << m_ui32ColumnCount << ")",
			ErrorType::BadParsing
		);

		for (uint32_t j = 0; j < m_ui32ColumnCount - 1; j++)
		{
			l_pMatrix->getBuffer()[j] = atof(matrix[j + 1].c_str());
		}

		m_pAlgorithmEncoder->encodeBuffer();

		const uint64_t l_ui64StartTime = ITimeArithmetics::secondsToTime(atof(matrix[0].c_str()));
		l_rDynamicBoxContext.markOutputAsReadyToSend(0, l_ui64StartTime, l_ui64StartTime);
	}

	ClearMatrix(m_vDataMatrix);

	return true;
}

bool CBoxAlgorithmCSVFileReader::processSpectrum()
{
	IMatrix* iMatrix = static_cast<OpenViBEToolkit::TSpectrumEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputMatrix();
	IMatrix* iFrequencyAbscissa = static_cast<OpenViBEToolkit::TSpectrumEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputFrequencyAbscissa();

	//Header
	if (!m_bHeaderSent)
	{
		iMatrix->setDimensionCount(2);
		iMatrix->setDimensionSize(0, m_ui32ColumnCount - 1);
		iMatrix->setDimensionSize(1, m_vDataMatrix.size());

		for (uint32_t i = 1; i < m_ui32ColumnCount; i++)
		{
			iMatrix->setDimensionLabel(0, i - 1, m_vHeaderFile[i].c_str());
		}
		iFrequencyAbscissa->setDimensionCount(1);
		iFrequencyAbscissa->setDimensionSize(0, m_vDataMatrix.size());
		if (m_vDataMatrix.size() > 1)
		{
			for (uint32_t frequencyBandIndex = 0; frequencyBandIndex < m_vDataMatrix.size(); frequencyBandIndex++)
			{
				// @FIXME CERT
				// this formula is really different from the writer, that is why the ReadWrite
				// test failed.
				/*double curFrequencyAbscissa = std::stod(m_vDataMatrix[frequencyBandIndex][m_ui32ColumnCount].c_str())
						+ static_cast<double>(frequencyBandIndex) / (m_vDataMatrix.size() - 1) * (std::stod(m_vDataMatrix[frequencyBandIndex][m_ui32ColumnCount+1].c_str()) - std::stod(m_vDataMatrix[frequencyBandIndex][m_ui32ColumnCount].c_str()));
				*/

				// compute 	ip_pFrequencyAbscissa with the same formula than in the writer
				const double l_dCurFrequencyAbscissa = std::stod(m_vDataMatrix[frequencyBandIndex][m_ui32ColumnCount]);
				const double l_dHalf = frequencyBandIndex > 0
										   ? (l_dCurFrequencyAbscissa - std::stod(m_vDataMatrix[frequencyBandIndex - 1][m_ui32ColumnCount])) / 2.
										   : (std::stod(m_vDataMatrix[frequencyBandIndex][m_ui32ColumnCount + 1]) - l_dCurFrequencyAbscissa) / 2.;

				iFrequencyAbscissa->getBuffer()[frequencyBandIndex] = l_dCurFrequencyAbscissa + l_dHalf;

				std::stringstream l_sLabel;
				l_sLabel << l_dCurFrequencyAbscissa;
				iFrequencyAbscissa->setDimensionLabel(0, frequencyBandIndex, l_sLabel.str().c_str());
			}
		}
		else { iFrequencyAbscissa->getBuffer()[0] = 0; }

		static_cast<OpenViBEToolkit::TSpectrumEncoder<CBoxAlgorithmCSVFileReader>*>(m_pAlgorithmEncoder)->getInputSamplingRate() = uint64_t(m_vDataMatrix.size() /
			(std::stod(m_vDataMatrix[m_vDataMatrix.size() - 1][m_ui32ColumnCount]) - std::stod(m_vDataMatrix[0][m_ui32ColumnCount])));
		m_bHeaderSent = true;
		m_pAlgorithmEncoder->encodeHeader();

		this->getDynamicBoxContext().markOutputAsReadyToSend(0, 0, 0);
	}

	std::vector<std::vector<std::string>> l_vSpectrumBloc;
	for (uint32_t i = 0; i < m_vDataMatrix.size(); i++)
	{
		l_vSpectrumBloc.push_back(m_vDataMatrix[i]);
	}

	//clear matrix
	ClearMatrix(m_vDataMatrix);

	for (size_t i = 0; i < l_vSpectrumBloc.size(); i++)
	{
		m_vDataMatrix.push_back(l_vSpectrumBloc[i]);
		//send the current bloc if the next data hasn't the same date
		if (i >= l_vSpectrumBloc.size() - 1 || l_vSpectrumBloc[i + 1][0] != m_vDataMatrix[0][0])
		{
			OV_ERROR_UNLESS_KRF(
				convertVectorDataToMatrix(iMatrix),
				"Error converting vector data to spectrum",
				ErrorType::Internal
			);

			m_pAlgorithmEncoder->encodeBuffer();
			const uint64_t l_ui64Date = ITimeArithmetics::secondsToTime(std::stod(m_vDataMatrix[0][0]));
			this->getDynamicBoxContext().markOutputAsReadyToSend(0, l_ui64Date - 1, l_ui64Date);

			//clear matrix
			ClearMatrix(m_vDataMatrix);
		}
	}

	//clear matrix
	ClearMatrix(l_vSpectrumBloc);
	return true;
}

bool CBoxAlgorithmCSVFileReader::convertVectorDataToMatrix(IMatrix* matrix)
{
	// note: Chunk size shouldn't change after encoding header, do not mess with it here, even if the input has different size

	// We accept partial data, but not buffer overruns ...
	OV_ERROR_UNLESS_KRF(
		matrix->getDimensionSize(1) >= m_vDataMatrix.size() && matrix->getDimensionSize(0) >= (m_ui32ColumnCount-1),
		"Matrix size incompatibility, data suggests " << m_ui32ColumnCount-1 << "x" << static_cast<uint64_t>(m_vDataMatrix.size())
		<< ", expected at most " << matrix->getDimensionSize(0) << "x" << matrix->getDimensionSize(0),
		ErrorType::Overflow
	);

	std::stringstream l_sMatrix;
	for (uint32_t i = 0; i < m_vDataMatrix.size(); i++)
	{
		l_sMatrix << "at time (" << m_vDataMatrix[i][0].c_str() << "):";
		for (uint32_t j = 0; j < m_ui32ColumnCount - 1; j++)
		{
			matrix->getBuffer()[j * matrix->getDimensionSize(1) + i] = std::stod(m_vDataMatrix[i][j + 1].c_str());
			l_sMatrix << matrix->getBuffer()[j * matrix->getDimensionSize(1) + i] << ";";
		}
		l_sMatrix << "\n";
	}
	getLogManager() << LogLevel_Debug << "Matrix:\n" << l_sMatrix.str().c_str();
	getLogManager() << LogLevel_Debug << "Matrix:\n" << l_sMatrix.str().c_str();

	return true;
}
