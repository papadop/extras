#include "ovpCBoxAlgorithmSignalConcatenation.h"
#include <openvibe/ovITimeArithmetics.h>

#include <system/ovCMemory.h>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace FileIO;

using namespace std;

bool CBoxAlgorithmSignalConcatenation::initialize()
{
	m_signalChunkBuffers.resize(this->getStaticBoxContext().getInputCount() >> 1);
	m_stimulationChunkBuffers.resize(this->getStaticBoxContext().getInputCount() >> 1);

	m_timeOut = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_timeOut = m_timeOut << 32;
	this->getLogManager() << LogLevel_Info << "Timeout set to " << time64(m_timeOut) << ".\n";
	for (uint32_t i = 0; i < this->getStaticBoxContext().getInputCount(); i += 2)
	{
		m_eofStimulations.push_back(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), (i >> 2) + 1));
		m_eofReached.push_back(false);
		m_fileEndTimes.push_back(0);
	}

	for (uint32_t i = 0; i < this->getStaticBoxContext().getInputCount(); i += 2)
	{
		auto* signalDecoder = new OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmSignalConcatenation>(*this, i);
		auto* stimDecoder = new OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmSignalConcatenation>(*this, i + 1);

		m_signalDecoders.push_back(signalDecoder);
		m_stimulationDecoders.push_back(stimDecoder);
		auto* stimSet = new CStimulationSet();
		m_stimulationSets.push_back(stimSet);
	}

	m_stimulationEncoder.initialize(*this, 1);
	m_stimulationEncoder.getInputStimulationSet().setReferenceTarget(m_stimulationDecoders[0]->getOutputStimulationSet());

	m_signalEncoder.initialize(*this, 0);

	m_triggerEncoder.initialize(*this, 2);
	m_triggerEncoder.getInputStimulationSet().setReferenceTarget(m_stimulationDecoders[0]->getOutputStimulationSet());

	m_headerReceivedCount = 0;
	m_endReceivedCount = 0;

	m_headerSent = false;
	m_endSent = false;
	m_stimHeaderSent = false;
	m_finished = false;
	m_resynchroDone = false;
	m_statsPrinted = false;

	m_state.m_CurrentFileIndex = 0;
	m_state.m_CurrentChunkIndex = 0;
	m_state.m_CurrentStimulationIndex = 0;

	m_triggerDate = 0;
	m_lastChunkStartTime = 0;
	m_lastChunkEndTime = 0;

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmSignalConcatenation::uninitialize()
{
	m_stimulationEncoder.uninitialize();
	m_signalEncoder.uninitialize();
	m_triggerEncoder.uninitialize();

	for (uint32_t i = 0; i < m_signalDecoders.size(); ++i)
	{
		m_signalDecoders[i]->uninitialize();
		m_stimulationDecoders[i]->uninitialize();
		delete m_signalDecoders[i];
		delete m_stimulationDecoders[i];
	}

	for (auto& signalChunkBuffer : m_signalChunkBuffers)
	{
		for (auto& signalChunk : signalChunkBuffer)
		{
			delete signalChunk.m_Buffer;
		}
	}

	for (auto& stimulationChunkBuffer : m_stimulationChunkBuffers)
	{
		for (auto& stimulationChunk : stimulationChunkBuffer)
		{
			delete stimulationChunk.m_StimulationSet;
		}
	}

	for (auto& stimulationSet : m_stimulationSets) { delete stimulationSet; }

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmSignalConcatenation::processInput(uint32_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmSignalConcatenation::processClock(CMessageClock& /*messageClock*/)
{
	if (!m_headerSent || m_finished) { return true; }

	const uint64_t currentTime = this->getPlayerContext().getCurrentTime();

	for (uint32_t i = 0; i < m_fileEndTimes.size(); i++)
	{
		if (!m_eofReached[i] && currentTime > m_fileEndTimes[i] + m_timeOut)
		{
			m_eofReached[i] = true;
			this->getLogManager() << LogLevel_Info << "File #" << i + 1 << "/" << (this->getStaticBoxContext().getInputCount() / 2) << " has timed out (effective end time: " << time64(m_fileEndTimes[i]) << ").\n";
		}
	}

	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmSignalConcatenation::process()
{
	const IBox& staticBoxContext = this->getStaticBoxContext();
	IBoxIO& boxContext = this->getDynamicBoxContext();

	//SIGNAL INPUTS
	for (uint32_t input = 0; input < staticBoxContext.getInputCount(); input += 2)
	{
		const uint32_t idx = input >> 1;

		for (uint32_t chunk = 0; chunk < boxContext.getInputChunkCount(input); chunk++)
		{
			m_signalDecoders[idx]->decode(chunk, true);

			if (m_signalDecoders[idx]->isHeaderReceived())
			{
				// Not received all headers we expect? Decode and test ...
				if (m_headerReceivedCount < staticBoxContext.getInputCount() / 2)
				{
					const uint64_t samplingFrequency = m_signalDecoders[idx]->getOutputSamplingRate();
					const uint32_t channelCount = m_signalDecoders[idx]->getOutputMatrix()->getDimensionSize(0);
					const uint32_t sampleCountPerBuffer = m_signalDecoders[idx]->getOutputMatrix()->getDimensionSize(1);

					// Note that the stream may be decoded in any order, hence e.g. stream  2 header may be received before stream 1	 ...
					if (m_headerReceivedCount == 0)
					{
						this->getLogManager() << LogLevel_Info << "Common sampling rate is " << samplingFrequency << ", channel count is " << channelCount << " and sample count per buffer is " << sampleCountPerBuffer << ".\n";

						// Set the encoder to follow the parameters of this first received input
						m_signalEncoder.getInputSamplingRate().setReferenceTarget(m_signalDecoders[idx]->getOutputSamplingRate());
						m_signalEncoder.getInputMatrix().setReferenceTarget(m_signalDecoders[idx]->getOutputMatrix());

						m_signalEncoder.encodeHeader();
						boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(input, chunk), boxContext.getInputChunkEndTime(input, chunk));
						m_headerSent = true;
					}
					else
					{
						if (m_signalEncoder.getInputSamplingRate() != samplingFrequency)
						{
							this->getLogManager() << LogLevel_Error << "File #"
								<< idx + 1 << "/" << (staticBoxContext.getInputCount() / 2)
								<< " has a different sampling rate (" << samplingFrequency
								<< "Hz) than other file(s) (" << m_signalEncoder.getInputSamplingRate() << "Hz).\n";
							return false;
						}
						if (m_signalEncoder.getInputMatrix()->getDimensionSize(0) != channelCount)
						{
							this->getLogManager() << LogLevel_Error << "File #"
								<< idx + 1 << "/" << (staticBoxContext.getInputCount() / 2)
								<< " has a different channel count (" << channelCount
								<< ") than other file(s) (" << m_signalEncoder.getInputMatrix()->getDimensionSize(0) << ").\n";
							return false;
						}
						if (m_signalEncoder.getInputMatrix()->getDimensionSize(1) != sampleCountPerBuffer)
						{
							this->getLogManager() << LogLevel_Error << "File #"
								<< idx + 1 << "/" << (staticBoxContext.getInputCount() / 2)
								<< " has a different sample count per buffer (" << sampleCountPerBuffer
								<< ") than other file(s) (" << m_signalEncoder.getInputMatrix()->getDimensionSize(1) << ").\n";
							return false;
						}
					}

					m_headerReceivedCount++;
				}
			}

			if (m_signalDecoders[idx]->isBufferReceived() && !m_eofReached[idx])
			{
				IMemoryBuffer* buffer = new CMemoryBuffer();
				buffer->setSize(boxContext.getInputChunk(input, chunk)->getSize(), true);
				System::Memory::copy(buffer->getDirectPointer(), boxContext.getInputChunk(input, chunk)->getDirectPointer(), buffer->getSize());
				SChunk val;
				val.m_Buffer = buffer;
				val.m_StartTime = boxContext.getInputChunkStartTime(input, chunk);
				val.m_EndTime = boxContext.getInputChunkEndTime(input, chunk);
				m_signalChunkBuffers[idx].push_back(val);

				if (boxContext.getInputChunkEndTime(input, chunk) < m_fileEndTimes[idx])
				{
					this->getLogManager() << LogLevel_Warning << "Oops, added extra chunk  "
						<< time64(boxContext.getInputChunkStartTime(input, chunk))
						<< " to " << time64(boxContext.getInputChunkEndTime(input, chunk))
						<< "\n";
				}

				m_fileEndTimes[idx] = boxContext.getInputChunkEndTime(input, chunk);
			}

			if (m_signalDecoders[idx]->isEndReceived())
			{
				// we assume the signal chunks must be continuous, so the end time is the end of the last buffer, don't set here
				//just discard it (automatic by decoder)
			}
		}
	}

	//STIMULATION INPUTS
	for (uint32_t input = 1; input < staticBoxContext.getInputCount(); input += 2)
	{
		const uint32_t idx = input >> 1;

		for (uint32_t chunk = 0; chunk < boxContext.getInputChunkCount(input); chunk++)
		{
			m_stimulationDecoders[idx]->decode(chunk, true);
			if (m_stimulationDecoders[idx]->isHeaderReceived() && !m_stimHeaderSent)
			{
				m_stimulationEncoder.encodeHeader();
				boxContext.markOutputAsReadyToSend(1, boxContext.getInputChunkStartTime(input, chunk), boxContext.getInputChunkEndTime(input, chunk));
				m_triggerEncoder.encodeHeader();
				boxContext.markOutputAsReadyToSend(2, boxContext.getInputChunkStartTime(input, chunk), boxContext.getInputChunkEndTime(input, chunk));
				m_stimHeaderSent = true;
			}
			if (m_stimulationDecoders[idx]->isBufferReceived() && !m_eofReached[idx])
			{
				const IStimulationSet* stimSet = m_stimulationDecoders[idx]->getOutputStimulationSet();

				SStimulationChunk val;
				val.m_StartTime = boxContext.getInputChunkStartTime(input, chunk);
				val.m_EndTime = boxContext.getInputChunkEndTime(input, chunk);

				if (stimSet->getStimulationCount() > 0) { val.m_StimulationSet = new CStimulationSet(); }
				else { val.m_StimulationSet = nullptr; }

				m_stimulationChunkBuffers[idx].push_back(val); // we store even if empty to be able to retain the chunking structure of the stimulation input stream

				for (uint64_t stim = 0; stim < stimSet->getStimulationCount(); stim++)
				{
					val.m_StimulationSet->appendStimulation(stimSet->getStimulationIdentifier(stim), stimSet->getStimulationDate(stim), stimSet->getStimulationDuration(stim));

					this->getLogManager() << LogLevel_Trace << "Input " << input
						<< ": Discovered stim " << stimSet->getStimulationIdentifier(stim)
						<< " at date [" << time64(stimSet->getStimulationDate(stim))
						<< "] in chunk [" << time64(val.m_StartTime)
						<< ", " << time64(val.m_EndTime)
						<< "]\n";

					if (stimSet->getStimulationIdentifier(stim) == m_eofStimulations[idx])
					{
						m_eofReached[idx] = true;
						m_fileEndTimes[idx] = val.m_EndTime;
						this->getLogManager() << LogLevel_Info << "File #" << idx + 1 << "/" << (staticBoxContext.getInputCount() / 2) << " is finished (end time: " << time64(m_fileEndTimes[idx]) << "). Later signal chunks will be discarded.\n";

						break;
					}
				}
			}
			if (m_stimulationDecoders[idx]->isEndReceived() && !m_endSent) { m_endReceivedCount++; }
			if (m_endReceivedCount == staticBoxContext.getInputCount() / 2 - 1) { m_endSent = true; }
		}
	}

	bool shouldConcatenate = true;
	for (auto&& eof : m_eofReached) { shouldConcatenate &= eof; }

	if (shouldConcatenate && !m_statsPrinted)
	{
		for (uint32_t i = 0; i < m_stimulationChunkBuffers.size(); ++i)
		{
			if (!m_signalChunkBuffers[i].empty())
			{
				this->getLogManager() << LogLevel_Trace << "File " << i
					<< " has 1st signal chunk at " << time64(m_signalChunkBuffers[i][0].m_StartTime)
					<< " last at [" << time64(m_signalChunkBuffers[i].back().m_EndTime)
					<< ", " << time64(m_signalChunkBuffers[i].back().m_EndTime)
					<< "].\n";
			}
			if (!m_stimulationChunkBuffers[i].empty())
			{
				this->getLogManager() << LogLevel_Trace << "File " << i
					<< " has 1st stim chunk at " << time64(m_stimulationChunkBuffers[i][0].m_StartTime)
					<< " last at [" << time64(m_stimulationChunkBuffers[i].back().m_EndTime)
					<< ", " << time64(m_stimulationChunkBuffers[i].back().m_EndTime)
					<< "].\n";
			}
			this->getLogManager() << LogLevel_Trace << "File " << i
				<< " EOF is at " << time64(m_fileEndTimes[i])
				<< "\n";
		}
		m_statsPrinted = true;
	}

	if (shouldConcatenate && !m_finished)
	{
		if (!this->concate()) { return true; }
		m_stimulationEncoder.encodeEnd();
		boxContext.markOutputAsReadyToSend(1, m_lastChunkEndTime, m_lastChunkEndTime);
		m_triggerEncoder.encodeEnd();
		boxContext.markOutputAsReadyToSend(2, m_lastChunkEndTime, m_lastChunkEndTime);
		m_signalEncoder.encodeEnd();
		boxContext.markOutputAsReadyToSend(0, m_lastChunkEndTime, m_lastChunkEndTime);

		m_triggerEncoder.getInputStimulationSet()->appendStimulation(OVTK_StimulationId_EndOfFile, this->getPlayerContext().getCurrentTime(), 0);
		m_triggerEncoder.encodeBuffer();
		boxContext.markOutputAsReadyToSend(2, this->getPlayerContext().getCurrentTime(), this->getPlayerContext().getCurrentTime());
		m_finished = true;
	}

	return true;
}


bool CBoxAlgorithmSignalConcatenation::concate()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();
	if (!m_resynchroDone)
	{
		this->getLogManager() << LogLevel_Info << "Concatenation in progress...\n";
		this->getLogManager() << LogLevel_Trace << "Resynchronizing Chunks ...\n";

		// note: m_stimulationSets and m_signalChunkBuffers should have the same size (== number of files)

		uint64_t offset = m_fileEndTimes[0];

		for (uint32_t i = 1; i < m_stimulationChunkBuffers.size(); i++)
		{
			for (auto& stimulationChunkBuffer : m_stimulationChunkBuffers[i])
			{
				IStimulationSet* stimSet = stimulationChunkBuffer.m_StimulationSet;
				if (stimSet)
				{
					for (uint64_t k = 0; k < stimSet->getStimulationCount(); k++)
					{
						const uint64_t synchronizedDate = stimSet->getStimulationDate(k) + offset;
						stimSet->setStimulationDate(k, synchronizedDate);
						//this->getLogManager() << LogLevel_Info << "Resynchronizing stim ["<<m_vStimulations[i][j].first<<"] from time ["<<m_vStimulations[i][j].second<<"] to ["<<l_ui64SynchronizedDate<<"]\n";
					}
				}
				stimulationChunkBuffer.m_StartTime += offset;
				stimulationChunkBuffer.m_EndTime += offset;
			}

			for (auto& signalChunkBuffer : m_signalChunkBuffers[i])
			{
				signalChunkBuffer.m_StartTime += offset;
				signalChunkBuffer.m_EndTime += offset;
			}

			offset = offset + m_fileEndTimes[i];
		}

		this->getLogManager() << LogLevel_Trace << "Resynchronization finished.\n";
		m_resynchroDone = true;
	}

	// When we get here, resynchro has been done

	// note that the iterators are references on purpose...

	for (uint32_t& i = m_state.m_CurrentFileIndex; i < m_signalChunkBuffers.size(); i++)
	{
		const std::vector<SChunk>& chunkVector = m_signalChunkBuffers[i];
		const std::vector<SStimulationChunk>& stimulusChunkVector = m_stimulationChunkBuffers[i];

		// Send a signal chunk
		uint32_t& chunk = m_state.m_CurrentChunkIndex;
		if (chunk < chunkVector.size())
		{
			// we write the signal memory buffer
			const IMemoryBuffer* buffer = chunkVector[chunk].m_Buffer;
			IMemoryBuffer* oMemoryBuffer = boxContext.getOutputChunk(0);
			oMemoryBuffer->setSize(buffer->getSize(), true);
			System::Memory::copy(oMemoryBuffer->getDirectPointer(), buffer->getDirectPointer(), buffer->getSize());
			boxContext.markOutputAsReadyToSend(0, chunkVector[chunk].m_StartTime, chunkVector[chunk].m_EndTime);

			/*
			if(ITimeArithmetics::timeToSeconds(l_rChunkVector[l_rChunk].m_StartTime)>236)

			{
				this->getLogManager() << LogLevel_Info << "Adding signalchunk " << i << "," << l_rChunk << " ["
					<< time64(l_rChunkVector[l_rChunk].m_StartTime)
					<< ", " << time64(l_rChunkVector[l_rChunk].m_EndTime)
					<< "\n";
			}
			*/

			const uint64_t signalChunkEnd = chunkVector[chunk].m_EndTime;

			// Write stimulations up to this point
			for (uint32_t& k = m_state.m_CurrentStimulationIndex; k < stimulusChunkVector.size() && stimulusChunkVector[k].m_EndTime <= signalChunkEnd; ++k)
			{
				const SStimulationChunk& stimChunk = stimulusChunkVector[k];
				const IStimulationSet* bufferedStimSet = stimChunk.m_StimulationSet;

				IStimulationSet* stimSet = m_stimulationEncoder.getInputStimulationSet();
				stimSet->clear();

				if (bufferedStimSet)
				{
					for (uint64_t s = 0; s < bufferedStimSet->getStimulationCount(); s++)
					{
						stimSet->appendStimulation(bufferedStimSet->getStimulationIdentifier(s), bufferedStimSet->getStimulationDate(s), bufferedStimSet->getStimulationDuration(s));

						this->getLogManager() << LogLevel_Trace << "Adding stimulation " << bufferedStimSet->getStimulationIdentifier(s)
							<< " at date [" << time64(stimSet->getStimulationDate(s))
							<< "] to chunk [" << time64(stimChunk.m_StartTime)
							<< ", " << time64(stimChunk.m_EndTime)
							<< "]\n";
					}
				}

				// encode the stim memory buffer even if it is empty
				m_stimulationEncoder.encodeBuffer();
				boxContext.markOutputAsReadyToSend(1, stimChunk.m_StartTime, stimChunk.m_EndTime);
				/*
				if(ITimeArithmetics::timeToSeconds(l_rStimChunk.m_StartTime)>238 &&
					ITimeArithmetics::timeToSeconds(l_rStimChunk.m_StartTime)<242)

				{
					this->getLogManager() << LogLevel_Info << "Adding stimchunk " << i << "," << k << " ["
						<< time64(l_rStimChunk.m_StartTime)
						<< ", " << time64(l_rStimChunk.m_EndTime)
						<< "\n";
				}
				*/
			}

			// Let the kernel send blocks up to now, prevent freezing up sending everything at once
			chunk++;
			return false;
		}

		// For now we don't support stimuli that don't correspond to signal data, these ones are after the last signal chunk
		for (uint32_t& k = m_state.m_CurrentStimulationIndex; k < stimulusChunkVector.size(); ++k)
		{
			const SStimulationChunk& stimChunk = stimulusChunkVector[k];
			const IStimulationSet* bufferedStimSet = stimChunk.m_StimulationSet;

			if (i == m_signalChunkBuffers.size() - 1)
			{
				// last file, let pass

				IStimulationSet* stimSet = m_stimulationEncoder.getInputStimulationSet();
				stimSet->clear();

				if (bufferedStimSet)
				{
					for (uint64_t s = 0; s < bufferedStimSet->getStimulationCount(); s++)
					{
						stimSet->appendStimulation(bufferedStimSet->getStimulationIdentifier(s), bufferedStimSet->getStimulationDate(s), bufferedStimSet->getStimulationDuration(s));

						this->getLogManager() << LogLevel_Warning << "Stimulation " << bufferedStimSet->getStimulationIdentifier(s)
							<< " at date [" << time64(stimSet->getStimulationDate(s))
							<< "] in chunk [" << time64(stimChunk.m_StartTime)
							<< ", " << time64(stimChunk.m_EndTime)
							<< "] is after signal ended, but last file, so adding.\n";
					}
				}

				// encode the stim memory buffer even if it is empty
				m_stimulationEncoder.encodeBuffer();
				boxContext.markOutputAsReadyToSend(1, stimChunk.m_StartTime, stimChunk.m_EndTime);
			}
			else
			{
				if (bufferedStimSet)
				{
					for (uint64_t s = 0; s < bufferedStimSet->getStimulationCount(); s++)
					{
						if (!chunkVector.empty())
						{
							this->getLogManager() << LogLevel_Warning
								<< "Stimulation " << bufferedStimSet->getStimulationIdentifier(s)
								<< "'s chunk at [" << time64(stimChunk.m_StartTime)
								<< ", " << time64(stimChunk.m_EndTime)
								<< "] is after the last signal chunk end time " << time64(chunkVector.back().m_EndTime)
								<< ", discarded.\n";
						}
					}
				}
			}
		}


		// Finished with the file

		//	if(l_rStimChunk.m_EndTime < l_rChunkVector[m_CurrentChunkIndex].m_EndTime) 
		//	{
		// There is no corresponding signal anymore, skip the rest of the stimulations from this file
		//this->getLogManager() << LogLevel_Info << "Stimulus time " << time64(l_rStimulusChunkVector[j].m_EndTime) 
		//	<< " exceeds the last signal buffer end time " << time64(l_rChunkVector[l_rChunkVector.size()-1].m_EndTime) 
		//	<< "\n";
		//break;
		//}
		m_state.m_CurrentChunkIndex = 0;
		m_state.m_CurrentStimulationIndex = 0;

		this->getLogManager() << LogLevel_Info << "File #" << i + 1 << " Finished.\n";
	}

	//We search for the last file with data.
	for (uint32_t lastFile = m_signalChunkBuffers.size(); lastFile > 0; lastFile--)
	{
		const uint32_t lastChunkOfLastFile = m_signalChunkBuffers[lastFile - 1].size();
		if (lastChunkOfLastFile != 0)
		{
			m_lastChunkEndTime = m_signalChunkBuffers[lastFile - 1][lastChunkOfLastFile - 1].m_EndTime;
			break;
		}
	}

	this->getLogManager() << LogLevel_Info << "Concatenation finished !\n";


	return true;
}
