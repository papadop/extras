#ifndef __OpenViBEPlugins_FileIO_CGDFFileReader_H__
#define __OpenViBEPlugins_FileIO_CGDFFileReader_H__

#include "../ovp_defines.h"
#include "../ovp_gdf_helpers.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

#include <system/ovCMemory.h>

#include <fstream>
#include <string>
#include <vector>

#include <iostream>
using namespace std;

#define GDFReader_ExperimentInfoOutput  0
#define GDFReader_SignalOutput          1
#define GDFReader_StimulationOutput     2

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		/**
		* The GDF reader plugin main class
		*
		*/
		class CGDFFileReader : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			//Helper structures
			class CExperimentInfoHeader
			{
			public:
				uint64_t m_ui64ExperimentId;
				std::string m_sExperimentDate;

				uint64_t m_ui64SubjectId;
				std::string m_sSubjectName;
				uint64_t m_ui64SubjectAge;
				uint64_t m_ui64SubjectSex;

				uint64_t m_ui64LaboratoryId;
				std::string m_sLaboratoryName;
				uint64_t m_ui64TechnicianId;
				std::string m_sTechnicianName;

				bool m_bReadyToSend;
			};

			// Used to store information about the signal stream
			class CSignalDescription
			{
			public:
				CSignalDescription() : m_ui32StreamVersion(1), m_ui32SamplingRate(0), m_ui32ChannelCount(0), m_ui32SampleCount(0), m_ui32CurrentChannel(0), m_bReadyToSend(false) { }

			public:
				uint32_t m_ui32StreamVersion;
				uint32_t m_ui32SamplingRate;
				uint32_t m_ui32ChannelCount;
				uint32_t m_ui32SampleCount;
				std::vector<std::string> m_pChannelName;
				uint32_t m_ui32CurrentChannel;

				bool m_bReadyToSend;
			};

		public:

			CGDFFileReader();

			void release() override;

			bool initialize() override;

			bool uninitialize() override;

			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;

			bool process() override;

			uint64_t getClockFrequency() override { return m_ui64ClockFrequency; }

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_GDFFileReader)

		public:
			bool readFileHeader();
		public:

			bool m_bErrorOccurred;	//true if an error has occurred while reading the GDF file

			//The GDF filename and handle
			OpenViBE::CString m_sFileName;
			std::ifstream m_oFile;
			uint64_t m_ui64FileSize;
			uint64_t m_ui64Header3Length;

			float m_f32FileVersion;

			OpenViBEToolkit::TSignalEncoder<CGDFFileReader>* m_pSignalEncoder;
			OpenViBEToolkit::TExperimentInformationEncoder<CGDFFileReader>* m_pExperimentInformationEncoder;
			OpenViBEToolkit::TStimulationEncoder<CGDFFileReader>* m_pStimulationEncoder;


			//Stream information
			uint64_t m_ui32SamplesPerBuffer;	//user defined

			//input
			uint64_t m_ui64NumberOfDataRecords;
			double m_f64DurationOfDataRecord;
			uint16_t m_ui16NumberOfChannels;

			uint32_t m_ui32NumberOfSamplesPerRecord; // We only handle the files where it is the same for all the channels

			//info about channel's data type in data record
			uint32_t* m_pChannelType;
			uint16_t* m_pChannelDataSize;
			double* m_pChannelScale;
			double* m_pChannelTranslate;

			//Size of a data record
			uint64_t m_ui64DataRecordSize;

			//The current data record's data
			uint8_t* m_pDataRecordBuffer;

			//pointers to each channel's information in the current data record
			uint8_t** m_pChannelDataInDataRecord;

			//Output Stream matrix
			double* m_pMatrixBuffer;
			uint64_t m_ui64MatrixBufferSize;
			bool m_bMatricesSent;

			//Total number of samples sent up to now (used to compute start/end time)
			uint32_t m_ui32SentSampleCount;

			//indexes of current data record, channel, and sample
			uint64_t m_ui64CurrentDataRecord;
			uint32_t m_ui32CurrentSampleInDataRecord;

			//Events variables
			uint8_t m_ui8EventTableMode;		//mode of the event table
			uint32_t m_ui32NumberOfEvents;		//number of events in the event table
			uint32_t* m_pEventsPositionBuffer;	//pointer on the array of event's positions
			uint16_t* m_pEventsTypeBuffer;		//pointer on the array of event's types

			std::vector<GDF::CGDFEvent> m_oEvents;		//current stimulation block

			uint32_t m_ui32CurrentEvent;		//current event in event table
			bool m_bEventsSent;			//true if all the events have been sent
			bool m_bAppendEOF;				//true if the file does contains a recognized EOF marker, then we add our own

			uint64_t m_ui64StimulationPerBuffer;	//user defined

			//helper structures
			CExperimentInfoHeader* m_pExperimentInfoHeader;
			bool m_bExperimentInformationSent;

			CSignalDescription m_pSignalDescription;
			bool m_bSignalDescriptionSent;

			uint64_t m_ui64ClockFrequency;

			bool m_bTranslateByMinimum;


		private:

			void writeExperimentInformation();
			void writeSignalInformation();
			void writeEvents();

			bool initializeFile();

			template <class T>
			double GDFTypeToFloat64(T val, uint32_t ui32Channel)
			{
				return m_pChannelScale[ui32Channel] * static_cast<double>(val) + m_pChannelTranslate[ui32Channel];
			}

			template <class T>
			void GDFTypeBufferToFloat64Buffer(double* out, T* in, uint64_t inputBufferSize, uint32_t ui32Channel)
			{
				for (uint64_t i = 0; i < inputBufferSize; i++)
				{
					out[i] = GDFTypeToFloat64<T>(in[i], ui32Channel);
				}
			}

			void GDFBufferToFloat64Buffer(double* out, void* in, uint64_t inputBufferSize, uint32_t ui32Channel);
		};

		// template<> OpenViBE::float64 CGDFFileReader::GDFTypeToFloat64<OpenViBE::float32>(OpenViBE::float32 val, uint32_t ui32Channel);
		// template<> OpenViBE::float64 CGDFFileReader::GDFTypeToFloat64<OpenViBE::float64>(OpenViBE::float64 val, uint32_t ui32Channel);

		/**
		* Description of the GDF Reader plugin
		*/
		class CGDFFileReaderDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("GDF file reader"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Bruno Renier, Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("GDF file reader"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Reads .GDF format files"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/GDF"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.9.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-open"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_GDFFileReader; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CGDFFileReader(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				// Adds box outputs
				rPrototype.addOutput("Experiment information", OV_TypeId_ExperimentInformation);
				rPrototype.addOutput("EEG stream", OV_TypeId_Signal);
				rPrototype.addOutput("Stimulations", OV_TypeId_Stimulations);

				// Adds settings
				rPrototype.addSetting("Filename", OV_TypeId_Filename, "");
				rPrototype.addSetting("Samples per buffer", OV_TypeId_Integer, "32");
				rPrototype.addSetting("Subtract physical minimum", OV_TypeId_Boolean, "false");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_GDFFileReaderDesc)
		};
	}
}

#endif
