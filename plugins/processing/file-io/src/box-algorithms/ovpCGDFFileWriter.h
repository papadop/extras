#ifndef __OpenViBEPlugins_FileIO_CGDFFileWriter_H__
#define __OpenViBEPlugins_FileIO_CGDFFileWriter_H__

#include "../ovp_defines.h"
#include "../ovp_gdf_helpers.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <system/ovCMemory.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <stack>

#include <cstdio>

namespace OpenViBEPlugins
{
	namespace FileIO
	{

		/**
		* The plugin's main class
		*
		*/
		class CGDFFileWriter : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CGDFFileWriter();

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_GDFFileWriter)

		public:
			void setChannelCount(const uint32_t ui32ChannelCount);
			void setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName);
			void setSampleCountPerBuffer(const uint32_t ui32SampleCountPerBuffer);
			void setSamplingRate(const uint32_t ui32SamplingFrequency);
			void setSampleBuffer(const double* pBuffer);

			void setExperimentInformation();

			void setStimulation(const OpenViBE::uint64 ui64StimulationIdentifier, const uint64_t ui64StimulationDate);

			void saveMatrixData();
			void saveEvents();
			void padByEvents();

		public:
			std::ofstream m_oFile;
			OpenViBE::CString m_sFileName;

			OpenViBEToolkit::TSignalDecoder<CGDFFileWriter>* m_pSignalDecoder;
			OpenViBEToolkit::TExperimentInformationDecoder<CGDFFileWriter>* m_pExperimentInformationDecoder;
			OpenViBEToolkit::TStimulationDecoder<CGDFFileWriter>* m_pStimulationDecoder;

			//GDF structures
			GDF::CFixedGDF1Header m_oFixedHeader;
			//std::vector< GDF::CVariableGDF1HeaderPerChannel > m_vVariableHeader;
			GDF::CVariableGDF1Header m_oVariableHeader;


			std::vector<std::vector<double>> m_vSamples;
			std::vector<int64_t> m_vSampleCount;


			uint32_t m_ui32SamplesPerChannel;
			uint64_t m_ui64SamplingFrequency;

			std::vector<std::pair<OpenViBE::uint64, uint64_t>> m_oEvents;

			bool m_bError;
			double m_f64Precision; // because of GDF writing problem (no scaling)
		};

		/**
		* Plugin's description
		*/
		class CGDFFileWriterDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("GDF file writer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("This algorithm records on disk what comes from a specific output"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This algorithm dumps on disk a stream from a specific output in the standard GDF file format"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/GDF"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.6"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-save"); }

			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_GDFFileWriter; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CGDFFileWriter(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				// Adds box inputs //swap order of the first two
				rPrototype.addInput("Experiment information", OV_TypeId_ExperimentInformation);
				rPrototype.addInput("Signal", OV_TypeId_Signal);
				rPrototype.addInput("Stimulation", OV_TypeId_Stimulations);

				// Adds box settings
				rPrototype.addSetting("Filename", OV_TypeId_Filename, "record-[$core{date}-$core{time}].gdf");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_GDFFileWriterDesc)
		};
	};
};

#endif // __OpenViBEPlugins_FileIO_CGDFFileWriter_H__
