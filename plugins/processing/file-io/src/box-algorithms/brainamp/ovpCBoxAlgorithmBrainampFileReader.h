#ifndef __OpenViBEPlugins_BoxAlgorithm_BrainampFileReader_H__
#define __OpenViBEPlugins_BoxAlgorithm_BrainampFileReader_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		class CBoxAlgorithmBrainampFileReader : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_BrainampFileReader);

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pBrainampFileReader;
			OpenViBE::Kernel::IAlgorithmProxy* m_pExperimentInformationStreamEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalStreamEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationStreamEncoder;

			bool m_bHeaderSent;
		};

		class CBoxAlgorithmBrainampFileReaderDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("BrainVision Format file reader"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Reads input having the BrainAmp file format"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/BrainVision Format"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-open"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_BrainampFileReader; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmBrainampFileReader; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				// Adds box outputs
				rBoxAlgorithmPrototype.addOutput("Experiment information", OV_TypeId_ExperimentInformation);
				rBoxAlgorithmPrototype.addOutput("EEG stream", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Stimulations", OV_TypeId_Stimulations);

				// Adds settings
				rBoxAlgorithmPrototype.addSetting("Filename (header)", OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Epoch size (in sec)", OV_TypeId_Float, "0.0625");
				rBoxAlgorithmPrototype.addSetting("Convert stimuli to OpenViBE labels", OV_TypeId_Boolean, "true");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_BrainampFileReaderDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_BrainampFileReader_H__
