#ifndef __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriter_H__
#define __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriter_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <fstream>
#include <map>

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		class CBoxAlgorithmBrainampFileWriter : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_BrainampFileWriter)

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalStreamDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationStreamDecoder;

			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pSignalMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pStimulationsMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pStimulationSet;
			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64SamplingRate;

			OpenViBE::CString m_sOutputFileFullPath;
			OpenViBE::CString m_sDictionaryFileName;
			bool m_bTransformStimulations;
			bool m_bShouldWriteFullFileNames;

		private:
			std::ofstream m_oHeaderFileStream;
			std::ofstream m_oEEGFileStream;
			std::ofstream m_oMarkerFileStream;

			std::map<uint64_t, std::string> m_mStimulationToMarker;

			uint64_t m_ui64MarkersWritten;
			bool m_bWasMarkerHeaderWritten;
		};

		class CBoxAlgorithmBrainampFileWriterDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("BrainVision Format File Writer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jozef Legeny"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Mensia Technologies"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Writes its input into a BrainVision format file"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box allows to write the input signal under BrainVision file format."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/BrainVision Format"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-save"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_BrainampFileWriter; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmBrainampFileWriter; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				// Adds box outputs
				rBoxAlgorithmPrototype.addInput("EEG stream", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);

				// Adds settings
				rBoxAlgorithmPrototype.addSetting("Filename (header in vhdr format)", OV_TypeId_Filename, "record-[$core{date}-$core{time}].vhdr");
				rBoxAlgorithmPrototype.addSetting("Marker to OV Stimulation dictionary", OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Convert OpenViBE Stimulations to markers", OV_TypeId_Boolean, "true");
				rBoxAlgorithmPrototype.addSetting("Use full data and marker file names in header", OV_TypeId_Boolean, "false");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_BrainampFileWriterDesc)
		};
	}
}

#endif // __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriter_H__
