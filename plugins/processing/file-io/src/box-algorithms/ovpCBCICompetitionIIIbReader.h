#ifndef __OpenViBEPlugins_file_io_CBCICompetitionIIIbReader_H__
#define __OpenViBEPlugins_file_io_CBCICompetitionIIIbReader_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>
#include <string>
#include <sstream>
#include <fstream>

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		class CBCICompetitionIIIbReader : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CBCICompetitionIIIbReader();

			void release() override { delete this; }

			bool initialize() override;

			bool uninitialize() override;

			bool process() override;

			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;

			uint64_t getClockFrequency() override { return m_ui64ClockFrequency; }

		public:
			void writeSignalInformation();

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_BCICompetitionIIIbReader)


		public:
			bool m_bErrorOccurred;	//true if an error has occurred while reading the GDF file

			//The filename and handle
			std::ifstream m_oSignalFile;
			uint64_t m_ui64FileSize;

			OpenViBEToolkit::TSignalEncoder<CBCICompetitionIIIbReader> m_oSignalEncoder;
			OpenViBEToolkit::TStimulationEncoder<CBCICompetitionIIIbReader> m_oStimulationEncoder;

			uint64_t m_ui64ClockFrequency;

			std::vector<uint64_t> m_oTriggerTime;
			std::vector<uint64_t> m_oEndOfTrial;
			std::vector<uint64_t> m_oCueDisplayStart;
			std::vector<uint64_t> m_oFeedbackStart;
			std::vector<uint64_t> m_oClassLabels;
			std::vector<bool> m_oArtifacts;
			std::vector<uint64_t> m_oTrueLabels;


			uint32_t m_ui32SamplesPerBuffer;
			uint32_t m_ui32SamplingRate;
			uint32_t m_ui32SentSampleCount;

			OpenViBE::IMatrix* m_pMatrixBuffer;
			bool m_bEndOfFile;

			uint32_t m_ui32CurrentTrial;

			bool m_bKeepTrainingSamples;
			bool m_bKeepTestSamples;
			bool m_bKeepArtifactSamples;

			double m_f64TrialLength;
			double m_f64CueDisplayStart;
			double m_f64FeedbackStart;
		};


		class CBCICompetitionIIIbReaderDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("BCI competition IIIb reader"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Bruno Renier"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Reads ASCII version of BCI competition IIIb datasets."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Reads signal samples, stimulations and class labels from the BCI competition IIIb ASCII datasets."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/BCI Competition"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.7"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-open"); }

			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BCICompetitionIIIbReader; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBCICompetitionIIIbReader(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				// Adds box outputs
				rPrototype.addOutput("Signal", OV_TypeId_Signal);
				rPrototype.addOutput("Stimulations", OV_TypeId_Stimulations);

				// Adds settings
				rPrototype.addSetting("Signal file", OV_TypeId_Filename, "");
				rPrototype.addSetting("Triggers file", OV_TypeId_Filename, "");
				rPrototype.addSetting("Labels file", OV_TypeId_Filename, "");
				rPrototype.addSetting("Artifact file", OV_TypeId_Filename, "");
				rPrototype.addSetting("True labels file", OV_TypeId_Filename, "");

				rPrototype.addSetting("Samples per buffer", OV_TypeId_Integer, "32");

				rPrototype.addSetting("Offline", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Train?", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Test?", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Keep artifacts?", OV_TypeId_Boolean, "false");

				rPrototype.addSetting("Trial length", OV_TypeId_Float, "8.0");
				rPrototype.addSetting("CUE display Start", OV_TypeId_Float, "3.0");
				rPrototype.addSetting("Feedback start", OV_TypeId_Float, "4.0");

				rPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BCICompetitionIIIbReaderDesc)
		};
	};
};

#endif
