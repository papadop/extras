#ifndef BCI2000READER_H
#define BCI2000READER_H


#include <string>
#include <fstream>
#include <istream>
#include <ostream>
#include <vector>
#include <map>
#include <boost/regex.hpp>

#include "ovpCBitfield.h"

namespace BCI2000
{
	/**
	* \class CBCI2000ReaderHelper
	* \author Olivier Rochel (INRIA)
	* \brief BCI2000 file format parser and utilities. Uses the m_oBitfield utility class.
	**/
	class CBCI2000ReaderHelper
	{
	protected:
		std::ifstream m_oBCIFile;

		float m_f32BCI2000version; // file version.
		int32_t m_i32HeaderLength;       // header size (inc. meta)
		int32_t m_i32SourceChannelCount;        // number of channels
		int32_t m_i32StateVectorLength; // size of state field
		OpenViBE::CString m_sDataFormat; // data format (float, int16_t...)

		std::vector<OpenViBE::CString> m_vChannelNames;

		int32_t m_i32NbSamples;
		int32_t m_i32SampleSize;
		int32_t m_i32SamplesLeft;

		bool m_bGood; 	// m_bGood is true if file open, header looks m_bGood (may
		// still be truncated or broken in a silly way)

		std::map<OpenViBE::CString, OpenViBE::CString> m_mParameters;
		// state vector
		CBitfield m_oBitfield;
		// helpers
		bool parseMeta(OpenViBE::CString& rMeta);
		bool parseHeader(std::istream& is);

	private:
		template <class TFrom, class TTo>
		OpenViBE::int32 readSamplesInternal(TTo* pSamples, uint32_t* pStates, int32_t i32Nb);

	public:
		/**
		* Constructor from a BCI2000 file.
		* \param filename BCI2000 file name.
		**/
		CBCI2000ReaderHelper(const char* filename);
		~CBCI2000ReaderHelper();

		void printInfo(std::ostream& os);
		float getRate() const;
		OpenViBE::CString getChannelName(uint32_t ui32Index) const;

		std::vector<float> readSample();
		OpenViBE::int32 readSamples(double* pSamples, uint32_t* pStates, int32_t i32Nb);

		// getters
		int32_t getNbSamples() const
		{
			return m_i32NbSamples;
		}

		int32_t getSampleSize() const
		{
			return m_i32SampleSize;
		}

		int32_t getChannels() const
		{
			return m_i32SourceChannelCount;
		}

		int32_t getSamplesLeft() const
		{
			return m_i32SamplesLeft;
		}

		bool isGood() const
		{
			return m_bGood;
		}

		int32_t getStateVectorSize() const
		{
			return m_oBitfield.size();
		}

		const OpenViBE::CString& getStateName(int32_t i) const
		{
			return m_oBitfield.getFieldName(i);
		}
	};
}

#endif
