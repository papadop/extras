#ifndef __OpenViBEPlugins_BoxAlgorithm_BCI2000Reader_H__
#define __OpenViBEPlugins_BoxAlgorithm_BCI2000Reader_H__

#include "ovpCBCI2000ReaderHelper.h"
#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_BCI2000Reader OpenViBE::CIdentifier(0xFF78DAF4, 0xC41544B8)
#define OVP_ClassId_BoxAlgorithm_BCI2000ReaderDesc OpenViBE::CIdentifier(0xFF53D107, 0xC31144B8)

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		/**
		 * \class CBoxAlgorithmBCI2000Reader
		 * \author Olivier Rochel (INRIA)
		 * \date Tue Jun 21 11:11:04 2011
		 * \brief The class CBoxAlgorithmBCI2000Reader describes the box BCI2000 Reader.
		 *
		 */
		class CBoxAlgorithmBCI2000Reader : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override
			{
				delete this;
			}

			bool initialize() override;
			bool uninitialize() override;

			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			uint64_t getClockFrequency() override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_BCI2000Reader);

		protected:

			bool m_bHeaderSent;

			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmBCI2000Reader> m_oSignalEncoder;
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmBCI2000Reader> m_oStateEncoder;

			// These 2 were from the time the matrices were built, not given by the encoders.
			// They could be removed, but for now make the code a bit easier to read - that's
			// why they're still there.
			OpenViBE::IMatrix* m_pSignalOutputMatrix;
			OpenViBE::IMatrix* m_pStateOutputMatrix;

			uint32_t m_ui32Rate;
			uint32_t m_ui32ChannelCount;
			uint32_t m_ui32SampleCountPerBuffer;
			double* m_pBuffer; 	// temporary buffer as we'll have to transpose data for signal_out
			uint32_t* m_pStates; 	// state variables, to be converted too;
			uint64_t m_ui32SamplesSent;
			BCI2000::CBCI2000ReaderHelper* m_pB2KReaderHelper;
			// helpers
			void sendHeader();
		};

		/**
		 * \class CBoxAlgorithmBCI2000ReaderDesc
		 * \author Olivier Rochel (INRIA)
		 * \date Tue Jun 21 11:11:04 2011
		 * \brief Descriptor of the box BCI2000 Reader.
		 *
		 */
		class CBoxAlgorithmBCI2000ReaderDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("BCI2000 File Reader"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Olivier Rochel"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Reads BCI2000 .dat files."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("The box reads EEG/States signals from a BCI2000 file (.dat)"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/BCI2000"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.3"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-open"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_BCI2000Reader; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmBCI2000Reader; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addOutput("Signal",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("State",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addSetting("File name",OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Samples per buffer",OV_TypeId_Integer, "16");
				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable); // meuh non !

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_BCI2000ReaderDesc);
		};
	};
};

#endif
