#ifndef __OpenViBEPlugins_BoxAlgorithm_SignalConcatenation_H__
#define __OpenViBEPlugins_BoxAlgorithm_SignalConcatenation_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <cstdio>
#include <vector>
#include <list>
// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_SignalConcatenation OpenViBE::CIdentifier(0x372F3A9D, 0x49E20CD2)
#define OVP_ClassId_BoxAlgorithm_SignalConcatenationDesc OpenViBE::CIdentifier(0x372F3A9D, 0x49E20CD2)

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		/**
		 * \class CBoxAlgorithmSignalConcatenation
		 * \author Laurent Bonnet (INRIA)
		 * \date Tue Jun 28 09:52:48 2011
		 * \brief The class CBoxAlgorithmSignalConcatenation describes the box Signal Concatenation.
		 *
		 */
		class CBoxAlgorithmSignalConcatenation : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processClock(OpenViBE::CMessageClock& messageClock) override;
			uint64_t getClockFrequency() override { return 8LL << 32; }

			bool processInput(uint32_t index) override;


			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_SignalConcatenation);

		protected:
			virtual bool concate();
			bool m_finished = false;
			bool m_resynchroDone = false;
			uint64_t m_timeOut = 0;

			bool m_headerSent = false;
			uint32_t m_headerReceivedCount = 0;
			uint32_t m_endReceivedCount = 0;
			bool m_stimHeaderSent = false;
			bool m_endSent = false;
			bool m_statsPrinted = false;

			std::vector<uint64_t> m_eofStimulations;
			std::vector<bool> m_eofReached;

			struct SChunk
			{
				OpenViBE::IMemoryBuffer* m_Buffer;
				uint64_t m_StartTime;
				uint64_t m_EndTime;
			};

			struct SStimulationChunk
			{
				OpenViBE::IStimulationSet* m_StimulationSet;
				uint64_t m_StartTime;
				uint64_t m_EndTime;
			};

			uint64_t m_stimChunkLength = 0;

			// File end times
			std::vector<uint64_t> m_fileEndTimes;

			// The signal buffers, one per file
			std::vector<std::vector<SChunk>> m_signalChunkBuffers;

			std::vector<std::vector<SStimulationChunk>> m_stimulationChunkBuffers;

			// The stimulations are stored in one stimulation set per file. The chunk are reconstructed.
			std::vector<OpenViBE::IStimulationSet *> m_stimulationSets;
			
			//The decoders, (1 signal/1 stim) per file
			std::vector<OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmSignalConcatenation> *> m_stimulationDecoders;
			std::vector<OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmSignalConcatenation> *> m_signalDecoders;
			
			// the encoders : signal, stim and trigger encoder.
			OpenViBEToolkit::TSignalEncoder<CBoxAlgorithmSignalConcatenation> m_signalEncoder;
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmSignalConcatenation> m_stimulationEncoder;
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmSignalConcatenation> m_triggerEncoder;

			uint64_t m_triggerDate = 0;
			uint64_t m_lastChunkStartTime = 0;
			uint64_t m_lastChunkEndTime = 0;

			struct SConcatenationState
			{
				uint32_t m_CurrentFileIndex;
				uint32_t m_CurrentChunkIndex;
				uint32_t m_CurrentStimulationIndex;
			};

			SConcatenationState m_state;
		};

		
		// The box listener can be used to call specific callbacks whenever the box structure changes : input added, name changed, etc.
		// Please uncomment below the callbacks you want to use.
		class CBoxAlgorithmSignalConcatenationListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool check(OpenViBE::Kernel::IBox& rBox) const
			{
				for (uint32_t i = 0; i < rBox.getInputCount() >> 1; i++)
				{
					rBox.setInputName(i * 2, ("Input signal " + std::to_string(i + 1)).c_str());
					rBox.setInputType(i * 2, OV_TypeId_Signal);

					rBox.setInputName(i * 2 + 1, ("Input stimulations " + std::to_string(i + 1)).c_str());
					rBox.setInputType(i * 2 + 1, OV_TypeId_Stimulations);

					rBox.setSettingName(i + 1, ("End-of-file stimulation for input " + std::to_string(i + 1)).c_str());
				}

				return true;
			}

			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t index) override
			{
				if (index & 1) { rBox.removeInput(index - 1); }	// odd index
				else { rBox.removeInput(index); }				// even index
				rBox.removeSetting(index >> 1);
				return this->check(rBox);
			}

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t /*index*/) override
			{
				rBox.addInput("", OV_TypeId_Stimulations);
				rBox.addSetting("",OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStop");
				return this->check(rBox);
			};

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};
		

		/**
		 * \class CBoxAlgorithmSignalConcatenationDesc
		 * \author Laurent Bonnet (INRIA)
		 * \date Tue Jun 28 09:52:48 2011
		 * \brief Descriptor of the box Signal Concatenation.
		 *
		 */
		class CBoxAlgorithmSignalConcatenationDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Signal Concatenation"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bonnet"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Concatenates multiple signal streams"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("The signal stream concatenation box reads multiple streams in parallel, and produces a single stream that is the concatenation of all inputs."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("2.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-add"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_SignalConcatenation; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmSignalConcatenation; }


			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmSignalConcatenationListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input signal 1",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Input stimulations 1",OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Input signal 2",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Input stimulations 2",OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);

				rBoxAlgorithmPrototype.addOutput("Signal",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Stimulations",OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addOutput("Status",OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Time out before assuming end-of-file (in sec)",OV_TypeId_Integer, "5");
				rBoxAlgorithmPrototype.addSetting("End-of-file stimulation for input 1",OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStop");
				rBoxAlgorithmPrototype.addSetting("End-of-file stimulation for input 2",OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStop");
				
				//rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_SignalConcatenationDesc);
		};
	}  // namespace FileIO
}  // namespace OpenViBEPlugins

#endif // __OpenViBEPlugins_BoxAlgorithm_SignalConcatenation_H__
