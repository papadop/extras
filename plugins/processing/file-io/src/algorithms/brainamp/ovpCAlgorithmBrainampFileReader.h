#ifndef __OpenViBEPlugins_Algorithm_BrainampFileReader_H__
#define __OpenViBEPlugins_Algorithm_BrainampFileReader_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <fstream>

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		class CAlgorithmBrainampFileReader : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_BrainampFileReader);

		protected:

			enum EStatus
			{
				Status_Nothing,
				Status_CommonInfos,
				Status_BinrayInfos,
				Status_ChannelInfos,
				Status_MarkerInfos,
				Status_Comment,
			};

			enum EBinaryFormat
			{
				BinaryFormat_Integer16,
				BinaryFormat_UnsignedInteger16,
				BinaryFormat_Float32,
			};

			enum EEndianness
			{
				Endianness_LittleEndian,
				Endianness_BigEndian,
			};

			typedef struct
			{
				uint64_t m_ui64Identifier;
				uint64_t m_ui64StartIndex;
				uint64_t m_ui64Duration;
				std::string m_sName;
			} SStimulation;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::CString*> ip_sFilename;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64EpochDuration;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SeekTime;
			OpenViBE::Kernel::TParameterHandler<bool> ip_bConvertStimuli;

			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64CurrentStartTime;
			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64CurrentEndTime;
			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64SamplingRate;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pSignalMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pStimulations;

			OpenViBE::CString m_sFilename;

			uint32_t m_ui32BinaryFormat;
			uint32_t m_ui32Endianness;
			uint32_t m_ui32ChannelCount;
			uint64_t m_ui64StartSampleIndex;
			uint64_t m_ui64EndSampleIndex;
			uint64_t m_ui64SampleCountPerBuffer;

			uint8_t* m_pBuffer;
			std::vector<double> m_vChannelScale;
			std::vector<SStimulation> m_vStimulation;

			std::ifstream m_oHeaderFile;
			std::ifstream m_oDataFile;
			std::ifstream m_oMarkerFile;
		};

		class CAlgorithmBrainampFileReaderDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Brainamp file reader"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Reads input having the BrainAmp file format"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/Brainamp"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_BrainampFileReader; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmBrainampFileReader; }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_BrainampFileReader_InputParameterId_Filename, "Filename", OpenViBE::Kernel::ParameterType_String);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_BrainampFileReader_InputParameterId_EpochDuration, "Epoch duration", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_BrainampFileReader_InputParameterId_SeekTime, "Seek time", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_BrainampFileReader_InputParameterId_ConvertStimuli, "Convert stimuli", OpenViBE::Kernel::ParameterType_Boolean);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_CurrentStartTime, "Current start time", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_CurrentEndTime, "Current end time", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_SamplingRate, "Sampling rate", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_SignalMatrix, "Signal samples", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_BrainampFileReader_OutputParameterId_Stimulations, "Stimulations", OpenViBE::Kernel::ParameterType_StimulationSet);
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_BrainampFileReader_InputTriggerId_Open, "Open");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_BrainampFileReader_InputTriggerId_Seek, "Seek");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_BrainampFileReader_InputTriggerId_Next, "Next");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_BrainampFileReader_InputTriggerId_Close, "Close");
				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_BrainampFileReader_OutputTriggerId_Error, "Error");
				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_BrainampFileReader_OutputTriggerId_DataProduced, "Data produced");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_BrainampFileReaderDesc);
		};
	};
};

#endif // __OpenViBEPlugins_Algorithm_BrainampFileReader_H__
