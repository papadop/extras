#ifndef __OpenViBEPlugins_FileIO_GDFHelpers_H__
#define __OpenViBEPlugins_FileIO_GDFHelpers_H__

#include <openvibe/ov_all.h>

#include <system/ovCMemory.h>

#include <fstream>
#include <vector>

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		/**
		 * Useful classes for GDF file format handling
		*/
		class GDF
		{
		public:

			class CFixedGDFHeader
			{
			public:
				virtual ~CFixedGDFHeader() {}

				virtual bool read(std::ifstream& oFile) = 0;

				virtual bool save(std::ofstream& oFile) = 0;
				virtual bool update(std::ofstream& oFile) =0;

				virtual uint64_t getSubjectIdentifier();
				virtual std::string getSubjectName();
				virtual uint64_t getSubjectSex();
				virtual uint64_t getSubjectAge();
				virtual uint64_t getExperimentIdentifier();
				virtual std::string getExperimentDate();
				virtual uint64_t getLaboratoryIdentifier();
				virtual uint64_t getTechnicianIdentifier();
				virtual std::string getLaboratoryName();
				virtual std::string getTechnicianName();

				virtual double getDataRecordDuration() = 0;
				virtual uint64_t getNumberOfDataRecords() = 0;
				virtual uint64_t getChannelCount() = 0;
			};

			/**
		 	 * An helper class to manipulate GDF1 fixed-size headers
			 */
			class CFixedGDF1Header : public CFixedGDFHeader
			{
			public:
				CFixedGDF1Header();

				virtual ~CFixedGDF1Header() {}

				/**
				 * Reads a GDF1 fixed Header from a file
				 * \param oFile The input file.
				 * \return true if the operation was successful
				 */
				bool read(std::ifstream& oFile) override;

				/**
				 * Saves a GDF1 fixed Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				bool save(std::ofstream& oFile) override;

				/**
				 * Updates the number of data records field in
				 * a GDF1 fixed Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				bool update(std::ofstream& oFile) override;

				std::string getSubjectName() override;
				uint64_t getLaboratoryIdentifier() override;
				uint64_t getTechnicianIdentifier() override;

				double getDataRecordDuration() override;
				uint64_t getNumberOfDataRecords() override;
				uint64_t getChannelCount() override;

				char m_sVersionId[8];
				char m_sPatientId[80];
				char m_sRecordingId[80];
				char m_sStartDateAndTimeOfRecording[16];
				int64_t m_i64NumberOfBytesInHeaderRecord;
				uint64_t m_ui64EquipmentProviderId;
				uint64_t m_ui64LaboratoryId;
				uint64_t m_ui64TechnicianId;
				char m_sReservedSerialNumber[20];
				int64_t m_i64NumberOfDataRecords;
				uint32_t m_ui32DurationOfADataRecordNumerator;
				uint32_t m_ui32DurationOfADataRecordDenominator;
				uint32_t m_ui32NumberOfSignals;
			};

			/**
			 * An helper class to manipulate GDF2 fixed-size headers
			 */
			class CFixedGDF2Header : public CFixedGDFHeader
			{
			public:

				CFixedGDF2Header();

				virtual ~CFixedGDF2Header() {}

				/**
				* Reads a GDF2 fixed Header from a file
				* \param oFile The input file.
				* \return true if the operation was successful
				 */
				bool read(std::ifstream& oFile) override;

				/**
				 * Saves a GDF2 fixed Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				bool save(std::ofstream& oFile) override;

				/**
				 * Updates the number of data records field in
				 * a GDF2 fixed Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				bool update(std::ofstream& oFile) override { return true; }

				std::string getExperimentDate() override;
				std::string getSubjectName() override;
				uint64_t getSubjectSex() override;
				uint64_t getSubjectAge() override;

				double getDataRecordDuration() override;
				uint64_t getNumberOfDataRecords() override;
				uint64_t getChannelCount() override;

				char m_sVersionId[8];
				char m_sPatientId[66];

				uint8_t m_ui8Reserved[10];
				uint8_t m_ui8HealthInformation; //smoking...
				uint8_t m_ui8Weight;
				uint8_t m_ui8Height;
				uint8_t m_ui8SubjectInformation;//gender...

				char m_sRecordingId[64];
				uint32_t m_ui32RecordingLocation[4];
				uint32_t m_ui32StartDateAndTimeOfRecording[2];
				uint32_t m_ui32Birthday[2];
				uint16_t m_ui16NumberOfBlocksInHeader;
				uint8_t m_ui8Reserved2[6];
				uint64_t m_ui64EquipmentProviderId;
				uint8_t m_ui8IPAdress[6];
				uint16_t m_ui16HeadSize[3];
				float m_f32PositionReferenceElectrode[3];
				float m_f32GroundElectrode[3];
				int64_t m_i64NumberOfDataRecords;
				uint32_t m_ui32DurationOfADataRecordNumerator;
				uint32_t m_ui32DurationOfADataRecordDenominator;
				uint16_t m_ui16NumberOfSignals;
				uint16_t m_ui16Reserved3;
			};


			/**
			 * An helper class to manipulate GDF2.51 fixed-size headers
			 */
			class CFixedGDF251Header : public CFixedGDFHeader
			{
			public:

				CFixedGDF251Header();

				virtual ~CFixedGDF251Header() {}

				/**
				* Reads a GDF2 fixed Header from a file
				* \param oFile The input file.
				* \return true if the operation was successful
				 */
				bool read(std::ifstream& oFile) override;

				/**
				 * Saves a GDF2 fixed Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				bool save(std::ofstream& oFile) override;

				/**
				 * Updates the number of data records field in
				 * a GDF2 fixed Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				bool update(std::ofstream& oFile) override { return true; }

				std::string getExperimentDate() override;
				std::string getSubjectName() override;
				uint64_t getSubjectSex() override;
				uint64_t getSubjectAge() override;

				double getDataRecordDuration() override;
				uint64_t getNumberOfDataRecords() override;
				uint64_t getChannelCount() override;

				struct gdfFixedHeader1
				{
					char m_sVersionId[8];
					char m_sPatientId[66];

					uint8_t m_ui8Reserved[10];
					uint8_t m_ui8HealthInformation; //smoking...
					uint8_t m_ui8Weight;
					uint8_t m_ui8Height;
					uint8_t m_ui8SubjectInformation;//gender...

					char m_sRecordingId[64];
					uint32_t m_ui32RecordingLocation[4];
					uint32_t m_ui32StartDateAndTimeOfRecording[2];
					uint32_t m_ui32Birthday[2];
					uint16_t m_ui16HeaderLength;
					char m_sPatientClassification[6];
					uint64_t m_ui64EquipmentProviderId;
					uint8_t m_ui8Reserved1[6];
					uint16_t m_ui16HeadSize[3];
					float m_f32PositionReferenceElectrode[3];
					float m_f32GroundElectrode[3];
					int64_t m_i64NumberOfDataRecords;
					double m_f64Duration;			// Not double in the 2.51 spec document at the time of writing this, but seems to be so in practice?
					uint16_t m_ui16NumberOfSignals;
					uint16_t m_ui16Reserved2;
				};

				gdfFixedHeader1 m_oHeader1;
			};

			/**
			* Base class for GDF file's variable headers
			*/
			class CVariableGDFHeader
			{
			public:
				virtual ~CVariableGDFHeader() {};
				virtual bool save(std::ofstream& oFile) = 0;
			};

			/**
			 * GDF1 variable header class
			 */
			class CVariableGDF1Header : public CVariableGDFHeader
			{
				//! Stores information for one channel
				class CVariableGDF1HeaderPerChannel
				{
				public:

					CVariableGDF1HeaderPerChannel();

					char m_sLabel[16];
					char m_sTranducerType[80];
					char m_sPhysicalDimension[8];
					double m_f64PhysicalMinimum;
					double m_f64PhysicalMaximum;
					int64_t m_i64DigitalMinimum;
					int64_t m_i64DigitalMaximum;
					char m_sPreFiltering[80];
					uint32_t m_ui32NumberOfSamplesInEachRecord;
					uint32_t m_ui32ChannelType;
					char m_sReserved[32];
				};

			public:

				virtual ~CVariableGDF1Header() {}

				/**
				* Saves a GDF1 variable Header in a file
				* \param oFile The output file.
				* \return true if the operation was successful
				 */
				bool save(std::ofstream& oFile) override;

				/**
				 * Updates the Physical/digital Min/max fields in
				 * a GDF1 variable Header in a file
				 * \param oFile The output file.
				 * \return true if the operation was successful
				 */
				virtual bool update(std::ofstream& oFile);

				/**
				 * Sets the number of channels in the file.
				 * \param ui32ChannelCount Number of channels.
				 */
				virtual void setChannelCount(uint32_t ui32ChannelCount);
				virtual CVariableGDF1HeaderPerChannel& operator[](uint32_t ui32Channel);

				std::vector<CVariableGDF1HeaderPerChannel> m_vVariableHeader;
			};


			class CGDFEvent
			{
			public:
				uint32_t m_ui32Position;
				uint16_t m_ui16Type;
			};

			enum ChannelType
			{
				ChannelType_int8_t = 1,
				ChannelType_uint8_t = 2,
				ChannelType_int16_t = 3,
				ChannelType_uint16_t = 4,
				ChannelType_int32_t = 5,
				ChannelType_uint32_t = 6,
				ChannelType_int64_t = 7,
				ChannelType_uint64_t = 8,
				ChannelType_float = 16,
				ChannelType_double = 17,
				ChannelType_float128 = 18,
				ChannelType_int24 = 279,
				ChannelType_uint24 = 535
			};

			/**
			 * Gets the data size in bytes of the GDF data type
			 * \param ui32ChannelType The GDF type
			 * \return The size in bytes of this GDF type's data
			 */
			static uint16_t GDFDataSize(uint32_t ui32ChannelType);
		};
	};
};

#endif // __OpenViBEPlugins_FileIO_GDFHelpers_H__
