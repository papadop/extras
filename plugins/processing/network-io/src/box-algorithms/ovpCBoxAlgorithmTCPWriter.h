#ifndef __OpenViBEPlugins_BoxAlgorithm_TCPWriter_H__
#define __OpenViBEPlugins_BoxAlgorithm_TCPWriter_H__

#ifdef TARGET_HAS_Boost

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <ctime>
#include <iostream>
#include <string>
#include <boost/bind.hpp>
#include <boost/asio.hpp>

// The unique identifiers for the box and its descriptor.
#define OVP_ClassId_BoxAlgorithm_TCPWriter OpenViBE::CIdentifier(0x02F24947, 0x17FA0477)
#define OVP_ClassId_BoxAlgorithm_TCPWriterDesc OpenViBE::CIdentifier(0x3C32489D, 0x46F565D3)

#define OVP_TypeID_TCPWriter_OutputStyle     OpenViBE::CIdentifier(0x6D7E53DD, 0x6A0A4753)
#define OVP_TypeID_TCPWriter_RawOutputStyle  OpenViBE::CIdentifier(0x77D3E238, 0xB954EC48)

enum { TCPWRITER_RAW, TCPWRITER_HEX, TCPWRITER_STRING }; // stimulation output types

namespace OpenViBEPlugins
{
	namespace NetworkIO
	{
		/**
		 * \class CBoxAlgorithmTCPWriter
		 * \author Jussi T. Lindgren (Inria)
		 * \date Wed Sep 11 12:55:22 2013
		 * \brief The class CBoxAlgorithmTCPWriter describes the box TCP Writer.
		 *
		 */
		class CBoxAlgorithmTCPWriter : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
				
			//Here is the different process callbacks possible
			// - On clock ticks :
			//virtual bool processClock(OpenViBE::CMessageClock& rMessageClock);
			// - On new input received (the most common behaviour for signal processing) :
			bool processInput(uint32_t inputIndex) override;
			
			// If you want to use processClock, you must provide the clock frequency.
			//virtual OpenViBE::uint64 getClockFrequency();

			bool process() override;

			// As we do with any class in openvibe, we use the macro below 
			// to associate this box to an unique identifier. 
			// The inheritance information is also made available, 
			// as we provide the superclass OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_TCPWriter);

		protected:

			bool sendToClients(const void* pBuffer, size_t ui32BufferLength);

			// Stream decoder
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmTCPWriter> m_oStimulationDecoder;
			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmTCPWriter> m_oMatrixDecoder;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmTCPWriter> m_oSignalDecoder;
			OpenViBEToolkit::TDecoder<CBoxAlgorithmTCPWriter>* m_pActiveDecoder;

			boost::asio::io_service m_oIOService;
			boost::asio::ip::tcp::acceptor* m_pAcceptor;
			std::vector<boost::asio::ip::tcp::socket*> m_vSockets;

			uint64_t m_ui64OutputStyle;

			OpenViBE::CIdentifier m_oInputType;

			// Data written as global output header, 8*4 = 32 bytes. Padding allows dumb readers to step with float64 (==8 bytes).
			uint32_t m_ui32RawVersion;					// in network byte order, version of the raw stream
			uint32_t m_ui32Endianness;					// in network byte order, 0==unknown, 1==little, 2==big, 3==pdp
			uint32_t m_ui32Frequency;					// this and the rest are in host byte order
			uint32_t m_ui32NumberOfChannels;
			uint32_t m_ui32NumberOfSamplesPerChunk;
			uint32_t m_ui32Reserved0;
			uint32_t m_ui32Reserved1;
			uint32_t m_ui32Reserved2;

			void startAccept();
			void handleAccept(const boost::system::error_code& ec, boost::asio::ip::tcp::socket* pSocket);
		};

		class CBoxAlgorithmTCPWriterListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:
			CBoxAlgorithmTCPWriterListener(): m_oLastType(OV_UndefinedIdentifier) { }

			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				OpenViBE::CIdentifier l_oNewInputType;
				rBox.getInputType(ui32Index, l_oNewInputType);
				// Set the right enumeration according to the type if we actualy change it
				// TODO find a way to init m_oLastType with the right value
				if (m_oLastType != l_oNewInputType)
				{
					if (l_oNewInputType != OV_TypeId_Stimulations) { rBox.setSettingType(1, OVP_TypeID_TCPWriter_RawOutputStyle); }
					else { rBox.setSettingType(1, OVP_TypeID_TCPWriter_OutputStyle); }
					rBox.setSettingValue(1, "Raw");
					m_oLastType = l_oNewInputType;
				}
				return true;
			}

		private:
			OpenViBE::CIdentifier m_oLastType;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		/**
		 * \class CBoxAlgorithmTCPWriterDesc
		 * \author Jussi T. Lindgren (Inria)
		 * \date Wed Sep 11 12:55:22 2013
		 * \brief Descriptor of the box TCP Writer.
		 *
		 */
		class CBoxAlgorithmTCPWriterDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("TCP Writer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Send input stream out via a TCP socket"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("\n"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_TCPWriter; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmTCPWriter; }

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmTCPWriterListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input 1",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Port",OV_TypeId_Integer, "5678");
				rBoxAlgorithmPrototype.addSetting("Output format", OVP_TypeID_TCPWriter_RawOutputStyle, "Raw");

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);

				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_TCPWriterDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_TCPWriter_H__

#endif // TARGET_HAS_Boost
