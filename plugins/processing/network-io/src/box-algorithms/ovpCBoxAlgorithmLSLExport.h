#ifndef __OpenViBEPlugins_BoxAlgorithm_LSLExport_H__
#define __OpenViBEPlugins_BoxAlgorithm_LSLExport_H__

#ifdef TARGET_HAS_ThirdPartyLSL

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <lsl_cpp.h>


#include <ctime>
#include <iostream>
#include <string>

// The unique identifiers for the box and its descriptor.
#define OVP_ClassId_BoxAlgorithm_LSLExport     OpenViBE::CIdentifier(0x6F3467FF, 0x52794DA6)
#define OVP_ClassId_BoxAlgorithm_LSLExportDesc OpenViBE::CIdentifier(0x40C03C3F, 0x034A19C2)

namespace OpenViBEPlugins
{
	namespace NetworkIO
	{
		/**
		 * \class CBoxAlgorithmLSLExport
		 * \author Jussi T. Lindgren (Inria)
		 * \date Fri Jan 30 09:55:22 2015
		 * \brief The class CBoxAlgorithmLSLExport describes the box LSL Export.
		 *
		 */
		class CBoxAlgorithmLSLExport : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
				
			//Here is the different process callbacks possible
			// - On clock ticks :
			//virtual OpenViBE::boolean processClock(OpenViBE::CMessageClock& rMessageClock);
			// - On new input received (the most common behaviour for signal processing) :
			bool processInput(uint32_t inputIndex) override;
			
			// If you want to use processClock, you must provide the clock frequency.
			//virtual OpenViBE::uint64 getClockFrequency();

			bool process() override;

			// As we do with any class in openvibe, we use the macro below 
			// to associate this box to an unique identifier. 
			// The inheritance information is also made available, 
			// as we provide the superclass OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_LSLExport);

		protected:

			// Decoders
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmLSLExport> m_oStimulationDecoder;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmLSLExport> m_oSignalDecoder;

			lsl::stream_outlet* m_pSignalOutlet;
			lsl::stream_outlet* m_pStimulusOutlet;

			float* m_pSingleSampleBuffer;

			OpenViBE::CString m_sSignalStreamName;
			OpenViBE::CString m_sSignalStreamID;
			OpenViBE::CString m_sMarkerStreamName;
			OpenViBE::CString m_sMarkerStreamID;
		};

		class CBoxAlgorithmLSLExportListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		/**
		 * \class CBoxAlgorithmLSLExportDesc
		 * \author Jussi T. Lindgren (Inria)
		 * \date Fri Jan 30 09:55:22 2015
		 * \brief Descriptor of the box LSL Export.
		 *
		 */
		class CBoxAlgorithmLSLExportDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("LSL Export"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Send input stream out via LabStreamingLayer (LSL)"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("\n"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LSLExport; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmLSLExport; }

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmLSLExportListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Input stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Signal stream", OV_TypeId_String, "openvibeSignal");
				rBoxAlgorithmPrototype.addSetting("Marker stream", OV_TypeId_String, "openvibeMarkers");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LSLExportDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_LSLExport_H__

#endif // TARGET_HAS_Boost
