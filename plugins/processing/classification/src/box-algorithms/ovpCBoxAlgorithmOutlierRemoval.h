#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>
#include <map>

#define OVP_ClassId_BoxAlgorithm_OutlierRemovalDesc    OpenViBE::CIdentifier(0x11DA1C24, 0x4C7A74C0)
#define OVP_ClassId_BoxAlgorithm_OutlierRemoval        OpenViBE::CIdentifier(0x09E41B92, 0x4291B612)

namespace OpenViBEPlugins
{
	namespace Classification
	{
		class CBoxAlgorithmOutlierRemoval : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_OutlierRemoval);

		protected:

			typedef struct
			{
				OpenViBE::CMatrix* m_FeatureVectorMatrix;
				uint64_t m_StartTime;
				uint64_t m_EndTime;
			} SFeatureVector;

			bool pruneSet(std::vector<SFeatureVector>& pruned);

			OpenViBEToolkit::TFeatureVectorDecoder<CBoxAlgorithmOutlierRemoval> m_oFeatureVectorDecoder;
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmOutlierRemoval> m_oStimulationDecoder;

			OpenViBEToolkit::TFeatureVectorEncoder<CBoxAlgorithmOutlierRemoval> m_oFeatureVectorEncoder;
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmOutlierRemoval> m_oStimulationEncoder;

			std::vector<SFeatureVector> m_vDataset;

			double m_lowerQuantile = 0;
			double m_upperQuantile = 0;
			uint64_t m_trigger = 0;
			uint64_t m_triggerTime = 0;
		};

		class CBoxAlgorithmOutlierRemovalDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Outlier removal"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Discards feature vectors with extremal values"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Simple outlier removal based on quantile estimation"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Classification"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_OutlierRemoval; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmOutlierRemoval; }
			OpenViBE::CString getStockItemName() const override { return "gtk-cut"; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Input features", OV_TypeId_FeatureVector);

				rBoxAlgorithmPrototype.addOutput("Output stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Output features", OV_TypeId_FeatureVector);

				rBoxAlgorithmPrototype.addSetting("Lower quantile", OV_TypeId_Float, "0.01");
				rBoxAlgorithmPrototype.addSetting("Upper quantile", OV_TypeId_Float, "0.99");
				rBoxAlgorithmPrototype.addSetting("Start trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Train");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_OutlierRemovalDesc);
		};
	};
};
