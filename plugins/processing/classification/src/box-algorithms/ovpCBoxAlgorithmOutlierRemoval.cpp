#include "ovpCBoxAlgorithmOutlierRemoval.h"

#include <openvibe/ovITimeArithmetics.h>

#include <algorithm>
#include <iterator>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace Classification;

using namespace std;

bool CBoxAlgorithmOutlierRemoval::initialize()
{
	m_oStimulationDecoder.initialize(*this, 0);
	m_oFeatureVectorDecoder.initialize(*this, 1);

	m_oStimulationEncoder.initialize(*this, 0);
	m_oFeatureVectorEncoder.initialize(*this, 1);

	// get the quantile parameters
	m_lowerQuantile = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_upperQuantile = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_trigger = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);

	m_lowerQuantile = std::min<double>(std::max<double>(m_lowerQuantile, 0.0), 1.0);
	m_upperQuantile = std::min<double>(std::max<double>(m_upperQuantile, 0.0), 1.0);

	m_triggerTime = -1LL;

	return true;
}

bool CBoxAlgorithmOutlierRemoval::uninitialize()
{
	m_oFeatureVectorEncoder.uninitialize();
	m_oStimulationEncoder.uninitialize();

	m_oFeatureVectorDecoder.uninitialize();
	m_oStimulationDecoder.uninitialize();

	for (auto& data : m_vDataset)
	{
		delete data.m_FeatureVectorMatrix;
		data.m_FeatureVectorMatrix = nullptr;
	}
	m_vDataset.clear();

	return true;
}

bool CBoxAlgorithmOutlierRemoval::processInput(uint32_t /*inputIndex*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool PairLess(std::pair<double, uint32_t> a, std::pair<double, uint32_t> b)
{
	return a.first < b.first;
};

bool CBoxAlgorithmOutlierRemoval::pruneSet(std::vector<SFeatureVector>& pruned)
{
	if (m_vDataset.empty()) { return true; }

	const size_t datasetSize = m_vDataset.size(),
				 featureDims = m_vDataset[0].m_FeatureVectorMatrix->getDimensionSize(0),
				 lowerIndex = size_t(m_lowerQuantile * datasetSize),
				 upperIndex = size_t(m_upperQuantile * datasetSize);

	this->getLogManager() << LogLevel_Trace << "Examined dataset is [" << datasetSize << "x" << featureDims << "].\n";

	std::vector<size_t> keptIndexes;
	keptIndexes.resize(datasetSize);
	for (size_t i = 0; i < datasetSize; i++) { keptIndexes[i] = i; }

	std::vector<std::pair<double, size_t>> featureValues;
	featureValues.resize(datasetSize);

	for (size_t f = 0; f < featureDims; f++)
	{
		for (size_t i = 0; i < datasetSize; i++)
		{
			featureValues[i] = std::pair<double, uint32_t>(m_vDataset[i].m_FeatureVectorMatrix->getBuffer()[f], i);
		}

		std::sort(featureValues.begin(), featureValues.end(), PairLess);

		std::vector<size_t> newIndexes;
		newIndexes.resize(upperIndex - lowerIndex);
		for (size_t j = lowerIndex, cnt = 0; j < upperIndex; j++, cnt++)
		{
			newIndexes[cnt] = featureValues[j].second;
		}

		this->getLogManager() << LogLevel_Trace << "For feature " << (f + 1) << ", the retained range is [" << featureValues[lowerIndex].first
			<< ", " << featureValues[upperIndex - 1].first << "]\n";

		std::sort(newIndexes.begin(), newIndexes.end());

		std::vector<size_t> l_vIntersection;
		std::set_intersection(newIndexes.begin(), newIndexes.end(), keptIndexes.begin(), keptIndexes.end(), std::back_inserter(l_vIntersection));

		keptIndexes = l_vIntersection;

		this->getLogManager() << LogLevel_Debug << "After analyzing feat " << uint32_t(f) << ", kept " << static_cast<int32_t>(keptIndexes.size()) << " examples.\n";
	}

	this->getLogManager() << LogLevel_Trace << "Kept " << uint64_t(keptIndexes.size())
		<< " examples in total (" << (100.0 * keptIndexes.size() / double(m_vDataset.size()))
		<< "% of " << uint64_t(m_vDataset.size()) << ")\n";

	pruned.clear();
	for (unsigned int idx : keptIndexes) { pruned.push_back(m_vDataset[idx]); }

	return true;
}

bool CBoxAlgorithmOutlierRemoval::process()
{
	// IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO& boxContext = this->getDynamicBoxContext();

	// Stimulations
	for (uint32_t i = 0; i < boxContext.getInputChunkCount(0); i++)
	{
		m_oStimulationDecoder.decode(i);
		if (m_oStimulationDecoder.isHeaderReceived())
		{
			m_oStimulationEncoder.encodeHeader();

			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}
		if (m_oStimulationDecoder.isBufferReceived())
		{
			const IStimulationSet* stimSet = m_oStimulationDecoder.getOutputStimulationSet();
			for (uint32_t s = 0; s < stimSet->getStimulationCount(); s++)
			{
				if (stimSet->getStimulationIdentifier(s) == m_trigger)
				{
					std::vector<SFeatureVector> pruned;
					if (!pruneSet(pruned)) { return false; }

					// encode
					for (auto& feature : pruned)
					{
						OpenViBEToolkit::Tools::Matrix::copy(*m_oFeatureVectorEncoder.getInputMatrix(), *feature.m_FeatureVectorMatrix);
						m_oFeatureVectorEncoder.encodeBuffer();
						boxContext.markOutputAsReadyToSend(1, feature.m_StartTime, feature.m_EndTime);
					}

					const uint64_t halfSecondHack = ITimeArithmetics::secondsToTime(0.5);
					m_triggerTime = stimSet->getStimulationDate(s) + halfSecondHack;
				}
			}

			m_oStimulationEncoder.getInputStimulationSet()->clear();

			if (m_triggerTime >= boxContext.getInputChunkStartTime(0, i) && m_triggerTime < boxContext.getInputChunkEndTime(0, i))
			{
				m_oStimulationEncoder.getInputStimulationSet()->appendStimulation(m_trigger, m_triggerTime, 0);
				m_triggerTime = -1LL;
			}

			m_oStimulationEncoder.encodeBuffer();

			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}
		if (m_oStimulationDecoder.isEndReceived())
		{
			m_oStimulationEncoder.encodeEnd();

			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}
	}

	// Feature vectors

	for (uint32_t i = 0; i < boxContext.getInputChunkCount(1); i++)
	{
		m_oFeatureVectorDecoder.decode(i);
		if (m_oFeatureVectorDecoder.isHeaderReceived())
		{
			OpenViBEToolkit::Tools::Matrix::copyDescription(*m_oFeatureVectorEncoder.getInputMatrix(), *m_oFeatureVectorDecoder.getOutputMatrix());

			m_oFeatureVectorEncoder.encodeHeader();

			boxContext.markOutputAsReadyToSend(1, boxContext.getInputChunkStartTime(1, i), boxContext.getInputChunkEndTime(1, i));
		}

		// pad feature to set
		if (m_oFeatureVectorDecoder.isBufferReceived())
		{
			const IMatrix* pFeatureVectorMatrix = m_oFeatureVectorDecoder.getOutputMatrix();

			SFeatureVector featureVector;
			featureVector.m_FeatureVectorMatrix = new CMatrix();
			featureVector.m_StartTime = boxContext.getInputChunkStartTime(1, i);
			featureVector.m_EndTime = boxContext.getInputChunkEndTime(1, i);

			OpenViBEToolkit::Tools::Matrix::copy(*featureVector.m_FeatureVectorMatrix, *pFeatureVectorMatrix);
			m_vDataset.push_back(featureVector);
		}

		if (m_oFeatureVectorDecoder.isEndReceived())
		{
			m_oFeatureVectorEncoder.encodeEnd();

			boxContext.markOutputAsReadyToSend(1, boxContext.getInputChunkStartTime(1, i), boxContext.getInputChunkEndTime(1, i));
		}
	}

	return true;
}
