#pragma once

#if defined TARGET_HAS_ThirdPartyEIGEN

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define OVP_ClassId_Algorithm_ClassifierMLP                                          OpenViBE::CIdentifier(0xF3FAB4BE, 0xDC401260)
#define OVP_ClassId_Algorithm_ClassifierMLP_DecisionAvailable                        OpenViBE::CIdentifier(0xF3FAB4BE, 0xDC401261)
#define OVP_ClassId_Algorithm_ClassifierMLPDesc                                      OpenViBE::CIdentifier(0xF3FAB4BE, 0xDC401262)

#define OVP_Algorithm_ClassifierMLP_InputParameterId_HiddenNeuronCount               OpenViBE::CIdentifier(0xF3FAB4BE, 0xDC401263)
#define OVP_Algorithm_ClassifierMLP_InputParameterId_Epsilon                         OpenViBE::CIdentifier(0xF3FAB4BE, 0xDC401264)
#define OVP_Algorithm_ClassifierMLP_InputParameterId_Alpha                           OpenViBE::CIdentifier(0xF3FAB4BE, 0xDC401265)

#include <Eigen/Dense>

#include <xml/IXMLNode.h>
#include <vector>

namespace OpenViBEPlugins
{
	namespace Classification
	{
		int32_t MLPClassificationCompare(OpenViBE::IMatrix& firstClassificationValue, OpenViBE::IMatrix& secondClassificationValue);

		class CAlgorithmClassifierMLP : public OpenViBEToolkit::CAlgorithmClassifier
		{
		public:

			bool initialize() override;
			bool uninitialize() override;

			bool train(const OpenViBEToolkit::IFeatureVectorSet& featureVectorSet) override;
			bool classify(const OpenViBEToolkit::IFeatureVector& featureVector, double& classLabel,
						  OpenViBEToolkit::IVector& distance, OpenViBEToolkit::IVector& probability) override;

			XML::IXMLNode* saveConfiguration() override;
			bool loadConfiguration(XML::IXMLNode* configurationNode) override;

			uint32_t getOutputProbabilityVectorLength() override;
			uint32_t getOutputDistanceVectorLength() override;

			_IsDerivedFromClass_Final_(CAlgorithmClassifier, OVP_ClassId_Algorithm_ClassifierMLP)

		protected:

		private:
			//Helpers for load or sotre data in XMLNode
			static void dumpData(XML::IXMLNode* node, Eigen::MatrixXd& matrix);
			static void dumpData(XML::IXMLNode* node, Eigen::VectorXd& vector);
			static void dumpData(XML::IXMLNode* node, int64_t value);
			static void dumpData(XML::IXMLNode* node, double value);

			static void loadData(XML::IXMLNode* node, Eigen::MatrixXd& matrix, size_t rowCount, size_t colCount);
			static void loadData(XML::IXMLNode* node, Eigen::VectorXd& vector);
			static void loadData(XML::IXMLNode* node, int64_t& value);
			static void loadData(XML::IXMLNode* node, double& value);

			std::vector<double> m_oLabelList;

			Eigen::MatrixXd m_oInputWeight;
			Eigen::VectorXd m_oInputBias;

			Eigen::MatrixXd m_oHiddenWeight;
			Eigen::VectorXd m_oHiddenBias;

			double m_min = 0;
			double m_max = 0;
		};

		class CAlgorithmClassifierMLPDesc : public OpenViBEToolkit::CAlgorithmClassifierDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("MLP Classifier"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Serrière"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria / Loria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Multi-layer perceptron algorithm"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_ClassifierMLP; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmClassifierMLP; }

			bool getAlgorithmPrototype(OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				CAlgorithmClassifierDesc::getAlgorithmPrototype(rAlgorithmPrototype);

				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_HiddenNeuronCount, "Number of neurons in hidden layer", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Epsilon, "Learning stop condition", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Alpha, "Learning coefficient", OpenViBE::Kernel::ParameterType_Float);
				return true;
			}

			_IsDerivedFromClass_Final_(CAlgorithmClassifierDesc, OVP_ClassId_Algorithm_ClassifierMLPDesc)
		};
	}
}
#endif // TARGET_HAS_ThirdPartyEIGEN
