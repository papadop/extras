#include "ovpCAlgorithmClassifierSVM.h"
#include "../ovp_defines.h"

#include <sstream>
#include <iostream>
#include <cstring>
#include <string>
#include <cmath>

namespace
{
	const char* const c_sTypeNodeName = "SVM";
	const char* const c_sParamNodeName = "Param";
	const char* const c_sSvmTypeNodeName = "svm_type";
	const char* const c_sKernelTypeNodeName = "kernel_type";
	const char* const c_sDegreeNodeName = "degree";
	const char* const c_sGammaNodeName = "gamma";
	const char* const c_sCoef0NodeName = "coef0";
	const char* const c_sModelNodeName = "Model";
	const char* const c_sNrClassNodeName = "nr_class";
	const char* const c_sTotalSvNodeName = "total_sv";
	const char* const c_sRhoNodeName = "rho";
	const char* const c_sLabelNodeName = "label";
	const char* const c_sProbANodeName = "probA";
	const char* const c_sProbBNodeName = "probB";
	const char* const c_sNrSvNodeName = "nr_sv";
	const char* const c_sSvsNodeName = "SVs";
	const char* const c_sSVNodeName = "SV";
	const char* const c_sCoefNodeName = "coef";
	const char* const c_sValueNodeName = "value";
}

int32_t OpenViBEPlugins::Classification::SVMClassificationCompare(OpenViBE::IMatrix& firstClassificationValue, OpenViBE::IMatrix& secondClassificationValue)
{
	if (ov_float_equal(std::fabs(firstClassificationValue[0]), std::fabs(secondClassificationValue[0]))) { return 0; }
	if (std::fabs(firstClassificationValue[0]) > std::fabs(secondClassificationValue[0])) { return -1; }
	return 1;
}

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace Classification;

using namespace OpenViBEToolkit;

CAlgorithmClassifierSVM::CAlgorithmClassifierSVM() : m_model(nullptr), m_modelWasTrained(false) {}

bool CAlgorithmClassifierSVM::initialize()
{
	TParameterHandler<int64_t> iSVMType(this->getInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMType));
	TParameterHandler<int64_t> iSVMKernelType(this->getInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMKernelType));
	TParameterHandler<int64_t> iDegree(this->getInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMDegree));
	TParameterHandler<double> iGamma(this->getInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMGamma));
	TParameterHandler<double> iCoef0(this->getInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMCoef0));
	TParameterHandler<double> iCost(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCost));
	TParameterHandler<double> iNu(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMNu));
	TParameterHandler<double> iEpsilon(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMEpsilon));
	TParameterHandler<double> iCacheSize(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCacheSize));
	TParameterHandler<double> iEpsilonTolerance(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMTolerance));
	TParameterHandler<bool> iShrinking(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMShrinking));
	//TParameterHandler < bool > iProbabilityEstimate(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMProbabilityEstimate));
	TParameterHandler<CString*> ip_sWeight(this->getInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMweight));
	TParameterHandler<CString*> ip_sWeightLabel(this->getInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMWeightLabel));

	iSVMType = C_SVC;
	iSVMKernelType = LINEAR;
	iDegree = 3;
	iGamma = 0;
	iCoef0 = 0;
	iCost = 1;
	iNu = 0.5;
	iEpsilon = 0.1;
	iCacheSize = 100;
	iEpsilonTolerance = 0.001;
	iShrinking = true;
	//iProbabilityEstimate=true;
	*ip_sWeight = "";
	*ip_sWeightLabel = "";

	TParameterHandler<XML::IXMLNode*> configuration(this->getOutputParameter(OVTK_Algorithm_Classifier_OutputParameterId_Configuration));
	configuration = nullptr;
	m_oProb.y = nullptr;
	m_oProb.x = nullptr;

	m_oParam.weight = nullptr;
	m_oParam.weight_label = nullptr;

	m_model = nullptr;
	m_modelWasTrained = false;

	return CAlgorithmClassifier::initialize();
}

bool CAlgorithmClassifierSVM::uninitialize()
{
	if (m_oProb.x != nullptr && m_oProb.y != nullptr)
	{
		for (int i = 0; i < m_oProb.l; ++i)
		{
			delete[] m_oProb.x[i];
		}
		delete[] m_oProb.y;
		delete[] m_oProb.x;
		m_oProb.y = nullptr;
		m_oProb.x = nullptr;
	}

	if (m_oParam.weight != nullptr)
	{
		delete[] m_oParam.weight;
		m_oParam.weight = nullptr;
	}

	if (m_oParam.weight_label != nullptr)
	{
		delete[] m_oParam.weight_label;
		m_oParam.weight_label = nullptr;
	}

	deleteModel(m_model, !m_modelWasTrained);
	m_model = nullptr;
	m_modelWasTrained = false;

	return CAlgorithmClassifier::uninitialize();
}

void CAlgorithmClassifierSVM::deleteModel(svm_model* model, bool freeSupportVectors)
{
	if (model != nullptr)
	{
		delete[] model->rho;
		delete[] model->probA;
		delete[] model->probB;
		delete[] model->label;
		delete[] model->nSV;

		for (int i = 0; i < model->nr_class - 1; ++i) { delete[] model->sv_coef[i]; }
		delete[] model->sv_coef;

		// We need the following depending on how the model was allocated. If we got it from svm_train,
		// the support vectors are pointers to the problem structure which is freed elsewhere. 
		// If we loaded the model from disk, we allocated the vectors separately.
		if (freeSupportVectors)
		{
			for (int i = 0; i < model->l; ++i) { delete[] model->SV[i]; }
		}
		delete[] model->SV;

		delete model;
		model = nullptr;
	}
}

void CAlgorithmClassifierSVM::setParameter()
{
	this->initializeExtraParameterMechanism();

	m_oParam.svm_type = int(this->getEnumerationParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMType, OVP_TypeId_SVMType));
	m_oParam.kernel_type = int(this->getEnumerationParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMKernelType, OVP_TypeId_SVMKernelType));
	m_oParam.degree = int(this->getInt64Parameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMDegree));
	m_oParam.gamma = this->getFloat64Parameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMGamma);
	m_oParam.coef0 = this->getFloat64Parameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMCoef0);
	m_oParam.C = this->getFloat64Parameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCost);
	m_oParam.nu = this->getFloat64Parameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMNu);
	m_oParam.p = this->getFloat64Parameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMEpsilon);
	m_oParam.cache_size = this->getFloat64Parameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCacheSize);
	m_oParam.eps = this->getFloat64Parameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMTolerance);
	m_oParam.shrinking = static_cast<int>(this->getBooleanParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMShrinking));
	//	m_oParam.probability        = this->getBooleanParameter(           OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMShrinking);
	m_oParam.probability = 1;
	const CString paramWeight = *this->getCStringParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMweight);
	const CString paramWeightLabel = *this->getCStringParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMWeightLabel);

	this->uninitializeExtraParameterMechanism();

	std::vector<double> l_vWeight;
	std::stringstream l_oStreamString(static_cast<const char*>(paramWeight));
	double l_f64CurrentValue;
	while (l_oStreamString >> l_f64CurrentValue)
	{
		l_vWeight.push_back(l_f64CurrentValue);
	}
	m_oParam.nr_weight = l_vWeight.size();
	double* weight = new double[l_vWeight.size()];
	for (uint32_t i = 0; i < l_vWeight.size(); ++i)
	{
		weight[i] = l_vWeight[i];
	}
	m_oParam.weight = weight;//nullptr;

	std::vector<int64_t> l_vWeightLabel;
	std::stringstream l_oStreamStringLabel(static_cast<const char*>(paramWeightLabel));
	int64_t l_i64CurrentValue;
	while (l_oStreamStringLabel >> l_i64CurrentValue)
	{
		l_vWeightLabel.push_back(l_i64CurrentValue);
	}

	//the number of weight label need to be equal to the number of weight
	while (l_vWeightLabel.size() < l_vWeight.size())
	{
		l_vWeightLabel.push_back(l_vWeightLabel.size() + 1);
	}

	int* weightLabel = new int[l_vWeight.size()];
	for (size_t i = 0; i < l_vWeight.size(); ++i)
	{
		weightLabel[i] = int(l_vWeightLabel[i]);
	}
	m_oParam.weight_label = weightLabel;//nullptr;
}

bool CAlgorithmClassifierSVM::train(const IFeatureVectorSet& featureVectorSet)
{
	if (m_oProb.x != nullptr && m_oProb.y != nullptr)
	{
		for (int i = 0; i < m_oProb.l; ++i)
		{
			delete[] m_oProb.x[i];
		}
		delete[] m_oProb.y;
		delete[] m_oProb.x;
		m_oProb.y = nullptr;
		m_oProb.x = nullptr;
	}
	// default Param values
	//std::cout<<"param config"<<std::endl;
	this->setParameter();
	this->getLogManager() << LogLevel_Trace << paramToString(&m_oParam);

	//configure m_oProb
	//std::cout<<"prob config"<<std::endl;
	m_oProb.l = featureVectorSet.getFeatureVectorCount();
	m_numberOfFeatures = featureVectorSet[0].getSize();

	m_oProb.y = new double[m_oProb.l];
	m_oProb.x = new svm_node*[m_oProb.l];

	//std::cout<< "number vector:"<<l_oProb.l<<" size of vector:"<<m_numberOfFeatures<<std::endl;

	for (int i = 0; i < m_oProb.l; ++i)
	{
		m_oProb.x[i] = new svm_node[m_numberOfFeatures + 1];
		m_oProb.y[i] = featureVectorSet[i].getLabel();
		for (size_t j = 0; j < m_numberOfFeatures; ++j)
		{
			m_oProb.x[i][j].index = j + 1;
			m_oProb.x[i][j].value = featureVectorSet[i].getBuffer()[j];
		}
		m_oProb.x[i][m_numberOfFeatures].index = -1;
	}

	// Gamma of zero is interpreted as a request for automatic selection
	if (m_oParam.gamma == 0)
	{
		m_oParam.gamma = 1.0 / (m_numberOfFeatures > 0 ? m_numberOfFeatures : 1.0);
	}

	if (m_oParam.kernel_type == PRECOMPUTED)
	{
		for (int i = 0; i < m_oProb.l; ++i)
		{
			if (m_oProb.x[i][0].index != 0)
			{
				this->getLogManager() << LogLevel_Error << "Wrong input format: first column must be 0:sample_serial_number\n";
				return false;
			}
			if (m_oProb.x[i][0].value <= 0 || m_oProb.x[i][0].value > m_numberOfFeatures)
			{
				this->getLogManager() << LogLevel_Error << "Wrong input format: sample_serial_number out of range\n";
				return false;
			}
		}
	}

	this->getLogManager() << LogLevel_Trace << problemToString(&m_oProb);

	//make a model
	//std::cout<<"svm_train"<<std::endl;
	if (m_model != nullptr)
	{
		//std::cout<<"delete model"<<std::endl;
		deleteModel(m_model, !m_modelWasTrained);
		m_model = nullptr;
		m_modelWasTrained = false;
	}
	m_model = svm_train(&m_oProb, &m_oParam);

	if (m_model == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "the training with SVM had failed\n";
		return false;
	}

	m_modelWasTrained = true;

	//std::cout<<"log model"<<std::endl;
	this->getLogManager() << LogLevel_Trace << modelToString();

	return true;
}

bool CAlgorithmClassifierSVM::classify(const IFeatureVector& featureVector, double& classLabel, IVector& distance, IVector& probability)
{
	//std::cout<<"classify"<<std::endl;
	if (m_model == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Classification is impossible with a model equalling nullptr\n";
		return false;
	}
	if (m_model->nr_class == 0 || m_model->rho == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "The model wasn't loaded correctly\n";
		return false;
	}
	if (m_numberOfFeatures != featureVector.getSize())
	{
		this->getLogManager() << LogLevel_Error << "Classifier expected " << m_numberOfFeatures << " features, got " << featureVector.getSize() << "\n";
		return false;
	}
	if (m_model->param.gamma == 0 &&
		(m_model->param.kernel_type == POLY || m_model->param.kernel_type == RBF || m_model->param.kernel_type == SIGMOID))
	{
		m_model->param.gamma = 1.0 / (m_numberOfFeatures > 0 ? m_numberOfFeatures : 1.0);
		this->getLogManager() << LogLevel_Warning << "The SVM model had gamma=0. Setting it to [" << m_model->param.gamma << "].\n";
	}

	//std::cout<<"create X"<<std::endl;
	svm_node* X = new svm_node[featureVector.getSize() + 1];
	//std::cout<<"featureVector.getSize():"<<featureVector.getSize()<<"m_numberOfFeatures"<<m_numberOfFeatures<<std::endl;
	for (unsigned int i = 0; i < featureVector.getSize(); ++i)
	{
		X[i].index = i + 1;
		X[i].value = featureVector.getBuffer()[i];
		//std::cout<< X[i].index << ";"<<X[i].value<<" ";
	}
	X[featureVector.getSize()].index = -1;

	//std::cout<<"create ProbEstimates"<<std::endl;
	double* probEstimates = new double[m_model->nr_class];
	for (int i = 0; i < m_model->nr_class; ++i)
	{
		probEstimates[i] = 0;
	}

	classLabel = svm_predict_probability(m_model, X, probEstimates);

	//std::cout<<classLabel<<std::endl;
	//std::cout<<"probability"<<std::endl;

	//If we are not in these modes, label is nullptr and there is no probability
	if (m_model->param.svm_type == C_SVC || m_model->param.svm_type == NU_SVC)
	{
		probability.setSize(m_model->nr_class);
		this->getLogManager() << LogLevel_Trace << "Label predict: " << classLabel << "\n";

		for (int i = 0; i < m_model->nr_class; ++i)
		{
			this->getLogManager() << LogLevel_Trace << "index:" << i << " label:" << m_model->label[i] << " probability:" << probEstimates[i] << "\n";
			probability[(m_model->label[i])] = probEstimates[i];
		}
	}
	else
	{
		probability.setSize(0);
	}

	//The hyperplane distance is disabled for SVM
	distance.setSize(0);

	//std::cout<<";"<<classLabel<<";"<<distance[0] <<";"<<ProbEstimates[0]<<";"<<ProbEstimates[1]<<std::endl;
	//std::cout<<"Label predict "<<classLabel<< " proba:"<<distance[0]<<std::endl;
	//std::cout<<"end classify"<<std::endl;
	delete[] X;
	delete[] probEstimates;

	return true;
}

XML::IXMLNode* CAlgorithmClassifierSVM::saveConfiguration()
{
	//xml file
	//std::cout<<"model save"<<std::endl;

	std::vector<CString> l_vSVCoef;
	std::vector<CString> l_vSVValue;

	//std::cout<<"model save: rho"<<std::endl;
	std::stringstream l_sRho;
	l_sRho << std::scientific << m_model->rho[0];

	for (int i = 1; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
	{
		l_sRho << " " << m_model->rho[i];
	}

	//std::cout<<"model save: sv_coef and SV"<<std::endl;
	for (int i = 0; i < m_model->l; ++i)
	{
		std::stringstream l_sSVCoef;
		std::stringstream l_sSVValue;

		l_sSVCoef << m_model->sv_coef[0][i];
		for (int j = 1; j < m_model->nr_class - 1; ++j)
		{
			l_sSVCoef << " " << m_model->sv_coef[j][i];
		}

		const svm_node* p = m_model->SV[i];

		if (m_model->param.kernel_type == PRECOMPUTED)
		{
			l_sSVValue << "0:" << double(p->value);
		}
		else
		{
			if (p->index != -1)
			{
				l_sSVValue << p->index << ":" << p->value;
				p++;
			}
			while (p->index != -1)
			{
				l_sSVValue << " " << p->index << ":" << p->value;
				p++;
			}
		}
		l_vSVCoef.emplace_back(l_sSVCoef.str().c_str());
		l_vSVValue.emplace_back(l_sSVValue.str().c_str());
	}

	XML::IXMLNode* svmNode = XML::createNode(c_sTypeNodeName);

	//Param node
	XML::IXMLNode* paramNode = XML::createNode(c_sParamNodeName);
	XML::IXMLNode* tempNode = XML::createNode(c_sSvmTypeNodeName);
	tempNode->setPCData(get_svm_type(m_model->param.svm_type));
	paramNode->addChild(tempNode);

	tempNode = XML::createNode(c_sKernelTypeNodeName);
	tempNode->setPCData(get_kernel_type(m_model->param.kernel_type));
	paramNode->addChild(tempNode);

	if (m_model->param.kernel_type == POLY)
	{
		std::stringstream l_sParamDegree;
		l_sParamDegree << m_model->param.degree;

		tempNode = XML::createNode(c_sDegreeNodeName);
		tempNode->setPCData(l_sParamDegree.str().c_str());
		paramNode->addChild(tempNode);
	}
	if (m_model->param.kernel_type == POLY || m_model->param.kernel_type == RBF || m_model->param.kernel_type == SIGMOID)
	{
		std::stringstream l_sParamGamma;
		l_sParamGamma << m_model->param.gamma;

		tempNode = XML::createNode(c_sGammaNodeName);
		tempNode->setPCData(l_sParamGamma.str().c_str());
		paramNode->addChild(tempNode);
	}
	if (m_model->param.kernel_type == POLY || m_model->param.kernel_type == SIGMOID)
	{
		std::stringstream l_sParamCoef0;
		l_sParamCoef0 << m_model->param.coef0;

		tempNode = XML::createNode(c_sCoef0NodeName);
		tempNode->setPCData(l_sParamCoef0.str().c_str());
		paramNode->addChild(tempNode);
	}
	svmNode->addChild(paramNode);
	//End param node

	//Model Node
	XML::IXMLNode* modelNode = XML::createNode(c_sModelNodeName);
	{
		tempNode = XML::createNode(c_sNrClassNodeName);
		std::stringstream l_sModelNrClass;
		l_sModelNrClass << m_model->nr_class;
		tempNode->setPCData(l_sModelNrClass.str().c_str());
		modelNode->addChild(tempNode);

		tempNode = XML::createNode(c_sTotalSvNodeName);
		std::stringstream l_sModelTotalSV;
		l_sModelTotalSV << m_model->l;
		tempNode->setPCData(l_sModelTotalSV.str().c_str());
		modelNode->addChild(tempNode);

		tempNode = XML::createNode(c_sRhoNodeName);
		tempNode->setPCData(l_sRho.str().c_str());
		modelNode->addChild(tempNode);

		if (m_model->label != nullptr)
		{
			std::stringstream l_sLabel;
			l_sLabel << m_model->label[0];
			for (int i = 1; i < m_model->nr_class; ++i)
			{
				l_sLabel << " " << m_model->label[i];
			}

			tempNode = XML::createNode(c_sLabelNodeName);
			tempNode->setPCData(l_sLabel.str().c_str());
			modelNode->addChild(tempNode);
		}
		if (m_model->probA != nullptr)
		{
			std::stringstream l_sProbA;
			l_sProbA << std::scientific << m_model->probA[0];
			for (int i = 1; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
			{
				l_sProbA << " " << m_model->probA[i];
			}

			tempNode = XML::createNode(c_sProbANodeName);
			tempNode->setPCData(l_sProbA.str().c_str());
			modelNode->addChild(tempNode);
		}
		if (m_model->probB != nullptr)
		{
			std::stringstream l_sProbB;
			l_sProbB << std::scientific << m_model->probB[0];
			for (int i = 1; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
			{
				l_sProbB << " " << m_model->probB[i];
			}

			tempNode = XML::createNode(c_sProbBNodeName);
			tempNode->setPCData(l_sProbB.str().c_str());
			modelNode->addChild(tempNode);
		}
		if (m_model->nSV != nullptr)
		{
			std::stringstream l_sNrSV;
			l_sNrSV << m_model->nSV[0];
			for (int i = 1; i < m_model->nr_class; ++i)
			{
				l_sNrSV << " " << m_model->nSV[i];
			}

			tempNode = XML::createNode(c_sNrSvNodeName);
			tempNode->setPCData(l_sNrSV.str().c_str());
			modelNode->addChild(tempNode);
		}

		XML::IXMLNode* svsNode = XML::createNode(c_sSvsNodeName);
		{
			for (int i = 0; i < m_model->l; ++i)
			{
				XML::IXMLNode* svNode = XML::createNode(c_sSVNodeName);
				{
					tempNode = XML::createNode(c_sCoefNodeName);
					tempNode->setPCData(l_vSVCoef[i]);
					svNode->addChild(tempNode);

					tempNode = XML::createNode(c_sValueNodeName);
					tempNode->setPCData(l_vSVValue[i]);
					svNode->addChild(tempNode);
				}
				svsNode->addChild(svNode);
			}
		}
		modelNode->addChild(svsNode);
	}
	svmNode->addChild(modelNode);
	return svmNode;
}

bool CAlgorithmClassifierSVM::loadConfiguration(XML::IXMLNode* configurationNode)
{
	if (m_model != nullptr)
	{
		//std::cout<<"delete m_model load config"<<std::endl;
		deleteModel(m_model, !m_modelWasTrained);
		m_model = nullptr;
		m_modelWasTrained = false;
	}
	//std::cout<<"load config"<<std::endl;
	m_model = new svm_model();
	m_model->rho = nullptr;
	m_model->probA = nullptr;
	m_model->probB = nullptr;
	m_model->label = nullptr;
	m_model->nSV = nullptr;
	m_indexSV = -1;

	loadParamNodeConfiguration(configurationNode->getChildByName(c_sParamNodeName));
	loadModelNodeConfiguration(configurationNode->getChildByName(c_sModelNodeName));

	this->getLogManager() << LogLevel_Trace << modelToString();
	return true;
}

void CAlgorithmClassifierSVM::loadParamNodeConfiguration(XML::IXMLNode* paramNode)
{
	//svm_type
	XML::IXMLNode* tempNode = paramNode->getChildByName(c_sSvmTypeNodeName);
	for (int i = 0; get_svm_type(i) != nullptr; ++i)
	{
		if (strcmp(get_svm_type(i), tempNode->getPCData()) == 0)
		{
			m_model->param.svm_type = i;
		}
	}
	if (get_svm_type(m_model->param.svm_type) == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "load configuration error: bad value for the parameter svm_type\n";
	}

	//kernel_type
	tempNode = paramNode->getChildByName(c_sKernelTypeNodeName);
	for (int i = 0; get_kernel_type(i) != nullptr; ++i)
	{
		if (strcmp(get_kernel_type(i), tempNode->getPCData()) == 0)
		{
			m_model->param.kernel_type = i;
		}
	}
	if (get_kernel_type(m_model->param.kernel_type) == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "load configuration error: bad value for the parameter kernel_type\n";
	}

	//Following parameters aren't required

	//degree
	tempNode = paramNode->getChildByName(c_sDegreeNodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		l_sData >> m_model->param.degree;
	}

	//gamma
	tempNode = paramNode->getChildByName(c_sGammaNodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		l_sData >> m_model->param.gamma;
	}

	//coef0
	tempNode = paramNode->getChildByName(c_sCoef0NodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		l_sData >> m_model->param.coef0;
	}
}

void CAlgorithmClassifierSVM::loadModelNodeConfiguration(XML::IXMLNode* modelNode)
{

	//nr_class
	XML::IXMLNode* tempNode = modelNode->getChildByName(c_sNrClassNodeName);
	std::stringstream l_sModelNrClass(tempNode->getPCData());
	l_sModelNrClass >> m_model->nr_class;
	//total_sv
	tempNode = modelNode->getChildByName(c_sTotalSvNodeName);
	std::stringstream l_sModelTotalSv(tempNode->getPCData());
	l_sModelTotalSv >> m_model->l;
	//rho
	tempNode = modelNode->getChildByName(c_sRhoNodeName);
	std::stringstream l_sModelRho(tempNode->getPCData());
	m_model->rho = new double[m_model->nr_class * (m_model->nr_class - 1) / 2];
	for (int i = 0; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
	{
		l_sModelRho >> m_model->rho[i];
	}

	//label
	tempNode = modelNode->getChildByName(c_sLabelNodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		m_model->label = new int[m_model->nr_class];
		for (int i = 0; i < m_model->nr_class; ++i)
		{
			l_sData >> m_model->label[i];
		}
	}
	//probA
	tempNode = modelNode->getChildByName(c_sProbANodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		m_model->probA = new double[m_model->nr_class * (m_model->nr_class - 1) / 2];
		for (int i = 0; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
		{
			l_sData >> m_model->probA[i];
		}
	}
	//probB
	tempNode = modelNode->getChildByName(c_sProbBNodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		m_model->probB = new double[m_model->nr_class * (m_model->nr_class - 1) / 2];
		for (int i = 0; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
		{
			l_sData >> m_model->probB[i];
		}
	}
	//nr_sv
	tempNode = modelNode->getChildByName(c_sNrSvNodeName);
	if (tempNode != nullptr)
	{
		std::stringstream l_sData(tempNode->getPCData());
		m_model->nSV = new int[m_model->nr_class];
		for (int i = 0; i < m_model->nr_class; ++i)
		{
			l_sData >> m_model->nSV[i];
		}
	}

	loadModelSVsNodeConfiguration(modelNode->getChildByName(c_sSvsNodeName));
}

void CAlgorithmClassifierSVM::loadModelSVsNodeConfiguration(XML::IXMLNode* svsNodeParam)
{
	//Reserve all memory space required
	m_model->sv_coef = new double*[m_model->nr_class - 1];
	for (int i = 0; i < m_model->nr_class - 1; ++i)
	{
		m_model->sv_coef[i] = new double[m_model->l];
	}
	m_model->SV = new svm_node*[m_model->l];

	//Now fill SV
	for (size_t i = 0; i < svsNodeParam->getChildCount(); ++i)
	{
		XML::IXMLNode* tempNode = svsNodeParam->getChild(i);
		std::stringstream l_sCoefData(tempNode->getChildByName(c_sCoefNodeName)->getPCData());
		for (int j = 0; j < m_model->nr_class - 1; ++j)
		{
			l_sCoefData >> m_model->sv_coef[j][i];
		}

		std::stringstream l_sValueData(tempNode->getChildByName(c_sValueNodeName)->getPCData());
		std::vector<int> l_vSVMIndex;
		std::vector<double> l_vSVMValue;
		char l_cSeparateChar;
		while (!l_sValueData.eof())
		{
			int l_iIndex;
			double l_dValue;
			l_sValueData >> l_iIndex;
			l_sValueData >> l_cSeparateChar;
			l_sValueData >> l_dValue;
			l_vSVMIndex.push_back(l_iIndex);
			l_vSVMValue.push_back(l_dValue);
		}

		m_numberOfFeatures = l_vSVMIndex.size();
		m_model->SV[i] = new svm_node[l_vSVMIndex.size() + 1];
		for (size_t j = 0; j < l_vSVMIndex.size(); ++j)
		{
			m_model->SV[i][j].index = l_vSVMIndex[j];
			m_model->SV[i][j].value = l_vSVMValue[j];
		}
		m_model->SV[i][l_vSVMIndex.size()].index = -1;
	}
}

CString CAlgorithmClassifierSVM::paramToString(svm_parameter* param)
{
	std::stringstream l_sParam;
	if (param == nullptr)
	{
		l_sParam << "Param: nullptr\n";
		return CString(l_sParam.str().c_str());
	}
	l_sParam << "Param:\n";
	l_sParam << "\tsvm_type: " << get_svm_type(param->svm_type) << "\n";
	l_sParam << "\tkernel_type: " << get_kernel_type(param->kernel_type) << "\n";
	l_sParam << "\tdegree: " << param->degree << "\n";
	l_sParam << "\tgamma: " << param->gamma << "\n";
	l_sParam << "\tcoef0: " << param->coef0 << "\n";
	l_sParam << "\tnu: " << param->nu << "\n";
	l_sParam << "\tcache_size: " << param->cache_size << "\n";
	l_sParam << "\tC: " << param->C << "\n";
	l_sParam << "\teps: " << param->eps << "\n";
	l_sParam << "\tp: " << param->p << "\n";
	l_sParam << "\tshrinking: " << param->shrinking << "\n";
	l_sParam << "\tprobability: " << param->probability << "\n";
	l_sParam << "\tnr weight: " << param->nr_weight << "\n";
	std::stringstream l_sWeightLabel;
	for (int i = 0; i < param->nr_weight; ++i)
	{
		l_sWeightLabel << param->weight_label[i] << ";";
	}
	l_sParam << "\tweight label: " << l_sWeightLabel.str().c_str() << "\n";
	std::stringstream l_sWeight;
	for (int i = 0; i < param->nr_weight; ++i)
	{
		l_sWeight << param->weight[i] << ";";
	}
	l_sParam << "\tweight: " << l_sWeight.str().c_str() << "\n";
	return CString(l_sParam.str().c_str());
}


CString CAlgorithmClassifierSVM::modelToString()
{
	std::stringstream l_sModel;
	if (m_model == nullptr)
	{
		l_sModel << "Model: nullptr\n";
		return CString(l_sModel.str().c_str());
	}
	l_sModel << paramToString(&m_model->param);
	l_sModel << "Model:" << "\n";
	l_sModel << "\tnr_class: " << m_model->nr_class << "\n";
	l_sModel << "\ttotal_sv: " << m_model->l << "\n";
	l_sModel << "\trho: ";
	if (m_model->rho != nullptr)
	{
		l_sModel << m_model->rho[0];
		for (int i = 1; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
		{
			l_sModel << " " << m_model->rho[i];
		}
	}
	l_sModel << "\n";
	l_sModel << "\tlabel: ";
	if (m_model->label != nullptr)
	{
		l_sModel << m_model->label[0];
		for (int i = 1; i < m_model->nr_class; ++i)
		{
			l_sModel << " " << m_model->label[i];
		}
	}
	l_sModel << "\n";
	l_sModel << "\tprobA: ";
	if (m_model->probA != nullptr)
	{
		l_sModel << m_model->probA[0];
		for (int i = 1; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
		{
			l_sModel << " " << m_model->probA[i];
		}
	}
	l_sModel << "\n";
	l_sModel << "\tprobB: ";
	if (m_model->probB != nullptr)
	{
		l_sModel << m_model->probB[0];
		for (int i = 1; i < m_model->nr_class * (m_model->nr_class - 1) / 2; ++i)
		{
			l_sModel << " " << m_model->probB[i];
		}
	}
	l_sModel << "\n";
	l_sModel << "\tnr_sv: ";
	if (m_model->nSV != nullptr)
	{
		l_sModel << m_model->nSV[0];
		for (int i = 1; i < m_model->nr_class; ++i)
		{
			l_sModel << " " << m_model->nSV[i];
		}
	}
	l_sModel << "\n";

	return CString(l_sModel.str().c_str());
}

CString CAlgorithmClassifierSVM::problemToString(svm_problem* prob)
{
	std::stringstream l_sProb;
	if (prob == nullptr)
	{
		l_sProb << "Problem: nullptr\n";
		return CString(l_sProb.str().c_str());
	}
	l_sProb << "Problem";
	l_sProb << "\ttotal sv: " << prob->l << "\n";
	l_sProb << "\tnb features: " << m_numberOfFeatures << "\n";

	return CString(l_sProb.str().c_str());
}

uint32_t CAlgorithmClassifierSVM::getOutputProbabilityVectorLength()
{
	return 1;
}

uint32_t CAlgorithmClassifierSVM::getOutputDistanceVectorLength()
{
	return 0;
}
