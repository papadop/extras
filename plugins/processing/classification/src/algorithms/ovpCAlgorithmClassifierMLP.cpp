#if defined TARGET_HAS_ThirdPartyEIGEN

#include "ovpCAlgorithmClassifierMLP.h"
#include "../ovp_defines.h"

#include <map>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cmath>

#include <system/ovCMemory.h>

#include <Eigen/Dense>
#include <Eigen/Core>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;
using namespace OpenViBEPlugins::Classification;
using namespace OpenViBEToolkit;

using namespace Eigen;

//Need to be reachable from outside
const char* const c_sMLPEvaluationFunctionName = "Evaluation function";
const char* const c_sMLPTransferFunctionName = "Transfer function";

namespace
{
	const char* const c_sMLPTypeNodeName = "MLP";
	const char* const c_sMLPNeuronConfigurationNodeName = "Neuron-configuration";
	const char* const c_sMLPInputNeuronCountNodeName = "Input-neuron-count";
	const char* const c_sMLPHiddenNeuronCountNodeName = "Hidden-neuron-count";
	const char* const c_sMLPMaximumNodeName = "Maximum";
	const char* const c_sMLPMinimumNodeName = "Minimum";
	const char* const c_sMLPInputBiasNodeName = "Input-bias";
	const char* const c_sMLPInputWeightNodeName = "Input-weight";
	const char* const c_sMLPHiddenBiasNodeName = "Hidden-bias";
	const char* const c_sMLPHiddenWeightNodeName = "Hidden-weight";
	const char* const c_sMLPClassLabelNodeName = "Class-label";
}

int32_t OpenViBEPlugins::Classification::MLPClassificationCompare(IMatrix& firstClassificationValue, IMatrix& secondClassificationValue)
{
	//We first need to find the best classification of each.
	double* classificationValueBuffer = firstClassificationValue.getBuffer();
	const double maxFirst = *(std::max_element(classificationValueBuffer, classificationValueBuffer + firstClassificationValue.getBufferElementCount()));

	classificationValueBuffer = secondClassificationValue.getBuffer();
	const double maxSecond = *(std::max_element(classificationValueBuffer, classificationValueBuffer + secondClassificationValue.getBufferElementCount()));

	//Then we just compared them
	if (ov_float_equal(maxFirst, maxSecond)) { return 0; }
	if (maxFirst > maxSecond) { return -1; }
	return 1;
}

#define MLP_DEBUG 0
#if MLP_DEBUG
void dumpMatrix(ILogManager& rMgr, const MatrixXd& mat, const CString& desc)
{
	rMgr << LogLevel_Info << desc << "\n";
	for (int i = 0; i < mat.rows(); ++i) {
		rMgr << LogLevel_Info << "Row " << i << ": ";
		for (int j = 0; j < mat.cols(); ++j) {
			rMgr << mat(i, j) << " ";
		}
		rMgr << "\n";
	}
}
#else
void dumpMatrix(ILogManager&, const MatrixXd&, const CString&) { };
#endif


bool CAlgorithmClassifierMLP::initialize()
{
	TParameterHandler<int64_t> iHidden(this->getInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_HiddenNeuronCount));
	iHidden = 3;

	TParameterHandler<XML::IXMLNode*> oConfiguration(this->getOutputParameter(OVTK_Algorithm_Classifier_OutputParameterId_Configuration));
	oConfiguration = nullptr;

	TParameterHandler<double> iAlpha(this->getInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Alpha));
	iAlpha = 0.01;
	TParameterHandler<double> iEpsilon(this->getInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Epsilon));
	iEpsilon = 0.000001;
	return true;
}

bool CAlgorithmClassifierMLP::uninitialize()
{
	return true;
}

bool CAlgorithmClassifierMLP::train(const IFeatureVectorSet& featureVectorSet)
{
	m_oLabelList.clear();

	this->initializeExtraParameterMechanism();
	size_t hiddenNeuronCount = size_t(this->getInt64Parameter(OVP_Algorithm_ClassifierMLP_InputParameterId_HiddenNeuronCount));
	double alpha = this->getFloat64Parameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Alpha);
	double epsilon = this->getFloat64Parameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Epsilon);
	this->uninitializeExtraParameterMechanism();

	if (hiddenNeuronCount < 1)
	{
		this->getLogManager() << LogLevel_Error << "Invalid amount of neuron in the hidden layer. Fallback to default value (3)\n";
		hiddenNeuronCount = 3;
	}
	if (alpha <= 0)
	{
		this->getLogManager() << LogLevel_Error << "Invalid value for learning coefficient (" << alpha << "). Fallback to default value (0.01)\n";
		alpha = 0.01;
	}
	if (epsilon <= 0)
	{
		this->getLogManager() << LogLevel_Error << "Invalid value for stop learning condition (" << epsilon << "). Fallback to default value (0.000001)\n";
		epsilon = 0.000001;
	}

	std::map<double, size_t> classCount;
	std::map<double, VectorXd> targetList;
	//We need to compute the min and the max of data in order to normalize and center them
	for (size_t i = 0; i < featureVectorSet.getFeatureVectorCount(); ++i)
	{
		classCount[featureVectorSet[i].getLabel()]++;
	}
	size_t validationElementCount = 0;

	//We generate the list of class
	for (auto iter = classCount.begin(); iter != classCount.end(); ++iter)
	{
		//We keep 20% percent of the training set for the validation for each class
		validationElementCount += size_t(iter->second * 0.2);
		m_oLabelList.push_back(iter->first);
		iter->second = size_t(iter->second * 0.2);
	}

	const size_t nbClass = m_oLabelList.size();
	const size_t featureSize = featureVectorSet.getFeatureVector(0).getSize();

	//Generate the target vector for each class. To save time and memory, we compute only one vector per class
	//Vector tagret looks like following [0 0 1 0] for class 3 (if 4 classes)
	for (size_t i = 0; i < nbClass; ++i)
	{
		VectorXd oTarget = VectorXd::Zero(nbClass);
		//class 1 is at index 0
		oTarget[size_t(m_oLabelList[i])] = 1.;
		targetList[m_oLabelList[i]] = oTarget;
	}

	//We store each normalize vector we get for training. This not optimal in memory but avoid a lot of computation later
	//List of the class of the feature vectors store in the same order are they are in validation/training set(to be able to get the target)
	std::vector<double> oTrainingSet;
	std::vector<double> oValidationSet;
	MatrixXd oTrainingDataMatrix(featureSize, featureVectorSet.getFeatureVectorCount() - validationElementCount);
	MatrixXd oValidationDataMatrix(featureSize, validationElementCount);

	//We don't need to make a shuffle it has already be made by the trainer box
	//We store 20% of the feature vectors for validation
	int32_t validationIndex = 0, trainingIndex = 0;
	for (size_t i = 0; i < featureVectorSet.getFeatureVectorCount(); ++i)
	{
		const Map<VectorXd> oFeatureVec(const_cast<double*>(featureVectorSet.getFeatureVector(i).getBuffer()), featureSize);
		VectorXd oData = oFeatureVec;
		if (classCount[featureVectorSet.getFeatureVector(i).getLabel()] > 0)
		{
			oValidationDataMatrix.col(validationIndex++) = oData;
			oValidationSet.push_back(featureVectorSet.getFeatureVector(i).getLabel());
			--classCount[featureVectorSet.getFeatureVector(i).getLabel()];
		}
		else
		{
			oTrainingDataMatrix.col(trainingIndex++) = oData;
			oTrainingSet.push_back(featureVectorSet.getFeatureVector(i).getLabel());
		}
	}

	//We now get the min and the max of the training set for normalization
	m_max = oTrainingDataMatrix.maxCoeff();
	m_min = oTrainingDataMatrix.minCoeff();
	//Normalization of the data. We need to do it to avoid saturation of tanh.
	for (size_t i = 0; i < size_t(oTrainingDataMatrix.cols()); ++i)
	{
		for (size_t j = 0; j < size_t(oTrainingDataMatrix.rows()); ++j)
		{
			oTrainingDataMatrix(j, i) = 2 * (oTrainingDataMatrix(j, i) - m_min) / (m_max - m_min) - 1;
		}
	}
	for (size_t i = 0; i < size_t(oValidationDataMatrix.cols()); ++i)
	{
		for (size_t j = 0; j < size_t(oValidationDataMatrix.rows()); ++j)
		{
			oValidationDataMatrix(j, i) = 2 * (oValidationDataMatrix(j, i) - m_min) / (m_max - m_min) - 1;
		}
	}

	const size_t featureCount = oTrainingSet.size();
	const double boundValue = 1. / (featureSize + 1);
	double previousError = std::numeric_limits<double>::max();
	double cumulativeError = 0;

	//Let's generate randomly weights and biases
	//We restrain the weight between -1/(fan-in) and 1/(fan-in) to avoid saturation in the worst case
	m_oInputWeight = MatrixXd::Random(hiddenNeuronCount, featureSize) * boundValue;
	m_oInputBias = VectorXd::Random(hiddenNeuronCount) * boundValue;

	m_oHiddenWeight = MatrixXd::Random(nbClass, hiddenNeuronCount) * boundValue;
	m_oHiddenBias = VectorXd::Random(nbClass) * boundValue;

	MatrixXd oDeltaInputWeight = MatrixXd::Zero(hiddenNeuronCount, featureSize);
	VectorXd oDeltaInputBias = VectorXd::Zero(hiddenNeuronCount);
	MatrixXd oDeltaHiddenWeight = MatrixXd::Zero(nbClass, hiddenNeuronCount);
	VectorXd oDeltaHiddenBias = VectorXd::Zero(nbClass);

	MatrixXd oY1, oA2;
	//A1 is the value compute in hidden neuron before applying tanh
	//Y1 is the output vector of hidden layer
	//A2 is the value compute by output neuron before applying transfer function
	//Y2 is the value of output after the transfer function (softmax)
	while (true)
	{
		oDeltaInputWeight.setZero();
		oDeltaInputBias.setZero();
		oDeltaHiddenWeight.setZero();
		oDeltaHiddenBias.setZero();
		//The first cast of tanh has to been explicit for windows compilation
		oY1.noalias() = ((m_oInputWeight * oTrainingDataMatrix).colwise() + m_oInputBias).unaryExpr(std::ptr_fun<double, double>((double(*)(double))tanh));
		oA2.noalias() = (m_oHiddenWeight * oY1).colwise() + m_oHiddenBias;
		for (size_t i = 0; i < featureCount; ++i)
		{
			const VectorXd& oTarget = targetList[oTrainingSet[i]];
			const VectorXd& oData = oTrainingDataMatrix.col(i);

			//Now we compute all deltas of output layer
			VectorXd oOutputDelta = oA2.col(i) - oTarget;
			for (size_t j = 0; j < nbClass; ++j)
			{
				for (size_t k = 0; k < hiddenNeuronCount; ++k)
				{
					oDeltaHiddenWeight(j, k) -= oOutputDelta[j] * oY1.col(i)[k];
				}
			}
			oDeltaHiddenBias.noalias() -= oOutputDelta;

			//Now we take care of the hidden layer
			VectorXd oHiddenDelta = VectorXd::Zero(hiddenNeuronCount);
			for (size_t j = 0; j < hiddenNeuronCount; ++j)
			{
				for (size_t k = 0; k < nbClass; ++k)
				{
					oHiddenDelta[j] += oOutputDelta[k] * m_oHiddenWeight(k, j);
				}
				oHiddenDelta[j] *= (1 - pow(oY1.col(i)[j], 2));
			}

			for (size_t j = 0; j < hiddenNeuronCount; ++j)
			{
				for (size_t k = 0; k < featureSize; ++k)
				{
					oDeltaInputWeight(j, k) -= oHiddenDelta[j] * oData[k];
				}
			}
			oDeltaInputBias.noalias() -= oHiddenDelta;
		}
		//We finish the loop, let's apply deltas
		m_oHiddenWeight.noalias() += oDeltaHiddenWeight / featureCount * alpha;
		m_oHiddenBias.noalias() += oDeltaHiddenBias / featureCount * alpha;
		m_oInputWeight.noalias() += oDeltaInputWeight / featureCount * alpha;
		m_oInputBias.noalias() += oDeltaInputBias / featureCount * alpha;

		dumpMatrix(this->getLogManager(), m_oHiddenWeight, "m_oHiddenWeight");
		dumpMatrix(this->getLogManager(), m_oHiddenBias, "m_oHiddenBias");
		dumpMatrix(this->getLogManager(), m_oInputWeight, "m_oInputWeight");
		dumpMatrix(this->getLogManager(), m_oInputBias, "m_oInputBias");

		//Now we compute the cumulative error in the validation set
		cumulativeError = 0;
		//We don't compute Y2 because we train on the identity
		oA2.noalias() = (m_oHiddenWeight * ((m_oInputWeight * oValidationDataMatrix).colwise() + m_oInputBias).unaryExpr(std::ptr_fun<double, double>(tanh))).colwise() + m_oHiddenBias;
		for (size_t i = 0; i < oValidationSet.size(); ++i)
		{
			const VectorXd& oTarget = targetList[oValidationSet[i]];
			const VectorXd& oIdentityResult = oA2.col(i);

			//Now we need to compute the error
			for (size_t j = 0; j < nbClass; ++j)
			{
				cumulativeError += 0.5 * pow(oIdentityResult[j] - oTarget[j], 2);
			}
		}
		cumulativeError /= oValidationSet.size();
		//If the delta of error is under Epsilon we consider that the training is over
		if (previousError - cumulativeError < epsilon) { break; }
		previousError = cumulativeError;
	}
	dumpMatrix(this->getLogManager(), m_oHiddenWeight, "oHiddenWeight");
	dumpMatrix(this->getLogManager(), m_oHiddenBias, "oHiddenBias");
	dumpMatrix(this->getLogManager(), m_oInputWeight, "oInputWeight");
	dumpMatrix(this->getLogManager(), m_oInputBias, "oInputBias");
	return true;
}

bool CAlgorithmClassifierMLP::classify(const IFeatureVector& featureVector, double& classLabel, IVector& distance, IVector& probability)
{
	if (featureVector.getSize() != size_t(m_oInputWeight.cols()))
	{
		this->getLogManager() << LogLevel_Error << "Classifier expected " << size_t(m_oInputWeight.cols()) << " features, got " << featureVector.getSize() << "\n";
		return false;
	}

	const Map<VectorXd> oFeatureVec(const_cast<double*>(featureVector.getBuffer()), featureVector.getSize());
	VectorXd oData = oFeatureVec;
	//we normalize and center data on 0 to avoid saturation
	for (size_t j = 0; j < featureVector.getSize(); ++j)
	{
		oData[j] = 2 * (oData[j] - m_min) / (m_max - m_min) - 1;
	}

	const size_t classCount = m_oLabelList.size();

	VectorXd oA2 = m_oHiddenBias + (m_oHiddenWeight * (m_oInputBias + (m_oInputWeight * oData)).unaryExpr(std::ptr_fun<double, double>(tanh)));

	//The final transfer function is the softmax
	VectorXd oY2 = oA2.unaryExpr(std::ptr_fun<double, double>(exp));
	oY2 /= oY2.sum();

	distance.setSize(classCount);
	probability.setSize(classCount);

	//We use A2 as the classification values output, and the Y2 as the probability
	double max = oY2[0];
	size_t classFound = 0;
	distance[0] = oA2[0];
	probability[0] = oY2[0];
	for (size_t i = 1; i < classCount; ++i)
	{
		if (oY2[i] > max)
		{
			max = oY2[i];
			classFound = i;
		}
		distance[i] = oA2[i];
		probability[i] = oY2[i];
	}

	classLabel = m_oLabelList[classFound];

	return true;
}

XML::IXMLNode* CAlgorithmClassifierMLP::saveConfiguration()
{
	XML::IXMLNode* rootNode = XML::createNode(c_sMLPTypeNodeName);

	std::stringstream classes;
	for (int32_t i = 0; i < m_oHiddenBias.size(); ++i)
	{
		classes << m_oLabelList[i] << " ";
	}
	XML::IXMLNode* classLabelNode = XML::createNode(c_sMLPClassLabelNodeName);
	classLabelNode->setPCData(classes.str().c_str());
	rootNode->addChild(classLabelNode);

	XML::IXMLNode* configuration = XML::createNode(c_sMLPNeuronConfigurationNodeName);

	//The input and output neuron count are not mandatory but they facilitate a lot the loading process
	XML::IXMLNode* tempNode = XML::createNode(c_sMLPInputNeuronCountNodeName);
	dumpData(tempNode, int64_t(m_oInputWeight.cols()));
	configuration->addChild(tempNode);

	tempNode = XML::createNode(c_sMLPHiddenNeuronCountNodeName);
	dumpData(tempNode, int64_t(m_oInputWeight.rows()));
	configuration->addChild(tempNode);
	rootNode->addChild(configuration);

	tempNode = XML::createNode(c_sMLPMinimumNodeName);
	dumpData(tempNode, m_min);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(c_sMLPMaximumNodeName);
	dumpData(tempNode, m_max);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(c_sMLPInputWeightNodeName);
	dumpData(tempNode, m_oInputWeight);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(c_sMLPInputBiasNodeName);
	dumpData(tempNode, m_oInputBias);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(c_sMLPHiddenBiasNodeName);
	dumpData(tempNode, m_oHiddenBias);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(c_sMLPHiddenWeightNodeName);
	dumpData(tempNode, m_oHiddenWeight);
	rootNode->addChild(tempNode);

	return rootNode;
}

bool CAlgorithmClassifierMLP::loadConfiguration(XML::IXMLNode* configurationNode)
{
	m_oLabelList.clear();
	std::stringstream data(configurationNode->getChildByName(c_sMLPClassLabelNodeName)->getPCData());
	double temp;
	while (data >> temp)
	{
		m_oLabelList.push_back(temp);
	}

	int64_t featureSize, hiddenNeuronCount;
	XML::IXMLNode* neuronConfigurationNode = configurationNode->getChildByName(c_sMLPNeuronConfigurationNodeName);

	loadData(neuronConfigurationNode->getChildByName(c_sMLPHiddenNeuronCountNodeName), hiddenNeuronCount);
	loadData(neuronConfigurationNode->getChildByName(c_sMLPInputNeuronCountNodeName), featureSize);

	loadData(configurationNode->getChildByName(c_sMLPMaximumNodeName), m_max);
	loadData(configurationNode->getChildByName(c_sMLPMinimumNodeName), m_min);

	loadData(configurationNode->getChildByName(c_sMLPInputWeightNodeName), m_oInputWeight, hiddenNeuronCount, featureSize);
	loadData(configurationNode->getChildByName(c_sMLPInputBiasNodeName), m_oInputBias);
	loadData(configurationNode->getChildByName(c_sMLPHiddenWeightNodeName), m_oHiddenWeight, m_oLabelList.size(), hiddenNeuronCount);
	loadData(configurationNode->getChildByName(c_sMLPHiddenBiasNodeName), m_oHiddenBias);

	return true;
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, MatrixXd& matrix)
{
	std::stringstream data;

	data << std::scientific;
	for (size_t i = 0; i < size_t(matrix.rows()); ++i)
	{
		for (size_t j = 0; j < size_t(matrix.cols()); ++j)
		{
			data << " " << matrix(i, j);
		}
	}

	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, VectorXd& vector)
{
	std::stringstream data;

	data << std::scientific;
	for (size_t i = 0; i < size_t(vector.size()); ++i)
	{
		data << " " << vector[i];
	}

	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, const int64_t value)
{
	std::stringstream data;
	data << value;
	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, const double value)
{
	std::stringstream l_sData;
	l_sData << std::scientific;
	l_sData << value;
	node->setPCData(l_sData.str().c_str());
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, MatrixXd& matrix, const size_t rowCount, const size_t colCount)
{
	matrix = MatrixXd(rowCount, colCount);
	std::stringstream l_sData(node->getPCData());

	std::vector<double> l_vCoefficients;
	double l_f64Value;
	while (l_sData >> l_f64Value)
	{
		l_vCoefficients.push_back(l_f64Value);
	}

	size_t index = 0;
	for (size_t i = 0; i < rowCount; ++i)
	{
		for (size_t j = 0; j < colCount; ++j)
		{
			matrix(int32_t(i), int32_t(j)) = l_vCoefficients[index];
			++index;
		}
	}
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, VectorXd& vector)
{
	std::stringstream data(node->getPCData());
	std::vector<double> coefficients;
	double value;
	while (data >> value)
	{
		coefficients.push_back(value);
	}
	vector = VectorXd(coefficients.size());

	for (size_t i = 0; i < coefficients.size(); ++i)
	{
		vector[i] = coefficients[i];
	}
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, int64_t& value)
{
	std::stringstream data(node->getPCData());
	data >> value;
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, double& value)
{
	std::stringstream data(node->getPCData());
	data >> value;
}

uint32_t CAlgorithmClassifierMLP::getOutputProbabilityVectorLength()
{
	return m_oLabelList.size();
}

uint32_t CAlgorithmClassifierMLP::getOutputDistanceVectorLength()
{
	return m_oLabelList.size();
}
#endif
