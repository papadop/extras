#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <xml/IXMLNode.h>

#include <stack>
#include "../../../../../contrib/packages/libSVM/svm.h"

#define OVP_ClassId_Algorithm_ClassifierSVM                                        OpenViBE::CIdentifier(0x50486EC2, 0x6F2417FC)
#define OVP_ClassId_Algorithm_ClassifierSVM_DecisionAvailable                      OpenViBE::CIdentifier(0x21A61E69, 0xD522CE01)
#define OVP_ClassId_Algorithm_ClassifierSVMDesc                                    OpenViBE::CIdentifier(0x272B056E, 0x0C6502AC)

#define OVP_Algorithm_ClassifierSVM_InputParameterId_SVMType					   OpenViBE::CIdentifier(0x0C347BBA, 0x180577F9)
#define OVP_Algorithm_ClassifierSVM_InputParameterId_SVMKernelType				   OpenViBE::CIdentifier(0x1952129C, 0x6BEF38D7)
#define OVP_Algorithm_ClassifierSVM_InputParameterId_SVMDegree					   OpenViBE::CIdentifier(0x0E284608, 0x7323390E)
#define OVP_Algorithm_ClassifierSVM_InputParameterId_SVMGamma					   OpenViBE::CIdentifier(0x5D4A358F, 0x29043846)
#define OVP_Algorithm_ClassifierSVM_InputParameterId_SVMCoef0					   OpenViBE::CIdentifier(0x724D5EC5, 0x13E56658)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCost					   OpenViBE::CIdentifier(0x353662E8, 0x041D7610)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMNu					       OpenViBE::CIdentifier(0x62334FC3, 0x49594D32)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMEpsilon					   OpenViBE::CIdentifier(0x09896FD2, 0x523775BA)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCacheSize				   OpenViBE::CIdentifier(0x4BCE65A7, 0x6A103468)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMTolerance				   OpenViBE::CIdentifier(0x2658168C, 0x0914687C)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMShrinking				   OpenViBE::CIdentifier(0x63F5286A, 0x6A9D18BF)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMProbabilityEstimate		   OpenViBE::CIdentifier(0x05DC16EA, 0x5DBD51C2)
#define OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMweight					   OpenViBE::CIdentifier(0x0BA132BE, 0x17DD3B8F)
#define OVP_Algorithm_ClassifierSVM_InputParameterId_SVMWeightLabel				   OpenViBE::CIdentifier(0x22C27048, 0x5CC6214A)

#define OVP_TypeId_SVMType														   OpenViBE::CIdentifier(0x2AF426D1, 0x72FB7BAC)
#define OVP_TypeId_SVMKernelType												   OpenViBE::CIdentifier(0x54BB0016, 0x6AA27496)

namespace OpenViBEPlugins
{
	namespace Classification
	{
		int32_t SVMClassificationCompare(OpenViBE::IMatrix& firstClassificationValue, OpenViBE::IMatrix& secondClassificationValue);


		class CAlgorithmClassifierSVM : public OpenViBEToolkit::CAlgorithmClassifier
		{
		public:

			CAlgorithmClassifierSVM();

			bool initialize() override;
			bool uninitialize() override;

			bool train(const OpenViBEToolkit::IFeatureVectorSet& featureVectorSet) override;
			bool classify(const OpenViBEToolkit::IFeatureVector& featureVector, double& classLabel,
						  OpenViBEToolkit::IVector& distance, OpenViBEToolkit::IVector& probability) override;

			XML::IXMLNode* saveConfiguration() override;
			bool loadConfiguration(XML::IXMLNode* configurationNode) override;
			virtual OpenViBE::CString paramToString(svm_parameter* param);
			virtual OpenViBE::CString modelToString();
			virtual OpenViBE::CString problemToString(svm_problem* prob);

			uint32_t getOutputProbabilityVectorLength() override;
			uint32_t getOutputDistanceVectorLength() override;

			_IsDerivedFromClass_Final_(CAlgorithmClassifier, OVP_ClassId_Algorithm_ClassifierSVM);

		protected:

			std::vector<double> m_class;
			struct svm_parameter m_oParam;

			//struct svm_parameter *m_oParam; // set by parse_command_line
			struct svm_problem m_oProb;     // set by read_problem
			struct svm_model* m_model;

			bool m_modelWasTrained; // true if from svm_train(), false if loaded

			int32_t m_indexSV;
			uint32_t m_numberOfFeatures;
			OpenViBE::CMemoryBuffer m_oConfiguration;
			//todo a modifier en fonction de svn_save_model
			//vector m_oCoefficients;

		private:
			void loadParamNodeConfiguration(XML::IXMLNode* paramNode);
			void loadModelNodeConfiguration(XML::IXMLNode* modelNode);
			void loadModelSVsNodeConfiguration(XML::IXMLNode* svsNodeParam);

			void setParameter();

			static void deleteModel(svm_model* model, bool freeSupportVectors);
		};

		class CAlgorithmClassifierSVMDesc : public OpenViBEToolkit::CAlgorithmClassifierDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("SVM classifier"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bougrain / Baptiste Payan"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("UHP_Nancy1/LORIA INRIA/LORIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_ClassifierSVM; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmClassifierSVM; }

			bool getAlgorithmPrototype(OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				CAlgorithmClassifierDesc::getAlgorithmPrototype(rAlgorithmPrototype);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMType, "SVM type", OpenViBE::Kernel::ParameterType_Enumeration, OVP_TypeId_SVMType);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMKernelType, "Kernel type", OpenViBE::Kernel::ParameterType_Enumeration, OVP_TypeId_SVMKernelType);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMDegree, "Degree", OpenViBE::Kernel::ParameterType_Integer);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMGamma, "Gamma", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMCoef0, "Coef 0", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCost, "Cost", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMNu, "Nu", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMEpsilon, "Epsilon", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMCacheSize, "Cache size", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMTolerance, "Epsilon tolerance", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMShrinking, "Shrinking", OpenViBE::Kernel::ParameterType_Boolean);
				//rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMProbabilityEstimate,"Probability estimate",OpenViBE::Kernel::ParameterType_Boolean);
				rAlgorithmPrototype.addInputParameter(OVP_ALgorithm_ClassifierSVM_InputParameterId_SVMweight, "Weight", OpenViBE::Kernel::ParameterType_String);
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_ClassifierSVM_InputParameterId_SVMWeightLabel, "Weight Label", OpenViBE::Kernel::ParameterType_String);
				return true;
			}

			_IsDerivedFromClass_Final_(CAlgorithmClassifierDesc, OVP_ClassId_Algorithm_ClassifierSVMDesc);
		};
	};
};
