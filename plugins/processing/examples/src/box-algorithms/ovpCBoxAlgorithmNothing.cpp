#include "ovpCBoxAlgorithmNothing.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace Examples;

/*
uint64_t CBoxAlgorithmNothing::getClockFrequency()
{
	return 0; // the box clock frequency
}
*/

bool CBoxAlgorithmNothing::initialize()
{
	return true;
}

bool CBoxAlgorithmNothing::uninitialize()
{
	// ...

	return true;
}

/*
bool CBoxAlgorithmNothing::processEvent(IMessageEvent& rMessageEvent)
{
	// ...

	// getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
*/

/*
bool CBoxAlgorithmNothing::processSignal(IMessageSignal& rMessageSignal)
{
	// ...

	// getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
*/

/*
bool CBoxAlgorithmNothing::processClock(IMessageClock& rMessageClock)
{
	// ...

	// getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
*/

/*
bool CBoxAlgorithmNothing::processInput(uint32_t inputIndex)
{
	// ...

	// getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
*/

bool CBoxAlgorithmNothing::process()
{
	// IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	// IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

	// ...

	// l_rStaticBoxContext.getInputCount();
	// l_rStaticBoxContext.getOutputCount();
	// l_rStaticBoxContext.getSettingCount();

	// l_rDynamicBoxContext.getInputChunkCount()
	// l_rDynamicBoxContext.getInputChunk(i, )
	// l_rDynamicBoxContext.getOutputChunk(i, )

	return true;
}
