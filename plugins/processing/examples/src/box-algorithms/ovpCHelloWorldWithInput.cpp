/*
 * Prints user-specified greeting to the log every time process() is called. Passes the signal through. 
 */
#include "ovpCHelloWorldWithInput.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace OpenViBEPlugins::Examples;

void CHelloWorldWithInput::release()
{
	delete this;
}

bool CHelloWorldWithInput::processInput(uint32_t inputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CHelloWorldWithInput::process()
{
	const IBox* l_pStaticBoxContext = getBoxAlgorithmContext()->getStaticBoxContext();
	IBoxIO* l_pDynamicBoxContext = getBoxAlgorithmContext()->getDynamicBoxContext();

	uint64_t l_ui64StartTime = 0;
	uint64_t l_ui64EndTime = 0;
	uint64_t l_ui64ChunkSize = 0;
	const uint8_t* l_pChunkBuffer = nullptr;

	for (uint32_t i = 0; i < l_pStaticBoxContext->getInputCount(); i++)
	{
		for (uint32_t j = 0; j < l_pDynamicBoxContext->getInputChunkCount(i); j++)
		{
			l_pDynamicBoxContext->getInputChunk(i, j, l_ui64StartTime, l_ui64EndTime, l_ui64ChunkSize, l_pChunkBuffer);
			l_pDynamicBoxContext->appendOutputChunkData(i, l_pChunkBuffer, l_ui64ChunkSize);
			l_pDynamicBoxContext->markOutputAsReadyToSend(i, l_ui64StartTime, l_ui64EndTime);
			l_pDynamicBoxContext->markInputAsDeprecated(i, j);
		}
	}

	const CString l_sMyGreeting = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	getLogManager() << LogLevel_Info << ": " << l_sMyGreeting << "\n";

	return true;
}
