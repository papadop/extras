#include "ovpCLog.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;
using namespace OpenViBEPlugins;
using namespace Examples;
using namespace OpenViBEToolkit;

void CLog::release()
{
	delete this;
}

bool CLog::initialize()
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "initialize\n";
	return true;
}

bool CLog::uninitialize()
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "uninitialize\n";
	return true;
}

bool CLog::processClock(IMessageClock& rMessageClock)
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "processClock\n";
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CLog::processInput(uint32_t inputIndex)
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "processInput\n";
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CLog::process()
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "process\n";

	const IBox* l_pStaticBoxContext = getBoxAlgorithmContext()->getStaticBoxContext();
	IBoxIO* l_pDynamicBoxContext = getBoxAlgorithmContext()->getDynamicBoxContext();

	for (uint32_t i = 0; i < l_pStaticBoxContext->getInputCount(); i++)
	{
		for (uint32_t j = 0; j < l_pDynamicBoxContext->getInputChunkCount(i); j++)
		{
			l_pDynamicBoxContext->markInputAsDeprecated(i, j);
		}
	}

	return true;
}
