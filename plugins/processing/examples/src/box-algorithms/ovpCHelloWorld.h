#ifndef __SamplePlugin_CHelloWorld_H__
#define __SamplePlugin_CHelloWorld_H__

#include "../ovp_defines.h"
#include <toolkit/ovtk_all.h>
#include <cstdio>

namespace OpenViBEPlugins
{
	namespace Examples
	{
		class CHelloWorld : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override;
			uint64_t getClockFrequency() override;
			bool processClock(OpenViBE::Kernel::IMessageClock& /* rMessageClock */) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_HelloWorld)
		};

		class CHelloWorldListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CHelloWorldDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("HelloWorld"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Prints \"Hello World!\" to the log with a user-specified frequency"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Using several copies of this friendly box (with different names) can be used to e.g. examine box execution order"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Examples/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-copy"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_HelloWorld; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CHelloWorld(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CHelloWorldListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addSetting("Frequency (Hz)", OV_TypeId_Float, "1.0");
				rPrototype.addSetting("My greeting", OV_TypeId_String, "Hello World!");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_HelloWorldDesc)
		};
	};
};

#endif // __SamplePlugin_CHelloWorld_H__
