/*
 * Prints user-specified greeting to the log with given frequency 
 */
#include "ovpCHelloWorld.h"

#include <cstdlib>		// atof
#include <openvibe/ovITimeArithmetics.h>

using namespace OpenViBE;
using namespace OpenViBEPlugins::Examples;


uint64_t CHelloWorld::getClockFrequency()
{
	const double l_f64Frequency = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	// We need the freq in 32:32 fixed point time
	return ITimeArithmetics::secondsToTime(l_f64Frequency);
}

void CHelloWorld::release()
{
	delete this;
}

bool CHelloWorld::processClock(Kernel::IMessageClock& /* rMessageClock */)
{
	const CString l_sMyGreeting = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	getLogManager() << Kernel::LogLevel_Info << ": " << l_sMyGreeting << "\n";

	return true;
}

bool CHelloWorld::process()
{
	return true;
}
