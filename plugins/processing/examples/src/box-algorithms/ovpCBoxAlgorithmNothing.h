#ifndef __OpenViBEPlugins_BoxAlgorithm_Nothing_H__
#define __OpenViBEPlugins_BoxAlgorithm_Nothing_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_Nothing     OpenViBE::CIdentifier(0x273960C0, 0x485C407C)
#define OVP_ClassId_BoxAlgorithm_NothingDesc OpenViBE::CIdentifier(0x7A6167D9, 0x79070E22)


namespace OpenViBEPlugins
{
	namespace Examples
	{
		class CBoxAlgorithmNothing : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			// virtual OpenViBE::uint64 getClockFrequency();
			bool initialize() override;
			bool uninitialize() override;
			// virtual OpenViBE::boolean processEvent(OpenViBE::CMessageEvent& rMessageEvent);
			// virtual OpenViBE::boolean processSignal(OpenViBE::CMessageSignal& rMessageSignal);
			// virtual OpenViBE::boolean processClock(OpenViBE::CMessageClock& rMessageClock);
			// virtual OpenViBE::boolean processInput(uint32_t inputIndex);
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_Nothing);

		protected:

			// ...
		};

		class CBoxAlgorithmNothingDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Nothing"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("This box does nothing"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Examples/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-about"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_Nothing; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmNothing; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				// rBoxAlgorithmPrototype.addInput  ("input name", /* input type (OV_TypeId_Signal) */);
				// rBoxAlgorithmPrototype.addOutput ("output name", /* output type (OV_TypeId_Signal) */);
				// rBoxAlgorithmPrototype.addSetting("setting name", /* setting type (OV_TypeId_Integer), "default value" */);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_NothingDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_Nothing_H__
