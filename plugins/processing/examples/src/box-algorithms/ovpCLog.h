#ifndef __SamplePlugin_CLog_H__
#define __SamplePlugin_CLog_H__

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace Examples
	{
		class CLog : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override;
			uint64_t getClockFrequency() override { return 1LL << 32; }
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::Kernel::IMessageClock& rMessageClock) override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_Log)
		};

		class CLogListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			CLogListener() : m_eLogLevel(OpenViBE::Kernel::LogLevel_Info) {}

			bool initialize() override
			{
				this->getLogManager() << m_eLogLevel << "initialize\n";
				return true;
			};

			bool uninitialize() override
			{
				this->getLogManager() << m_eLogLevel << "uninitialize\n";
				return true;
			};

			bool onInitialized(OpenViBE::Kernel::IBox& rBox) override
			{
				this->getLogManager() << m_eLogLevel << "onInitialized\n";
				return true;
			};

			bool onNameChanged(OpenViBE::Kernel::IBox& rBox) override
			{
				this->getLogManager() << m_eLogLevel << "onNameChanged\n";
				return true;
			};

			bool onIdentifierChanged(OpenViBE::Kernel::IBox& rBox) override
			{
				this->getLogManager() << m_eLogLevel << "onIdentifierChanged\n";
				return true;
			};

			bool onAlgorithmClassIdentifierChanged(OpenViBE::Kernel::IBox& rBox) override
			{
				this->getLogManager() << m_eLogLevel << "onAlgorithmClassIdentifierChanged\n";
				return true;
			};

			virtual bool onProcessingUnitChangedon(OpenViBE::Kernel::IBox& rBox)
			{
				this->getLogManager() << m_eLogLevel << "onProcessingUnitChangedon\n";
				return true;
			};

			bool onInputConnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onInputConnected\n";
				return true;
			};

			bool onInputDisconnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onInputDisconnected\n";
				return true;
			};

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onInputAdded\n";
				return true;
			};

			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onInputRemoved\n";
				return true;
			};

			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onInputTypeChanged\n";
				return true;
			};

			bool onInputNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onInputNameChanged\n";
				return true;
			};

			bool onOutputConnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onOutputConnected\n";
				return true;
			};

			bool onOutputDisconnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onOutputDisconnected\n";
				return true;
			};

			bool onOutputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onOutputAdded\n";
				return true;
			};

			bool onOutputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onOutputRemoved\n";
				return true;
			};

			bool onOutputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onOutputTypeChanged\n";
				return true;
			};

			bool onOutputNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onOutputNameChanged\n";
				return true;
			};

			bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onSettingAdded\n";
				return true;
			};

			bool onSettingRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onSettingRemoved\n";
				return true;
			};

			bool onSettingTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onSettingTypeChanged\n";
				return true;
			};

			bool onSettingNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onSettingNameChanged\n";
				return true;
			};

			bool onSettingDefaultValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onSettingDefaultValueChanged\n";
				return true;
			};

			bool onSettingValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				this->getLogManager() << m_eLogLevel << "onSettingValueChanged\n";
				return true;
			};

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);

		protected:

			OpenViBE::Kernel::ELogLevel m_eLogLevel;
		};

		class CLogDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Log"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Logs stuffs (init, uninit, input, clock, process)"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This sample box shows how stuffs could be logged in the log manager. Note that the different inputs, outputs and parameters have no effect, they only exist to test the logging when there are modifications."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Examples/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Log; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CLog(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				// Adds box inputs
				rPrototype.addInput("Input stream", OV_TypeId_Signal);

				// Adds box outputs
				rPrototype.addOutput("Output stream", OV_TypeId_Signal);

				// Adds box settings
				rPrototype.addSetting("Integer setting", OV_TypeId_Integer, "0");
				rPrototype.addSetting("String setting", OV_TypeId_String, "");

				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddOutput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyOutput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifySetting);

				return true;
			}

			OpenViBE::CString getStockItemName() const override
			{
				return OpenViBE::CString("gtk-edit");
			}

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CLogListener; }
			virtual void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) { delete pBoxListener; }

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_LogDesc)
		};
	};
};

#endif // __SamplePlugin_CLog_H__
