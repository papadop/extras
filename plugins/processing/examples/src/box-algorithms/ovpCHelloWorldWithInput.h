#ifndef __SamplePlugin_CHelloWorldWithInput_H__
#define __SamplePlugin_CHelloWorldWithInput_H__

#include "../ovp_defines.h"
#include <toolkit/ovtk_all.h>
#include <cstdio>

namespace OpenViBEPlugins
{
	namespace Examples
	{
		class CHelloWorldWithInput : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_HelloWorldWithInput)
		};

		class CHelloWorldWithInputListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			// The purposes of the following functions is to make the output correspond to the input

			bool onInputNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				OpenViBE::CString l_sInputName;
				rBox.getInputName(ui32Index, l_sInputName);
				rBox.setOutputName(ui32Index, OpenViBE::CString("Copy of '") + l_sInputName + OpenViBE::CString("'"));
				return true;
			}

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				// Duplicate input as new output
				rBox.addOutput("Temporary name", OV_TypeId_EBMLStream);
				return true;
			}

			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeOutput(ui32Index);
				return true;
			}

			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				// Keep input and output types identical
				OpenViBE::CIdentifier l_oTypeIdentifier;
				rBox.getInputType(ui32Index, l_oTypeIdentifier);
				rBox.setOutputType(ui32Index, l_oTypeIdentifier);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CHelloWorldWithInputDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("HelloWorldWithInput"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Duplicates input to output and prints a message to the log for each input block"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Examples/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-copy"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_HelloWorldWithInput; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CHelloWorldWithInput(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CHelloWorldWithInputListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addSetting("Message", OV_TypeId_String, "Hello!");		// setting 0

				rPrototype.addInput("Input 0", OV_TypeId_Signal);
				rPrototype.addOutput("Copy of 'Input 0'", OV_TypeId_Signal);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_HelloWorldWithInputDesc)
		};
	};
};

#endif // __SamplePlugin_CHelloWorldWithInput_H__
