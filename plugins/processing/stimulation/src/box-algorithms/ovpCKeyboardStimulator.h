#ifndef __OpenViBEPlugins_Stimulation_CKeyboardStimulator_H__
#define __OpenViBEPlugins_Stimulation_CKeyboardStimulator_H__

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <vector>
#include <map>

#include <visualization-toolkit/ovviz_all.h>

namespace TCPTagging
{
	class IStimulusSender; // fwd declare
};

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CKeyboardStimulator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CKeyboardStimulator();

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			uint64_t getClockFrequency() override { return (32LL << 32); }

			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;

			bool process() override;

			bool parseConfigurationFile(const char* pFilename);

			virtual void processKey(guint uiKey, bool bState);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_KeyboardStimulator)

		public:

			OpenViBEToolkit::TStimulationEncoder<CKeyboardStimulator> m_oEncoder;

			GtkWidget* m_pWidget;

			typedef struct
			{
				uint64_t m_ui64StimulationPress;
				uint64_t m_ui64StimulationRelease;
				bool m_bStatus;
			} SKey;

			//! Stores keyvalue/stimulation couples
			std::map<guint, SKey> m_oKeyToStimulation;

			//! Vector of the stimulations to send when possible
			std::vector<uint64_t> m_oStimulationToSend;

			//! Plugin's previous activation date
			uint64_t m_ui64PreviousActivationTime;

			bool m_bError;

			// TCP Tagging
			TCPTagging::IStimulusSender* m_pStimulusSender;

		private:
			bool m_bUnknownKeyPressed;
			uint32_t m_ui32UnknownKeyCode;

			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};

		/**
		* Plugin's description
		*/
		class CKeyboardStimulatorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("Keyboard stimulator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Bruno Renier"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Stimulation generator"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Sends stimulations according to key presses"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_KeyboardStimulator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CKeyboardStimulator(); }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addOutput("Outgoing Stimulations", OV_TypeId_Stimulations);

				rPrototype.addSetting("Filename", OV_TypeId_Filename, "${Path_Data}/plugins/stimulation/simple-keyboard-to-stimulations.txt");
				// @note we don't want to expose these to the user as there is no latency correction in tcp tagging; its best to use localhost
				//				rPrototype.addSetting("TCP Tagging Host address", OV_TypeId_String, "");
				//				rPrototype.addSetting("TCP Tagging Host port",    OV_TypeId_Integer, "15361");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_KeyboardStimulatorDesc)
		};
	};
};

#endif

#endif
