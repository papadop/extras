#ifndef __OpenViBEPlugins_BoxAlgorithm_OpenALSoundPlayer_H__
#define __OpenViBEPlugins_BoxAlgorithm_OpenALSoundPlayer_H__

#if defined TARGET_HAS_ThirdPartyOpenAL

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <AL/alut.h>
#include <vorbis/vorbisfile.h>
#include <iostream>
#include <vector>

#define OVP_ClassId_BoxAlgorithm_OpenALSoundPlayer      OpenViBE::CIdentifier(0x7AC2396F, 0x7EE52EFE)
#define OVP_ClassId_BoxAlgorithm_OpenALSoundPlayerDesc  OpenViBE::CIdentifier(0x6FD040EF, 0x7E2F1284)

namespace TCPTagging
{
	class IStimulusSender; // fwd declare
};

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmOpenALSoundPlayer : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			struct OggVorbisStream
			{
				OggVorbis_File Stream;
				FILE* File;
				ALenum Format;
				ALsizei SampleRate;
			};

			enum ESupportedFileFormat
			{
				FILE_FORMAT_WAV = 0,
				FILE_FORMAT_OGG,
				FILE_FORMAT_UNSUPPORTED
			};

			CBoxAlgorithmOpenALSoundPlayer();

			void release() override { delete this; }

			uint64_t getClockFrequency() override { return (128LL << 32); }
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			virtual bool openSoundFile();
			virtual bool playSound();
			virtual bool stopSound(bool bForwardStim);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_OpenALSoundPlayer);

		protected:

			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmOpenALSoundPlayer> m_oStreamDecoder;
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmOpenALSoundPlayer> m_oStreamEncoder;

			uint64_t m_ui64LastOutputChunkDate;
			bool m_bStartOfSoundSent;
			bool m_bEndOfSoundSent;
			bool m_bLoop;
			uint64_t m_ui64PlayTrigger;
			uint64_t m_ui64StopTrigger;
			OpenViBE::CString m_sFileName;

			std::vector<ALuint> m_vOpenALSources;
			ESupportedFileFormat m_iFileFormat;

			//The handles
			ALuint m_uiSoundBufferHandle;
			ALuint m_uiSourceHandle;
			//OGG
			OggVorbisStream m_oOggVorbisStream;
			std::vector<char> m_vRawOggBufferFromFile;
			
			// For sending stimulations to the TCP Tagging
			TCPTagging::IStimulusSender* m_pStimulusSender;
		};

		class CBoxAlgorithmOpenALSoundPlayerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Sound Player"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Laurent Bonnet"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Play/Stop a sound, with or without loop."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Available format : WAV / OGG. Play and stop with input stimulations. Box based on OpenAL."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_OpenALSoundPlayer; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmOpenALSoundPlayer; }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-media-play"); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input triggers", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Resync triggers (deprecated)", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addSetting("PLAY trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("STOP trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("File to play", OV_TypeId_Filename, "${Path_Data}/plugins/stimulation/ov_beep.wav");
				rBoxAlgorithmPrototype.addSetting("Loop", OV_TypeId_Boolean, "false");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_OpenALSoundPlayerDesc);
		};
	};
};

#endif //TARGET_HAS_ThirdPartyOpenAL
#endif // __OpenViBEPlugins_BoxAlgorithm_OpenALSoundPlayer_H__
