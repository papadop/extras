#ifndef __OpenViBEPlugins_BoxAlgorithm_LuaStimulator_H__
#define __OpenViBEPlugins_BoxAlgorithm_LuaStimulator_H__

#if defined TARGET_HAS_ThirdPartyLua

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/version.hpp>

#include <map>
#include <vector>
#include <cstdio>
#include <cstring>

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_LuaStimulator     OpenViBE::CIdentifier(0x0B5A2787, 0x02750621)
#define OVP_ClassId_BoxAlgorithm_LuaStimulatorDesc OpenViBE::CIdentifier(0x67AF36F3, 0x2B424F46)

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmLuaStimulator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, public boost::noncopyable
		{
		public:

			CBoxAlgorithmLuaStimulator();
			~CBoxAlgorithmLuaStimulator() = default;

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_LuaStimulator);

			virtual bool _waitForStimulation(uint32_t inputIndex, uint32_t ui32StimulationIndex);

			virtual bool getInputCountCB(uint32_t& rui32Count);
			virtual bool getOutputCountCB(uint32_t& rui32Count);
			virtual bool getSettingCountCB(uint32_t& rui32Count);
			virtual bool getSettingCB(uint32_t ui32SettingIndex, OpenViBE::CString& rsSetting);
			virtual bool getConfigCB(const OpenViBE::CString& rsString, OpenViBE::CString& rsConfig);

			virtual bool getCurrentTimeCB(uint64_t& rui64Time);
			virtual bool sleepCB();
			virtual bool getStimulationCountCB(uint32_t inputIndex, uint32_t& rui32Count);
			virtual bool getStimulationCB(uint32_t inputIndex, uint32_t ui32StimulationIndex, uint64_t& rui64Identifier, uint64_t& rui64Time, uint64_t& rui64Duration);
			virtual bool removeStimulationCB(uint32_t inputIndex, uint32_t ui32StimulationIndex);
			virtual bool sendStimulationCB(uint32_t ui32OutputIndex, uint64_t ui64Identifier, uint64_t ui64Time, uint64_t ui64Duration);
			virtual bool log(const OpenViBE::Kernel::ELogLevel eLogLevel, const OpenViBE::CString& sText);

		public:

			void doThread();

			enum
			{
				State_Unstarted,
				State_Processing,
				State_Please_Quit,
				State_Finished,
			};

			uint32_t m_ui32State;
			bool m_bLuaThreadHadError;		// Set to true by the lua thread if there were issues
			bool m_bFilterMode;            // Output chunk generation driven by inputs (true) or by clock (false)

		protected:

			lua_State* m_pLuaState;

			uint64_t m_ui64LastTime;
			std::vector<std::multimap<uint64_t, std::pair<uint64_t, uint64_t>>> m_vOutputStimulation;
			std::vector<std::multimap<uint64_t, std::pair<uint64_t, uint64_t>>> m_vInputStimulation;

			boost::thread* m_pLuaThread;
			boost::mutex m_oMutex;
			boost::mutex::scoped_lock m_oInnerLock;
			boost::mutex::scoped_lock m_oOuterLock;
			boost::mutex::scoped_lock m_oExitLock;
			boost::condition m_oCondition;
			boost::condition m_oExitCondition;

		protected:

			std::vector<OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmLuaStimulator>*> m_vStreamDecoder;
			std::vector<OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmLuaStimulator>*> m_vStreamEncoder;

		private:

			CBoxAlgorithmLuaStimulator(const CBoxAlgorithmLuaStimulator&);

			bool runLuaThread();
			bool sendStimulations(uint64_t ui64StartTime, uint64_t ui64EndTime);
		};

		class CBoxAlgorithmLuaStimulatorListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.setInputType(ui32Index, OV_TypeId_Stimulations);
				return true;
			}

			bool onOutputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.setOutputType(ui32Index, OV_TypeId_Stimulations);
				return true;
			}

			bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				char l_sSettingName[1024];
				sprintf(l_sSettingName, "Setting %i", ui32Index);
				rBox.setSettingType(ui32Index, OV_TypeId_String);
				rBox.setSettingName(ui32Index, l_sSettingName);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmLuaStimulatorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Lua Stimulator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Generates some stimulations according to an Lua script"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Scripting"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LuaStimulator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmLuaStimulator; }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmLuaStimulatorListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addOutput("Stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Lua Script", OV_TypeId_Script, "");

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddOutput);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifySetting);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LuaStimulatorDesc);
		};
	};
};

#endif // TARGET_HAS_ThirdPartyLua

#endif // __OpenViBEPlugins_BoxAlgorithm_LuaStimulator_H__
