#ifndef __OpenViBEPlugins_BoxAlgorithm_RunCommand_H__
#define __OpenViBEPlugins_BoxAlgorithm_RunCommand_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <cstdio>
#include <vector>
#include <map>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_RunCommand     OpenViBE::CIdentifier(0x48843891, 0x7BFC57F4)
#define OVP_ClassId_BoxAlgorithm_RunCommandDesc OpenViBE::CIdentifier(0x29D449AE, 0x2CA94942)

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmRunCommand : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			// virtual OpenViBE::uint64 getClockFrequency();
			bool initialize() override;
			bool uninitialize() override;
			// virtual OpenViBE::boolean processEvent(OpenViBE::CMessageEvent& rMessageEvent);
			// virtual OpenViBE::boolean processSignal(OpenViBE::CMessageSignal& rMessageSignal);
			// virtual OpenViBE::boolean processClock(OpenViBE::CMessageClock& rMessageClock);
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_RunCommand);

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoder;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pStimulationSet;
			std::map<uint64_t, std::vector<OpenViBE::CString>> m_vCommand;
		};

		class CBoxAlgorithmRunCommandListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool check(OpenViBE::Kernel::IBox& rBox)
			{
				char l_sName[1024];
				for (uint32_t i = 0; i < rBox.getSettingCount(); i += 2)
				{
					sprintf(l_sName, "Stimulation %i", i / 2 + 1);
					rBox.setSettingName(i, l_sName);
					rBox.setSettingType(i, OV_TypeId_Stimulation);
					sprintf(l_sName, "Command %i", i / 2 + 1);
					rBox.setSettingName(i + 1, l_sName);
					rBox.setSettingType(i + 1, OV_TypeId_String);
				}
				return true;
			}

			bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				char l_sName[1024];
				sprintf(l_sName, "OVTK_StimulationId_Label_%02X", ui32Index / 2 + 1);
				rBox.setSettingDefaultValue(ui32Index, l_sName);
				rBox.setSettingValue(ui32Index, l_sName);
				rBox.addSetting("", OV_TypeId_String, "");
				this->check(rBox);
				return true;
			}

			bool onSettingRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeSetting((ui32Index / 2) * 2);
				this->check(rBox);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmRunCommandDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Run Command"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Runs some command using system call depending on provided stimulations"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_RunCommand; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmRunCommand; }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmRunCommandListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addSetting("Stimulation 1", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Command 1", OV_TypeId_String, "");
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_RunCommandDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_RunCommand_H__
