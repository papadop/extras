#ifndef __OpenViBEPlugins_SignChangeDetector_H__
#define __OpenViBEPlugins_SignChangeDetector_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CSignChangeDetector :
			virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_SignChangeDetector);

		protected:

			OpenViBEToolkit::TStreamedMatrixDecoder<CSignChangeDetector> m_oStreamedMatrixDecoder;
			OpenViBEToolkit::TStimulationEncoder<CSignChangeDetector> m_oStimulationEncoder;

			uint64_t m_ui64OnStimulationId;
			uint64_t m_ui64OffStimulationId;
			uint64_t m_ui64ChannelIndex;
			uint64_t m_ui64SamplesPerChannel;
			double m_f64LastSample;
			bool m_bFirstSample;
		};

		class CSignChangeDetectorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Sign Change Detector"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Joan Fruitet and Jozef Legeny"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria Sophia"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Detects the change of input's sign"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Triggers a stimulation when one of the input's sign changes (input gets positive or negative"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.2"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_SignChangeDetector; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CSignChangeDetector; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Signal", OV_TypeId_StreamedMatrix);
				rPrototype.addOutput("Generated stimulations", OV_TypeId_Stimulations);
				rPrototype.addSetting("Sign switch to positive stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rPrototype.addSetting("Sign switch to negative stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rPrototype.addSetting("Channel Index", OV_TypeId_Integer, "1");
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_SignChangeDetectorDesc);
		};
	};
};

#endif // __OpenViBEPlugins_SignChangeDetector_H__
