#ifndef __OpenViBEPlugins_BoxAlgorithm_StimulationFilter_H__
#define __OpenViBEPlugins_BoxAlgorithm_StimulationFilter_H__

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <vector>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_StimulationFilter     OpenViBE::CIdentifier(0x02F96101, 0x5E647CB8)
#define OVP_ClassId_BoxAlgorithm_StimulationFilterDesc OpenViBE::CIdentifier(0x4D2A23FC, 0x28191E18)
#define OVP_TypeId_StimulationFilterAction             OpenViBE::CIdentifier(0x09E59E57, 0x8D4A553A)
#define OVP_TypeId_StimulationFilterAction_Select      OpenViBE::CIdentifier(0xBDBBA98D, 0xC0477399)
#define OVP_TypeId_StimulationFilterAction_Reject      OpenViBE::CIdentifier(0xB7C594D2, 0x32474226)

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmStimulationFilter : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_StimulationFilter);

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> ip_pStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pMemoryBuffer;

			typedef struct
			{
				uint64_t ui64Action;
				uint64_t ui64StartStimulationId;
				uint64_t ui64EndStimulationId;
			} SRule;

			uint64_t m_ui64DefaultAction;
			uint64_t m_ui64StartTime;
			uint64_t m_ui64EndTime;
			std::vector<SRule> m_vRules;
		};

		class CBoxAlgorithmStimulationFilterListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:
			bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeSetting(ui32Index);
				//we had a whole rule (3 settings)
				rBox.addSetting("Action to perform", OVP_TypeId_StimulationFilterAction, "Select");
				rBox.addSetting("Stimulation range begin", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBox.addSetting("Stimulation range end", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_0F");

				return true;
			}

			bool onSettingRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				//we must remove the 2 other settings corresponding to the rule
				uint32_t l_ui32SettingGroupIndex = (ui32Index - 3) / 3;
				rBox.removeSetting(l_ui32SettingGroupIndex * 3 + 3);
				rBox.removeSetting(l_ui32SettingGroupIndex * 3 + 3);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmStimulationFilterDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Stimulation Filter"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Filters incoming stimulations selecting or rejecting specific ranges of stimulations"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation/Adaptation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_StimulationFilter; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmStimulationFilter; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Modified Stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Default action", OVP_TypeId_StimulationFilterAction, "Reject");
				rBoxAlgorithmPrototype.addSetting("Time range begin", OV_TypeId_Float, "0");
				rBoxAlgorithmPrototype.addSetting("Time range end", OV_TypeId_Float, "0");
				rBoxAlgorithmPrototype.addSetting("Action to perform", OVP_TypeId_StimulationFilterAction, "Select");
				rBoxAlgorithmPrototype.addSetting("Stimulation range begin", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Stimulation range end", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_0F");

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);
				// rBoxAlgorithmPrototype.addFlag   (OV_AttributeId_Box_FlagIsUnstable);
				return true;
			}

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmStimulationFilterListener; }
			virtual void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) { delete pBoxListener; }

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StimulationFilterDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_StimulationFilter_H__
