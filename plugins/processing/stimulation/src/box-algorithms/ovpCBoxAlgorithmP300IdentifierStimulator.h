#ifndef __OpenViBEPlugins_BoxAlgorithm_P300IdentifierStimulator_H__
#define __OpenViBEPlugins_BoxAlgorithm_P300IdentifierStimulator_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmP300IdentifierStimulator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t ui32Index) override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;
			virtual bool reset();

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_P300IdentifierStimulator);

		protected:

			uint64_t m_ui64StartStimulation;
			uint64_t m_ui64StimulationBase;

			uint64_t m_ui64ImagesCount;

			double m_f64PercentRepetitionTarget;

			uint64_t m_ui64RepetitionCountInTrial;
			uint64_t m_ui64TrialCount;
			uint64_t m_ui64FlashDuration;
			uint64_t m_ui64NoFlashDuration;
			uint64_t m_ui64InterRepetitionDuration;
			uint64_t m_ui64InterTrialDuration;

			// OpenViBE::boolean m_bAvoidNeighborFlashing;

		private:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationTargetDecoder;
			uint64_t m_ui64LastTime;
			bool m_bHeaderSent;
			bool m_bStartReceived;

			uint32_t m_ui32LastState;
			uint64_t m_ui64TrialStartTime;

			uint64_t m_ui64FlashCountInRepetition;
			uint64_t m_ui64FlashCountInRepetitionWithoutTarget;
			uint64_t m_ui64RepetitionDuration;
			uint64_t m_ui64RepetitionDurationWithoutTarget;
			uint64_t m_ui64TrialDuration;
			uint64_t m_ui64TrialIndex;

			bool m_bRepetitionWithoutTarget; //true if the repetition doesn't contains the target

			uint64_t m_ui64RepetitionIndex;
			int64_t m_i64TargetNumber;

			uint64_t* m_pRepetitionTarget; //for every Repetition of one trial this table indicate the number of target should see for the begin of the trial
			std::vector<uint32_t> m_vImages;

			void generate_sequence();
			void generate_trial_vars();
			int64_t getCurrentTimeInRepetition(uint64_t ui64CurrentTimeInTrial);
		};

		class CBoxAlgorithmP300IdentifierStimulatorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("P300 Identifier Stimulator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Baptiste Payan"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Generates a P300 stimulation sequence"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-select-font"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_P300IdentifierStimulator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmP300IdentifierStimulator; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Incoming stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Produced stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Target Stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Start stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Stimulation base", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");

				rBoxAlgorithmPrototype.addSetting("Number of identifiable objects", OV_TypeId_Integer, "6");

				rBoxAlgorithmPrototype.addSetting("Percent of repetitions containing the target", OV_TypeId_Float, "100");

				rBoxAlgorithmPrototype.addSetting("Number of repetitions", OV_TypeId_Integer, "5");
				rBoxAlgorithmPrototype.addSetting("Number of trials", OV_TypeId_Integer, "5");
				rBoxAlgorithmPrototype.addSetting("Flash duration (in sec)", OV_TypeId_Float, "0.075");
				rBoxAlgorithmPrototype.addSetting("No flash duration (in sec)", OV_TypeId_Float, "0.125");
				rBoxAlgorithmPrototype.addSetting("Inter-repetition delay (in sec)", OV_TypeId_Float, "2");
				rBoxAlgorithmPrototype.addSetting("Inter-trial delay (in sec)", OV_TypeId_Float, "5");

				// rBoxAlgorithmPrototype.addSetting("Avoid neighbor flashing",         OV_TypeId_Boolean,     "false");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300IdentifierStimulatorDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_P300IdentifierStimulator_H__
