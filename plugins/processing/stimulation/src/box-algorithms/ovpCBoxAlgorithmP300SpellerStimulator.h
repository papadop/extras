#ifndef __OpenViBEPlugins_BoxAlgorithm_P300SpellerStimulator_H__
#define __OpenViBEPlugins_BoxAlgorithm_P300SpellerStimulator_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_P300SpellerStimulator     OpenViBE::CIdentifier(0x88857F9A, 0xF560D3EB)
#define OVP_ClassId_BoxAlgorithm_P300SpellerStimulatorDesc OpenViBE::CIdentifier(0xCEAFBB05, 0x5DA19DCB)

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmP300SpellerStimulator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t ui32Index) override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_P300SpellerStimulator);

		protected:

			uint64_t m_ui64StartStimulation;
			uint64_t m_ui64RowStimulationBase;
			uint64_t m_ui64ColumnStimulationBase;

			uint64_t m_ui64RowCount;
			uint64_t m_ui64ColumnCount;

			uint64_t m_ui64RepetitionCountInTrial;
			uint64_t m_ui64TrialCount;
			uint64_t m_ui64FlashDuration;
			uint64_t m_ui64NoFlashDuration;
			uint64_t m_ui64InterRepetitionDuration;
			uint64_t m_ui64InterTrialDuration;

			bool m_bAvoidNeighborFlashing;

		private:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoder;
			uint64_t m_ui64LastTime;
			bool m_bHeaderSent;
			bool m_bStartReceived;

			uint32_t m_ui32LastState;
			uint64_t m_ui64TrialStartTime;

			uint64_t m_ui64FlashCountInRepetition;
			uint64_t m_ui64RepetitionDuration;
			uint64_t m_ui64TrialDuration;
			uint64_t m_ui64TrialIndex;

			std::map<OpenViBE::uint64, uint64_t> m_vRow;
			std::map<OpenViBE::uint64, uint64_t> m_vColumn;

		private:

			void generate_sequence();
		};

		class CBoxAlgorithmP300SpellerStimulatorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("P300 Speller Stimulator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Generates a stimulation sequence suitable for a P300 speller"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Stimulation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-select-font"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_P300SpellerStimulator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmP300SpellerStimulator; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Incoming stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Produced stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Start stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Row stimulation base", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Column stimulation base", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_07");

				rBoxAlgorithmPrototype.addSetting("Number of rows", OV_TypeId_Integer, "6");
				rBoxAlgorithmPrototype.addSetting("Number of columns", OV_TypeId_Integer, "6");

				rBoxAlgorithmPrototype.addSetting("Number of repetitions", OV_TypeId_Integer, "5");
				rBoxAlgorithmPrototype.addSetting("Number of trials", OV_TypeId_Integer, "5");
				rBoxAlgorithmPrototype.addSetting("Flash duration (in sec)", OV_TypeId_Float, "0.075");
				rBoxAlgorithmPrototype.addSetting("No flash duration (in sec)", OV_TypeId_Float, "0.125");
				rBoxAlgorithmPrototype.addSetting("Inter-repetition delay (in sec)", OV_TypeId_Float, "2");
				rBoxAlgorithmPrototype.addSetting("Inter-trial delay (in sec)", OV_TypeId_Float, "5");

				rBoxAlgorithmPrototype.addSetting("Avoid neighbor flashing", OV_TypeId_Boolean, "false");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300SpellerStimulatorDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_P300SpellerStimulator_H__
