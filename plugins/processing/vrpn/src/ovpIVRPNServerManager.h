#ifndef __OpenViBEPlugins_VRPN_IVRPNServerManager_H__
#define __OpenViBEPlugins_VRPN_IVRPNServerManager_H__

#if defined TARGET_HAS_ThirdPartyVRPN

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace VRPN
	{
		class IVRPNServerManager
		{
		public:

			static IVRPNServerManager& getInstance();

			virtual ~IVRPNServerManager() { }

			virtual bool initialize() =0;
			virtual bool uninitialize() =0;

			virtual bool process() =0;
			virtual bool reportAnalog(
				const OpenViBE::CIdentifier& rServerIdentifier) =0;
			virtual bool reportButton(
				const OpenViBE::CIdentifier& rServerIdentifier) =0;

			virtual bool addServer(
				const OpenViBE::CString& sServerName,
				OpenViBE::CIdentifier& rServerIdentifier) =0;
			virtual bool isServer(
				const OpenViBE::CIdentifier& rServerIdentifier) const =0;
			virtual bool isServer(
				const OpenViBE::CString& sServerName) const =0;
			virtual bool getServerIdentifier(
				const OpenViBE::CString& sServerName,
				OpenViBE::CIdentifier& rServerIdentifier) const =0;
			virtual bool getServerName(
				const OpenViBE::CIdentifier& rServerIdentifier,
				OpenViBE::CString& sServerName) const =0;
			virtual bool removeServer(
				const OpenViBE::CIdentifier& rServerIdentifier) =0;

			virtual bool setButtonCount(
				const OpenViBE::CIdentifier& rServerIdentifier,
				const uint32_t ui32ButtonCount) =0;
			virtual bool setButtonState(
				const OpenViBE::CIdentifier& rServerIdentifier,
				const uint32_t ui32ButtonIndex,
				const bool bButtonStatus) =0;
			virtual bool getButtonState(
				const OpenViBE::CIdentifier& rServerIdentifier,
				const uint32_t ui32ButtonIndex) const =0;

			virtual bool setAnalogCount(
				const OpenViBE::CIdentifier& rServerIdentifier,
				const uint32_t ui32AnalogCount) =0;
			virtual bool setAnalogState(
				const OpenViBE::CIdentifier& rServerIdentifier,
				const uint32_t ui32AnalogIndex,
				const double f64AnalogStatus) =0;
			virtual double getAnalogState(
				const OpenViBE::CIdentifier& rServerIdentifier,
				const uint32_t ui32AnalogIndex) const =0;
		};
	};
};

#endif // OVP_HAS_Vrpn

#endif // __OpenViBEPlugins_VRPN_IVRPNServerManager_H__
