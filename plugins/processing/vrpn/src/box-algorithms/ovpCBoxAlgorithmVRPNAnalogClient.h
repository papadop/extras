#ifndef __OpenViBEPlugins_BoxAlgorithm_VRPNAnalogClient_H__
#define __OpenViBEPlugins_BoxAlgorithm_VRPNAnalogClient_H__

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>
#include <deque>

#include <vrpn_Analog.h>

#define OVP_ClassId_BoxAlgorithm_VRPNAnalogClient                                        OpenViBE::CIdentifier(0x7CF4A95E, 0x7270D07B)
#define OVP_ClassId_BoxAlgorithm_VRPNAnalogClientDesc                                        OpenViBE::CIdentifier(0x77B2AE79, 0xFDC31871)

namespace OpenViBEPlugins
{
	namespace VRPN
	{
		class CBoxAlgorithmVRPNAnalogClient : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_VRPNAnalogClient);

			void setAnalog(uint32_t ui32AnalogCount, const double* pAnalog);

		protected:

			uint64_t m_ui64LastChunkEndTime;
			uint64_t m_ui64ChunkDuration;
			uint64_t m_ui64SamplingRate;
			uint32_t m_ui32ChannelCount;
			uint32_t m_ui32SampleCountPerSentBlock;

			bool m_bFirstStart;

			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoder;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pMatrix;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SamplingRate;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pMemoryBuffer;

			OpenViBE::CString m_sPeripheralName;

			std::deque<std::vector<double>> m_dSampleBuffer;	// Used as a limited-size buffer of sample vectors
			std::vector<double> m_vLastSample;					// The last sample received from VRPN

			vrpn_Analog_Remote* m_pVRPNAnalogRemote;
		};

		class CBoxAlgorithmVRPNAnalogClientDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Analog VRPN Client"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Connects to an external VRPN device and translate an analog information into OpenViBE signal"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("-"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO/VRPN"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_VRPNAnalogClient; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmVRPNAnalogClient; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				// rBoxAlgorithmPrototype.addInput  ("input name", /* input type (OV_TypeId_Signal) */);
				rBoxAlgorithmPrototype.addOutput("Output", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn@localhost");
				rBoxAlgorithmPrototype.addSetting("Sampling Rate", OV_TypeId_Integer, "512");
				rBoxAlgorithmPrototype.addSetting("Number of Channels", OV_TypeId_Integer, "16");
				rBoxAlgorithmPrototype.addSetting("Sample Count per Sent Block", OV_TypeId_Integer, "32");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_VRPNAnalogClientDesc);
		};
	};
};
#endif // TARGET_HAS_ThirdPartyVRPN

#endif // __OpenViBEPlugins_BoxAlgorithm_VRPNAnalogClient_H__
