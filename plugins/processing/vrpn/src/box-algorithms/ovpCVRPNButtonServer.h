#ifndef __OpenViBEPlugins_VRPN_CVRPNButtonServer_H__
#define __OpenViBEPlugins_VRPN_CVRPNButtonServer_H__

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <map>
#include <cstdio>

namespace OpenViBEPlugins
{
	namespace VRPN
	{
		class CVRPNButtonServer : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CVRPNButtonServer();
			void release() override { delete this; }
			uint64_t getClockFrequency() override { return 64LL << 32; } // 64 times per second
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::Kernel::IMessageClock& rMessageClock) override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_VRPNButtonServer)

			virtual void setStimulation(const uint32_t ui32StimulationIndex, const OpenViBE::uint64 ui64StimulationIdentifier, const uint64_t ui64StimulationDate);

		protected:

			std::vector<OpenViBEToolkit::TStimulationDecoder<CVRPNButtonServer>*> m_vStimulationDecoders;

			//Start and end time of the last buffer
			uint64_t m_ui64StartTime;
			uint64_t m_ui64EndTime;

			uint32_t m_ui32CurrentInput;

			OpenViBE::CIdentifier m_oServerIdentifier;

			//Pairs of start/stop stimulations id
			std::map<uint32_t, std::pair<OpenViBE::uint64, uint64_t>> m_vStimulationPair;
		};

		class CVRPNButtonServerListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool check(OpenViBE::Kernel::IBox& rBox)
			{
				char l_sName[1024];
				uint32_t i;

				for (i = 0; i < rBox.getInputCount(); i++)
				{
					sprintf(l_sName, "Input %u", i + 1);
					rBox.setInputName(i, l_sName);
					rBox.setInputType(i, OV_TypeId_Stimulations);
				}

				for (i = 0; i < rBox.getInputCount(); i++)
				{
					sprintf(l_sName, "Button %u ON", i + 1);
					rBox.setSettingName(i * 2 + 1, l_sName);
					rBox.setSettingType(i * 2 + 1, OV_TypeId_Stimulation);

					sprintf(l_sName, "Button %u OFF", i + 1);
					rBox.setSettingName(i * 2 + 2, l_sName);
					rBox.setSettingType(i * 2 + 2, OV_TypeId_Stimulation);
				}

				return true;
			}

			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeSetting(ui32Index * 2 + 1);
				rBox.removeSetting(ui32Index * 2 + 1);

				return this->check(rBox);
			};

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
				rBox.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");

				return this->check(rBox);
			};

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CVRPNButtonServerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			OpenViBE::CString getName() const override { return OpenViBE::CString("Button VRPN Server"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Creates VRPN button servers (one per input)."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Creates VRPN button servers to make data from the plugin's inputs available to VRPN client applications."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO/VRPN"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_VRPNButtonServer; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CVRPNButtonServer(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CVRPNButtonServerListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input 1", OVTK_TypeId_Stimulations);
				rPrototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn");
				rPrototype.addSetting("Button 1 ON", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
				rPrototype.addSetting("Button 1 OFF", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_VRPNButtonServerDesc)
		};
	};
};

#endif // OVP_HAS_Vrpn

#endif // __OpenViBEPlugins_VRPN_CVRPNButtonServer_H__
