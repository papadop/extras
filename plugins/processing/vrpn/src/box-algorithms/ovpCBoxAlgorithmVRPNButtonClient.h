#ifndef __OpenViBEPlugins_BoxAlgorithm_VRPNButtonClient_H__
#define __OpenViBEPlugins_BoxAlgorithm_VRPNButtonClient_H__

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <list>
#include <vrpn_Button.h>

#define OVP_ClassId_BoxAlgorithm_VRPNButtonClient     OpenViBE::CIdentifier(0x40714327, 0x458877D2)
#define OVP_ClassId_BoxAlgorithm_VRPNButtonClientDesc OpenViBE::CIdentifier(0x16FB6283, 0x45EC313F)

namespace OpenViBEPlugins
{
	namespace VRPN
	{
		class CBoxAlgorithmVRPNButtonClient : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_VRPNButtonClient);

			std::list<std::pair<uint32_t, bool>> m_vButtonList;
			void setButton(uint32_t ui32ButtonIndex, bool bPressed);

		protected:

			uint64_t m_ui64LastChunkEndTime;
			bool m_bGotStimulation;
			std::vector<OpenViBE::Kernel::IAlgorithmProxy*> m_vStreamEncoder;
			std::vector<OpenViBE::IStimulationSet*> m_vStimulationSet;
			std::vector<uint64_t> m_vStimulationIdentifierOn;
			std::vector<uint64_t> m_vStimulationIdentifierOff;
			vrpn_Button_Remote* m_pVRPNButtonRemote;
		};

		class CBoxAlgorithmVRPNButtonClientListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			virtual bool check(OpenViBE::Kernel::IBox& rBox)
			{
				char l_sOutputName[1024];
				char l_sButtonOnName[1024];
				char l_sButtonOffName[1024];
				for (uint32_t i = 0; i < rBox.getOutputCount(); i++)
				{
					sprintf(l_sOutputName, "Output %i", i + 1);
					sprintf(l_sButtonOnName, "Button %i ON", i + 1);
					sprintf(l_sButtonOffName, "Button %i OFF", i + 1);
					rBox.setOutputName(i, l_sOutputName);
					rBox.setSettingName(i * 2 + 1, l_sButtonOnName);
					rBox.setSettingName(i * 2 + 2, l_sButtonOffName);
				}
				return true;
			}
			/*
						virtual bool initialize() { return true; }
						virtual bool uninitialize() { return true; }
						virtual bool onInitialized(OpenViBE::Kernel::IBox& rBox) { return true; };
						virtual bool onNameChanged(OpenViBE::Kernel::IBox& rBox) { return true; };
						virtual bool onIdentifierChanged(OpenViBE::Kernel::IBox& rBox) { return true; };
						virtual bool onAlgorithmClassIdentifierChanged(OpenViBE::Kernel::IBox& rBox) { return true; };
						virtual bool onProcessingUnitChanged(OpenViBE::Kernel::IBox& rBox) { return true; };
						virtual bool onInputConnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onInputDisconnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onInputNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onOutputConnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onOutputDisconnected(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
			*/
			bool onOutputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.setOutputType(ui32Index, OV_TypeId_Stimulations);
				rBox.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
				rBox.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");
				return this->check(rBox);
			}

			bool onOutputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeSetting(ui32Index * 2 + 2);
				rBox.removeSetting(ui32Index * 2 + 1);
				return this->check(rBox);
			}
			/*
						virtual bool onOutputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onOutputNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onSettingRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onSettingTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onSettingNameChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onSettingDefaultValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
						virtual bool onSettingValueChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) { return true; };
			*/

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmVRPNButtonClientDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Button VRPN Client"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Connects to an external VRPN device and translate a button information into OpenViBE stimulations"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("-"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO/VRPN"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_VRPNButtonClient; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmVRPNButtonClient; }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmVRPNButtonClientListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				// rBoxAlgorithmPrototype.addInput  ("input name", /* input type (OV_TypeId_Signal) */);
				rBoxAlgorithmPrototype.addOutput("Output", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn@localhost");
				rBoxAlgorithmPrototype.addSetting("Button 1 ON", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
				rBoxAlgorithmPrototype.addSetting("Button 1 OFF", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");
				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddOutput);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_VRPNButtonClientDesc);
		};
	};
};
#endif // TARGET_HAS_ThirdPartyVRPN

#endif // __OpenViBEPlugins_BoxAlgorithm_VRPNButtonClient_H__
