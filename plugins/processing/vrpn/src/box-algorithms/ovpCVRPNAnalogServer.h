#ifndef __OpenViBEPlugins_VRPN_CVRPNAnalogServer_H__
#define __OpenViBEPlugins_VRPN_CVRPNAnalogServer_H__

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <cstdio>
#include <map>

namespace OpenViBEPlugins
{
	namespace VRPN
	{
		class CVRPNAnalogServer : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CVRPNAnalogServer();
			void release() override { delete this; }
			uint64_t getClockFrequency() override { return 64LL << 32; } // 64hz
			bool initialize() override;
			bool uninitialize() override;
			bool processClock(OpenViBE::Kernel::IMessageClock& rMessageClock) override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_VRPNAnalogServer)

		protected:

			OpenViBE::CIdentifier m_oServerIdentifier;
			bool m_bAnalogSet;

			std::map<uint32_t, OpenViBE::Kernel::IAlgorithmProxy*> m_vStreamDecoder;
			std::map<uint32_t, uint32_t> m_vAnalogCount;
		};

		class CVRPNAnalogServerListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool check(OpenViBE::Kernel::IBox& rBox)
			{
				char l_sName[1024];
				uint32_t i;

				for (i = 0; i < rBox.getInputCount(); i++)
				{
					sprintf(l_sName, "Input %u", i + 1);
					rBox.setInputName(i, l_sName);
					rBox.setInputType(i, OV_TypeId_StreamedMatrix);
				}

				return true;
			}

			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				return this->check(rBox);
			}

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				return this->check(rBox);
			};

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CVRPNAnalogServerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			OpenViBE::CString getName() const override { return OpenViBE::CString("Analog VRPN Server"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Bruno Renier/Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Creates VRPN analog servers (one per input)."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Creates VRPN analog servers to make data from the plugin's inputs available to VRPN client applications."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO/VRPN"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_VRPNAnalogServer; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CVRPNAnalogServer(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CVRPNAnalogServerListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input 1", OV_TypeId_StreamedMatrix);
				rPrototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn");
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_VRPNAnalogServerDesc)
		};
	};
};

#endif // OVP_HAS_Vrpn

#endif // __OpenViBEPlugins_VRPN_CVRPNAnalogServer_H__
