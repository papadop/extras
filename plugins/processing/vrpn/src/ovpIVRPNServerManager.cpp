#if defined TARGET_HAS_ThirdPartyVRPN

#include "ovpIVRPNServerManager.h"

#include <vrpn_Connection.h>
#include <vrpn_Analog.h>
#include <vrpn_Button.h>

#include <vector>
#include <string>
#include <map>
#include <iostream>

using namespace OpenViBE;
using namespace Plugins;
using namespace Kernel;
using namespace OpenViBEPlugins;
using namespace VRPN;
using namespace std;


namespace OpenViBEPlugins
{
	namespace VRPN
	{
		namespace
		{
			class CVRPNServerManager : public IVRPNServerManager
			{
			public:

				CVRPNServerManager();
				virtual ~CVRPNServerManager();

				bool initialize() override;
				bool uninitialize() override;

				bool process() override;
				bool reportAnalog(
					const CIdentifier& rServerIdentifier) override;
				bool reportButton(
					const CIdentifier& rServerIdentifier) override;

				bool addServer(
					const CString& sServerName,
					CIdentifier& rServerIdentifier) override;
				bool isServer(
					const CIdentifier& rServerIdentifier) const override;
				bool isServer(
					const CString& sServerName) const override;
				bool getServerIdentifier(
					const CString& sServerName,
					CIdentifier& rServerIdentifier) const override;
				bool getServerName(
					const CIdentifier& rServerIdentifier,
					CString& sServerName) const override;
				bool removeServer(
					const CIdentifier& rServerIdentifier) override;

				bool setButtonCount(
					const CIdentifier& rServerIdentifier,
					const uint32_t ui32ButtonCount) override;
				bool setButtonState(
					const CIdentifier& rServerIdentifier,
					const uint32_t ui32ButtonIndex,
					const bool bButtonStatus) override;
				bool getButtonState(
					const CIdentifier& rServerIdentifier,
					const uint32_t ui32ButtonIndex) const override;

				bool setAnalogCount(
					const CIdentifier& rServerIdentifier,
					const uint32_t ui32AnalogCount) override;
				bool setAnalogState(
					const CIdentifier& rServerIdentifier,
					const uint32_t ui32AnalogIndex,
					const double f64AnalogStatus) override;
				double getAnalogState(
					const CIdentifier& rServerIdentifier,
					const uint32_t ui32AnalogIndex) const override;

			protected:

				vrpn_Connection* m_pConnection;

				map<CIdentifier, CString> m_vServerName;
				map<CIdentifier, vrpn_Button_Server*> m_vButtonServer;
				map<CIdentifier, vrpn_Analog_Server*> m_vAnalogServer;
				map<CIdentifier, vector<bool>> m_vButtonCache;

				uint32_t m_ui32InitializeCount;
			};
		};

		IVRPNServerManager& IVRPNServerManager::getInstance()
		{
			static CVRPNServerManager g_oVRPNServerManager;
			return g_oVRPNServerManager;
		}
	};
};

CVRPNServerManager::CVRPNServerManager()
	: m_pConnection(nullptr)
	  , m_ui32InitializeCount(0) {}

CVRPNServerManager::~CVRPNServerManager() {}

bool CVRPNServerManager::initialize()
{
	if (!m_ui32InitializeCount)
	{
		//m_pConnection=new vrpn_Connection;
		m_pConnection = vrpn_create_server_connection();
	}
	m_ui32InitializeCount++;
	return true;
}

bool CVRPNServerManager::uninitialize()
{
	m_ui32InitializeCount--;
	if (!m_ui32InitializeCount)
	{
		map<CIdentifier, vrpn_Analog_Server*>::iterator itAnalogServer;
		for (itAnalogServer = m_vAnalogServer.begin(); itAnalogServer != m_vAnalogServer.end(); ++itAnalogServer)
		{
			if (itAnalogServer->second)
			{
				delete itAnalogServer->second;
				itAnalogServer->second = nullptr;
			}
		}
		m_vAnalogServer.clear();
		map<CIdentifier, vrpn_Button_Server*>::iterator itButtonServer;
		for (itButtonServer = m_vButtonServer.begin(); itButtonServer != m_vButtonServer.end(); ++itButtonServer)
		{
			if (itButtonServer->second)
			{
				delete itButtonServer->second;
				itButtonServer->second = nullptr;
			}
		}
		m_vButtonServer.clear();

		// $$$ UGLY !
		// The following function should destroy correctly the connection, but does not.
		//vrpn_ConnectionManager::instance().deleteConnection(m_pConnection);
		delete m_pConnection;
		m_pConnection = nullptr;
	}
	return true;
}

bool CVRPNServerManager::process()
{
	map<CIdentifier, vrpn_Analog_Server*>::iterator itAnalogServer;
	for (itAnalogServer = m_vAnalogServer.begin(); itAnalogServer != m_vAnalogServer.end(); ++itAnalogServer)
	{
		if (itAnalogServer->second)
		{
			itAnalogServer->second->mainloop();
		}
	}

	map<CIdentifier, vrpn_Button_Server*>::iterator itButtonServer;
	for (itButtonServer = m_vButtonServer.begin(); itButtonServer != m_vButtonServer.end(); ++itButtonServer)
	{
		if (itButtonServer->second)
		{
			itButtonServer->second->mainloop();
		}
	}

	if (m_pConnection)
	{
		m_pConnection->mainloop();
	}
	return true;
}

bool CVRPNServerManager::reportAnalog(
	const CIdentifier& rServerIdentifier)
{
	map<CIdentifier, vrpn_Analog_Server*>::iterator itAnalogServer = m_vAnalogServer.find(rServerIdentifier);
	if (itAnalogServer != m_vAnalogServer.end())
	{
		if (itAnalogServer->second)
		{
			// This is public function and mainloop won't call it for me
			// Thank you VRPN for this to be similar to button behavior ;o)
			itAnalogServer->second->report();
		}
	}
	return true;
}

bool CVRPNServerManager::reportButton(
	const CIdentifier& rServerIdentifier)
{
	map<CIdentifier, vrpn_Button_Server*>::iterator itButtonServer = m_vButtonServer.find(rServerIdentifier);
	if (itButtonServer != m_vButtonServer.end())
	{
		if (itButtonServer->second)
		{
			// This is not public function, however, mainloop calls it for me
			// Thank you VRPN for this to be similar to analog behavior ;o)
			// itButtonServer->second->report_changes();
		}
	}
	return true;
}

bool CVRPNServerManager::addServer(
	const CString& sServerName,
	CIdentifier& rServerIdentifier)
{
	if (this->isServer(sServerName))
	{
		return this->getServerIdentifier(sServerName, rServerIdentifier);
	}

	rServerIdentifier = CIdentifier::random();
	while (m_vServerName.find(rServerIdentifier) != m_vServerName.end())
	{
		++rServerIdentifier;
	}

	m_vServerName[rServerIdentifier] = sServerName;
	return true;
}

bool CVRPNServerManager::isServer(
	const CIdentifier& rServerIdentifier) const
{
	return m_vServerName.find(rServerIdentifier) != m_vServerName.end();
}

bool CVRPNServerManager::isServer(
	const CString& sServerName) const
{
	map<CIdentifier, CString>::const_iterator itServerName;
	for (itServerName = m_vServerName.begin(); itServerName != m_vServerName.end(); ++itServerName)
	{
		if (itServerName->second == sServerName)
		{
			return true;
		}
	}
	return false;
}

bool CVRPNServerManager::getServerIdentifier(
	const CString& sServerName,
	CIdentifier& rServerIdentifier) const
{
	map<CIdentifier, CString>::const_iterator itServerName;
	for (itServerName = m_vServerName.begin(); itServerName != m_vServerName.end(); ++itServerName)
	{
		if (itServerName->second == sServerName)
		{
			rServerIdentifier = itServerName->first;
			return true;
		}
	}
	return false;
}

bool CVRPNServerManager::getServerName(
	const CIdentifier& rServerIdentifier,
	CString& sServerName) const
{
	map<CIdentifier, CString>::const_iterator itServerName = m_vServerName.find(rServerIdentifier);
	if (itServerName == m_vServerName.end())
	{
		return false;
	}
	sServerName = itServerName->second;
	return true;
}

bool CVRPNServerManager::removeServer(
	const CIdentifier& rServerIdentifier)
{
	if (!this->isServer(rServerIdentifier))
	{
		return false;
	}

	// TODO
	return true;
}

bool CVRPNServerManager::setButtonCount(
	const CIdentifier& rServerIdentifier,
	const uint32_t ui32ButtonCount)
{
	if (!this->isServer(rServerIdentifier))
	{
		return false;
	}
	delete m_vButtonServer[rServerIdentifier];
	m_vButtonServer[rServerIdentifier] = new vrpn_Button_Server(m_vServerName[rServerIdentifier], m_pConnection, ui32ButtonCount);
	m_vButtonCache[rServerIdentifier].clear();
	m_vButtonCache[rServerIdentifier].resize(ui32ButtonCount);
	return true;
}

bool CVRPNServerManager::setButtonState(
	const CIdentifier& rServerIdentifier,
	const uint32_t ui32ButtonIndex,
	const bool bButtonStatus)
{
	if (!this->isServer(rServerIdentifier))
	{
		return false;
	}
	if (m_vButtonServer.find(rServerIdentifier) == m_vButtonServer.end())
	{
		return false;
	}
	m_vButtonServer[rServerIdentifier]->set_button(ui32ButtonIndex, bButtonStatus ? 1 : 0);
	m_vButtonCache[rServerIdentifier][ui32ButtonIndex] = bButtonStatus;
	return true;
}

bool CVRPNServerManager::getButtonState(
	const CIdentifier& rServerIdentifier,
	const uint32_t ui32ButtonIndex) const
{
	if (!this->isServer(rServerIdentifier))
	{
		return false;
	}
	map<CIdentifier, vrpn_Button_Server*>::const_iterator itButtonServer = m_vButtonServer.find(rServerIdentifier);
	if (itButtonServer == m_vButtonServer.end())
	{
		return false;
	}
	map<CIdentifier, vector<bool>>::const_iterator itButtonCache = m_vButtonCache.find(rServerIdentifier);
	return itButtonCache->second[ui32ButtonIndex];
}

bool CVRPNServerManager::setAnalogCount(
	const CIdentifier& rServerIdentifier,
	const uint32_t ui32AnalogCount)
{
	if (!this->isServer(rServerIdentifier))
	{
		return false;
	}
	if (m_vAnalogServer[rServerIdentifier])
	{
		delete m_vAnalogServer[rServerIdentifier];
	}
	m_vAnalogServer[rServerIdentifier] = new vrpn_Analog_Server(m_vServerName[rServerIdentifier], m_pConnection);
	if ((uint32_t)m_vAnalogServer[rServerIdentifier]->setNumChannels(ui32AnalogCount) != ui32AnalogCount)
	{
		return false;
	}
	return true;
}

bool CVRPNServerManager::setAnalogState(
	const CIdentifier& rServerIdentifier,
	const uint32_t ui32AnalogIndex,
	const double f64AnalogStatus)
{
	if (!this->isServer(rServerIdentifier))
	{
		return false;
	}
	if (m_vAnalogServer.find(rServerIdentifier) == m_vAnalogServer.end())
	{
		return false;
	}
	uint32_t l_u32numChannels = m_vAnalogServer[rServerIdentifier]->getNumChannels();
	if (ui32AnalogIndex >= l_u32numChannels)
	{
		return false;
	}

	m_vAnalogServer[rServerIdentifier]->channels()[ui32AnalogIndex] = f64AnalogStatus;
	return true;
}

double CVRPNServerManager::getAnalogState(
	const CIdentifier& rServerIdentifier,
	const uint32_t ui32AnalogIndex) const
{
	if (!this->isServer(rServerIdentifier))
	{
		return 0;
	}
	map<CIdentifier, vrpn_Analog_Server*>::const_iterator itAnalogServer = m_vAnalogServer.find(rServerIdentifier);
	if (itAnalogServer == m_vAnalogServer.end())
	{
		return 0;
	}
	return itAnalogServer->second->channels()[ui32AnalogIndex];
}

#endif // OVP_HAS_Vrpn
