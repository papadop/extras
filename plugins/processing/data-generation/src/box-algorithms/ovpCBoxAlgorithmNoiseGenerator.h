#ifndef __SamplePlugin_CNoiseGenerator_H__
#define __SamplePlugin_CNoiseGenerator_H__

#include "../ovp_defines.h"

#include <random>

#include <toolkit/ovtk_all.h>

#define OVP_TypeId_NoiseType                                                    OpenViBE::CIdentifier(0x2E85E95E, 0x8A1A8365)
#define OVP_TypeId_NoiseType_Uniform                                            OpenViBE::CIdentifier(0x00000000, 0x00000001)
#define OVP_TypeId_NoiseType_Gaussian                                           OpenViBE::CIdentifier(0x00000000, 0x00000002)

namespace OpenViBEPlugins
{
	namespace DataGeneration
	{
		class CNoiseGenerator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CNoiseGenerator();

			void release() override;

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;

			bool processClock(OpenViBE::Kernel::IMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_NoiseGenerator)

		protected:


			OpenViBEToolkit::TSignalEncoder<CNoiseGenerator> m_oSignalEncoder;

			bool m_bHeaderSent;
			uint64_t m_ui64ChannelCount;
			uint64_t m_ui64SamplingFrequency;
			uint64_t m_ui64GeneratedEpochSampleCount;
			uint64_t m_ui64SentSampleCount;
			uint64_t m_ui64NoiseType;

			std::mt19937 m_oGenerator;

			std::normal_distribution<> m_oNormalDistribution{ 0.0, 1.0 };         // Mean=0, Var=1
			std::uniform_real_distribution<> m_oUniformDistribution{ 0.0, 1.0 };  // Min=0, Max=1
		};

		class CNoiseGeneratorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Noise generator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Simple random noise generator"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Generates uniform or Gaussian random data"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Data generation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_NoiseGenerator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CNoiseGenerator(); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addOutput("Generated signal", OV_TypeId_Signal);

				rPrototype.addSetting("Channel count", OV_TypeId_Integer, "4");
				rPrototype.addSetting("Sampling frequency", OV_TypeId_Integer, "512");
				rPrototype.addSetting("Generated epoch sample count", OV_TypeId_Integer, "32");
				rPrototype.addSetting("Noise type", OVP_TypeId_NoiseType, OVP_TypeId_NoiseType_Uniform.toString());

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_NoiseGeneratorDesc)
		};
	};
};

#endif // __SamplePlugin_CNoiseGenerator_H__
