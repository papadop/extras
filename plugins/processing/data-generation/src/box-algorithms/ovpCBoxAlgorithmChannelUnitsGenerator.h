#ifndef __OpenViBEPlugins_BoxAlgorithm_CChannelUnitsGenerator_H__
#define __OpenViBEPlugins_BoxAlgorithm_CChannelUnitsGenerator_H__

#include "../ovp_defines.h"
#include <toolkit/ovtk_all.h>
#include <cstdio>

#define OVP_ClassId_ChannelUnitsGenerator             OpenViBE::CIdentifier(0x42B09186, 0x582C8422)
#define OVP_ClassId_ChannelUnitsGeneratorDesc         OpenViBE::CIdentifier(0x4901A752, 0xD8578577)

namespace OpenViBEPlugins
{
	namespace DataGeneration
	{
		class CChannelUnitsGenerator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override;
			uint64_t getClockFrequency() override { return 1LL << 32; }
			bool initialize() override;
			bool uninitialize() override;

			bool processClock(OpenViBE::Kernel::IMessageClock& /* rMessageClock */) override;
			bool process() override;

			bool m_bHeaderSent;
			uint64_t m_ui64ChannelCount;
			uint64_t m_ui64Unit;
			uint64_t m_ui64Factor;

			OpenViBEToolkit::TChannelUnitsEncoder<CChannelUnitsGenerator> m_oEncoder;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_ChannelUnitsGenerator)
		};

		class CChannelUnitsGeneratorListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CChannelUnitsGeneratorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Channel units generator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Generates channel units"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box can generate a channel unit stream if specific measurement units are needed. The box is mainly provided for completeness."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Data generation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_ChannelUnitsGenerator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CChannelUnitsGenerator(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CChannelUnitsGeneratorListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addSetting("Number of channels", OV_TypeId_Integer, "4");
				rPrototype.addSetting("Unit", OV_TypeId_MeasurementUnit, "V");
				rPrototype.addSetting("Factor", OV_TypeId_Factor, "1e-06");

				rPrototype.addOutput("Channel units", OV_TypeId_ChannelUnits);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_ChannelUnitsGeneratorDesc)
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_CChannelUnitsGenerator_H__
