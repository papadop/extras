#ifndef __SamplePlugin_CSinusSignalGenerator_H__
#define __SamplePlugin_CSinusSignalGenerator_H__

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace DataGeneration
	{
		class CSinusSignalGenerator : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CSinusSignalGenerator();

			void release() override;

			uint64_t getClockFrequency() override;
			bool initialize() override;
			bool uninitialize() override;

			bool processClock(OpenViBE::Kernel::IMessageClock& rMessageClock) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_SinusSignalGenerator)

		protected:

			OpenViBEToolkit::TSignalEncoder<CSinusSignalGenerator> m_oSignalEncoder;

			bool m_bHeaderSent;
			uint32_t m_ui32ChannelCount;
			uint32_t m_ui32SamplingFrequency;
			uint32_t m_ui32GeneratedEpochSampleCount;
			uint32_t m_ui32SentSampleCount;
		};

		class CSinusSignalGeneratorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Sinus oscillator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Simple sinus signal generator"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Data generation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_SinusSignalGenerator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CSinusSignalGenerator(); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addOutput("Generated signal", OV_TypeId_Signal);

				rPrototype.addSetting("Channel count", OV_TypeId_Integer, "4");
				rPrototype.addSetting("Sampling frequency", OV_TypeId_Integer, "512");
				rPrototype.addSetting("Generated epoch sample count", OV_TypeId_Integer, "32");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_SinusSignalGeneratorDesc)
		};
	};
};

#endif // __SamplePlugin_CSinusSignalGenerator_H__
