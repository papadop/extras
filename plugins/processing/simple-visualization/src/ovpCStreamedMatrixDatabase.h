#ifndef __OpenViBEPlugins_SimpleVisualization_CStreamedMatrixDatabase_H__
#define __OpenViBEPlugins_SimpleVisualization_CStreamedMatrixDatabase_H__

#include "ovp_defines.h"
#include "ovpIStreamDatabase.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>
#include <deque>
#include <queue>
#include <string>
#include <cfloat>
#include <iostream>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		/**
		* This class is used to store information about the incoming matrix stream. It can request a IStreamDisplayDrawable
		* object to redraw itself upon changes in its data.
		*/
		class CStreamedMatrixDatabase : public IStreamDatabase
		{
		public:
			/**
			 * \brief Constructor
			 * \param
			 */
			CStreamedMatrixDatabase(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>& oPlugin);

			/**
			 * \brief Destructor
			 */
			virtual ~CStreamedMatrixDatabase();

			bool initialize() override;

			void setDrawable(IStreamDisplayDrawable* pDrawable) override;
			void setRedrawOnNewData(bool bRedrawOnNewData) override;

			bool isFirstBufferReceived() override;
			bool setMaxBufferCount(uint32_t ui32MaxBufferCount) override;
			bool setTimeScale(double f64TimeScale) override;
			bool decodeMemoryBuffer(const OpenViBE::IMemoryBuffer* pMemoryBuffer, uint64_t ui64StartTime, uint64_t ui64EndTime) override;

			uint32_t getMaxBufferCount() override;
			uint32_t getCurrentBufferCount() override;

			const double* getBuffer(uint32_t ui32BufferIndex) override;

			uint64_t getStartTime(uint32_t ui32BufferIndex) override;
			uint64_t getEndTime(uint32_t ui32BufferIndex) override;

			uint32_t getBufferElementCount() override;
			uint64_t getBufferDuration() override;
			bool isBufferTimeStepComputed() override;
			uint64_t getBufferTimeStep() override;

			uint32_t getSampleCountPerBuffer() override;
			uint32_t getChannelCount() override;

			bool getChannelLabel(const uint32_t ui32ChannelIndex, OpenViBE::CString& rElectrodeLabel) override;
			bool getChannelMinMaxValues(uint32_t ui32Channel, double& f64Min, double& f64Max) override;
			bool getGlobalMinMaxValues(double& f64Min, double& f64Max) override;
			bool getLastBufferChannelMinMaxValues(uint32_t ui32Channel, double& f64Min, double& f64Max) override;
			bool getLastBufferGlobalMinMaxValues(double& f64Min, double& f64Max) override;

		protected:
			bool onBufferCountChanged();

			virtual bool decodeHeader();

			virtual bool decodeBuffer(
				uint64_t ui64StartTime,
				uint64_t ui64EndTime);

		protected:
			// parent plugin
			OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>& m_oParentPlugin;
			//decoder algorithm
			OpenViBE::Kernel::IAlgorithmProxy* m_pDecoder;
			//drawable object to update (if needed)
			IStreamDisplayDrawable* m_pDrawable;
			//flag stating whether to redraw the IStreamDisplayDrawable upon new data reception if true (default)
			bool m_bRedrawOnNewData;
			//flag stating whether first samples buffer has been received
			bool m_bFirstBufferReceived;
			//flag stating whether buffer time step was computed
			bool m_bBufferTimeStepComputed;
			//time difference between start times of two consecutive buffers
			uint64_t m_ui64BufferTimeStep;
			//maximum number of buffers stored in database
			uint32_t m_ui32MaxBufferCount;
			//flag stating whether time scale should be ignored (max buffer count externally set)
			bool m_bIgnoreTimeScale;
			//maximum duration of displayed buffers (in seconds)
			double m_f64TimeScale;
			//double-linked list of start times of stored buffers
			std::deque<uint64_t> m_oStartTime;
			//double-linked list of end times of stored buffers
			std::deque<uint64_t> m_oEndTime;
			//streamed matrix header
			OpenViBE::CMatrix m_oStreamedMatrixHeader;
			//streamed matrix	history
			std::deque<OpenViBE::CMatrix*> m_oStreamedMatrices;
			//min/max values for each channel
			std::vector<std::deque<std::pair<double, double>>> m_oChannelMinMaxValues;
		};
	}
}

#endif //#ifndef __OpenViBEPlugins_SimpleVisualization_CStreamedMatrixDatabase_H__
