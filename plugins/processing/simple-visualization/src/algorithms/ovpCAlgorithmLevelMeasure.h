#ifndef __OpenViBEPlugins_Algorithm_LevelMeasure_H__
#define __OpenViBEPlugins_Algorithm_LevelMeasure_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gtk/gtk.h>
#include <vector>
#include <map>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		class CAlgorithmLevelMeasure : public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_LevelMeasure);

		protected:

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pMatrix;
			OpenViBE::Kernel::TParameterHandler<GtkWidget*> op_pMainWidget;
			OpenViBE::Kernel::TParameterHandler<GtkWidget*> op_pToolbarWidget;

			GtkBuilder* m_pMainWidgetInterface;
			GtkBuilder* m_pToolbarWidgetInterface;
			GtkWidget* m_pMainWindow;
			GtkWidget* m_pToolbarWidget;

		public:

			typedef struct
			{
				GtkProgressBar* m_pProgressBar;
				uint32_t m_ui32Score;
				bool m_bLastWasOverThreshold;
			} SProgressBar;

			std::vector<SProgressBar> m_vProgressBar;
			bool m_bShowPercentages;
			double m_f64Threshold;
		};

		class CAlgorithmLevelMeasureDesc : public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Level measure"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Displays sample chunk of each channel as a row of progress bars"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Another way to look at it: Each displayed row is a histogram normalized to sum to 1"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Simple visualization"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName() const { return OpenViBE::CString("gtk-go-up"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_LevelMeasure; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CAlgorithmLevelMeasure; }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmPrototype) const override
			{
				rAlgorithmPrototype.addInputParameter(OVP_Algorithm_LevelMeasure_InputParameterId_Matrix, "Matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_LevelMeasure_OutputParameterId_MainWidget, "Main widget", OpenViBE::Kernel::ParameterType_Pointer);
				rAlgorithmPrototype.addOutputParameter(OVP_Algorithm_LevelMeasure_OutputParameterId_ToolbarWidget, "Toolbar widget", OpenViBE::Kernel::ParameterType_Pointer);
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_LevelMeasure_InputTriggerId_Reset, "Reset");
				rAlgorithmPrototype.addInputTrigger(OVP_Algorithm_LevelMeasure_InputTriggerId_Refresh, "Refresh");
				rAlgorithmPrototype.addOutputTrigger(OVP_Algorithm_LevelMeasure_OutputTriggerId_Refreshed, "Refreshed");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_LevelMeasureDesc);
		};
	};
};

#endif // __OpenViBEPlugins_Algorithm_LevelMeasure_H__
