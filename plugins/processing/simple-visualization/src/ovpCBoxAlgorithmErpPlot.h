#ifndef __OpenViBEPlugins_BoxAlgorithm_ErpPlot_H__
#define __OpenViBEPlugins_BoxAlgorithm_ErpPlot_H__

//You may have to change this path to match your folder organisation
#include "ovp_defines.h"


#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <cairo.h>
#include <list>
#include <vector>
#include <iostream>
#include <cmath>
#include <system/ovCMemory.h>
#include <utility>
#include <stdio.h>
#include <string.h>
#include <cfloat>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		class Graph
		{
		public:
			//should be a list of colors;
			Graph(std::vector<GdkColor>& lineColor, std::vector<OpenViBE::CString>& lineText, int rowIndex, int colIndex, int curveSize) : m_cLineColor(lineColor), m_sLineText(lineText)
			{
				m_vCurves.resize(lineColor.size());
				m_vVariance.resize(lineColor.size());
				for (unsigned int i = 0; i < m_vCurves.size(); i++)
				{
					m_vCurves[i].resize(curveSize, 0);
					m_vVariance[i].resize(curveSize, 0);
				}
				this->rowIndex = rowIndex;
				this->colIndex = colIndex;
				this->curveSize = curveSize;
				pointCounter = new int[lineColor.size()];
				m_pMaximum = -FLT_MAX;
				m_pMinimum = FLT_MAX;
				for (unsigned int i = 0; i < lineColor.size(); i++)
				{
					pointCounter[i] = 0;
				}
			}

			~Graph()
			{
				delete [] pointCounter;
				m_vCurves.clear();
				m_vVariance.clear();
			}

			void resizeAxis(gint width, gint height, size_t nrOfGraphs);
			void draw(GtkWidget* widget);
			void drawAxis(cairo_t* cairoContext);
			void drawLine(cairo_t* cairoContext, double* Xo, double* Yo, double* Xe, double* Ye);
			void drawAxisLabels(cairo_t* cairoContext);
			void drawCurves(cairo_t* cairoContext);
			void drawLegend(cairo_t* cairoContext);
			void drawVar(cairo_t* cairoContext);
			void updateCurves(const double* curve, size_t howMany, unsigned int curveIndex);
			void snapCoords(cairo_t* cairoContext, double* x, double* y);
			double adjustValueToScale(double value);

			std::vector<std::vector<double>> m_vCurves; //private
			std::vector<std::vector<double>> m_vVariance;

			std::vector<GdkColor>& m_cLineColor; //private
			std::vector<OpenViBE::CString>& m_sLineText; //private
			double m_pMaximum = 1;
			double m_pMinimum = -1;
			std::pair<int, int> m_pArgMaximum;
			std::pair<int, int> m_pArgMinimum;
			double m_dGraphWidth;
			double m_dGraphHeight;
			double m_dGraphOriginX;
			double m_dGraphOriginY;
			int rowIndex; //private
			int colIndex; //private 
			int curveSize; //private
			int* pointCounter;
			uint64_t StartTime;

			uint64_t EndTime;

			double fontSize;
		};

		/**
		 * \class CBoxAlgorithmErpPlot
		 * \author Dieter Devlaminck (INRIA)
		 * \date Fri Nov 16 10:50:43 2012
		 * \brief The class CBoxAlgorithmErpPlot describes the box ERP plot.
		 *
		 */
		class CBoxAlgorithmErpPlot : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;
			virtual bool save();
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_ErpPlot);

		protected:
			OpenViBE::CString m_sFigureFileName;
			std::vector<GdkColor> m_oLegendColors;
			std::vector<OpenViBE::CString> m_oLegend;
			GtkWidget* m_pDrawWindow;
			std::list<Graph*>* m_lGraphList;
			bool m_bFirstHeaderReceived;
			std::vector<OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmErpPlot>*> m_vDecoders;
			std::vector<OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmErpPlot>*> m_vVarianceDecoders;
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmErpPlot>* m_oStimulationDecoder;
			uint64_t m_sTriggerToSave;
			bool m_bXStartsAt0;

		private:
			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};		
		
		// The box listener can be used to call specific callbacks whenever the box structure changes : input added, name changed, etc.
		// Please uncomment below the callbacks you want to use.
		class CBoxAlgorithmErpPlotListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.setInputType(ui32Index, OV_TypeId_StreamedMatrix);
				uint32_t l_ui32Class = ui32Index / 2 + 1;
				char l_sIndex[21];
				sprintf(l_sIndex, "%d", l_ui32Class);


				char l_pInputLabel[25] = "ERP";
				rBox.setInputName(ui32Index, strcat(l_pInputLabel, l_sIndex));


				char l_pColorLabel[25] = "Line color ";
				char l_pTextLabel[25] = "Line label ";
				rBox.addSetting(strcat(l_pColorLabel, l_sIndex),OV_TypeId_Color, "0,0,0");
				rBox.addSetting(strcat(l_pTextLabel, l_sIndex),OV_TypeId_String, "curve");
				//add the corresponding variance input
				char l_pVarianceLabel[25] = "Variance";
				rBox.addInput(strcat(l_pVarianceLabel, l_sIndex), OV_TypeId_StreamedMatrix);

				return true;
			};

			bool onInputRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				rBox.removeSetting(ui32Index * 2 + 2);
				rBox.removeSetting(ui32Index * 2 + 1);
				return true;
			};

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};
		

		/**
		 * \class CBoxAlgorithmErpPlotDesc
		 * \author Dieter Devlaminck (INRIA)
		 * \date Fri Nov 16 10:50:43 2012
		 * \brief Descriptor of the box ERP plot.
		 *
		 */
		class CBoxAlgorithmErpPlotDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("ERP plot"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Dieter Devlaminck"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Plots event-related potentials"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("plots target ERP versus non-target ERP"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Visualization/Presentation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString(""); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ErpPlot; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmErpPlot; }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmErpPlotListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Trigger",OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("ERP1",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addInput("Variance1",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddInput);

				rBoxAlgorithmPrototype.addSetting("Filename final figure",OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Trigger to save figure",OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStop");
				rBoxAlgorithmPrototype.addSetting("X starts at 0",OV_TypeId_Boolean, "true");
				rBoxAlgorithmPrototype.addSetting("Line color 1",OV_TypeId_Color, "0,0,0");
				rBoxAlgorithmPrototype.addSetting("Line label 1",OV_TypeId_String, "curve 1");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ErpPlotDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_ErpPlot_H__
