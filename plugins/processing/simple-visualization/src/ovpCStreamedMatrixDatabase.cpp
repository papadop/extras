#include <system/ovCMemory.h>
#include <algorithm>
#include <cmath>

#include "ovpCStreamedMatrixDatabase.h"

#include <openvibe/ovITimeArithmetics.h>

using namespace OpenViBE;
using namespace Plugins;
using namespace Kernel;

using namespace OpenViBEPlugins;
using namespace SimpleVisualization;

using namespace OpenViBEToolkit;

using namespace std;

CStreamedMatrixDatabase::CStreamedMatrixDatabase(TBoxAlgorithm<IBoxAlgorithm>& oPlugin) :
	m_oParentPlugin(oPlugin),
	m_pDecoder(nullptr),
	m_pDrawable(nullptr),
	m_bRedrawOnNewData(true),
	m_bFirstBufferReceived(false),
	m_bBufferTimeStepComputed(false),
	m_ui64BufferTimeStep(0),
	m_ui32MaxBufferCount(2), //store at least 2 buffers so that it is possible to determine whether they overlap
	m_bIgnoreTimeScale(false),
	m_f64TimeScale(10)
//m_bError(false)
{}

CStreamedMatrixDatabase::~CStreamedMatrixDatabase()
{
	if (m_pDecoder != nullptr)
	{
		m_pDecoder->uninitialize();
		m_oParentPlugin.getAlgorithmManager().releaseAlgorithm(*m_pDecoder);
	}

	while (!m_oStreamedMatrices.empty())
	{
		delete m_oStreamedMatrices.front();
		m_oStreamedMatrices.pop_front();
	}
}

bool CStreamedMatrixDatabase::initialize()
{
	if (m_pDecoder != nullptr)
	{
		return false;
	}

	m_pDecoder = &m_oParentPlugin.getAlgorithmManager().getAlgorithm(
		m_oParentPlugin.getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixStreamDecoder));

	m_pDecoder->initialize();

	return true;
}

void CStreamedMatrixDatabase::setDrawable(IStreamDisplayDrawable* pDrawable)
{
	m_pDrawable = pDrawable;
}

void CStreamedMatrixDatabase::setRedrawOnNewData(bool bRedrawOnNewData)
{
	m_bRedrawOnNewData = bRedrawOnNewData;
}

bool CStreamedMatrixDatabase::isFirstBufferReceived()
{
	return m_bFirstBufferReceived;
}

bool CStreamedMatrixDatabase::setMaxBufferCount(uint32_t ui32MaxBufferCount)
{
	//max buffer count computed directly
	m_bIgnoreTimeScale = true;

	m_ui32MaxBufferCount = ui32MaxBufferCount;

	onBufferCountChanged();

	return true;
}

bool CStreamedMatrixDatabase::setTimeScale(double f64TimeScale)
{
	if (f64TimeScale <= 0)
	{
		return false;
	}

	//max buffer count computed from time scale
	m_bIgnoreTimeScale = false;

	//update time scale
	m_f64TimeScale = f64TimeScale;

	//if step between buffers is not known yet, this method will have to be called again later
	if (m_bBufferTimeStepComputed == false)
	{
		return false;
	}

	//compute maximum number of buffers needed to cover time scale
	uint32_t l_ui32MaxBufferCount = 0;

	if (m_ui64BufferTimeStep > 0)
	{
		uint64_t l_ui64TimeScale = ITimeArithmetics::secondsToTime(m_f64TimeScale);
		l_ui32MaxBufferCount = (uint32_t)(ceil((double)l_ui64TimeScale / m_ui64BufferTimeStep));
	}

	//display at least one buffer
	if (l_ui32MaxBufferCount == 0)
	{
		l_ui32MaxBufferCount = 1;
	}

	//acknowledge maximum buffer count
	bool l_bMaxBufferCountChanged = false;
	if (l_ui32MaxBufferCount != m_ui32MaxBufferCount)
	{
		m_ui32MaxBufferCount = l_ui32MaxBufferCount;
		l_bMaxBufferCountChanged = true;
		onBufferCountChanged();
	}

	return l_bMaxBufferCountChanged;
}

bool CStreamedMatrixDatabase::onBufferCountChanged()
{
	//if new number of buffers is smaller than before, destroy extra buffers
	while (m_oStreamedMatrices.size() > m_ui32MaxBufferCount)
	{
		delete m_oStreamedMatrices.front();
		m_oStreamedMatrices.pop_front();
		m_oStartTime.pop_front();
		m_oEndTime.pop_front();
		for (uint32_t c = 0; c < getChannelCount(); c++)
		{
			m_oChannelMinMaxValues[c].pop_front();
		}
	}

	return true;
}

bool CStreamedMatrixDatabase::decodeMemoryBuffer(const IMemoryBuffer* pMemoryBuffer, uint64 ui64StartTime, uint64_t ui64EndTime)
{
	//feed memory buffer to algorithm
	m_pDecoder->getInputParameter(OVP_GD_Algorithm_StreamedMatrixStreamDecoder_InputParameterId_MemoryBufferToDecode)->setReferenceTarget(&pMemoryBuffer);

	//process buffer
	m_pDecoder->process();

	//has flow header been received?
	if (m_pDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixStreamDecoder_OutputTriggerId_ReceivedHeader) == true)
	{
		decodeHeader();

		//create widgets
		m_pDrawable->init();
	}

	//has a buffer been received?
	if (m_pDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixStreamDecoder_OutputTriggerId_ReceivedBuffer) == true)
	{
		decodeBuffer(ui64StartTime, ui64EndTime);

		//redraw widgets
		if (m_bRedrawOnNewData)
		{
			m_pDrawable->redraw();
		}
	}

	return true;
}

uint32_t CStreamedMatrixDatabase::getMaxBufferCount()
{
	return m_ui32MaxBufferCount;
}

uint32_t CStreamedMatrixDatabase::getCurrentBufferCount()
{
	return (uint32_t)m_oStreamedMatrices.size();
}

const double* CStreamedMatrixDatabase::getBuffer(uint32_t ui32Index)
{
	if (ui32Index >= m_oStreamedMatrices.size())
	{
		return nullptr;
	}
	else
	{
		return m_oStreamedMatrices[ui32Index]->getBuffer();
	}
}

uint64_t CStreamedMatrixDatabase::getStartTime(uint32_t ui32BufferIndex)
{
	if (ui32BufferIndex >= m_oStartTime.size())
	{
		return 0;
	}
	else
	{
		return m_oStartTime[ui32BufferIndex];
	}
}

uint64_t CStreamedMatrixDatabase::getEndTime(uint32_t ui32BufferIndex)
{
	if (ui32BufferIndex >= m_oEndTime.size())
	{
		return 0;
	}
	else
	{
		return m_oEndTime[ui32BufferIndex];
	}
}

uint32_t CStreamedMatrixDatabase::getBufferElementCount()
{
	if (m_oStreamedMatrices.empty())
	{
		return 0;
	}
	else
	{
		return m_oStreamedMatrices[0]->getBufferElementCount();
	}
}

uint64_t CStreamedMatrixDatabase::getBufferDuration()
{
	if (m_oStartTime.empty() || m_oEndTime.empty())
	{
		return 0;
	}
	else
	{
		return m_oEndTime[0] - m_oStartTime[0];
	}
}

bool CStreamedMatrixDatabase::isBufferTimeStepComputed()
{
	return m_bBufferTimeStepComputed;
}

uint64_t CStreamedMatrixDatabase::getBufferTimeStep()
{
	if (m_bBufferTimeStepComputed == false)
	{
		return 0;
	}
	else
	{
		return m_ui64BufferTimeStep;
	}
}

uint32_t CStreamedMatrixDatabase::getSampleCountPerBuffer()
{
	if (m_oStreamedMatrixHeader.getDimensionCount() == 0)
	{
		return 0;
	}
	else
	{
		return m_oStreamedMatrixHeader.getDimensionSize(1);
	}
}

uint32_t CStreamedMatrixDatabase::getChannelCount()
{
	if (m_oStreamedMatrixHeader.getDimensionCount() == 0)
	{
		return 0;
	}
	else
	{
		return m_oStreamedMatrixHeader.getDimensionSize(0);
	}
}

bool CStreamedMatrixDatabase::getChannelLabel(const uint32_t ui32ChannelIndex, CString& rElectrodeLabel)
{
	if (m_oStreamedMatrixHeader.getDimensionCount() == 0 || m_oStreamedMatrixHeader.getDimensionSize(0) <= ui32ChannelIndex)
	{
		rElectrodeLabel = "";
		return false;
	}
	else
	{
		rElectrodeLabel = m_oStreamedMatrixHeader.getDimensionLabel(0, ui32ChannelIndex);
		return true;
	}
}

bool CStreamedMatrixDatabase::getChannelMinMaxValues(uint32_t ui32Channel, float64& f64Min, double& f64Max)
{
	f64Min = +DBL_MAX;
	f64Max = -DBL_MAX;

	if (m_bFirstBufferReceived == false)
	{
		return false;
	}

	if (ui32Channel >= getChannelCount())
	{
		return false;
	}

	for (uint64_t i = 0; i < m_oChannelMinMaxValues[(size_t)ui32Channel].size(); i++)
	{
		if (f64Min > m_oChannelMinMaxValues[(size_t)ui32Channel][(size_t)i].first)
		{
			f64Min = m_oChannelMinMaxValues[(size_t)ui32Channel][(size_t)i].first;
		}
		if (f64Max < m_oChannelMinMaxValues[(size_t)ui32Channel][(size_t)i].second)
		{
			f64Max = m_oChannelMinMaxValues[(size_t)ui32Channel][(size_t)i].second;
		}
	}

	return true;
}

bool CStreamedMatrixDatabase::getGlobalMinMaxValues(float64& f64Min, double& f64Max)
{
	f64Min = +DBL_MAX;
	f64Max = -DBL_MAX;

	if (m_bFirstBufferReceived == false)
	{
		return false;
	}

	for (uint32_t c = 0; c < getChannelCount(); c++)
	{
		for (uint64_t i = 0; i < m_oChannelMinMaxValues[(size_t)c].size(); i++)
		{
			if (f64Min > m_oChannelMinMaxValues[(size_t)c][(size_t)i].first)
			{
				f64Min = m_oChannelMinMaxValues[(size_t)c][(size_t)i].first;
			}
			if (f64Max < m_oChannelMinMaxValues[(size_t)c][(size_t)i].second)
			{
				f64Max = m_oChannelMinMaxValues[(size_t)c][(size_t)i].second;
			}
		}
	}

	return true;
}

bool CStreamedMatrixDatabase::getLastBufferChannelMinMaxValues(uint32_t ui32Channel, float64& f64Min, double& f64Max)
{
	f64Min = +DBL_MAX;
	f64Max = -DBL_MAX;

	if (m_bFirstBufferReceived == false)
	{
		return false;
	}

	if (ui32Channel >= getChannelCount())
	{
		return false;
	}

	f64Min = m_oChannelMinMaxValues[ui32Channel].back().first;
	f64Max = m_oChannelMinMaxValues[ui32Channel].back().second;
	return true;
}

bool CStreamedMatrixDatabase::getLastBufferGlobalMinMaxValues(float64& f64Min, double& f64Max)
{
	f64Min = +DBL_MAX;
	f64Max = -DBL_MAX;

	if (m_bFirstBufferReceived == false)
	{
		return false;
	}

	for (uint32_t c = 0; c < m_oChannelMinMaxValues.size(); c++)
	{
		if (f64Min > m_oChannelMinMaxValues[c].back().first)
		{
			f64Min = m_oChannelMinMaxValues[c].back().first;
		}
		if (f64Max < m_oChannelMinMaxValues[c].back().second)
		{
			f64Max = m_oChannelMinMaxValues[c].back().second;
		}
	}

	return true;
}

bool CStreamedMatrixDatabase::decodeHeader()
{
	//copy streamed matrix header
	TParameterHandler<IMatrix*> l_oStreamedMatrix;
	l_oStreamedMatrix.initialize(m_pDecoder->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixStreamDecoder_OutputParameterId_Matrix));
	Tools::Matrix::copyDescription(m_oStreamedMatrixHeader, *l_oStreamedMatrix);

	m_oChannelMinMaxValues.resize(getChannelCount());

	return true;
}

bool CStreamedMatrixDatabase::decodeBuffer(uint64 ui64StartTime, uint64_t ui64EndTime)
{
	//first buffer received
	if (m_bFirstBufferReceived == false)
	{
		uint64_t l_ui64BufferDuration = ui64EndTime - ui64StartTime;

		if (l_ui64BufferDuration == 0)
		{
			m_oParentPlugin.getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Warning << "Error : buffer start time and end time are equal : " << ui64StartTime << "\n";
			//m_bError = true;
			return false;
		}

		m_bFirstBufferReceived = true;
	}

	//compute time step between two buffers
	if (m_bBufferTimeStepComputed == false && m_oStreamedMatrices.size() >= 2)
	{
		m_ui64BufferTimeStep = m_oStartTime[1] - m_oStartTime[0];
		m_bBufferTimeStepComputed = true;

		if (m_bIgnoreTimeScale == false)
		{
			//compute maximum number of buffers from time scale
			setTimeScale(m_f64TimeScale);
		}
	}

	//store new buffer data
	CMatrix* l_pCurrentMatrix = nullptr;
	if (m_oStreamedMatrices.size() < m_ui32MaxBufferCount)
	{
		l_pCurrentMatrix = new CMatrix();
		Tools::Matrix::copyDescription(*l_pCurrentMatrix, m_oStreamedMatrixHeader);
		m_oStreamedMatrices.push_back(l_pCurrentMatrix);
	}
	else //reuse memory for new buffer
	{
		//move front matrix to back of list
		l_pCurrentMatrix = m_oStreamedMatrices.front();
		m_oStreamedMatrices.push_back(l_pCurrentMatrix);

		//remove first matrix data
		m_oStreamedMatrices.pop_front();
		m_oStartTime.pop_front();
		m_oEndTime.pop_front();
		for (uint32_t c = 0; c < getChannelCount(); c++)
		{
			m_oChannelMinMaxValues[c].pop_front();
		}
	}

	//store samples
	TParameterHandler<IMatrix*> l_oStreamedMatrix;
	l_oStreamedMatrix.initialize(m_pDecoder->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixStreamDecoder_OutputParameterId_Matrix));
	Tools::Matrix::copyContent(*l_pCurrentMatrix, *l_oStreamedMatrix);

	//store time stamps
	m_oStartTime.push_back(ui64StartTime);
	m_oEndTime.push_back(ui64EndTime);

	//store min/max values
	double* l_pBuffer = l_pCurrentMatrix->getBuffer();

	for (uint32_t c = 0; c < getChannelCount(); c++)
	{
		double l_f64ChannelMin = DBL_MAX;
		double l_f64ChannelMax = -DBL_MAX;

		for (uint64_t i = 0; i < getSampleCountPerBuffer(); i++, l_pBuffer++)
		{
			if (*l_pBuffer < l_f64ChannelMin)
			{
				l_f64ChannelMin = *l_pBuffer;
			}
			if (*l_pBuffer > l_f64ChannelMax)
			{
				l_f64ChannelMax = *l_pBuffer;
			}
		}

		m_oChannelMinMaxValues[c].push_back(pair<float64, double>(l_f64ChannelMin, l_f64ChannelMax));
	}

	return true;
}
