#ifndef __OpenViBEPlugins_SimpleVisualization_CBottomTimeRuler_H__
#define __OpenViBEPlugins_SimpleVisualization_CBottomTimeRuler_H__

#include <openvibe/ov_all.h>
#include "ovpCBufferDatabase.h"
#include <gtk/gtk.h>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		class CBufferDatabase;

		/**
		 * Used to display an horizontal temporal ruler.
		 * Uses information fetched from a signal database object.
		 * \param pDatabase Object from which to fetch time data
		 * \param i32WidthRequest Width to be requested by widget
		 * \param i32HeightRequest Height to be requested by widget
		 */
		class CBottomTimeRuler
		{
		public:
			CBottomTimeRuler(
				CBufferDatabase& pDatabase,
				int32_t i32WidthRequest,
				int32_t i32HeightRequest);

			~CBottomTimeRuler();

			//! returns the widget, so it can be added to the main interface
			GtkWidget* getWidget() const;

			//! draws the ruler
			void draw();

			/**
			 * \brief Resize this ruler when the widget passed in parameter is resized
			 * \param pWidget Widget whose width is matched by this widget
			 */
			void linkWidthToWidget(
				GtkWidget* pWidget);

			//! in scan mode, leftmost displayed time is not always the one of the oldest buffer
			void setLeftmostDisplayedTime(uint64_t ui64LeftmostDisplayedTime);

			/**
			 * \brief Callback notified upon resize events
			 * \param i32Width New window width
			 * \param i32Height New window height
			 */
			void onResizeEventCB(
				gint i32Width,
				gint i32Height);

		private:
			//! Draw ruler
			void drawRuler(
				int64_t i64BaseX,
				int32_t i32RulerWidth,
				double f64StartTime,
				double f64EndTime,
				double f64Length,
				double f64BaseValue,
				double f64ValueStep,
				int32_t ui32ClipLeft,
				int32_t ui32ClipRight);

		private:
			//! Gtk widget
			GtkWidget* m_pBottomRuler;
			//! Signal database from which time information is retrieved
			CBufferDatabase* m_pDatabase;
			//! Height request
			int32_t m_i32HeightRequest;
			//! Space allocated per label
			uint64_t m_ui64PixelsPerBottomRulerLabel;
			//! When in scan mode, current leftmost displayed time
			uint64_t m_ui64LeftmostDisplayedTime;
		};
	};
};

#endif
