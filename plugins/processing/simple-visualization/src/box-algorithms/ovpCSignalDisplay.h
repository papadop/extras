#ifndef __OpenViBEPlugins_SimpleVisualization_CSignalDisplay_H__
#define __OpenViBEPlugins_SimpleVisualization_CSignalDisplay_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <vector>
#include <string>

#include "../ovpCBufferDatabase.h"
#include "ovpCSignalDisplay/ovpCSignalDisplayView.h"

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		/**
		* This plugin opens a new GTK window and displays the incoming signals. The user may change the zoom level,
		* the width of the time window displayed, ...
		*/
		class CSignalDisplay : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CSignalDisplay();

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_SignalDisplay)

		public:

			OpenViBEToolkit::TDecoder<CSignalDisplay>* m_pStreamDecoder;
			OpenViBEToolkit::TStimulationDecoder<CSignalDisplay> m_oStimulationDecoder;
			OpenViBEToolkit::TChannelUnitsDecoder<CSignalDisplay> m_oUnitDecoder;

			//The main object used for the display (contains all the GUI code)
			CSignalDisplayDrawable* m_pSignalDisplayView;

			//Contains all the data about the incoming signal
			CBufferDatabase* m_pBufferDatabase;

			OpenViBE::CIdentifier m_oInputTypeIdentifier;

		protected:

			uint64_t m_ui64LastScaleRefreshTime;

			double m_f64RefreshInterval;

		private:
			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};


		class CSignalDisplayListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				if (ui32Index == 1)
				{
					OpenViBE::CIdentifier l_oSettingType;
					rBox.getSettingType(ui32Index, l_oSettingType);
					if (l_oSettingType != OV_TypeId_Stimulations)
					{
						this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Error: Only stimulation type supported for input 2\n";
						rBox.setInputType(ui32Index, OV_TypeId_Stimulations);
					}
				}
				else if (ui32Index == 2)
				{
					OpenViBE::CIdentifier l_oSettingType;
					rBox.getSettingType(ui32Index, l_oSettingType);
					if (l_oSettingType != OV_TypeId_ChannelUnits)
					{
						this->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "Error: Only measurement unit type supported for input 3\n";
						rBox.setInputType(ui32Index, OV_TypeId_ChannelUnits);
					}
				}

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		/**
		 * Signal Display plugin descriptor
		 */
		class CSignalDisplayDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("Signal display"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Bruno Renier, Yann Renard, Alison Cellard, Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Displays the incoming stream"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box can be used to visualize signal and matrix streams"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Visualization/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.3"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_SignalDisplay; }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-zoom-fit"); }
			OpenViBE::Plugins::IPluginObject* create() override { return new CSignalDisplay(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CSignalDisplayListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addSetting("Display Mode", OVP_TypeId_SignalDisplayMode, "Scan");
				rPrototype.addSetting("Auto vertical scale", OVP_TypeId_SignalDisplayScaling, CSignalDisplayView::m_vScalingModes[0]);
				rPrototype.addSetting("Scale refresh interval (secs)", OV_TypeId_Float, "5");
				rPrototype.addSetting("Vertical Scale",OV_TypeId_Float, "100");
				rPrototype.addSetting("Vertical Offset",OV_TypeId_Float, "0");
				rPrototype.addSetting("Time Scale", OV_TypeId_Float, "10");
				rPrototype.addSetting("Bottom ruler", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Left ruler", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Multiview", OV_TypeId_Boolean, "false");

				rPrototype.addInput("Data", OV_TypeId_Signal);
				rPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rPrototype.addInput("Channel Units", OV_TypeId_ChannelUnits);

				rPrototype.addInputSupport(OV_TypeId_Signal);
				rPrototype.addInputSupport(OV_TypeId_StreamedMatrix);
				rPrototype.addInputSupport(OV_TypeId_ChannelUnits);

				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_SignalDisplayDesc)
		};
	};
};

#endif
