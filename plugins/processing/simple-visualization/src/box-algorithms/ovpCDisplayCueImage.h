#ifndef __OpenViBEPlugins_SimpleVisualization_CDisplayCueImage_H__
#define __OpenViBEPlugins_SimpleVisualization_CDisplayCueImage_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <gtk/gtk.h>
#include <vector>
#include <string>
#include <map>
#include <deque>

#define OVP_ClassId_DisplayCueImage                                            OpenViBE::CIdentifier(0x005789A4, 0x3AB78A36)
#define OVP_ClassId_DisplayCueImageDesc                                        OpenViBE::CIdentifier(0x086185A4, 0x796A854C)

namespace TCPTagging
{
	class IStimulusSender; // fwd declare
};

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		class CDisplayCueImage :
			public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CDisplayCueImage();

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			uint64_t getClockFrequency() override { return (128LL << 32); }				// 128hz
			bool processClock(OpenViBE::CMessageClock& rMessageClock) override;
			bool process() override;
			virtual void redraw();
			virtual void resize(uint32_t ui32Width, uint32_t ui32Height);

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_DisplayCueImage)

		public:

			void flushQueue();					// Sends all accumulated stimuli to the TCP Tagging

			static const uint32_t m_ui32NonCueSettingsCount = 3; // fullscreen + scale + clear

		protected:

			virtual void drawCuePicture(uint32_t uint32_tCueID);

			//The Builder handler used to create the interface
			GtkBuilder* m_pBuilderInterface;
			GtkWidget* m_pMainWindow;
			GtkWidget* m_pDrawingArea;

			OpenViBEToolkit::TStimulationDecoder<CDisplayCueImage> m_oStimulationDecoder;
			OpenViBEToolkit::TStimulationEncoder<CDisplayCueImage> m_oStimulationEncoder;

			// For the display of the images:
			bool m_bImageRequested;        //when true: a new image must be drawn
			int32_t m_i32RequestedImageID;  //ID of the requested image. -1 => clear the screen

			bool m_bImageDrawn;            //when true: the new image has been drawn
			int32_t m_i32DrawnImageID;      //ID of the drawn image. -1 => clear the screen

			// Data corresponding to each cue image. Could be refactored to a vector of structs.
			std::vector<GdkPixbuf*> m_vOriginalPicture;
			std::vector<GdkPixbuf*> m_vScaledPicture;
			std::vector<uint64_t> m_vStimulationsId;
			std::vector<OpenViBE::CString> m_vImageNames;

			GdkColor m_oBackgroundColor;
			GdkColor m_oForegroundColor;

			//Settings
			uint32_t m_ui32NumberOfCues;
			uint64_t m_ui64ClearScreenStimulation;
			bool m_bFullScreen;
			bool m_bScaleImages;

			//Start and end time of the last buffer
			uint64_t m_ui64StartTime;
			uint64_t m_ui64EndTime;
			uint64_t m_ui64LastOutputChunkDate;

			//We save the received stimulations
			OpenViBE::CStimulationSet m_oPendingStimulationSet;

			// For queuing stimulations to the TCP Tagging
			std::vector<uint64_t> m_vStimuliQueue;
			guint m_uiIdleFuncTag;
			TCPTagging::IStimulusSender* m_pStimulusSender;

		private:
			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};

		class CDisplayCueImageListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onSettingAdded(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				const uint32_t l_ui32PreviousCues = ((ui32Index - 1) - CDisplayCueImage::m_ui32NonCueSettingsCount) / 2 + 1;
				const uint32_t l_ui32CueNumber = l_ui32PreviousCues + 1;

				char l_sValue[1024];
				sprintf(l_sValue, "${Path_Data}/plugins/simple-visualization/p300-magic-card/%02d.png", l_ui32CueNumber);

				rBox.setSettingDefaultValue(ui32Index, l_sValue);
				rBox.setSettingValue(ui32Index, l_sValue);

				sprintf(l_sValue, "OVTK_StimulationId_Label_%02X", l_ui32CueNumber);
				rBox.addSetting("", OV_TypeId_Stimulation, l_sValue);
				rBox.setSettingDefaultValue(ui32Index + 1, l_sValue);
				rBox.setSettingValue(ui32Index + 1, l_sValue);

				checkSettingNames(rBox);

				return true;
			}

			bool onSettingRemoved(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				// Remove also the associated setting in the other slot
				const uint32_t l_ui32IndexNumber = (ui32Index - CDisplayCueImage::m_ui32NonCueSettingsCount);

				if (l_ui32IndexNumber % 2 == 0)
				{
					// This was the 'cue image' setting, remove 'stimulation setting'
					// when onSettingRemoved is called, ui32Index has already been removed, so using it will effectively mean 'remove next setting'.
					rBox.removeSetting(ui32Index);
				}
				else
				{
					// This was the 'stimulation setting'. Remove the 'cue image' setting.
					rBox.removeSetting(ui32Index - 1);
				}

				checkSettingNames(rBox);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);

		private:

			// This function is used to make sure the setting names and types are correct
			bool checkSettingNames(OpenViBE::Kernel::IBox& rBox)
			{
				char l_sName[1024];
				for (uint32_t i = CDisplayCueImage::m_ui32NonCueSettingsCount; i < rBox.getSettingCount() - 1; i += 2)
				{
					sprintf(l_sName, "Cue Image %i", i / 2);
					rBox.setSettingName(i, l_sName);
					rBox.setSettingType(i, OV_TypeId_Filename);
					sprintf(l_sName, "Stimulation %i", i / 2);
					rBox.setSettingName(i + 1, l_sName);
					rBox.setSettingType(i + 1, OV_TypeId_Stimulation);
				}
				return true;
			}
		};

		/**
		 * Plugin's description
		 */
		class CDisplayCueImageDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("Display cue image"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Joan Fruitet, Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria Sophia, Inria Rennes"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Display cue images when receiving stimulations"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Display cue images when receiving specified stimulations. Forwards the stimulations to the AS using TCP Tagging."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Visualization/Presentation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.2"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_DisplayCueImage; }

			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-fullscreen"); }
			OpenViBE::Plugins::IPluginObject* create() override { return new CDisplayCueImage(); }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CDisplayCueImageListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rPrototype.addOutput("Stimulations (deprecated)", OV_TypeId_Stimulations);
				rPrototype.addSetting("Display images in full screen", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Scale images to fit", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Clear screen Stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_VisualStimulationStop");
				rPrototype.addSetting("Cue Image 1", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/01.png");
				rPrototype.addSetting("Stimulation 1", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanAddSetting);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_DisplayCueImageDesc)
		};
	};
};

#endif
