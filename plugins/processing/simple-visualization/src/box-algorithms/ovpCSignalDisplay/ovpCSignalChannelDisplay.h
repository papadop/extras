#ifndef __OpenViBEPlugins_SimpleVisualization_CSignalChannelDisplay_H__
#define __OpenViBEPlugins_SimpleVisualization_CSignalChannelDisplay_H__

#include "../../ovp_defines.h"
#include "ovpCSignalDisplayLeftRuler.h"

#include <glib.h>
#include <glib/gprintf.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

#include <memory.h>
#include <cmath>

#include <vector>
#include <map>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		class CSignalDisplayView;
		class CBufferDatabase;

		class CSignalChannelDisplay
		{
		public:
			/**
			 * \brief Constructor
			 * \param pDisplayView Parent view
			 * \param i32ChannelDisplayWidthRequest Width to be requested by widget
			 * \param i32ChannelDisplayHeightRequest Height to be requested by widget
			 * \param i32LeftRulerWidthRequest Width to be requested by left ruler
			 * \param i32LeftRulerHeightRequest Height to be requested by left ruler
			 */
			CSignalChannelDisplay(
				CSignalDisplayView* pDisplayView,
				int32_t i32ChannelDisplayWidthRequest,
				int32_t i32ChannelDisplayHeightRequest,
				int32_t i32LeftRulerWidthRequest,
				int32_t i32LeftRulerHeightRequest);

			/**
			 * \brief Destructor
			 */
			~CSignalChannelDisplay();

			/**
			 * \brief Get ruler widget
			 * \return Pointer to ruler widget
			 */
			GtkWidget* getRulerWidget(uint32_t ui32Index) const;

			/**
			 * \brief Get signal display widget
			 * \return Pointer to signal display widget
			 */
			GtkWidget* getSignalDisplayWidget() const;

			/**
			 * \brief Callback notified upon resize events
			 * \param i32Width New window width
			 * \param i32Height New window height
			 */
			void onResizeEventCB(gint i32Width, gint i32Height);

			/**
			 * \brief Updates scale following a resize event or a time scale change
			 */
			void updateScale();

			// Updates some drawing limits, i.e. to limit drawing to [chn_i,...,chn_j]
			void updateLimits();

			/**
			 * \brief Reset list of channels displayed by this object
			 */
			void resetChannelList();

			/**
			 * \brief Add a channel to the list of channels to be displayed
			 * \param ui32Channel Index of channel to be displayed
			 */
			void addChannel(uint32_t ui32Channel);

			void addChannelList(uint32_t ui32Channel);

			/**
			 * \brief Get rectangle to clear and redraw based on latest signal data received
			 * \param[out] rRect Rectangle holding part of drawing area to clear and update
			 */
			void getUpdateRectangle(GdkRectangle& rRect);

			/**
			 * \brief Flag widget so that its whole window is redrawn at next refresh
		   */
			void redrawAllAtNextRefresh(bool bRedraw);

			/**
			 * \brief Check whether the whole window must be redrawn
			 * \return True if the whole window must be redrawn, false otherwise
			 */
			bool mustRedrawAll() const;

			/**
			 * \brief Draws the signal on the signal's drawing area.
			 * \param rExposedArea Exposed area that needs to be redrawn
			 */
			void draw(const GdkRectangle& rExposedArea);

			/**
			 * \brief Clip signals to drawing area
			 * Computes the list of points used to draw the lines (m_pParentDisplayView->m_pPoints) using the raw points list
			 * (m_pParentDisplayView->m_pRawPoints) and by cropping the lines when they go out of the window.
			 * \param ui64PointCount Number of points to clip
			 * \return The number of points to display.
			 */
			uint64_t cropCurve(uint64_t ui64PointCount);

			/**
			 * \brief Computes the parameters necessary for the signal to be zoomed at the selected coordinates.
			 * \param bZoomIn If true, the operation is a zoom In, if false it's a zoom Out.
			 * \param f64XClick The X-coordinate of the center of the area we want to zoom in.
			 * \param f64YClick The Y-coordinate of the center of the area we want to zoom in.
			 */
			void computeZoom(bool bZoomIn, double f64XClick, double f64YClick);

			/**
			 * \brief Returns empiric y min and maxes of the currently shown signal chunks for all subchannels.
			 * Note that the actually used display limits may be different. This function can be used
			 * to get the data extremal values and then use these to configure the display appropriately.
			 */
			void getDisplayedValueRange(std::vector<double>& rDisplayedValueMin, std::vector<double>& rDisplayedValueMax) const;

			/*
			 * \brief Sets scale for all subchannels. 
			 */
			void setGlobalScaleParameters(const double f64Min, const double f64Max, const double f64Margin);

			/*
			 * \brief Sets scale for a single subchannel.
			 */
			void setLocalScaleParameters(const uint32_t subChannelIndex, const double rMin, const double rMax, const double f64Margin);

			/**
			 * \brief Updates signal scale and translation based on latest global range and margin
			 */
			void updateDisplayParameters();

		private:
			/**
			 * \brief Get first buffer to display index and position and first sample to display index
			 * \param[out] rFirstBufferToDisplay Index of first buffer to display
			 * \param[out] rFirstSampleToDisplay Index of first sample to display
			 * \param[out] rFirstBufferToDisplayPosition Position of first buffer to display (0-based, from left edge)
			 */
			void getFirstBufferToDisplayInformation(uint32_t& rFirstBufferToDisplay, uint32_t& rFirstSampleToDisplay, uint32_t& rFirstBufferToDisplayPosition);

			/**
			 * \brief Get start X coord of a buffer
			 * \param ui32Position Position of buffer on screen (0-based, from left edge)
			 * \return Floored X coordinate of buffer
			 */
			int32_t getBufferStartX(uint32_t ui32Position);

			/**
			 * \brief Get X coordinate of a sample
			 * \param ui32BufferPosition Position of buffer on screen (0-based, from left edge)
			 * \param ui32SampleIndex Index of sample in buffer
			 * \param f64XOffset X offset from which to start drawing. Used in scroll mode only.
			 * \return X coordinate of sample
			 */
			double getSampleXCoordinate(uint32_t ui32BufferPosition, uint32_t ui32SampleIndex, double f64XOffset);

			/**
			 * \brief Get Y coordinate of a sample
			 * \param f64Value Sample value and index of channel
			 * \return Y coordinate of sample
			 */
			double getSampleYCoordinate(double f64Value, uint32_t ui32ChannelIndex);

			/**
			 * \brief Get Y coordinate of a sample in Multiview mode
			 * \param f64Value Sample value and index of channel
			 * \return Y coordinate of sample
			 */
			double getSampleYMultiViewCoordinate(double f64Value);

			/**
			 * \brief Draw signals (and stimulations, if any) displayed by this channel
			 * \param firstBufferToDisplay Index of first buffer to display
			 * \param lastBufferToDisplay Index of last buffer to display
			 * \param firstSampleToDisplay Index of first sample to display in first buffer (subsequent buffers will start at sample 0)
			 * \param f64BaseX X offset to apply to signals (can be non null in scroll mode)
			 * \return True if all went ok, false otherwise
			 */
			bool drawSignals(uint32_t firstBufferToDisplay, uint32_t lastBufferToDisplay,
				uint32_t firstSampleToDisplay, double firstBufferStartX,
				uint32_t ui32FirstChannelToDisplay, uint32_t ui32LastChannelToDisplay);

			/**
			 * \brief Draw vertical line highlighting where data was last drawn
			 * \param ui32X X coordinate of line
			 */
			void drawProgressLine(uint32_t firstBufferToDisplay, uint32_t firstBufferToDisplayPosition);

			/**
			 * \brief Draw Y=0 line
			 */
			void drawZeroLine();

		public:
			//! Vector of Left rulers displaying signal scale. Indexed by channel id. @note This is a map as the active number of channels 
			// may change by the toolbar whereas this total set of rulers doesn't...
			std::map<uint32_t, CSignalDisplayLeftRuler*> m_oLeftRuler;
			//! The drawing area where the signal is to be drawn
			GtkWidget* m_pDrawingArea;
			//! Drawing area dimensions, in pixels
			uint32_t m_ui32Width, m_ui32Height;
			//! Available width per buffer, in pixels
			double m_f64WidthPerBuffer;
			//! Available width per sample point, in pixels
			double m_f64PointStep;
			//! The index list of the channels to display
			std::vector<uint32_t> m_oChannelList;
			//! The "parent" view (which uses this widget)
			CSignalDisplayView* m_pParentDisplayView;
			//! The database from which the information are to be read
			CBufferDatabase* m_pDatabase;

			/** \ name Extrema of displayed values for all channel in this display */
			//@{
			std::vector<double> m_vLocalMaximum;
			std::vector<double> m_vLocalMinimum;
			//@}

			/** \name Auto scaling parameters */
			//@{
			//		OpenViBE::float64 m_f64ScaleX;
			//		OpenViBE::float64 m_f64TranslateX;

			std::vector<double> m_vScaleY;
			std::vector<double> m_vTranslateY;
			//@}

			/** \name Zooming parameters (user controlled) */
			//@{
			double m_f64ZoomTranslateX;
			double m_f64ZoomTranslateY;
			double m_f64ZoomScaleX;
			double m_f64ZoomScaleY;
			//! The zoom factor step
			const double m_f64ZoomFactor;
			//@}

			/** \name Scale margin parameters */
			//@{
			std::vector<double> m_vOuterTopMargin;
			std::vector<double> m_vInnerTopMargin;
			std::vector<double> m_vInnerBottomMargin;
			std::vector<double> m_vOuterBottomMargin;
			//@}

			uint32_t m_i32LeftRulerWidthRequest, m_i32LeftRulerHeightRequest;

			//! Current signal display mode
			EDisplayMode m_eCurrentSignalMode;
			//! Time of latest displayed data
			uint64_t m_ui64LatestDisplayedTime;

			//! Should the whole window be redrawn at next redraw?
			bool m_bRedrawAll;

			//! Is it a multiview display ?
			bool m_bMultiView;

			// These parameters control that we don't unnecessarily draw parts of the signal which are not in view

			// Currently visible y segment in the drawing area
			uint32_t m_ui32StartY;
			uint32_t m_ui32StopY;

			// First and last channel to draw
			uint32_t m_ui32FirstChannelToDisplay;
			uint32_t m_ui32LastChannelToDisplay;
		};
	}
}

#endif
