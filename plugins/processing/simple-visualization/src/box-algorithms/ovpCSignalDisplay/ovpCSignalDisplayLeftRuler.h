#ifndef __OpenViBEPlugins_SimpleVisualization_CSignalDisplayLeftRuler_H__
#define __OpenViBEPlugins_SimpleVisualization_CSignalDisplayLeftRuler_H__

#include <openvibe/ov_all.h>
#include <gtk/gtk.h>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		/**
		 * Used to display a vertical "amplitude" ruler.
		 * */
		class CSignalDisplayLeftRuler
		{
		public:
			/**
			 * \brief Constructor
			 * \param i32WidthRequest Width to be requested by widget
			 * \param i32HeightRequest Height to be requested by widget
			 */
			CSignalDisplayLeftRuler(
				int32_t i32WidthRequest,
				int32_t i32HeightRequest);
			//	CSignalChannelDisplay* pParentChannelDisplay);

			/**
			 * \brief Destructor
			 */
			~CSignalDisplayLeftRuler();

			/*
			 * \brief Update ruler with latest min/max values and request a redraw
			 * \param f64Min Minimum value to be displayed
			 * \param f64Max Maximum value to be displayed
			 */
			void update(
				double f64Min,
				double f64Max);

			//! returns the widget, so it can be added to the main interface
			GtkWidget* getWidget() const;

			/**
			 * \brief Draws the ruler by using the information from the database.
			 */
			void draw();

		public:
			GtkWidget* m_pLeftRuler;
			int32_t m_i32WidthRequest;
			double m_f64MaximumDisplayedValue;
			double m_f64MinimumDisplayedValue;
			uint64_t m_ui64PixelsPerLeftRulerLabel;
			//			CSignalChannelDisplay* m_pParentChannelDisplay;
		};
	};
};

#endif
