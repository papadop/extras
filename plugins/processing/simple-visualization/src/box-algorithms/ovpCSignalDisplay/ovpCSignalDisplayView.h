#ifndef __OpenViBEPlugins_SimpleVisualization_CSignalDisplayView_H__
#define __OpenViBEPlugins_SimpleVisualization_CSignalDisplayView_H__

#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gtk/gtk.h>

#include "ovpCSignalChannelDisplay.h"
#include "../../ovpCBufferDatabase.h"

#include "../../ovpCBottomTimeRuler.h"

#include <vector>
#include <string>
#include <map>

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		/**
		* This class contains everything necessary to setup a GTK window and display
		* a signal thanks to a CBufferDatabase's information.
		*/
		class CSignalDisplayView : public CSignalDisplayDrawable
		{
		public:


			/**
			 * \brief Constructor
			 * \param [in] rBufferDatabase Signal database
			 * \param [in] f64TimeScale Initial time scale value
			 * \param [in] oDisplayMode Initial signal display mode
			 * \param [in] bAutoVerticalScale Initial auto vertical scale value
			 * \param [in] f64VerticalScale Initial vertical scale value
			 */
			CSignalDisplayView(CBufferDatabase& rBufferDatabase, OpenViBE::CIdentifier oDisplayMode, OpenViBE::CIdentifier oScalingMode,
							   double f64VerticalScale, double f64VerticalOffset, double f64TimeScale,
							   bool l_bHorizontalRuler, bool l_bVerticalRuler, bool l_bIsMultiview);

			/**
			 * \brief Base constructor
			 * \param [in] rBufferDatabase Signal database
			 * \param [in] f64TimeScale Initial time scale value
			 * \param [in] oDisplayMode Initial signal display mode
			 */
			void construct(CBufferDatabase& oBufferDatabase, double f64TimeScale, OpenViBE::CIdentifier oDisplayMode);

			/**
			 * \brief Destructor
			 */
			virtual ~CSignalDisplayView();
			/**
			 * \brief Get pointers to plugin main widget and (optional) toolbar widget
			 * \param [out] pWidget Pointer to main widget
			 * \param [out] pToolbarWidget Pointer to (optional) toolbar widget
			 */
			void getWidgets(GtkWidget*& pWidget, GtkWidget*& pToolbarWidget);
			/**
			 * Initializes the window.
			 */
			void init() override;
			/**
			 * Invalidates the window's content and tells it to redraw itself.
			 */
			void redraw() override;
			/**
			* Toggle left rulers on/off
			* \param bActive Show rulers if true.
			*/
			void toggleLeftRulers(bool bActive);
			/**
			 * Toggle time ruler on/off
			 * \param bActive Show the ruler if true.
			 */
			void toggleBottomRuler(bool bActive);
			/**
			 * Toggle a channel on/off
			 * \param ui64ChannelIndex The index of the channel to toggle.
			 * \param active Show the channel if true.
			 */
			void toggleChannel(size_t channelIndex, bool active);

			void toggleChannelMultiView(bool bActive);

			void changeMultiView();

			void removeOldWidgets();
			void recreateWidgets(uint32_t ui32ChannelCount);
			void updateMainTableStatus();

			void updateDisplayTableSize();

			void activateToolbarButtons(bool bActive);

			/**
			 * Callback called when display mode changes
			 * \param oDisplayMode New display mode
			 * \return True
			 */
			bool onDisplayModeToggledCB(OpenViBE::CIdentifier oDisplayMode);

			bool onUnitsToggledCB(bool active);


			/**
			 * Callback called when vertical scale mode changes
			 * \param pToggleButton Radio button toggled
			 * \return True
			 */
			bool onVerticalScaleModeToggledCB(
				GtkToggleButton* pToggleButton);

			/**
			 * Callback called when custom vertical scale is changed
			 * \param pSpinButton Custom vertical scale widget
			 * \return True if custom vertical scale value could be retrieved, false otherwise
			 */
			bool onCustomVerticalScaleChangedCB(GtkSpinButton* pSpinButton);
			bool onCustomVerticalOffsetChangedCB(GtkSpinButton* pSpinButton);

			/**
			 * \brief Get a channel display object
			 * \param ui32ChannelIndex Index of channel display
			 * \return Channel display object
			 */
			CSignalChannelDisplay* getChannelDisplay(size_t ui32ChannelIndex);

			bool isChannelDisplayVisible(size_t ui32ChannelIndex);

			void onStimulationReceivedCB(uint64_t ui64StimulationCode, const OpenViBE::CString& rStimulationName);

			bool setChannelUnits(const std::vector<std::pair<OpenViBE::CString, OpenViBE::CString>>& oChannelUnits);

			/**
			 * \brief Get a color from a stimulation code
			 * \remarks Only the lower 32 bits of the stimulation code are currently used to compute the color.
			 * \param[in] ui64StimulationCode Stimulation code
			 * \param[out] rColor Color computed from stimulation code
			 */
			void getStimulationColor(uint64_t ui64StimulationCode, GdkColor& rColor);

			/**
			 * \brief Get a color from a signal
			 * \param[in] ui32SignalIndex channel index
			 * \param[out] rColor Color computed from stimulation code
			 */
			void getMultiViewColor(size_t ui32ChannelIndex, GdkColor& rColor);

			void refreshScale();

		private:
			/**
			 * \brief Update stimulations color dialog with a new (stimulation, color) pair
			 * \param[in] rStimulationLabel Stimulation label
			 * \param[in] rStimulationColor Stimulation color
			 */
			void updateStimulationColorsDialog(const OpenViBE::CString& rStimulationLabel, const GdkColor& rStimulationColor);

		public:

			//! The Builder handler used to create the interface
			GtkBuilder* m_pBuilderInterface;

			//! The table containing the CSignalChannelDisplays
			GtkWidget* m_pSignalDisplayTable;

			GtkWidget* m_pSeparator;

			//! Array of the channel's labels
			std::vector<GtkWidget*> m_oChannelLabel;

			//! Array of CSignalChannelDisplays (one per channel, displays the corresponding channel)
			std::vector<CSignalChannelDisplay*> m_oChannelDisplay;
			std::map<uint32_t, GtkWidget*> m_vSeparator;

			//! Show left rulers when true
			bool m_bShowLeftRulers;

			//!Show bottom time ruler when true
			bool m_bShowBottomRuler;

			//! Time of displayed signals at the left of channel displays
			uint64_t m_ui64LeftmostDisplayedTime;

			//! Largest displayed value range, to be matched by all channels in global best fit mode
			double m_f64LargestDisplayedValueRange;

			double m_f64LargestDisplayedValue;
			double m_f64SmallestDisplayedValue;

			//! Current value range margin, used to avoid redrawing signals every time the largest value range changes
			double m_f64ValueRangeMargin;
			// OpenViBE::float64 m_f64ValueMaxMargin;
			/*! Margins added to largest and subtracted from smallest displayed values are computed as :
			m_f64MarginFactor * m_f64LargestDisplayedValueRange. If m_ui64MarginFactor = 0, there's no margin at all.
			If factor is 0.1, largest displayed value range is extended by 10% above and below its extremums at the time
			when margins are computed. */
			double m_f64MarginFactor;

			//! Normal/zooming cursors
			GdkCursor* m_pCursor[2];

			/** \name Vertical scale */
			//@{
			//! Flag set to true when you'd like the display to *check* if the scale needs to change and possibly update
			bool m_bVerticalScaleRefresh;
			//! Flag set to true when you'd like the display to update in any case
			bool m_bVerticalScaleForceUpdate;
			//! Value of custom vertical scale
			double m_f64CustomVerticalScaleValue;
			//! Value of custom vertical offset
			double m_f64CustomVerticalOffset;
			//@}

			//! The database that contains the information to use to draw the signals
			CBufferDatabase* m_pBufferDatabase;

			//! Vector of gdk points. Used to draw the signals.
			std::vector<GdkPoint> m_pPoints;

			//! Vector of raw points. Stores the points' coordinates before cropping.
			std::vector<std::pair<double, double>> m_pRawPoints;

			std::vector<OpenViBE::CString> m_vChannelName;

			//! Vector of indexes of the channels to display
			std::map<uint32_t, bool> m_vSelectedChannels;

			uint32_t m_ui32SelectedChannelCount;

			std::map<uint32_t, std::pair<OpenViBE::CString, OpenViBE::CString>> m_vChannelUnits;

			//! Flag set to true once multi view configuration dialog is initialized
			bool m_bMultiViewEnabled;

			//! Vector of indices of selected channels
			std::map<uint32_t, bool> m_vMultiViewSelectedChannels;

			//Map of stimulation codes received so far, and their corresponding name and color
			std::map<uint64_t, std::pair<OpenViBE::CString, GdkColor>> m_mStimulations;

			//Map of signal indices received so far, and their corresponding name and color
			std::map<uint64_t, std::pair<OpenViBE::CString, GdkColor>> m_mSignals;

			//! Bottom box containing bottom ruler
			GtkBox* m_pBottomBox;

			//! Bottom time ruler
			CBottomTimeRuler* m_pBottomRuler;

			//! Widgets for left rulers
			std::vector<GtkWidget *> m_oLeftRulers;

			OpenViBE::CIdentifier m_oScalingMode;

			static const char* m_vScalingModes[];

			std::vector<OpenViBE::CString> m_vErrorState;

			bool m_bStimulationColorsShown;
		};
	}
}

#endif
