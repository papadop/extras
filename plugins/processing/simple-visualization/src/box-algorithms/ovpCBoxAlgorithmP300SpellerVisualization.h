#ifndef __OpenViBEPlugins_BoxAlgorithm_P300SpellerVisualization_H__
#define __OpenViBEPlugins_BoxAlgorithm_P300SpellerVisualization_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <gtk/gtk.h>
#include <map>
#include <list>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_P300SpellerVisualization     OpenViBE::CIdentifier(0x195E41D6, 0x6E684D47)
#define OVP_ClassId_BoxAlgorithm_P300SpellerVisualizationDesc OpenViBE::CIdentifier(0x31DE2B0D, 0x028202E7)

namespace TCPTagging
{
	class IStimulusSender; // fwd declare
};

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		class CBoxAlgorithmP300SpellerVisualization : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t ui32Index) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_P300SpellerVisualization);

		private:

			typedef struct
			{
				GtkWidget* pWidget;
				GtkWidget* pChildWidget;
				GdkColor oBackgroundColor;
				GdkColor oForegroundColor;
				PangoFontDescription* pFontDescription;
			} SWidgetStyle;

			typedef void (CBoxAlgorithmP300SpellerVisualization::*_cache_callback_)(SWidgetStyle& rWidgetStyle, void* pUserData);

			void _cache_build_from_table_(GtkTable* pTable);
			void _cache_for_each_(_cache_callback_ fpCallback, void* pUserData);
			void _cache_for_each_if_(int iLine, int iColumn, _cache_callback_ fpIfCallback, _cache_callback_ fpElseCallback, void* pIfUserData, void* pElseUserData);
			void _cache_change_null_cb_(SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_background_cb_(SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_foreground_cb_(SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_font_cb_(SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_collect_widget_cb_(SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_collect_child_widget_cb_(SWidgetStyle& rWidgetStyle, void* pUserData);

		public:

			void flushQueue();					// Sends all accumulated stimuli to the TCP Tagging

		protected:

			OpenViBE::CString m_sInterfaceFilename;
			uint64_t m_ui64RowStimulationBase;
			uint64_t m_ui64ColumnStimulationBase;

			GdkColor m_oFlashBackgroundColor;
			GdkColor m_oFlashForegroundColor;
			uint64_t m_ui64FlashFontSize;
			PangoFontDescription* m_pFlashFontDescription;
			GdkColor m_oNoFlashBackgroundColor;
			GdkColor m_oNoFlashForegroundColor;
			uint64_t m_ui64NoFlashFontSize;
			PangoFontDescription* m_pNoFlashFontDescription;
			GdkColor m_oTargetBackgroundColor;
			GdkColor m_oTargetForegroundColor;
			uint64_t m_ui64TargetFontSize;
			PangoFontDescription* m_pTargetFontDescription;
			GdkColor m_oSelectedBackgroundColor;
			GdkColor m_oSelectedForegroundColor;
			uint64_t m_ui64SelectedFontSize;
			PangoFontDescription* m_pSelectedFontDescription;

		private:

			OpenViBE::Kernel::IAlgorithmProxy* m_pSequenceStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pTargetStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pTargetFlaggingStimulationEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pRowSelectionStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pColumnSelectionStimulationDecoder;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pSequenceMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pTargetMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IStimulationSet*> ip_pTargetFlaggingStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pSequenceStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pTargetStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pTargetFlaggingMemoryBuffer;
			uint64_t m_ui64LastTime;

			GtkBuilder* m_pMainWidgetInterface;
			GtkBuilder* m_pToolbarWidgetInterface;
			GtkWidget* m_pMainWindow;
			GtkWidget* m_pToolbarWidget;
			GtkTable* m_pTable;
			GtkLabel* m_pResult;
			GtkLabel* m_pTarget;
			uint64_t m_ui64RowCount;
			uint64_t m_ui64ColumnCount;

			int m_iLastTargetRow;
			int m_iLastTargetColumn;
			int m_iTargetRow;
			int m_iTargetColumn;
			int m_iSelectedRow;
			int m_iSelectedColumn;

			bool m_bTableInitialized;

			// @todo refactor to std::pair<long,long> ?
			std::map<unsigned long, std::map<unsigned long, SWidgetStyle>> m_vCache;
			std::list<std::pair<int, int>> m_vTargetHistory;

			// TCP Tagging
			std::vector<uint64_t> m_vStimuliQueue;
			guint m_uiIdleFuncTag;
			TCPTagging::IStimulusSender* m_pStimulusSender;

			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};

		class CBoxAlgorithmP300SpellerVisualizationDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("P300 Speller Visualization"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Yann Renard"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Visualizes the alphabet for P300 spellers"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Visualization/Presentation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-select-font"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_P300SpellerVisualization; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmP300SpellerVisualization; }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Sequence stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Target stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Row selection stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Column selection stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addOutput("Target / Non target flagging (deprecated)", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Interface filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-speller.ui");
				rBoxAlgorithmPrototype.addSetting("Row stimulation base", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Column stimulation base", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_07");

				rBoxAlgorithmPrototype.addSetting("Flash background color", OV_TypeId_Color, "10,10,10");
				rBoxAlgorithmPrototype.addSetting("Flash foreground color", OV_TypeId_Color, "100,100,100");
				rBoxAlgorithmPrototype.addSetting("Flash font size", OV_TypeId_Integer, "100");

				rBoxAlgorithmPrototype.addSetting("No flash background color", OV_TypeId_Color, "0,0,0");
				rBoxAlgorithmPrototype.addSetting("No flash foreground color", OV_TypeId_Color, "50,50,50");
				rBoxAlgorithmPrototype.addSetting("No flash font size", OV_TypeId_Integer, "75");

				rBoxAlgorithmPrototype.addSetting("Target background color", OV_TypeId_Color, "10,40,10");
				rBoxAlgorithmPrototype.addSetting("Target foreground color", OV_TypeId_Color, "60,100,60");
				rBoxAlgorithmPrototype.addSetting("Target font size", OV_TypeId_Integer, "100");

				rBoxAlgorithmPrototype.addSetting("Selected background color", OV_TypeId_Color, "70,20,20");
				rBoxAlgorithmPrototype.addSetting("Selected foreground color", OV_TypeId_Color, "30,10,10");
				rBoxAlgorithmPrototype.addSetting("Selected font size", OV_TypeId_Integer, "100");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300SpellerVisualizationDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_P300SpellerVisualization_H__
