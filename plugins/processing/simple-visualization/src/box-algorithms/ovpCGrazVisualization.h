#ifndef __OpenViBEPlugins_SimpleVisualization_CGrazVisualization_H__
#define __OpenViBEPlugins_SimpleVisualization_CGrazVisualization_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <gtk/gtk.h>

#include <vector>
#include <string>
#include <map>
#include <deque>

namespace TCPTagging
{
	class IStimulusSender; // fwd declare
};

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{
		enum EArrowDirection
		{
			EArrowDirection_None = 0,
			EArrowDirection_Left,
			EArrowDirection_Right,
			EArrowDirection_Up,
			EArrowDirection_Down,
		};

		enum EGrazVisualizationState
		{
			EGrazVisualizationState_Idle,
			EGrazVisualizationState_Reference,
			EGrazVisualizationState_Cue,
			EGrazVisualizationState_ContinousFeedback
		};

		/**
		*/
		class CGrazVisualization :
			virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CGrazVisualization();

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			virtual void redraw();
			virtual void resize(uint32_t ui32Width, uint32_t ui32Height);

		public:

			void flushQueue();					// Sends all accumulated stimuli to the TCP Tagging

		protected:

			virtual void setStimulation(const uint32_t ui32StimulationIndex, const OpenViBE::uint64 ui64StimulationIdentifier, const uint64_t ui64StimulationDate);

			virtual void setMatrixBuffer(const double* pBuffer);

			virtual void processState();


			virtual void drawReferenceCross();
			virtual void drawArrow(EArrowDirection eDirection);
			virtual void drawBar();
			virtual void drawAccuracy();
			virtual void updateConfusionMatrix(double f64Prediction);
			virtual double aggregatePredictions(bool bIncludeAll);

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_GrazVisualization)

		public:
			//! The Builder handler used to create the interface
			GtkBuilder* m_pBuilderInterface;

			GtkWidget* m_pMainWindow;

			GtkWidget* m_pDrawingArea;

			//ebml
			OpenViBEToolkit::TStimulationDecoder<CGrazVisualization> m_oStimulationDecoder;
			OpenViBEToolkit::TStreamedMatrixDecoder<CGrazVisualization> m_oMatrixDecoder;

			EGrazVisualizationState m_eCurrentState;
			EArrowDirection m_eCurrentDirection;

			double m_f64MaxAmplitude;
			double m_f64BarScale;

			//Start and end time of the last buffer
			uint64_t m_ui64StartTime;
			uint64_t m_ui64EndTime;

			bool m_bTwoValueInput;

			GdkPixbuf* m_pOriginalBar;
			GdkPixbuf* m_pLeftBar;
			GdkPixbuf* m_pRightBar;

			GdkPixbuf* m_pOriginalLeftArrow;
			GdkPixbuf* m_pOriginalRightArrow;
			GdkPixbuf* m_pOriginalUpArrow;
			GdkPixbuf* m_pOriginalDownArrow;

			GdkPixbuf* m_pLeftArrow;
			GdkPixbuf* m_pRightArrow;
			GdkPixbuf* m_pUpArrow;
			GdkPixbuf* m_pDownArrow;

			GdkColor m_oBackgroundColor;
			GdkColor m_oForegroundColor;

			std::deque<double> m_vAmplitude; // predictions for the current trial

			bool m_bShowInstruction;
			bool m_bShowFeedback;
			bool m_bDelayFeedback;
			bool m_bShowAccuracy;
			bool m_bPositiveFeedbackOnly;

			uint64_t m_i64PredictionsToIntegrate;

			OpenViBE::CMatrix m_oConfusion;
			
			// For queuing stimulations to the TCP Tagging
			std::vector<uint64_t> m_vStimuliQueue;
			guint m_uiIdleFuncTag;
			TCPTagging::IStimulusSender* m_pStimulusSender;

			uint64_t m_ui64LastStimulation;

		private:
			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};

		/**
		* Plugin's description
		*/
		class CGrazVisualizationDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:
			OpenViBE::CString getName() const override { return OpenViBE::CString("Graz visualization"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Bruno Renier, Jussi T. Lindgren"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INRIA/IRISA"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Visualization plugin for the Graz experiment"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Visualization/Feedback plugin for the Graz experiment"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Visualization/Presentation"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_GrazVisualization; }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-fullscreen"); }
			OpenViBE::Plugins::IPluginObject* create() override { return new CGrazVisualization(); }

			bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const override
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rPrototype.addInput("Amplitude", OV_TypeId_StreamedMatrix);

				rPrototype.addSetting("Show instruction", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Show feedback", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Delay feedback", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Show accuracy", OV_TypeId_Boolean, "false");
				rPrototype.addSetting("Predictions to integrate", OV_TypeId_Integer, "5");
				rPrototype.addSetting("Positive feedback only", OV_TypeId_Boolean, "false");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_GrazVisualizationDesc)
		};
	};
};

#endif
