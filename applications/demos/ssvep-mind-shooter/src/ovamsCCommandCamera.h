#ifndef __OpenViBEApplication_CCommandCamera_H__
#define __OpenViBEApplication_CCommandCamera_H__

#include <map>

#include "ovamsICommandOIS.h"

namespace OpenViBESSVEPMindShooter
{
	class CVRPNServer;

	class CCommandCamera : public ICommandOIS
	{
	public:
		CCommandCamera(CApplication* poApplication);
		~CCommandCamera();

		void processFrame() override;

		void receiveKeyPressedEvent(const OIS::KeyCode oKey) override;
		void receiveKeyReleasedEvent(const OIS::KeyCode oKey) override;
		void receiveMouseEvent(const OIS::MouseEvent& oEvent) override;

	private:
		void updateCamera();
		std::map<OIS::KeyCode, bool> m_vKeyPressed;

		bool m_bCameraMode;
	};
}


#endif // __OpenViBEApplication_CCommandCamera_H__
