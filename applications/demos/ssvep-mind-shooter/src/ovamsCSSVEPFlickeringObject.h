#ifndef __OpenViBEApplication_CSSVEPFlickeringObject_H__
#define __OpenViBEApplication_CSSVEPFlickeringObject_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

namespace OpenViBESSVEPMindShooter
{
	class CSSVEPFlickeringObject
	{
	public:
		//SP			CSSVEPFlickeringObject( Ogre::SceneNode* poObjectNode, uint32_t ui32LitFrames, uint32_t ui32DarkFrames );
		CSSVEPFlickeringObject(Ogre::SceneNode* poObjectNode, uint64_t ui64StimulationPattern);
		~CSSVEPFlickeringObject() {};

		virtual void setVisible(bool bVisibility);
		virtual void processFrame();

	protected:
		Ogre::SceneNode* m_poObjectNode;
		uint32_t m_ui32CurrentFrame;
		//SP			uint32_t m_ui32LitFrames;
		//SP			uint32_t m_ui32DarkFrames;
		uint64_t m_ui64StimulationPattern;

	private:

		bool m_bVisible;
		int m_iId;
	};
}


#endif // __OpenViBEApplication_CSSVEPFlickeringObject_H__
