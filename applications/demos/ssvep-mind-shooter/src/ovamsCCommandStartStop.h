#ifndef __OpenViBEApplication_CCommandStartStop_H__
#define __OpenViBEApplication_CCommandStartStop_H__


#include "ovamsICommandOIS.h"

namespace OpenViBESSVEPMindShooter
{
	class CVRPNServer;

	class CCommandStartStop : public ICommandOIS
	{
	public:
		CCommandStartStop(CApplication* poApplication);
		~CCommandStartStop();

		void processFrame() override;

		void receiveKeyPressedEvent(const OIS::KeyCode oKey) override;
		void receiveKeyReleasedEvent(const OIS::KeyCode oKey) override;

	private:
		CVRPNServer* m_poVRPNServer;
	};
}


#endif // __OpenViBEApplication_CCommandStartStop_H__
