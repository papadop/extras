#ifndef __OpenViBEKernel_Kernel_Log_CLogListenerFile_H__
#define __OpenViBEKernel_Kernel_Log_CLogListenerFile_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>
#include <cstdio>

#define OVK_ClassId_Kernel_Log_LogListenerFileBuffered OpenViBE::CIdentifier(0x0FD252FB, 0x315C1A97)

namespace OpenViBE
{
	namespace Kernel
	{
		class CLogListenerFileBuffered : public ILogListener
		{
		public:

			CLogListenerFileBuffered(const IKernelContext& rKernelContext, const CString& sApplicationName, const CString& sLogFilename);
			~CLogListenerFileBuffered();

			bool isActive(ELogLevel eLogLevel) override;
			bool activate(ELogLevel eLogLevel, bool bActive) override;
			bool activate(ELogLevel eStartLogLevel, ELogLevel eEndLogLevel, bool bActive) override;
			bool activate(bool bActive) override;

			void log(const time64 time64Value) override;

			void log(const uint64_t ui64Value) override;
			void log(const uint32_t ui32Value) override;
			void log(const uint16_t ui16Value) override;
			void log(const uint8_t ui8Value) override;

			void log(const int64_t i64Value) override;
			void log(const int32_t i32Value) override;
			void log(const int16_t i16Value) override;
			void log(const int8_t i8Value) override;

			void log(const double f64Value) override;
			void log(const float f32Value) override;

			void log(const bool bValue) override;

			void log(const CIdentifier& rValue) override;
			void log(const CString& rValue) override;
			void log(const char* pValue) override;

			void log(const ELogLevel eLogLevel) override;
			void log(const ELogColor eLogColor) override;

			_IsDerivedFromClass_Final_(OpenViBE::Kernel::ILogListener, OVK_ClassId_Kernel_Log_LogListenerFileBuffered);

		protected:

			std::map<ELogLevel, bool> m_vActiveLevel;
			CString m_sApplicationName;
			CString m_sLogFilename;

			FILE* m_f;
		};
	};
};

#endif // __OpenViBEKernel_Kernel_Log_CLogListenerFile_H__
