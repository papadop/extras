/// Control ship movements with the keyboard

#ifndef __OpenViBEApplication_CCommandControlDiscreteOIS_H__
#define __OpenViBEApplication_CCommandControlDiscreteOIS_H__

#include <map>

#include "../ovamsICommandOIS.h"

namespace OpenViBESSVEPMindShooter
{
	class CImpactApplication;

	class CCommandControlDiscreteOIS : public ICommandOIS
	{
	public:
		CCommandControlDiscreteOIS(CImpactApplication* poApplication);
		~CCommandControlDiscreteOIS() {}

		void processFrame() override;

		void receiveKeyPressedEvent(const OIS::KeyCode oKey) override;
		void receiveKeyReleasedEvent(const OIS::KeyCode oKey) override;
	private:
		std::map<OIS::KeyCode, bool> m_vKeyPressed;
	};
}


#endif // __OpenViBEApplication_CCommandControlDiscreteOIS_H__
