#ifndef __OpenViBEApplication_CGenericStimulatorApplication_H__
#define __OpenViBEApplication_CGenericStimulatorApplication_H__

#include <iostream>
#include <CEGUI.h>

#include "../ovamsCApplication.h"
#include "ovamsCGenericStimulatorFlickeringObject.h"

namespace OpenViBESSVEPMindShooter
{
	class CGenericStimulatorApplication : public CApplication
	{
	public:
		CGenericStimulatorApplication(OpenViBE::CString sScenarioDir);
		~CGenericStimulatorApplication() {}

		bool setup(OpenViBE::Kernel::IKernelContext* poKernelContext) override;
		void setTarget(int32_t i32Target) override;

		void startExperiment() override;
		void startFlickering() override;
		void stopFlickering() override;


	private:
		bool m_bActive;
		void processFrame(uint32_t ui32CurrentFrame) override;
		void addObject(CGenericStimulatorFlickeringObject* target);

		std::vector<CGenericStimulatorFlickeringObject*> m_oObjects;

		time_t m_ttStartTime;

		CEGUI::Window* m_poInstructionsReady;
	};
}


#endif // __OpenViBEApplication_CGenericStimulatorApplication_H__
