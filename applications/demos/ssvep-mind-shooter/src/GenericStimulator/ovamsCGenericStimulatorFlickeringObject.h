#ifndef __OpenViBEApplication_CGenericStimulatorFlickeringObject_H__
#define __OpenViBEApplication_CGenericStimulatorFlickeringObject_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

#include "../ovamsCSSVEPFlickeringObject.h"
#include "../ovamsCBasicPainter.h"

/**
 */
namespace OpenViBESSVEPMindShooter
{
	class CGenericStimulatorApplication;

	class CGenericStimulatorFlickeringObject : public CSSVEPFlickeringObject
	{
	public:
		static CGenericStimulatorFlickeringObject* createGenericFlickeringObject(uint32_t);
		static void initialize(CGenericStimulatorApplication* poApplication);

		void connectToNode(Ogre::SceneNode* poSceneNode);
		void setTarget(bool bIsTarget);

		bool isTarget()
		{
			return m_poPointer->getVisible();
		}

	private:
		static CGenericStimulatorApplication* m_poApplication;
		static Ogre::SceneNode* m_poParentNode;
		static CBasicPainter* m_poPainter;
		static float m_f32TargetWidth;
		static float m_f32TargetHeight;
		static Ogre::ColourValue m_oLightColour;
		static Ogre::ColourValue m_oDarkColour;

		CGenericStimulatorFlickeringObject(float f32PosX, float f32PosY, Ogre::ColourValue oColour, OpenViBE::CString sMaterial, uint32_t ui32StimulationPattern);


		Ogre::SceneNode* m_poElementNode;
		Ogre::MovableObject* m_poPointer;
	};
}


#endif // __OpenViBEApplication_CGenericStimulatorFlickeringObject_H__
