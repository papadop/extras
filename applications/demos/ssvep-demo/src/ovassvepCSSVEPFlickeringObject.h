#ifndef __OpenViBEApplication_CSSVEPFlickeringObject_H__
#define __OpenViBEApplication_CSSVEPFlickeringObject_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

namespace OpenViBESSVEP
{
	class CSSVEPFlickeringObject
	{
	public:
		CSSVEPFlickeringObject(Ogre::SceneNode* poObjectNode, uint32_t ui32LitFrames, uint32_t ui32DarkFrames);
		~CSSVEPFlickeringObject() {};

		virtual void setVisible(bool bVisibility);
		virtual void processFrame();

	protected:
		Ogre::SceneNode* m_poObjectNode;
		uint32_t m_ui32CurrentFrame;
		uint32_t m_ui32LitFrames;
		uint32_t m_ui32DarkFrames;

	private:

		bool m_bVisible;
	};
}


#endif // __OpenViBEApplication_CSSVEPFlickeringObject_H__
