#ifndef __OpenViBEApplication_CTrainerFlickeringObject_H__
#define __OpenViBEApplication_CTrainerFlickeringObject_H__

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

#include "../ovassvepCSSVEPFlickeringObject.h"
#include "../ovassvepCBasicPainter.h"

/**
 */
namespace OpenViBESSVEP
{
	class CTrainerApplication;

	class CTrainerFlickeringObject : public CSSVEPFlickeringObject
	{
	public:
		static CTrainerFlickeringObject* createTrainerFlickeringObject(uint32_t);
		static void initialize(CTrainerApplication* poApplication);

		void connectToNode(Ogre::SceneNode* poSceneNode);
		void setTarget(bool bIsTarget);

		bool isTarget()
		{
			return m_poPointer->getVisible();
		}

	private:
		static CTrainerApplication* m_poApplication;
		static Ogre::SceneNode* m_poParentNode;
		static CBasicPainter* m_poPainter;
		static float m_f32TargetWidth;
		static float m_f32TargetHeight;
		static Ogre::ColourValue m_oLightColour;
		static Ogre::ColourValue m_oDarkColour;

		CTrainerFlickeringObject(float f32PosX, float f32PosY, Ogre::ColourValue oColour, OpenViBE::uint8 ui8LitFrames, uint8_t ui8DarkFrames);


		Ogre::SceneNode* m_poElementNode;
		Ogre::MovableObject* m_poPointer;
	};
}


#endif // __OpenViBEApplication_CTrainerFlickeringObject_H__
