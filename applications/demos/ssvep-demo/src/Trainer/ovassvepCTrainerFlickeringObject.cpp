#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCTrainerFlickeringObject.h"
#include "ovassvepCTrainerApplication.h"

using namespace Ogre;
using namespace OpenViBESSVEP;

SceneNode* CTrainerFlickeringObject::m_poParentNode = nullptr;
CBasicPainter* CTrainerFlickeringObject::m_poPainter = nullptr;
ColourValue CTrainerFlickeringObject::m_oLightColour = ColourValue(1.0f, 1.0f, 1.0f);
ColourValue CTrainerFlickeringObject::m_oDarkColour = ColourValue(0.0f, 0.0f, 0.0f);
float CTrainerFlickeringObject::m_f32TargetWidth = 0.2f;
float CTrainerFlickeringObject::m_f32TargetHeight = 0.2f;
CTrainerApplication* CTrainerFlickeringObject::m_poApplication = nullptr;

void CTrainerFlickeringObject::initialize(CTrainerApplication* poApplication)
{
	m_poApplication = poApplication;
	OpenViBE::Kernel::IConfigurationManager* l_poConfigurationManager = poApplication->getConfigurationManager();

	m_poPainter = poApplication->getPainter();
	m_poParentNode = poApplication->getSceneNode();

	m_f32TargetWidth = (float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetWidth}"));
	m_f32TargetHeight = (float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetHeight}"));

	m_oLightColour = ColourValue(
		(float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetLightColourRed}")),
		(float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetLightColourGreen}")),
		(float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetLightColourBlue}")));

	m_oDarkColour = ColourValue(
		(float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetDarkColourRed}")),
		(float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetDarkColourGreen}")),
		(float)(l_poConfigurationManager->expandAsFloat("${SSVEP_TargetDarkColourBlue}")));
}

CTrainerFlickeringObject* CTrainerFlickeringObject::createTrainerFlickeringObject(uint32_t l_i32TargetId)
{
	OpenViBE::Kernel::IConfigurationManager* l_poConfigurationManager = m_poApplication->getConfigurationManager();

	if (m_poPainter != nullptr)
	{
		ColourValue l_oCurrentTargetColour = (l_i32TargetId == 0) ? m_oDarkColour : m_oLightColour;

		char l_sTargetIdString[255];
		sprintf(l_sTargetIdString, "%d", l_i32TargetId);

		OpenViBE::CIdentifier l_oTargetId = l_poConfigurationManager->createConfigurationToken("SSVEPTarget_Id", OpenViBE::CString(l_sTargetIdString));

		float l_f32TargetX = (float)(l_poConfigurationManager->expandAsFloat("${SSVEP_Target_X_${SSVEPTarget_Id}}"));
		float l_f32TargetY = (float)(l_poConfigurationManager->expandAsFloat("${SSVEP_Target_Y_${SSVEPTarget_Id}}"));
		uint32_t l_ui32FramesL = (*(m_poApplication->getFrequencies()))[l_i32TargetId].first;
		uint32_t l_ui32FramesD = (*(m_poApplication->getFrequencies()))[l_i32TargetId].second;

		m_poApplication->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "New trainer object : id=" << l_i32TargetId << " litFrames=" << l_ui32FramesL << " darkFrames=" << l_ui32FramesD << "\n";

		l_poConfigurationManager->releaseConfigurationToken(l_oTargetId);

		return new CTrainerFlickeringObject(l_f32TargetX, l_f32TargetY, l_oCurrentTargetColour, l_ui32FramesL, l_ui32FramesD);
	}
	else
	{
		m_poApplication->getLogManager() << OpenViBE::Kernel::LogLevel_Fatal << "TrainerTarget object was not properly initialized\n";
		return nullptr;
	}
}

CTrainerFlickeringObject::CTrainerFlickeringObject(float f32PosX, float f32PosY, ColourValue oColour, OpenViBE::uint8 ui8LitFrames, uint8_t ui8DarkFrames) :
	CSSVEPFlickeringObject(nullptr, ui8LitFrames, ui8DarkFrames)
{
	SceneNode* l_poPointerNode;

	MovableObject* l_poLitObject;
	MovableObject* l_poDarkObject;

	m_poElementNode = m_poParentNode->createChildSceneNode();
	m_poObjectNode = m_poElementNode->createChildSceneNode();
	l_poPointerNode = m_poElementNode->createChildSceneNode();

	RealRect l_oRectangle(f32PosX - m_f32TargetWidth / 2, f32PosY + m_f32TargetHeight / 2, f32PosX + m_f32TargetWidth / 2, f32PosY - m_f32TargetHeight / 2);

	l_poLitObject = m_poPainter->paintRectangle(l_oRectangle, oColour);

	m_poObjectNode->attachObject(l_poLitObject);
	l_poLitObject->setVisible(true);

	l_poDarkObject = m_poPainter->paintRectangle(l_oRectangle, m_oDarkColour);
	m_poObjectNode->attachObject(l_poDarkObject);
	l_poDarkObject->setVisible(false);

	m_poPointer = m_poPainter->paintTriangle(
		Point(f32PosX - 0.05f, f32PosY + m_f32TargetHeight),
		Point(f32PosX, f32PosY + m_f32TargetHeight - 0.05f),
		Point(f32PosX + 0.05f, f32PosY + m_f32TargetHeight),
		ColourValue(1, 1, 0));

	l_poPointerNode->attachObject(m_poPointer);
	m_poPointer->setVisible(false);
}

void CTrainerFlickeringObject::setTarget(bool bIsTarget)
{
	m_poPointer->setVisible(bIsTarget);
}


#endif
