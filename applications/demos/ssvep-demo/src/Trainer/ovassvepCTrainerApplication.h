#ifndef __OpenViBEApplication_CTrainerApplication_H__
#define __OpenViBEApplication_CTrainerApplication_H__

#include <iostream>
#include <CEGUI.h>

#include "../ovassvepCApplication.h"
#include "ovassvepCTrainerFlickeringObject.h"

namespace OpenViBESSVEP
{
	class CTrainerApplication : public CApplication
	{
	public:
		CTrainerApplication();
		~CTrainerApplication() {};

		bool setup(OpenViBE::Kernel::IKernelContext* poKernelContext) override;
		void setTarget(int32_t i32Target);

		void startExperiment() override;
		void startFlickering() override;
		void stopFlickering() override;


	private:
		bool m_bActive;
		void processFrame(uint32_t ui32CurrentFrame) override;
		void addObject(CTrainerFlickeringObject* target);

		std::vector<CTrainerFlickeringObject*> m_oObjects;

		time_t m_ttStartTime;

		CEGUI::Window* m_poInstructionsReady;
	};
}


#endif // __OpenViBEApplication_CTrainerApplication_H__
