#include "GenericVRPNServer.h"

#include <vrpn_Connection.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>

#include <cstdarg>

CGenericVrpnServer* CGenericVrpnServer::m_serverInstance = nullptr;

CGenericVrpnServer::CGenericVrpnServer(const int port)
{
	m_connection = vrpn_create_server_connection(port);
}

CGenericVrpnServer::~CGenericVrpnServer()
{
	deleteInstance();
}

CGenericVrpnServer* CGenericVrpnServer::getInstance(int port)
{
	if (m_serverInstance == nullptr) { m_serverInstance = new CGenericVrpnServer(port); }
	return m_serverInstance;
}

void CGenericVrpnServer::deleteInstance()
{
	for (auto it = m_serverInstance->m_buttonServer.begin(); it != m_serverInstance->m_buttonServer.end(); ++it)
	{
		delete it->second.m_Server;
	}
	m_serverInstance->m_buttonServer.clear();

	for (auto it = m_serverInstance->m_analogServer.begin(); it != m_serverInstance->m_analogServer.end(); ++it)
	{
		delete it->second.m_Server;
	}
	m_serverInstance->m_analogServer.clear();

	delete m_serverInstance;
	m_serverInstance = nullptr;
}

void CGenericVrpnServer::loop()
{
	for (auto it = m_buttonServer.begin(); it != m_buttonServer.end(); ++it) { it->second.m_Server->mainloop(); }
	for (auto it = m_analogServer.begin(); it != m_analogServer.end(); ++it) { it->second.m_Server->mainloop(); }
	m_connection->mainloop();
}


void CGenericVrpnServer::addButton(std::string name, int buttonCount)
{
	SButtonServer serverObject;

	serverObject.m_Server = new vrpn_Button_Server(name.c_str(), m_connection, buttonCount);
	serverObject.m_ButtonCount = buttonCount;

	m_buttonServer.insert(std::pair<std::string, SButtonServer>(name, serverObject));
	m_buttonServer[name].m_Cache.clear();
	m_buttonServer[name].m_Cache.resize(buttonCount);
}

void CGenericVrpnServer::changeButtonState(std::string name, int index, int state)
{
	m_buttonServer[name].m_Server->set_button(index, state);
	m_buttonServer[name].m_Cache[index] = state;
}

int CGenericVrpnServer::getButtonState(std::string name, int index)
{
	return m_buttonServer[name].m_Cache[index];
}

void CGenericVrpnServer::addAnalog(std::string name, int channelCount)
{
	SAnalogServer serverObject;

	serverObject.m_Server = new vrpn_Analog_Server(name.c_str(), m_connection, channelCount);
	serverObject.m_ChannelCount = channelCount;

	m_analogServer.insert(std::pair<std::string, SAnalogServer>(name, serverObject));
}

void CGenericVrpnServer::changeAnalogState(std::string name, ...)
{
	double* channels = m_analogServer[name].m_Server->channels();

	va_list list;
	va_start(list, name);

	for (int i = 0; i < m_analogServer[name].m_ChannelCount; i++)
	{
		channels[i] = va_arg(list, double);
	}

	va_end(list);

	m_analogServer[name].m_Server->report();
}

double* CGenericVrpnServer::getAnalogChannels(std::string name)
{
	return m_analogServer[name].m_Server->channels();
}

void CGenericVrpnServer::reportAnalogChanges(std::string name)
{
	m_analogServer[name].m_Server->report();
}
