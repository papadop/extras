#pragma once

#include <openvibe/ov_all.h>

#include <fs/Files.h>

namespace OpenViBETracker
{

	/**
	 * \class WorkspaceNotes 
	 * \brief An overly complicated wrapper for a text string.
	 * \author J. T. Lindgren
	 *
	 * This class is needed in order to use CCommentEditorDialog from Designer. The
	 * implementation is a quick hack. IDs and Attributable features are not supported at all.
	 *
	 */
	class WorkspaceNotes : public OpenViBE::Kernel::IComment
	{
	public:
		OpenViBE::CIdentifier getIdentifier() const override { return 0; }

		OpenViBE::CString getText() const override { return m_Notes; }

		virtual bool setIdentifier(
			const OpenViBE::CIdentifier& rIdentifier) override { return true; }

		bool setText(
			const OpenViBE::CString& sText) override
		{
			m_Notes = sText;
			return true;
		}

		virtual bool initializeFromExistingComment(
			const IComment& rExisitingComment) override
		{
			m_Notes = rExisitingComment.getText();
			return true;
		}

		// Attributable
		bool addAttribute(
			const OpenViBE::CIdentifier& rAttributeIdentifier,
			const OpenViBE::CString& sAttributeValue) override
		{
			return true;
		}

		bool removeAttribute(
			const OpenViBE::CIdentifier& rAttributeIdentifier) override
		{
			return true;
		}

		bool removeAllAttributes() override { return true; }

		OpenViBE::CString getAttributeValue(
			const OpenViBE::CIdentifier& rAttributeIdentifier) const override
		{
			return OpenViBE::CString("");
		}

		bool setAttributeValue(
			const OpenViBE::CIdentifier& rAttributeIdentifier,
			const OpenViBE::CString& sAttributeValue) override
		{
			return true;
		}

		bool hasAttribute(
			const OpenViBE::CIdentifier& rAttributeIdentifier) const override
		{
			return false;
		}

		bool hasAttributes() const override { return false; }

		OpenViBE::CIdentifier getNextAttributeIdentifier(
			const OpenViBE::CIdentifier& rPreviousIdentifier) const override
		{
			return OpenViBE::CIdentifier();
		}

		OpenViBE::CIdentifier getClassIdentifier() const override { return OpenViBE::CIdentifier(); }

		// Methods specific to this derived class

		// Save the notes
		bool save(const OpenViBE::CString& notesFile)
		{
			std::ofstream output;
			FS::Files::openOFStream(output, notesFile.toASCIIString());
			if (!output.is_open() || output.bad())
			{
				return false;
			}
			output << m_Notes.toASCIIString();

			output.close();

			return true;
		}

		// Load the notes
		bool load(const OpenViBE::CString& notesFile)
		{
			if (FS::Files::fileExists(notesFile.toASCIIString()))
			{
				std::ifstream input;
				FS::Files::openIFStream(input, notesFile.toASCIIString());
				if (input.is_open() && !input.bad())
				{
					std::string str((std::istreambuf_iterator<char>(input)),
									std::istreambuf_iterator<char>());
					m_Notes = OpenViBE::CString(str.c_str());
					input.close();
					return true;
				}
			}

			return false;
		}

	protected:

		OpenViBE::CString m_Notes;
	};
};
