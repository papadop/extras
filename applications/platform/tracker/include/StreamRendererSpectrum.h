//
// OpenViBE Tracker
//

#pragma once

#include "StreamRendererBase.h"

#include "TypeSpectrum.h"

namespace OpenViBETracker
{

	/**
	 * \class StreamRendererSpectrum 
	 * \brief Renderer for Spectrum streams
	 * \author J. T. Lindgren
	 *
	 */
	class StreamRendererSpectrum : public StreamRendererBase
	{
	public:
		StreamRendererSpectrum(const OpenViBE::Kernel::IKernelContext& ctx, std::shared_ptr<const Stream<TypeSpectrum>> stream)
			: StreamRendererBase(ctx), m_Stream(stream) { };

		bool initialize() override;

		bool spool(ovtime_t startTime, ovtime_t endTime) override
		{
			return spoolImpl<TypeSpectrum, StreamRendererSpectrum>(m_Stream, *this, startTime, endTime);
		}

		OpenViBE::CString renderAsText(uint32_t indent) const override;
		bool showChunkList() override;

	protected:

		friend bool spoolImpl<TypeSpectrum, StreamRendererSpectrum>(std::shared_ptr<const Stream<TypeSpectrum>> stream, StreamRendererSpectrum& renderer, ovtime_t startTime, ovtime_t endTime);

		bool finalize() override;
		bool reset(ovtime_t startTime, ovtime_t endTime) override;
		virtual bool push(const TypeSpectrum::Buffer& chunk, bool zeroInput = false);
		bool mouseButton(int32_t x, int32_t y, int32_t button, int status) override;

		bool preDraw() override;
		bool draw() override;

		size_t m_ChannelCount = 0;
		size_t m_SpectrumElements = 0;

		std::vector<float> m_vSwap;

		std::shared_ptr<const Stream<TypeSpectrum>> m_Stream;
		GtkWidget* m_StreamListWindow = nullptr;

		StreamRendererSpectrum() = delete;
	};
};
