//
//

#pragma once

#include <string>
#include <vector>

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <system/ovCTime.h>

#include "Contexted.h"

// using namespace OpenViBE;
// using namespace OpenViBE::Kernel;

namespace OpenViBE
{
	namespace Kernel
	{

		/**
		 * \class IBoxIOProxy
		 * \brief This proxy is intended for reusing codecs from the toolkit. 
		 * @fixme it'd be more elegant not to duplicate this interface as a very similar one is already implemented in BoxAdapterHelper. 
		 * however unifying the two would need some work.
		 * \author J. T. Lindgren
		 *
		 */
		class OV_API_Export IBoxIOProxy : public IBoxIO
		{
		public:
			virtual uint32_t getInputChunkCount(
				const uint32_t inputIndex) const override { return 0; }

			virtual bool getInputChunk(
				const uint32_t inputIndex,
				const uint32_t ui32ChunkIndex,
				uint64_t& rStartTime,
				uint64_t& rEndTime,
				uint64_t& rChunkSize,
				const uint8_t*& rpChunkBuffer) const override { return true; }

			virtual const IMemoryBuffer* getInputChunk(
				const uint32_t inputIndex,
				const uint32_t ui32ChunkIndex) const override { return &m_InBuffer; };

			virtual uint64_t getInputChunkStartTime(
				const uint32_t inputIndex,
				const uint32_t ui32ChunkIndex) const override { return 0; };

			virtual uint64_t getInputChunkEndTime(
				const uint32_t inputIndex,
				const uint32_t ui32ChunkIndex) const override { return 0; };

			virtual bool markInputAsDeprecated(
				const uint32_t inputIndex,
				const uint32_t ui32ChunkIndex) override { return true; };

			virtual uint64_t getOutputChunkSize(
				const uint32_t ui32OutputIndex) const override { return m_OutBuffer.getSize(); };

			virtual bool setOutputChunkSize(
				const uint32_t ui32OutputIndex,
				const uint64_t ui64Size,
				const bool bDiscard = true) override
			{
				return m_OutBuffer.setSize(ui64Size, true);
			};

			virtual uint8_t* getOutputChunkBuffer(
				const uint32_t ui32OutputIndex) override { return m_OutBuffer.getDirectPointer(); };

			virtual bool appendOutputChunkData(
				const uint32_t ui32OutputIndex,
				const uint8_t* pBuffer,
				const uint64_t ui64BufferSize) override
			{
				return false; /* not implemented */
			};

			virtual IMemoryBuffer* getOutputChunk(
				const uint32_t ui32OutputIndex) override { return &m_OutBuffer; };

			virtual bool markOutputAsReadyToSend(
				const uint32_t ui32OutputIndex,
				const uint64_t ui64StartTime,
				const uint64_t ui64EndTime) override { return true; }

			CIdentifier getClassIdentifier() const override { return 0; };

			CMemoryBuffer m_InBuffer;
			CMemoryBuffer m_OutBuffer;
		};
	}
}

/**
 * \class BoxAlgorithmProxy 
 * \brief This proxy is needed in order to use the stream codecs from the toolkit
 * \author J. T. Lindgren
 *
 */
class BoxAlgorithmProxy : protected OpenViBETracker::Contexted
{
public:
	BoxAlgorithmProxy(const OpenViBE::Kernel::IKernelContext& ctx) : Contexted(ctx) { };

	OpenViBE::Kernel::IBoxIO& getDynamicBoxContext() { return dummy; };
	bool markAlgorithmAsReadyToProcess() { return true; }

	OpenViBE::Kernel::IAlgorithmManager& getAlgorithmManager() { return Contexted::getAlgorithmManager(); };

	OpenViBE::Kernel::IBoxIOProxy dummy;
};
