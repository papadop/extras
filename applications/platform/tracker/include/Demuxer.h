#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>
#include <stack>
#include <map>
#include <vector>

#include <openvibe/ov_all.h>
#include <openvibe/ovCMatrix.h>

#include <socket/IConnectionServer.h>

#include <fs/Files.h>

#include <ebml/CReader.h>
#include <ebml/CReaderHelper.h>

#include "StreamBundle.h"

#include "EBMLSourceFile.h"
#include "Contexted.h"

#include "EncodedChunk.h"
#include "Decoder.h"

class CClientHandler;
class OutputEncoder;

class EBMLSource;

namespace OpenViBETracker
{

	/**
	 * \class Demuxer 
	 * \brief Demuxes (and decodes) EBML streams
	 * \details EBML containers such as .ov multiplex one or more streams. Demuxer takes an EBML source and splits and decodes it to a Stream Bundle.
	 *
	 * It has similar purpose as the Generic Stream Reader box.
	 *
	 * \author J. T. Lindgren
	 *
	 */
	class Demuxer : public EBML::IReaderCallback, protected Contexted
	{
	public:

		Demuxer(const IKernelContext& ctx, EBMLSourceFile& origin, StreamBundle& target)
			: Contexted(ctx)
			  , m_origin(origin)
			  , m_target(target)
			  , m_oReader(*this)
		{
			initialize();
		};

		bool initialize();
		bool uninitialize();

		// Reads some data from the origin and decodes it to target. Returns false on EOF.
		bool step();
		bool stop();

	protected:

		EBMLSourceFile& m_origin;
		StreamBundle& m_target;

		bool playingStarted;

		EBML::CReader m_oReader;
		EBML::CReaderHelper m_oReaderHelper;

		EncodedChunk m_oPendingChunk;

		uint32_t m_ChunksSent = 0;

		bool m_bPending = false;
		bool m_bHasEBMLHeader = false;

		std::stack<EBML::CIdentifier> m_vNodes;
		std::map<uint32_t, uint32_t> m_vStreamIndexToOutputIndex;
		std::map<uint32_t, CIdentifier> m_vStreamIndexToTypeIdentifier;

		std::vector<uint64_t> m_ChunkStreamType;

		std::vector<DecoderBase*> m_vDecoders;

	private:

		EBML::boolean isMasterChild(const EBML::CIdentifier& rIdentifier) override;
		void openChild(const EBML::CIdentifier& rIdentifier) override;
		void processChildData(const void* pBuffer, const EBML::uint64 ui64BufferSize) override;
		void closeChild() override;
	};
};
