#pragma once

#include "ovd_base.h"
#include "ovdCLogListenerDesigner.h"

#include <vector>
#include <map>

namespace OpenViBETracker
{

	/**
	 * \class CLogListenerTracker
	 * \brief Log listener wrapper for CLogListenerDesigner that can be called from multiple threads
	 * \author J. T. Lindgren
	 *
	 */
	class CLogListenerTracker : public OpenViBE::Kernel::ILogListener
	{
	public:

		CLogListenerTracker(const OpenViBE::Kernel::IKernelContext& rKernelContext, GtkBuilder* pBuilderInterface) : m_oPimpl(rKernelContext, pBuilderInterface) { } ;

		// @note Only call from gtk thread
		virtual void clearMessages()
		{
			std::unique_lock<std::mutex> lock(m_oMutex);
			// std::cout << "Intercepted\n"; 
			m_oPimpl.clearMessages();
		}

		bool isActive(OpenViBE::Kernel::ELogLevel eLogLevel) override
		{
			std::unique_lock<std::mutex> lock(m_oMutex);
			return m_oPimpl.isActive(eLogLevel);
		} ;

		bool activate(OpenViBE::Kernel::ELogLevel eLogLevel, bool bActive) override
		{
			std::unique_lock<std::mutex> lock(m_oMutex);
			return m_oPimpl.activate(eLogLevel, bActive);
		};

		bool activate(OpenViBE::Kernel::ELogLevel eStartLogLevel, OpenViBE::Kernel::ELogLevel eEndLogLevel, bool bActive) override
		{
			std::unique_lock<std::mutex> lock(m_oMutex);
			return m_oPimpl.activate(eStartLogLevel, eEndLogLevel, bActive);
		};

		bool activate(bool bActive) override
		{
			std::unique_lock<std::mutex> lock(m_oMutex);
			return m_oPimpl.activate(bActive);
		};

		// This function encapsulates CLogListenerDesigner so that its gtk calls will be run from g_idle_add().
		template <class T>
		void logWrapper(T param)
		{
			auto ptr = new std::pair<CLogListenerTracker*, T>(this, param);

			auto fun = [](gpointer pUserData)
			{
				auto ptr = static_cast<std::pair<CLogListenerTracker*, T>*>(pUserData);

				// LogListenerDesigner may not be thread safe, so we lock here for safety as activate() and isActive() do not go through the g_idle_add().
				std::unique_lock<std::mutex> lock(ptr->first->getMutex());

				ptr->first->getImpl().log(ptr->second);
				delete ptr;

				return (gboolean)FALSE;
			};

			g_idle_add(fun, ptr);
		}

		// Specialization for const char*. It frees memory. We don't call it 
		// template<> logWrapper() to avoid gcc complaining.
		void logWrapperCharPtr(const char* param)
		{
			auto ptr = new std::pair<CLogListenerTracker*, const char*>(this, param);

			auto fun = [](gpointer pUserData)
			{
				auto ptr = static_cast<std::pair<CLogListenerTracker*, const char*>*>(pUserData);

				// LogListenerDesigner may not be thread safe, so we lock here for safety as activate() and isActive() do not go through the g_idle_add().
				std::unique_lock<std::mutex> lock(ptr->first->getMutex());

				ptr->first->getImpl().log(ptr->second);
				delete[] ptr->second;
				delete ptr;

				return (gboolean)FALSE;
			};

			g_idle_add(fun, ptr);
		}

		void log(const OpenViBE::time64 time64Value) override { logWrapper(time64Value); };

		void log(const uint64_t ui64Value) override { logWrapper(ui64Value); };
		void log(const uint32_t ui32Value) override { logWrapper(ui32Value); };
		void log(const uint16_t ui16Value) override { logWrapper(ui16Value); };
		void log(const uint8_t ui8Value) override { logWrapper(ui8Value); };

		void log(const int64_t i64Value) override { logWrapper(i64Value); };
		void log(const int32_t i32Value) override { logWrapper(i32Value); };
		void log(const int16_t i16Value) override { logWrapper(i16Value); };
		void log(const int8_t i8Value) override { logWrapper(i8Value); };

		void log(const double f64Value) override { logWrapper(f64Value); };
		void log(const float f32Value) override { logWrapper(f32Value); };

		void log(const bool bValue) override { logWrapper(bValue); };

		void log(const OpenViBE::Kernel::ELogLevel eLogLevel) override { logWrapper(eLogLevel); };
		void log(const OpenViBE::Kernel::ELogColor eLogColor) override { logWrapper(eLogColor); };

		// With these calls we need to make copies, as by the time the actual calls are run in gtk_idle, the originals may be gone
		void log(const OpenViBE::CIdentifier& rValue) override
		{
			OpenViBE::CIdentifier nonRef = rValue;
			logWrapper(nonRef);
		};

		void log(const OpenViBE::CString& rValue) override
		{
			OpenViBE::CString nonRef = rValue;
			logWrapper(nonRef);
		};

		void log(const char* pValue) override
		{ 
			// This one is tricksy, we cannot simply use CString as we'd lose color, but with char* we need to free the memory. Use a specialization.
#if defined(TARGET_OS_Windows)
			const char* valueCopy = _strdup(pValue);
#else
				const char* valueCopy = strdup(pValue);
#endif
			logWrapperCharPtr(valueCopy);
		};

		// Callbacks need access to these
		virtual OpenViBEDesigner::CLogListenerDesigner& getImpl() { return m_oPimpl; }
		virtual std::mutex& getMutex() { return m_oMutex; };

		// getClassIdentifier()
		_IsDerivedFromClass_Final_(OpenViBE::Kernel::ILogListener, OV_UndefinedIdentifier);

	protected:

		OpenViBEDesigner::CLogListenerDesigner m_oPimpl;

		std::mutex m_oMutex;
	};
};
