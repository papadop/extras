#ifndef __OpenViBE_AcquisitionServer_CDriftCorrection_H__
#define __OpenViBE_AcquisitionServer_CDriftCorrection_H__

#include "ovas_base.h"

#include <vector>
#include <list>
#include <deque>

namespace OpenViBEAcquisitionServer
{
	typedef enum
	{
		DriftCorrectionPolicy_DriverChoice=0,
		DriftCorrectionPolicy_Forced,
		DriftCorrectionPolicy_Disabled,
	} EDriftCorrectionPolicy;

	/*
	 * \class CDriftCorrection
	 * \author Jussi T. Lindgren / Inria
	 * 
	 * \brief A refactoring of the old drift correction code that was originally mixed into the acquisition server main code.
	 *
	 */
	class CDriftCorrection
	{
	public:

		CDriftCorrection(const OpenViBE::Kernel::IKernelContext& rKernelContext);
		virtual ~CDriftCorrection();

		// To start a new drift estimation and stop it.
		virtual bool start(uint32_t ui32SamplingFrequency, uint64_t ui64StartTime);
		virtual void stop();

		// Estimate the drift
		// 
		// \param ui64NewSamples [in] : How many samples the driver returned
		virtual bool estimateDrift(const uint64_t ui64NewSamples);

		// Request a drift correction
		//
		// \param i64correction [in] : the number of samples to correct
		// \param ui64TotalSamples [out] : Number of total samples after correction for drift
		// \param vPendingBuffers [in/out] : The sample buffer to be corrected
		// \param oPendingStimulationSet [in/out] : The stimulation set to be realigned
		// \param vPaddingBuffer[in] : The sample to repeatedly add if i64Correction > 0
		virtual bool correctDrift(const int64_t i64Correction, uint64_t& ui64TotalSamples,
								  std::deque<std::vector<float>>& vPendingBuffers, OpenViBE::CStimulationSet& oPendingStimulationSet,
								  const std::vector<float>& vPaddingBuffer);
		
		// Status functions
		virtual bool isActive() const { return m_bIsActive; };

		// Prints various statistics but only if drift tolerance was exceeded
		virtual void printStats() const;

		// Result getters
		virtual double getDriftMs() const;		              		// current drift, in ms.
		virtual double getDriftTooFastMax() const;                   // maximum positive drift observed, in ms. (driver gave too many samples)
		virtual double getDriftTooSlowMax() const;		            // maximum negative drift observed, in ms. (driver gave too few samples)
		virtual int64_t getDriftSampleCount() const;	                // current drift, in samples
		virtual int64_t getSuggestedDriftCorrectionSampleCount() const; // number of samples to correct drift with
		virtual int64_t getDriftToleranceSampleCount() const { return m_i64DriftToleranceSampleCount; }

		// Parameter getters and setters
		virtual int64_t getInnerLatencySampleCount() const;
		virtual bool setInnerLatencySampleCount(int64_t i64SampleCount);

		EDriftCorrectionPolicy getDriftCorrectionPolicy() const;
		OpenViBE::CString getDriftCorrectionPolicyStr() const;
		bool setDriftCorrectionPolicy(EDriftCorrectionPolicy eDriftCorrectionPolicy);

		uint64_t getDriftToleranceDurationMs() const;					// In milliseconds
		bool setDriftToleranceDurationMs(uint64_t ui64DriftToleranceDuration);

		uint64_t getJitterEstimationCountForDrift() const;
		bool setJitterEstimationCountForDrift(uint64_t ui64JitterEstimationCountForDrift);

	protected:

		void reset();

		// Computes jitter in fractional samples
		double computeJitter(const uint64_t ui64CurrentTime);

		const OpenViBE::Kernel::IKernelContext& m_rKernelContext;

		// State of the drift correction
		bool m_bInitialized;
		bool m_bStarted;
		bool m_bIsActive;
		bool m_bInitialSkipPeriodPassed;

		// Parameters
		EDriftCorrectionPolicy m_eDriftCorrectionPolicy;
		uint64_t m_ui64DriftToleranceDurationMs;
		int64_t m_i64DriftToleranceSampleCount;
		int64_t m_i64InnerLatencySampleCount;
		uint64_t m_ui64JitterEstimationCountForDrift;
		uint32_t m_ui32SamplingFrequency;
		uint64_t m_ui64InitialSkipPeriod;

		// Results
		double m_f64ReceivedSampleCount;
		double m_f64CorrectedSampleCount;
		double m_f64DriftEstimate;				// In subsample accuracy, e.g. 1.2 samples.
		double m_f64DriftEstimateTooFastMax;		// maximum over time
		double m_f64DriftEstimateTooSlowMax;		// minimum over time

		// Stats
		int64_t m_i64DriftCorrectionSampleCountAdded;
		int64_t m_i64DriftCorrectionSampleCountRemoved;
		uint64_t m_ui64DriftCorrectionCount;

		// Timekeeping
		uint64_t m_ui64StartTime;
		uint64_t m_ui64LastEstimationTime;

		// Jitter estimation buffer. Each entry is the difference between the expected number of 
		// samples and the received number of samples per each call to estimateDrift(). The buffer has subsample accuracy to avoid rounding errors.
		// -> The average of the buffer items is the current aggregated drift estimate (in samples), convertable to ms by getDrift().
		std::list<double> m_vJitterEstimate;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriftCorrection_H__
