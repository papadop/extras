#ifndef __OpenViBE_AcquisitionServer_CAcquisitionServerGUI_H__
#define __OpenViBE_AcquisitionServer_CAcquisitionServerGUI_H__

#include "ovas_base.h"
#include "ovasIDriver.h"
#include "ovasIHeader.h"
#include "ovasCHeader.h"

#include <socket/IConnectionServer.h>

#include <thread>

#include <boost/algorithm/string.hpp>

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	class CAcquisitionServer;
	class CAcquisitionServerThread;
	class IAcquisitionServerPlugin;
	struct PluginSetting;
	class Property;

	class CAcquisitionServerGUI
	{
	public:

		CAcquisitionServerGUI(const OpenViBE::Kernel::IKernelContext& rKernelContext);
		virtual ~CAcquisitionServerGUI();

		virtual bool initialize();

		IDriver& getDriver();
		uint32_t getSampleCountPerBuffer();
		uint32_t getTCPPort();
		IHeader& getHeaderCopy();

		void setStateText(const char* sStateText);
		void setClientText(const char* sClientText);
		void setDriftMs(double f64DriftMs);
		void setImpedance(uint32_t ui32ChannelIndex, double f64Impedance);
		void disconnect();

		// GTK button callbacks
		virtual void buttonPreferencePressedCB(GtkButton* pButton);
		virtual void buttonConfigurePressedCB(GtkButton* pButton);
		virtual void buttonConnectToggledCB(GtkToggleButton* pButton);
		virtual void buttonStartPressedCB(GtkButton* pButton);
		virtual void buttonStopPressedCB(GtkButton* pButton);
		virtual void comboBoxDriverChanged(GtkComboBox* pComboBox);
		virtual void comboBoxSampleCountPerSentBlockChanged(GtkComboBox* pComboBox);

		/// registers a new acquisition server plugin, the plugin is activated immediately
		void registerPlugin(IAcquisitionServerPlugin* plugin);

		/// scans all plugins for settings and puts them into a flat structure easier to handle
		void scanPluginSettings();

		void savePluginSettings();

		class PropertyAndWidget
		{
		public:
			PropertyAndWidget(Property* prop, GtkWidget* widget) : m_pProperty(prop), m_pWidget(widget) { };
			Property* m_pProperty;
			GtkWidget* m_pWidget;
		};
		/// holds references to the plugins' settings for faster access
		std::vector<PropertyAndWidget> m_vPluginProperties;

	protected :

		const OpenViBE::Kernel::IKernelContext& m_rKernelContext;
		IDriver* m_pDriver;
		IDriverContext* m_pDriverContext;
		CAcquisitionServer* m_pAcquisitionServer;
		CAcquisitionServerThread* m_pAcquisitionServerThread;
		CHeader m_oHeaderCopy;

		uint32_t m_ui32SampleCountPerBuffer;

		std::vector<IDriver*> m_vDriver;

		GtkBuilder* m_pBuilderInterface;

		GtkWidget* m_pImpedanceWindow;
		std::vector<GtkWidget*> m_vLevelMesure;

		std::thread* m_pThread;
#if defined TARGET_OS_Windows && defined TARGET_HasMensiaAcquisitionDriver
		void* m_pLibMensiaAcquisition;
#endif
	};
};

#endif // __OpenViBE_AcquisitionServer_CAcquisitionServerGUI_H__
