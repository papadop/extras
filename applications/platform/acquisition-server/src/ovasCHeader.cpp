#include "ovasCHeader.h"

#include <map>
#include <string>

#define _NoValueI_ 0xffffffff

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace std;

//___________________________________________________________________//
//                                                                   //

namespace OpenViBEAcquisitionServer
{
	namespace
	{
		class CHeaderImpl : public IHeader
		{
		public:

			CHeaderImpl();
			virtual ~CHeaderImpl();
			void reset() override;

			// Experiment information
			bool setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier) override;
			bool setSubjectAge(const uint32_t ui32SubjectAge) override;
			bool setSubjectGender(const uint32_t ui32SubjectGender) override;

			uint32_t getExperimentIdentifier() const override;
			uint32_t getSubjectAge() const override;
			uint32_t getSubjectGender() const override;

			bool isExperimentIdentifierSet() const override;
			bool isSubjectAgeSet() const override;
			bool isSubjectGenderSet() const override;

			bool setImpedanceCheckRequested(const bool bImpedanceCheckRequested) override;
			bool isImpedanceCheckRequested() const override;
			uint32_t getImpedanceLimit() const override;
			bool setImpedanceLimit(const uint32_t ui32ImpedanceLimit) override;

			// Chanel information
			bool setChannelCount(const uint32_t ui32ChannelCount) override;
			bool setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName) override;
			bool setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain) override;
			bool setChannelUnits(const uint32_t ui32ChannelIndex, const uint32_t ui32ChannelUnit, const uint32_t ui32ChannelFactor) override;
			// virtual OpenViBE::boolean setChannelLocation(const uint32_t ui32ChannelIndex, const OpenViBE::float32 ui32ChannelLocationX, const OpenViBE::float32 ui32ChannelLocationY, const OpenViBE::float32 ui32ChannelLocationZ);

			uint32_t getChannelCount() const override;
			const char* getChannelName(const uint32_t ui32ChannelIndex) const override;
			float getChannelGain(const uint32_t ui32ChannelIndex) const override;
			bool getChannelUnits(const uint32_t ui32ChannelIndex, uint32_t& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const override;
			// virtual getChannelLocation(const uint32_t ui32ChannelIndex) const;

			bool isChannelCountSet() const override;
			bool isChannelNameSet() const override;
			bool isChannelGainSet() const override;
			// virtual OpenViBE::boolean isChannelLocationSet() const;
			bool isChannelUnitSet() const override;

			// Samples information
			bool setSamplingFrequency(const uint32_t ui32SamplingFrequency) override;

			uint32_t getSamplingFrequency() const override;

			bool isSamplingFrequencySet() const override;

		protected:

			// Experiment information
			uint32_t m_ui32ExperimentIdentifier;
			uint32_t m_ui32SubjectAge;
			uint32_t m_ui32SubjectGender;

			// Chanel information
			uint32_t m_ui32ChannelCount;
			std::map<uint32_t, std::string> m_vChannelName;
			std::map<uint32_t, float> m_vChannelGain;
			std::map<uint32_t, std::pair<uint32, uint32_t>> m_vChannelUnits;

			// Samples information
			uint32_t m_ui32SamplingFrequency;

			// Impedance check
			bool m_bIsImpedanceCheckRequested;
			uint32_t m_ui32ImpedanceLimit;
		};
	};
};

//___________________________________________________________________//
//                                                                   //

CHeaderImpl::CHeaderImpl()
	: m_ui32ExperimentIdentifier(_NoValueI_)
	  , m_ui32SubjectAge(18)
	  , m_ui32SubjectGender(OVTK_Value_Gender_NotSpecified)
	  , m_ui32ChannelCount(_NoValueI_)
	  , m_ui32SamplingFrequency(_NoValueI_)
	  , m_bIsImpedanceCheckRequested(false)
	  , m_ui32ImpedanceLimit(_NoValueI_) {}

CHeaderImpl::~CHeaderImpl() {}

void CHeaderImpl::reset()
{
	m_ui32ExperimentIdentifier = _NoValueI_;
	m_ui32SubjectAge = _NoValueI_;
	m_ui32SubjectGender = _NoValueI_;
	m_ui32ChannelCount = _NoValueI_;
	m_vChannelName.clear();
	m_vChannelGain.clear();
	m_vChannelUnits.clear();
	m_ui32SamplingFrequency = _NoValueI_;
	m_bIsImpedanceCheckRequested = false;
	m_ui32ChannelCount = _NoValueI_;
}

//___________________________________________________________________//
//                                                                   //

// Experiment information
bool CHeaderImpl::setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier)
{
	m_ui32ExperimentIdentifier = ui32ExperimentIdentifier;
	return m_ui32ExperimentIdentifier != _NoValueI_;
}

bool CHeaderImpl::setSubjectAge(const uint32_t ui32SubjectAge)
{
	m_ui32SubjectAge = ui32SubjectAge;
	return m_ui32SubjectAge != _NoValueI_;
}

bool CHeaderImpl::setSubjectGender(const uint32_t ui32SubjectGender)
{
	m_ui32SubjectGender = ui32SubjectGender;
	return m_ui32SubjectGender != _NoValueI_;
}

uint32_t CHeaderImpl::getExperimentIdentifier() const
{
	return m_ui32ExperimentIdentifier;
}

uint32_t CHeaderImpl::getSubjectAge() const
{
	return m_ui32SubjectAge;
}

uint32_t CHeaderImpl::getSubjectGender() const
{
	return m_ui32SubjectGender;
}

boolean CHeaderImpl::setImpedanceCheckRequested(const bool bImpedanceCheckRequested)
{
	m_bIsImpedanceCheckRequested = bImpedanceCheckRequested;
	return true;
}

bool CHeaderImpl::isImpedanceCheckRequested() const
{
	return m_bIsImpedanceCheckRequested;
}

uint32_t CHeaderImpl::getImpedanceLimit() const
{
	return m_ui32ImpedanceLimit;
}

bool CHeaderImpl::setImpedanceLimit(const uint32_t ui32ImpedanceLimit)
{
	m_ui32ImpedanceLimit = ui32ImpedanceLimit;
	return true;
}

bool CHeaderImpl::isExperimentIdentifierSet() const
{
	return m_ui32ExperimentIdentifier != _NoValueI_;
}

bool CHeaderImpl::isSubjectAgeSet() const
{
	return m_ui32SubjectAge != _NoValueI_;
}

bool CHeaderImpl::isSubjectGenderSet() const
{
	return m_ui32SubjectGender != _NoValueI_;
}

//___________________________________________________________________//
//                                                                   //

// Chanel information

bool CHeaderImpl::setChannelCount(const uint32_t ui32ChannelCount)
{
	m_ui32ChannelCount = ui32ChannelCount;
	// m_vChannelName.clear();
	// m_vChannelGain.clear();
	return m_ui32ChannelCount != _NoValueI_;
}

bool CHeaderImpl::setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName)
{
	m_vChannelName[ui32ChannelIndex] = sChannelName;
	return ui32ChannelIndex < m_ui32ChannelCount;
}

bool CHeaderImpl::setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain)
{
	m_vChannelGain[ui32ChannelIndex] = f32ChannelGain;
	return ui32ChannelIndex < m_ui32ChannelCount;
}

bool CHeaderImpl::setChannelUnits(const uint32_t ui32ChannelIndex, const uint32_t ui32ChannelUnit, const uint32_t ui32ChannelFactor)
{
	m_vChannelUnits[ui32ChannelIndex] = std::pair<uint32_t, uint32_t>(ui32ChannelUnit, ui32ChannelFactor);
	return ui32ChannelIndex < m_ui32ChannelCount;
}

// boolean CHeaderImpl::setChannelLocation(const uint32 ui32ChannelIndex, const float32 ui32ChannelLocationX, const float32 ui32ChannelLocationY, const float32 ui32ChannelLocationZ);

uint32_t CHeaderImpl::getChannelCount() const
{
	return m_ui32ChannelCount;
}

const char* CHeaderImpl::getChannelName(const uint32_t ui32ChannelIndex) const
{
	map<uint32_t, string>::const_iterator i = m_vChannelName.find(ui32ChannelIndex);
	if (i == m_vChannelName.end())
	{
		return "";
	}
	return i->second.c_str();
}

float CHeaderImpl::getChannelGain(const uint32_t ui32ChannelIndex) const
{
	map<uint32_t, float>::const_iterator i = m_vChannelGain.find(ui32ChannelIndex);
	if (i == m_vChannelGain.end())
	{
		return (ui32ChannelIndex < m_ui32ChannelCount ? 1.0f : 0.0f);
	}
	return i->second;
}

bool CHeaderImpl::getChannelUnits(const uint32 ui32ChannelIndex, uint32& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const
{
	map<uint32, std::pair<uint32, uint32_t>>::const_iterator i = m_vChannelUnits.find(ui32ChannelIndex);
	if (i == m_vChannelUnits.end())
	{
		ui32ChannelUnit = OVTK_UNIT_Unspecified;
		ui32ChannelFactor = OVTK_FACTOR_Base;

		return false;
	}

	ui32ChannelUnit = (i->second).first;
	ui32ChannelFactor = (i->second).second;

	return true;
}


// CHeaderImpl::getChannelLocation(const uint32 ui32ChannelIndex) const;

bool CHeaderImpl::isChannelCountSet() const
{
	return m_ui32ChannelCount != _NoValueI_ && m_ui32ChannelCount != 0;
}

bool CHeaderImpl::isChannelNameSet() const
{
	return isChannelCountSet();
}

bool CHeaderImpl::isChannelGainSet() const
{
	return isChannelCountSet();
}

// boolean CHeaderImpl::isChannelLocationSet() const

bool CHeaderImpl::isChannelUnitSet() const
{
	return !m_vChannelUnits.empty();
}


//___________________________________________________________________//
//                                                                   //

// Samples information
bool CHeaderImpl::setSamplingFrequency(const uint32_t ui32SamplingFrequency)
{
	m_ui32SamplingFrequency = ui32SamplingFrequency;
	return m_ui32SamplingFrequency != _NoValueI_;
}

uint32_t CHeaderImpl::getSamplingFrequency() const
{
	return m_ui32SamplingFrequency;
}

bool CHeaderImpl::isSamplingFrequencySet() const
{
	return m_ui32SamplingFrequency != _NoValueI_;
}

//___________________________________________________________________//
//                                                                   //

CHeader::CHeader()
	: m_pHeaderImpl(nullptr)
{
	m_pHeaderImpl = new CHeaderImpl();
}

CHeader::~CHeader()
{
	delete m_pHeaderImpl;
}

void CHeader::reset()
{
	m_pHeaderImpl->reset();
}

//___________________________________________________________________//
//                                                                   //

// Experiment information
bool CHeader::setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier)
{
	return m_pHeaderImpl->setExperimentIdentifier(ui32ExperimentIdentifier);
}

bool CHeader::setSubjectAge(const uint32_t ui32SubjectAge)
{
	return m_pHeaderImpl->setSubjectAge(ui32SubjectAge);
}

bool CHeader::setSubjectGender(const uint32_t ui32SubjectGender)
{
	return m_pHeaderImpl->setSubjectGender(ui32SubjectGender);
}

uint32_t CHeader::getExperimentIdentifier() const
{
	return m_pHeaderImpl->getExperimentIdentifier();
}

uint32_t CHeader::getSubjectAge() const
{
	return m_pHeaderImpl->getSubjectAge();
}

uint32_t CHeader::getSubjectGender() const
{
	return m_pHeaderImpl->getSubjectGender();
}

bool CHeader::isExperimentIdentifierSet() const
{
	return m_pHeaderImpl->isExperimentIdentifierSet();
}

bool CHeader::isSubjectAgeSet() const
{
	return m_pHeaderImpl->isSubjectAgeSet();
}

bool CHeader::isSubjectGenderSet() const
{
	return m_pHeaderImpl->isSubjectGenderSet();
}

//___________________________________________________________________//
//                                                                   //

bool CHeader::setImpedanceCheckRequested(const bool bImpedanceCheckRequested)
{
	return m_pHeaderImpl->setImpedanceCheckRequested(bImpedanceCheckRequested);
}

bool CHeader::isImpedanceCheckRequested() const
{
	return m_pHeaderImpl->isImpedanceCheckRequested();
}

uint32_t CHeader::getImpedanceLimit() const
{
	return m_pHeaderImpl->getImpedanceLimit();
}

bool CHeader::setImpedanceLimit(const uint32_t ui32ImpedanceLimit)
{
	return m_pHeaderImpl->setImpedanceLimit(ui32ImpedanceLimit);
}
//___________________________________________________________________//
//                                                                   //

// Chanel information

bool CHeader::setChannelCount(const uint32_t ui32ChannelCount)
{
	return m_pHeaderImpl->setChannelCount(ui32ChannelCount);
}

bool CHeader::setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName)
{
	return m_pHeaderImpl->setChannelName(ui32ChannelIndex, sChannelName);
}

bool CHeader::setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain)
{
	return m_pHeaderImpl->setChannelGain(ui32ChannelIndex, f32ChannelGain);
}

bool CHeader::setChannelUnits(const uint32_t ui32ChannelIndex, const uint32_t ui32ChannelUnit, const uint32_t ui32ChannelFactor)
{
	return m_pHeaderImpl->setChannelUnits(ui32ChannelIndex, ui32ChannelUnit, ui32ChannelFactor);
}

// boolean CHeader::setChannelLocation(const uint32 ui32ChannelIndex, const float32 ui32ChannelLocationX, const float32 ui32ChannelLocationY, const float32 ui32ChannelLocationZ);

uint32_t CHeader::getChannelCount() const
{
	return m_pHeaderImpl->getChannelCount();
}

const char* CHeader::getChannelName(const uint32_t ui32ChannelIndex) const
{
	return m_pHeaderImpl->getChannelName(ui32ChannelIndex);
}

float CHeader::getChannelGain(const uint32_t ui32ChannelIndex) const
{
	return m_pHeaderImpl->getChannelGain(ui32ChannelIndex);
}

bool CHeader::getChannelUnits(const uint32_t ui32ChannelIndex, uint32_t& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const
{
	return m_pHeaderImpl->getChannelUnits(ui32ChannelIndex, ui32ChannelUnit, ui32ChannelFactor);
}

// CHeader::getChannelLocation(const uint32 ui32ChannelIndex) const;

bool CHeader::isChannelCountSet() const
{
	return m_pHeaderImpl->isChannelCountSet();
}

bool CHeader::isChannelNameSet() const
{
	return m_pHeaderImpl->isChannelNameSet();
}

bool CHeader::isChannelGainSet() const
{
	return m_pHeaderImpl->isChannelGainSet();
}

// boolean CHeader::isChannelLocationSet() const

bool CHeader::isChannelUnitSet() const
{
	return m_pHeaderImpl->isChannelUnitSet();
}


//___________________________________________________________________//
//                                                                   //

// Samples information
bool CHeader::setSamplingFrequency(const uint32_t ui32SamplingFrequency)
{
	return m_pHeaderImpl->setSamplingFrequency(ui32SamplingFrequency);
}

uint32_t CHeader::getSamplingFrequency() const
{
	return m_pHeaderImpl->getSamplingFrequency();
}

bool CHeader::isSamplingFrequencySet() const
{
	return m_pHeaderImpl->isSamplingFrequencySet();
}
