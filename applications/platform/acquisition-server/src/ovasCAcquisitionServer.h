#ifndef __OpenViBE_AcquisitionServer_CAcquisitionServer_H__
#define __OpenViBE_AcquisitionServer_CAcquisitionServer_H__

#include "ovas_base.h"
#include "ovasIDriver.h"
#include "ovasIHeader.h"
#include "ovasCDriftCorrection.h"

#include <socket/IConnectionServer.h>

#include <mutex>
#include <thread>

#include <string>
#include <vector>
#include <list>
#include <deque>


namespace OpenViBEAcquisitionServer
{
	class CConnectionServerHandlerThread;
	class CConnectionClientHandlerThread;
	class IAcquisitionServerPlugin;

	typedef struct
	{
		uint64_t m_ui64ConnectionTime;                              // Time the client connected
		uint64_t m_ui64StimulationTimeOffset;                       // Time offset wrt acquisition start
		uint64_t m_ui64SignalSampleCountToSkip;                     // How many samples to skip wrt current buffer start. n.b. not a constant.
		CConnectionClientHandlerThread* m_pConnectionClientHandlerThread;  // Ptr to the class object that is executed by the client connection handler thread
		std::thread* m_pConnectionClientHandlerStdThread;                  // The actual thread handle
		bool m_bChannelUnitsSent;
		bool m_bChannelLocalisationSent;
	} SConnectionInfo;

	typedef enum
	{
		NaNReplacementPolicy_LastCorrectValue=0,
		NaNReplacementPolicy_Zero,
		NaNReplacementPolicy_Disabled,
	} ENaNReplacementPolicy;

	// Concurrency handling
	class DoubleLock
	{
		// Implements
		//   lock(mutex1);
		//   lock(mutex2);
		//   unlock(mutex1);
		// mutex2 lock is released when the object goes out of scope
		//
		// @details The two mutex 'pattern' is used to avoid thread starving which can happen e.g.
		// on Linux if just a single mutex is used; apparently the main loop just takes the 
		// mutex repeatedly without the gui thread sitting on the mutex being unlocked.
		// n.b. potentially calls for redesign
	public:
		DoubleLock(std::mutex* m1, std::mutex* m2) : lock2(*m2, std::defer_lock)
		{
			std::lock_guard<std::mutex> lock1(*m1);
			lock2.lock();
		}

	private:
		std::unique_lock<std::mutex> lock2;
	};

	class CDriverContext;

	class CAcquisitionServer : public IDriverCallback
	{
	public:

		CAcquisitionServer(const OpenViBE::Kernel::IKernelContext& rKernelContext);
		virtual ~CAcquisitionServer();

		virtual IDriverContext& getDriverContext();

		uint32_t getClientCount();
		double getImpedance(const uint32_t ui32ChannelIndex);

		bool loop();

		bool connect(IDriver& rDriver, IHeader& rHeaderCopy, uint32_t ui32SamplingCountPerSentBlock, uint32_t ui32ConnectionPort);
		bool start();
		bool stop();
		bool disconnect();

		// Driver samples information callback
		void setSamples(const float* pSample) override;
		void setSamples(const float* pSample, const uint32_t ui32SampleCount) override;
		void setStimulationSet(const OpenViBE::IStimulationSet& rStimulationSet) override;

		// Driver context callback
		virtual bool isConnected() const { return m_bInitialized; }
		virtual bool isStarted() const { return m_bStarted; }
		virtual bool updateImpedance(const uint32_t ui32ChannelIndex, const double f64Impedance);

		// General parameters configurable from the GUI
		ENaNReplacementPolicy getNaNReplacementPolicy();
		OpenViBE::CString getNaNReplacementPolicyStr();
		uint64_t getOversamplingFactor();
		bool setNaNReplacementPolicy(ENaNReplacementPolicy eNaNReplacementPolicy);
		bool isImpedanceCheckRequested();
		bool isChannelSelectionRequested();
		bool setOversamplingFactor(uint64_t ui64OversamplingFactor);
		bool setImpedanceCheckRequest(bool bActive);
		bool setChannelSelectionRequest(bool bActive);

		std::vector<IAcquisitionServerPlugin*> getPlugins() { return m_vPlugins; }

		//
		virtual bool acceptNewConnection(Socket::IConnection* pConnection);

	protected:

		bool requestClientThreadQuit(CConnectionClientHandlerThread* th);

	public:

		// See class DoubleLock
		std::mutex m_oProtectionMutex;
		std::mutex m_oExecutionMutex;

		std::mutex m_oPendingConnectionProtectionMutex;
		std::mutex m_oPendingConnectionExecutionMutex;

		std::thread* m_pConnectionServerHandlerStdThread;

	public:

		const OpenViBE::Kernel::IKernelContext& m_rKernelContext;
		CDriverContext* m_pDriverContext;
		IDriver* m_pDriver;
		const IHeader* m_pHeaderCopy;

		OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoder;
		OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SubjectIdentifier;
		OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SubjectAge;
		OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SubjectGender;
		OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pSignalMatrix;
		OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pChannelUnits;
		OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SignalSamplingRate;
		OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> ip_pStimulationSet;
		OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64BufferDuration;
		OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pEncodedMemoryBuffer;

		OpenViBE::Kernel::TParameterHandler<bool> ip_bEncodeChannelLocalisationData;
		OpenViBE::Kernel::TParameterHandler<bool> ip_bEncodeChannelUnitData;

		std::list<std::pair<Socket::IConnection*, SConnectionInfo>> m_vConnection;
		std::list<std::pair<Socket::IConnection*, SConnectionInfo>> m_vPendingConnection;
		std::deque<std::vector<float>> m_vPendingBuffer;
		std::vector<float> m_vSwapBuffer;
		std::vector<float> m_vOverSamplingSwapBuffer;
		std::vector<double> m_vImpedance;
		std::vector<uint32_t> m_vSelectedChannels;
		std::vector<OpenViBE::CString> m_vSelectedChannelNames;
		Socket::IConnectionServer* m_pConnectionServer;

		ENaNReplacementPolicy m_eNaNReplacementPolicy;
		bool m_bReplacementInProgress;

		bool m_bInitialized;
		bool m_bStarted;
		bool m_bIsImpedanceCheckRequested;
		bool m_bIsChannelSelectionRequested;
		bool m_bGotData;
		uint64_t m_ui64OverSamplingFactor;
		uint32_t m_ui32ChannelCount;
		uint32_t m_ui32SamplingFrequency;
		uint32_t m_ui32SampleCountPerSentBlock;
		uint64_t m_ui64SampleCount;
		uint64_t m_ui64LastSampleCount;
		uint64_t m_ui64StartTime;
		uint64_t m_ui64LastDeliveryTime;

		CDriftCorrection m_oDriftCorrection;

		uint64_t m_ui64JitterEstimationCountForDrift;
		uint64_t m_ui64DriverTimeoutDuration;               // ms after which the driver is considered having time-outed
		int64_t m_i64StartedDriverSleepDuration;            // ms, <0 == spin, 0 == yield thread, >0 sleep. Used when driver does not return samples.
		uint64_t m_ui64StoppedDriverSleepDuration;          // ms to sleep when driver is not running

		uint8_t* m_pSampleBuffer;
		OpenViBE::CStimulationSet m_oPendingStimulationSet;

		std::vector<IAcquisitionServerPlugin*> m_vPlugins;
	};
};

#endif // __OpenViBE_AcquisitionServer_CAcquisitionServer_H__
