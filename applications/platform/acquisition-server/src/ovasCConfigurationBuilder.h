#ifndef __OpenViBE_AcquisitionServer_CConfigurationBuilder_H__
#define __OpenViBE_AcquisitionServer_CConfigurationBuilder_H__

#include "ovas_base.h"

#include <gtk/gtk.h>

#include <string>
#include <map>

namespace OpenViBEAcquisitionServer
{
	class IHeader;

	class CConfigurationBuilder
	{
	public:
		typedef enum
		{
			Error_NoError = 0,
			Error_UserCancelled = 1,
			Error_Unknown = 2
		} EErrorCode;

		CConfigurationBuilder(const char* sGtkBuilderFileName);
		virtual ~CConfigurationBuilder();

		virtual bool configure(
			IHeader& rHeader);

		virtual void buttonChangeChannelNamesCB();
		virtual void buttonApplyChannelNameCB();
		virtual void buttonRemoveChannelNameCB();
		virtual void treeviewApplyChannelNameCB();

	protected:

		virtual bool preConfigure();
		virtual bool doConfigure(EErrorCode& result);
		virtual bool postConfigure();

	private:

		CConfigurationBuilder();

	protected:

		bool m_bApplyConfiguration;

		GtkBuilder* m_pBuilderConfigureInterface;
		GtkBuilder* m_pBuilderConfigureChannelInterface;

		GtkWidget* m_pDialog;

		GtkWidget* m_pIdentifier;
		GtkWidget* m_pAge;
		GtkWidget* m_pNumberOfChannels;
		GtkWidget* m_pSamplingFrequency;
		GtkWidget* m_pGender;
		GtkWidget* m_pImpedanceCheck;

		GtkListStore* m_pElectrodeNameListStore;
		GtkListStore* m_pChannelNameListStore;

		GtkWidget* m_pElectrodeNameTreeView;
		GtkWidget* m_pChannelNameTreeView;

		std::map<uint32_t, std::string> m_vChannelName;
		std::string m_sGtkBuilderFileName;
		std::string m_sElectrodeFileName;
		std::string m_sGtkBuilderChannelsFileName;
		IHeader* m_pHeader;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationBuilder_H__
