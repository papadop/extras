#ifndef __OpenViBE_AcquisitionServer_CDriverBrainProductsLiveAmp_H__
#define __OpenViBE_AcquisitionServer_CDriverBrainProductsLiveAmp_H__

#if defined TARGET_HAS_ThirdPartyLiveAmpAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <deque>

#ifndef BYTE
typedef unsigned char BYTE;
#endif

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverBrainProductsLiveAmp
	 * \author Ratko Petrovic (Brain Products GmbH)
	 * \date Mon Nov 21 14:57:37 2016
	 * \brief The CDriverBrainProductsLiveAmp allows the acquisition server to acquire data from a Brain Products LiveAmp device.
	 *
	 * TODO: details
	 *
	 * \sa CConfigurationBrainProductsLiveAmp
	 */
	class CDriverBrainProductsLiveAmp : public IDriver
	{
	public:

		CDriverBrainProductsLiveAmp(IDriverContext& rDriverContext);
		virtual ~CDriverBrainProductsLiveAmp();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			if (eFlag == DriverFlag_IsUnstable)
			{
				return false;  // switch to "Stable" on 3.5.2017 - RP
			}
			return false; // No flags are set
		}

	private:

		SettingsHelper m_oSettings;
		IDriverCallback* m_pCallback;
		void* m_pHandle;

		CHeader m_oHeader;
		OpenViBE::CStimulationSet m_oStimulationSet;
		uint32_t m_ui32SampleCountPerSentBlock;
		uint8_t* m_pSampleBuffer;

		uint32_t m_ui32BufferSize;
		uint32_t m_ui32SampleSize;

		uint32_t m_ui32PhysicalSampleRate;
		bool m_bUseAccChannels;
		bool m_bUseBipolarChannels;
		uint32_t m_ui32ImpedanceChannels;
		uint32_t m_ui32UsedChannelsCounter;
		uint32_t m_ui32EnabledChannels;

		uint32_t m_ui32ChannelCount;
		uint32_t m_ui32CountEEG;
		uint32_t m_ui32CountAux;
		uint32_t m_ui32CountACC;
		uint32_t m_ui32CountBipolar;
		uint32_t m_ui32CountTriggersIn;
		uint32_t m_ui32GoodImpedanceLimit;
		uint32_t m_ui32BadImpedanceLimit;

		std::string m_sSerialNumber;
		int32_t m_iRecordingMode;

		std::vector<float> m_vSample;
		std::deque<std::vector<float>> m_vSampleCache;
		std::vector<std::vector<float>> m_vSendBuffer;
		std::vector<uint32_t> m_vSamplingRatesArray;
		std::vector<uint16_t> m_vTriggerIndices;
		std::vector<uint16_t> m_vLastTriggerStates;
		std::vector<uint16_t> m_vDataTypeArray;
		std::vector<float> m_vResolutionArray;

		bool m_bSimulationMode;
		/**
		* \function initializeLiveAmp
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return True if LiveAmp is successful initialized. 
		* \brief Establish BT connection with the LiveAmp amplifier, identified with the serial number.
		*/
		bool initializeLiveAmp();

		/**
		* \function configureLiveAmp
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return True if LiveAmp is successful configured. 
		* \brief Configure LiveAmp, sampling rate and mode.
		*/
		bool configureLiveAmp();

		/**
		* \function checkAvailableChannels
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return True if checking is successful. 
		* \brief Check how many available channels has LiveAmp. Get count of EEG,AUX and ACC channels.
			Compares the count with the amplifier/driver settings.
		*/
		bool checkAvailableChannels();
		
		/**
		* \function disableAllAvailableChannels
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return True if function is successful. 
		* \brief 
		*/
		bool disableAllAvailableChannels();
		
		/**
		* \function getChannelIndices
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return True if function is successful.  
		* \brief Get indexes of the channel that will be used for acquisition.
		*/
		bool getChannelIndices();
		
		/**
		* \function impedanceMessureInit
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return True if function is successful.  
		* \brief Configures impedance measurement. 
		*/
		bool configureImpedanceMessure();
		
		/**
		* \function getLiveAmpSampleSize
		* \param rInputValue[in] : 
		* \param rOutputValue[out] : 
		* \return Size of 'one' acquired sample. 
		* \brief Calcualtes the size of 'one' sample that LiveAmp delivers. It is actually a sum of the one sample per each channel that will be acquired.
		   This information is needed to read data from buffer, in order to access to each sample of each channel.     
		*/
		uint32_t getLiveAmpSampleSize();

		/**
		* \function liveAmpExtractData
		* \param samplesRead[in] : count of samples read (obtained) from the amplifier 
		* \param extractData[out] : extract data from the buffer
		* \return 
		* \brief Extracts acquired data from the buffer, and puts them into the double vector 'extractData' , in the appropriate format.
		*/
		void liveAmpExtractData(int32_t samplesRead, std::vector<std::vector<float>>& extractData);
		
		/**
		* \function liveAmpExtractImpedanceData
		* \param samplesRead[in] : count of acquired samples from the amplifier 
		* \param extractData[out] : extract data from the buffer
		* \return 
		* \brief Extracts acquired impedance measurements from the buffer, and puts them into the double vector 'extractData', in the appropriate format. 
		*/
		void liveAmpExtractImpedanceData(int32_t samplesRead, std::vector<std::vector<float>>& extractData);
	};
};

#endif // TARGET_HAS_ThirdPartyLiveAmpAPI

#endif // __OpenViBE_AcquisitionServer_CDriverBrainProductsLiveAmp_H__
