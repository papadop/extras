#ifndef __OpenViBE_AcquisitionServer_CConfigurationBrainProductsLiveAmp_H__
#define __OpenViBE_AcquisitionServer_CConfigurationBrainProductsLiveAmp_H__

#ifdef TARGET_HAS_ThirdPartyLiveAmpAPI

#include "../ovasCConfigurationBuilder.h"
#include "ovasCDriverBrainProductsLiveAmp.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationBrainProductsLiveAmp
	 * \author Ratko Petrovic (Brain Products GmbH)
	 * \date Mon Nov 21 14:57:37 2016
	 * \brief The CConfigurationBrainProductsLiveAmp handles the configuration dialog specific to the Brain Products LiveAmp device.
	 *
	 * TODO: details
	 *
	 * \sa CDriverBrainProductsLiveAmp
	 */
	class CConfigurationBrainProductsLiveAmp : public CConfigurationBuilder
	{
	public:

		// you may have to add to your constructor some reference parameters
		// for example, a connection ID:
		CConfigurationBrainProductsLiveAmp(
			CDriverBrainProductsLiveAmp& rDriverContext,
			const char* sGtkBuilderFileName,
			uint32_t& rPhysicalSampleRate,
			uint32_t& rCountEEG,
			uint32_t& rCountBip,
			uint32_t& rCountAUX,
			uint32_t& rCountACC,
			bool& rUseAccChannels,
			uint32_t& rGoodImpedanceLimit,
			uint32_t& rBadImpedanceLimit,
			std::string& sSerialNumber,
			bool& rUseBipolarChannels);

		bool preConfigure() override;
		bool postConfigure() override;

	protected:

		CDriverBrainProductsLiveAmp& m_rDriverContext;

	private:

		/*
		 * Insert here all specific attributes, such as a connection ID.
		 * use references to directly modify the corresponding attribute of the driver
		 * Example:
		 */

	protected:

		uint32_t& m_rPhysicalSampleRate;
		uint32_t& m_rCountEEG;
		uint32_t& m_rCountBip;
		uint32_t& m_rCountAUX;
		uint32_t& m_rCountACC;
		bool& m_rUseAccChannels;
		bool& m_rUseBipolarChannels;

		uint32_t& m_rGoodImpedanceLimit;
		uint32_t& m_rBadImpedanceLimit;
		uint32_t& m_rNumberEEGChannels;
		uint32_t& m_rNumberAUXChannels;
		uint32_t& m_rTriggers;
		std::string& m_sSerialNumber;

		GtkWidget* m_pImageErrorIcon;
		GtkLabel* m_pLabelErrorMessage;
		GtkButton* m_pButtonErrorDLLink;
		GtkSpinButton* m_pSpinButtonDeviceId;
		GtkComboBox* m_pComboBoxMode;
		GtkComboBox* m_pComboBoxPhysicalSampleRate;
		GtkComboBox* m_pComboBoxADCDataFilter;
		GtkComboBox* m_pComboBoxADCDataDecimation;
		GtkSpinButton* m_pSpinButtonActiveShieldGain;
		GtkToggleButton* m_pCheckButtonUseAuxChannels;
		GtkSpinButton* m_pSpinButtonGoodImpedanceLimit;
		GtkSpinButton* m_pSpinButtonBadImpedanceLimit;

		GtkSpinButton* m_pNumberEEGChannels;
		GtkSpinButton* m_pNumberBipolar;
		GtkSpinButton* m_pNumberAUXChannels;
		GtkToggleButton* m_pEnableACCChannels;
		GtkToggleButton* m_pUseBipolarChannels;
		GtkEntry* m_tSerialNumber;
	};
};

#endif // TARGET_HAS_ThirdPartyLiveAmpAPI

#endif // __OpenViBE_AcquisitionServer_CConfigurationBrainProductsLiveAmp_H__
