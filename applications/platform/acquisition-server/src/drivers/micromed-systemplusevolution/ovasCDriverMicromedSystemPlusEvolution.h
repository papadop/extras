#ifndef __OpenViBE_AcquisitionServer_CDriverMicromedSystemPlusEvolution_H__
#define __OpenViBE_AcquisitionServer_CDriverMicromedSystemPlusEvolution_H__

#if defined(TARGET_HAS_ThirdPartyMicromed)

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows
#include <openvibe/ov_all.h>
#include <iostream>
#include <socket/IConnectionClient.h>
#include <socket/IConnectionServer.h>
#include <list>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverMicromedSystemPlusEvolution
	 * \author Yann Renard (INRIA)
	 */
	class CDriverMicromedSystemPlusEvolution : public IDriver
	{
	public:

		CDriverMicromedSystemPlusEvolution(IDriverContext& rDriverContext);
		virtual ~CDriverMicromedSystemPlusEvolution();
		const char* getName() override;

		/*
				virtual bool isFlagSet(
					const OpenViBEAcquisitionServer::EDriverFlag eFlag) const
				{
					return eFlag==DriverFlag_IsUnstable;
				}
		*/

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		virtual bool loadDLL();

		Socket::IConnectionServer* m_pConnectionServer;
		uint32_t m_ui32ServerHostPort;
		Socket::IConnection* m_pConnection;
		short MyReceive(char* buf, long dataLen);
		bool receiveAllHeader();
		bool loadNextHeader();

	protected:

		virtual bool dropData();

		SettingsHelper m_oSettings;

		bool m_bValid;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;

		uint32_t m_ui32IndexIn;
		uint32_t m_ui32IndexOut;
		uint32_t m_ui32BuffDataIndex;

		uint32_t m_ui32TimeOutMilliseconds;

		char* m_pStructHeader;
		char* m_pStructHeaderInfo;
		unsigned short int* m_pStructBuffData;
		unsigned char* m_pStructBuffNote;
		unsigned char* m_pStructBuffTrigger;
		uint64_t m_ui64PosFirstSampleOfCurrentBlock;
		OpenViBE::CStimulationSet m_oStimulationSet;

		uint32_t m_ui32nbSamplesBlock;
		uint32_t m_ui32DataSizeInByte;
		uint32_t m_ui32BuffSize;

		std::list<char*> m_lHeader;
		std::list<char> m_lTempBuff;
	};
};

#endif
#endif

#endif // __OpenViBE_AcquisitionServer_CDriverMicromedSystemPlusEvolution_H__
