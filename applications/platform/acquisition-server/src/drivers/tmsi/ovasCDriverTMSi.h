/*
 * ovasCDriverTMSi.h
 *
 * Copyright (c) 2014, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#ifndef __OpenViBE_AcquisitionServer_CDriverTMSi_H__
#define __OpenViBE_AcquisitionServer_CDriverTMSi_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include "ovas_base.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows

#include <gtk/gtk.h>
// Get Signal info

#include <windows.h>
#include <map>


namespace OpenViBEAcquisitionServer
{
	class CTMSiAccess;

	/**
	 * \class CDriverTMSi
	 * \author Mensia Technologies
	 */
	class CDriverTMSi : public IDriver
	{
	public:

		CDriverTMSi(IDriverContext& rDriverContext);
		virtual ~CDriverTMSi();
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		// saved parameters
		OpenViBE::CString m_sConnectionProtocol;
		OpenViBE::CString m_sDeviceIdentifier;
		bool m_bCommonAverageReference;
		uint64_t m_ui64ActiveEEGChannels;
		OpenViBE::CString m_sActiveAdditionalChannels;
		uint64_t m_ui64ImpedanceLimit;

		CTMSiAccess* m_pTMSiAccess;
		CHeader m_oHeader;

	protected:
		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;
		std::vector<double> m_vImpedance;

		bool m_bValid;

		uint32_t m_ui32TotalSampleReceived;
		OpenViBE::CStimulationSet m_oStimulationSet;

	private:
		bool m_bIgnoreImpedanceCheck;
	};
}

#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CDriverTMSi_H__
