#ifndef __OpenViBE_AcquisitionServer_CConfigurationNeuroskyMindset_H__
#define __OpenViBE_AcquisitionServer_CConfigurationNeuroskyMindset_H__

#if defined TARGET_HAS_ThirdPartyThinkGearAPI

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

#define OVAS_MINDSET_INVALID_COM_PORT 0xffff

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationNeuroskyMindset
	 * \author Laurent Bonnet (INRIA)
	 * \date 05 may 2010
	 * \erief The CConfigurationNeuroskyMindset handles the configuration dialog specific to the MindSet device.
	 *
	 * User can configure ... (TODO).
	 *
	 * \sa CDriverNeuroskyMindset
	 */
	class CConfigurationNeuroskyMindset : public OpenViBEAcquisitionServer::CConfigurationBuilder
	{
	public:

		CConfigurationNeuroskyMindset(OpenViBEAcquisitionServer::IDriverContext& rDriverContext, const char* sGtkBuilderFileName,uint32_t& rComPort,OpenViBE::boolean& rESenseChannels,OpenViBE::boolean& rBandPowerChannels,bool& rBlinkStimulations,bool& rBlinkStrengthChannel);

		virtual bool preConfigure();
		virtual bool postConfigure();

		//virtual void buttonCheckSignalQualityCB();
		//virtual void buttonRefreshCB();
	
	protected:

		OpenViBEAcquisitionServer::IDriverContext& m_rDriverContext;
		int32_t m_iDeviceCount;

		// the parameters passed to the driver :
		uint32_t& m_rComPort;
		bool& m_rESenseChannels;
		bool& m_rBandPowerChannels;
		bool& m_rBlinkStimulations;
		bool& m_rBlinkStrengthChannel;

		//widgets
		::GtkWidget* m_pComPortSpinButton;

		bool m_bCheckSignalQuality;

	private:

		uint32_t m_ui32CurrentConnectionId;
	};
};

#endif // TARGET_HAS_ThirdPartyThinkGearAPI

#endif // __OpenViBE_AcquisitionServer_CConfigurationNeuroskyMindset_H__
