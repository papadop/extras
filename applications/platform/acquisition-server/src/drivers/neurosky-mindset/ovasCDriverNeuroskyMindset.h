#ifndef __OpenViBE_AcquisitionServer_CDriverNeuroskyMindset_H__
#define __OpenViBE_AcquisitionServer_CDriverNeuroskyMindset_H__

#if defined TARGET_HAS_ThirdPartyThinkGearAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverNeuroskyMindset
	 * \author Laurent Bonnet (INRIA)
	 * \date 03 may 2010
	 * \erief The CDriverNeuroskyMindset allows the acquisition server to acquire data from a MindSet device (Neurosky)).
	 *
	 * The driver opens a connection to the device through a dedicated API called ThinkGear, part of the MindSet Development Tools (MDT).
	 * The MDT are available for free on the official Neurosky website (http://store.neurosky.com/products/mindset-development-tools).
	 *
	 */
	class CDriverNeuroskyMindset : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverNeuroskyMindset(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual ~CDriverNeuroskyMindset();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }

	protected:

		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;

		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32TotalSampleCount;
		float* m_pSample;

	private:

		int32_t m_i32ConnectionID;
		uint32_t m_ui32ComPort;
		bool m_bESenseChannels;
		bool m_bBandPowerChannels;
		bool m_bBlinkStimulations;
		bool m_bBlinkStrengthChannel;

		uint32_t m_ui32WarningCount;

	};
};

#endif // TARGET_HAS_ThirdPartyThinkGearAPI

#endif // __OpenViBE_AcquisitionServer_CDriverNeuroskyMindset_H__
