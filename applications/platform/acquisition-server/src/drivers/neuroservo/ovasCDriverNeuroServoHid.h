/*
* NeuroServo driver for OpenViBE
*
* \author (NeuroServo)
* \date Wed Nov 23 00:24:00 2016
*
*/

#ifndef __OpenViBE_AcquisitionServer_CDriverNeuroServoHid_H__
#define __OpenViBE_AcquisitionServer_CDriverNeuroServoHid_H__

#if defined TARGET_OS_Windows
#if defined TARGET_HAS_ThirdPartyNeuroServo

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

// Provicde necessary methods to allow connection with HID device
#include "HidDeviceApi\HidDevice.h"

#include <boost/lockfree/spsc_queue.hpp>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverNeuroServoHid
	 * \author  (NeuroServo)
	 * \date Wed Nov 23 00:24:00 2016
	 * \brief The CDriverNeuroServoHid allows the acquisition server to acquire data from a NeuroServo device.
	 *
	 * \sa CConfigurationNeuroServoHid
	 */
	class CDriverNeuroServoHid : public IDriver
	{
	public:

		CDriverNeuroServoHid(IDriverContext& rDriverContext);
		virtual ~CDriverNeuroServoHid();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			return eFlag == DriverFlag_IsUnstable;
		}

		/* Methods implemented for the specific needs of NeuroServo.
		   Execute the callback from the HID device services
		*/
	public:
		void processDataReceived(BYTE data[]);
		void deviceDetached();
		void deviceAttached();

	private:
		// Control "Automatic Shutdown" and "Device Light Enable" based on user configuration
		void deviceShutdownAndLightConfiguration();


	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;

		CHeader m_oHeader;

		float* m_pSample;
		OpenViBE::CStimulationSet m_oStimulationSet;

	private:
		// Create a buffer queue for 1 sec data
		boost::lockfree::spsc_queue<float, boost::lockfree::capacity<2048>> m_pBufferQueue;

		// Device related infos
		HidDevice m_oHidDevice;
		uint16_t m_ui16ProductId;
		uint16_t m_ui16VendorId;
		uint16_t m_ui16DataSize;
		OpenViBE::CString m_sDriverName;

		// Data processing related infos
		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32SampleIndexForSentBlock;
		uint32_t m_ui32NbSamplesReceived;

		uint64_t m_ui64TimeStampLastSentBlock;
		uint64_t m_ui64SendBlockRequiredTime;
		uint64_t m_ui64SendSampleRequiredTime;
		uint64_t m_ui64NbSwitchDrift;

		int64_t m_i64LastDriftSampleCount;
		int64_t m_i64DriftAutoCorrectionDelay;

		float m_f32SampleValue;
		float m_fDriftAutoCorrFactor;

		bool m_bIsDriftWasInEarlyDirection;
		bool m_bQueueOverflow;
		bool m_bQueueUnderflow;
		bool m_bDeviceEpochDetected;

		// Configuration
		bool m_bAutomaticShutdown;
		bool m_bShutdownOnDriverDisconnect;
		bool m_bDeviceLightEnable;
		bool m_bIsDeviceInitialized;

		// Device connection state
		bool m_bIsDeviceConnected;
	};
};

#endif
#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CDriverNeuroServoHid_H__
