/*
* NeuroServo driver for OpenViBE
*
* \author (NeuroServo)
* \date Wed Nov 23 00:24:00 2016
*
*/

#ifndef __OpenViBE_AcquisitionServer_CConfigurationNeuroServoHid_H__
#define __OpenViBE_AcquisitionServer_CConfigurationNeuroServoHid_H__

#if defined TARGET_OS_Windows
#if defined TARGET_HAS_ThirdPartyNeuroServo

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationNeuroServoHid
	 * \author  (NeuroServo)
	 * \date Wed Nov 23 00:24:00 2016
	 * \brief The CConfigurationNeuroServoHid handles the configuration dialog specific to the NeuroServo device.
	 *
	 * TODO: details
	 *
	 * \sa CDriverNeuroServoHid
	 */
	class CConfigurationNeuroServoHid : public CConfigurationBuilder
	{
	public:

		CConfigurationNeuroServoHid(IDriverContext& rDriverContext, const char* sGtkBuilderFileName);

		bool preConfigure() override;
		bool postConfigure() override;

		// Automatic Shutdown
		void checkRadioAutomaticShutdown(const bool bAutoShutdownStatus);
		bool getAutomaticShutdownStatus();
		void setRadioAutomaticShutdown(bool state);

		// Shutdown on driver disconnect
		void checkRadioShutdownOnDriverDisconnect(const bool bShutdownOnDriverDisconnectStatus);
		bool getShutdownOnDriverDisconnectStatus();
		void setRadioShutdownOnDriverDisconnect(bool state);

		// Device Light Enable
		void checkRadioDeviceLightEnable(const bool bDeviceLightEnableStatus);
		bool getDeviceLightEnableStatus();
		void setRadioDeviceLightEnable(bool state);

	protected:

		IDriverContext& m_rDriverContext;

	private:

		bool m_bAutomaticShutdown;
		bool m_bShutdownOnDriverDisconnect;
		bool m_bDeviceLightEnable;
	};
};

#endif
#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CConfigurationNeuroServoHid_H__
