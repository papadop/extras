/*
 * ovasCDriverBrainProductsActiCHamp.h
 *
 * Copyright (c) 2012, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#pragma once

#if defined TARGET_HAS_ThirdPartyActiCHampAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include <deque>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverBrainProductsActiCHamp
	 * \author Mensia Technologies
	 */
	class CDriverBrainProductsActiCHamp : public IDriver
	{
	public:

		CDriverBrainProductsActiCHamp(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(const uint32_t ui32SampleCountPerSentBlock, IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		OpenViBE::CString getError(int* pCode = nullptr);

	protected:

		void* m_pHandle;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		std::vector<unsigned int> m_vImpedance;
		std::vector<float> m_vSample;
		std::vector<float> m_vResolution;
		//std::vector < std::vector < OpenViBE::float32 > > m_vSampleCache;
		std::deque<std::vector<float>> m_vSampleCache;
		uint32_t m_ui32SampleCacheIndex;
		std::vector<double> m_vFilter;

		OpenViBE::CStimulationSet m_oStimulationSet;

		uint32_t m_ui32DeviceId;
		uint32_t m_ui32Mode;
		uint32_t m_ui32PhysicalSampleRate;
		uint32_t m_ui32ADCDataFilter;
		uint32_t m_ui32ADCDataDecimation;
		uint32_t m_ui32ActiveShieldGain;
		uint32_t m_ui32ModuleEnabled;
		bool m_bUseAuxChannels;

		uint32_t m_ui32PhysicalSampleRateHz;

		uint32_t m_ui32ChannelCount;
		uint32_t m_ui32CountEEG;
		uint32_t m_ui32CountAux;
		uint32_t m_ui32CountTriggersIn;

		uint64_t m_ui64Counter;
		uint64_t m_ui64CounterStep;
		uint64_t m_ui64SampleCount;
		int64_t m_i64DriftOffsetSampleCount;

		uint32_t m_ui32GoodImpedanceLimit;
		uint32_t m_ui32BadImpedanceLimit;

		signed int* m_pAux;
		signed int* m_pEEG;
		unsigned int* m_pTriggers;
		unsigned int m_uiSize;

		unsigned int m_uiLastTriggers;
		uint64_t m_ui64LastSampleDate;

		bool m_bMyButtonstate;
	};
};

#endif // TARGET_HAS_ThirdPartyActiCHampAPI
