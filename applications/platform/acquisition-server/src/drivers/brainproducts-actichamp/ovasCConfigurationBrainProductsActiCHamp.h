/*
 * ovasCConfigurationBrainProductsActiCHamp.h
 *
 * Copyright (c) 2012, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#pragma once

#if defined TARGET_HAS_ThirdPartyActiCHampAPI

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>

#include <vector>

namespace OpenViBEAcquisitionServer
{
	class CConfigurationBrainProductsActiCHamp : public CConfigurationBuilder
	{
	public:

		CConfigurationBrainProductsActiCHamp(
			const char* sGtkBuilderFileName,
			uint32_t& rDeviceId,
			uint32_t& rMode,
			uint32_t& rPhysicalSampleRate,
			uint32_t& rADCDataFilter,
			uint32_t& rADCDataDecimation,
			uint32_t& rActiveShieldGain,
			uint32_t& rModuleEnabled,
			bool& rUseAuxChannels,
			uint32_t& rGoodImpedanceLimit,
			uint32_t& rBadImpedanceLimit);

		bool preConfigure() override;
		bool postConfigure() override;

		void comboBoxDeviceChangedCB();
		void buttonModuleToggledCB(bool bToggleState);
		void buttonAuxChannelsToggledCB(bool bToggleState);

	protected:

		uint32_t& m_rDeviceId;
		uint32_t& m_rMode;
		uint32_t& m_rPhysicalSampleRate;
		uint32_t& m_rADCDataFilter;
		uint32_t& m_rADCDataDecimation;
		uint32_t& m_rActiveShieldGain;
		uint32_t& m_rModuleEnabled;
		bool& m_rUseAuxChannels;
		uint32_t& m_rGoodImpedanceLimit;
		uint32_t& m_rBadImpedanceLimit;

		GtkWidget* m_pImageErrorIcon;
		GtkLabel* m_pLabelErrorMessage;
		GtkButton* m_pButtonErrorDLLink;
		GtkComboBox* m_pComboBoxDeviceId;
		GtkComboBox* m_pComboBoxMode;
		GtkComboBox* m_pComboBoxPhysicalSampleRate;
		GtkComboBox* m_pComboBoxADCDataFilter;
		GtkComboBox* m_pComboBoxADCDataDecimation;
		GtkSpinButton* m_pSpinButtonActiveShieldGain;
		GtkToggleButton* m_pCheckButtonUseAuxChannels;
		GtkSpinButton* m_pSpinButtonGoodImpedanceLimit;
		GtkSpinButton* m_pSpinButtonBadImpedanceLimit;

		GtkToggleButton* m_pToggleModule1;
		GtkToggleButton* m_pToggleModule2;
		GtkToggleButton* m_pToggleModule3;
		GtkToggleButton* m_pToggleModule4;
		GtkToggleButton* m_pToggleModule5;
	};
};

#endif // TARGET_HAS_ThirdPartyActiCHampAPI
