#ifndef __OpenViBE_AcquisitionServer_CDriverSimulatedDeviator_H__
#define __OpenViBE_AcquisitionServer_CDriverSimulatedDeviator_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <random>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverSimulatedDeviator
	 * \brief Simulates a drifting acquisition device by changing the sample rate by a random process.
	 * \author Jussi T. Lindgren (Inria)
	 *
	 * The driver simulates a square wave attempting to model a steady, analog process. The square wave changes
	 * sign every 1 sec. This process is then sampled using a sampling rate that is changing during the 
	 * recording according to the parameters given to the driver. These parameters are
	 *
	 * Offset   - The center sampling frequency deviation from the declared driver sampling rate. Can be negative.
	 * Spread   - How big jumps the random walk takes; related to the sigma of the normal distribution
	 * MaxDev   - The maximum allowed deviation in Hz from the true sampling rate + Offset
	 * Pullback - How strongly the random walk is pulled towards the true sampling rate + Offset
	 * Update   - How often the sampling rate is changed (in seconds)
	 *
	 * The MaxDev and Pullback are used to keep the stochastic process from diverging.
	 *
	 */
	class CDriverSimulatedDeviator : public IDriver
	{
	public:

		CDriverSimulatedDeviator(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		std::vector<float> m_vSample;

		uint64_t m_ui64TotalSampleCount;		// Number of samples sent by the drifting sampling process
		uint64_t m_ui64TotalSampleCountReal;    // Number of samples of some imaginary, steady 'real' process, here a square wave

		uint64_t m_ui64StartTime;
		uint64_t m_ui64LastAdjustment;
		double m_usedSamplingFrequency;

	private:
		bool m_bSendPeriodicStimulations;
		double m_Offset;
		double m_Spread;
		double m_MaxDev;
		double m_Pullback;
		double m_Update;
		uint64_t m_Wavetype;

		double m_FreezeFrequency;
		double m_FreezeDuration;
		uint64_t m_NextFreezeTime;

		std::random_device m_rd;
		std::mt19937 m_gen;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverSimulatedDeviator_H__
