#ifndef __OpenViBE_AcquisitionServer_CConfigurationDriverSimulatedDeviator_H__
#define __OpenViBE_AcquisitionServer_CConfigurationDriverSimulatedDeviator_H__

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationDriverSimulatedDeviator
	 * \author Jussi T. Lindgren (Inria)
	 * \brief The CConfigurationDriverSimulatedDeviator handles the configuration dialog specific to the Simulated Deviator driver
	 *
	 * \sa CDriverSimulatedDeviator
	 */

	class CConfigurationDriverSimulatedDeviator : public CConfigurationBuilder
	{
	public:
		CConfigurationDriverSimulatedDeviator(IDriverContext& rDriverContext, const char* sGtkBuilderFileName
											  , bool& rSendPeriodicStimulations
											  , double& rOffset
											  , double& rSpread
											  , double& rMaxDev
											  , double& rPullback
											  , double& rUpdate
											  , uint64_t& rWavetype
											  , double& rFreezeFrequency
											  , double& rFreezeDuration
		);

		bool preConfigure() override;
		bool postConfigure() override;

	protected:

		IDriverContext& m_rDriverContext;

		bool& m_rSendPeriodicStimulations;
		double& m_Offset;
		double& m_Spread;
		double& m_MaxDev;
		double& m_Pullback;
		double& m_Update;
		uint64_t& m_Wavetype;
		double& m_FreezeFrequency;
		double& m_FreezeDuration;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationDriverSimulatedDeviator_H__
