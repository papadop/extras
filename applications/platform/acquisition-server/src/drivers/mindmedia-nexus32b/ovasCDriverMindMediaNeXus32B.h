#ifndef __OpenViBE_AcquisitionServer_CDriverMindMediaNeXus32B_H__
#define __OpenViBE_AcquisitionServer_CDriverMindMediaNeXus32B_H__

#if defined(TARGET_HAS_ThirdPartyNeXus)

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverMindMediaNeXus32B
	 * \author Yann Renard (INRIA)
	 */
	class CDriverMindMediaNeXus32B : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverMindMediaNeXus32B(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual void release();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }

		virtual void processData(uint32_t ui32SampleCount, uint32_t ui32Channel, float* pSample);

	protected:
		
		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;
		uint32_t m_ui32SampleIndex;

	};
};

#endif // defined TARGET_OS_Windows
#endif

#endif // __OpenViBE_AcquisitionServer_CDriverMindMediaNeXus32B_H__
