#ifndef __OpenViBE_AcquisitionServer_CDriverMensiaAcquisition_H__
#define __OpenViBE_AcquisitionServer_CDriverMensiaAcquisition_H__

#ifdef TARGET_OS_Windows


#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	class CDriverMensiaAcquisition : public IDriver
	{
	public:

		CDriverMensiaAcquisition(IDriverContext& rDriverContext, const char* sDriverIdentifier);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		SettingsHelper m_oSettings;
		bool m_bValid;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;

		uint32_t m_ui32TotalSampleCount;
		uint64_t m_ui64StartTime;

		uint32_t* m_pSampleCountPerBuffer;

	private:
		// Settings
		OpenViBE::CString m_sDeviceURL;

		template <typename T>
		void loadDLLfunct(T* functionPointer, const char* functionName);
		uint32_t m_ui32DriverId;
	};
}

#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CDriverMensiaAcquisition_H__
