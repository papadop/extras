#ifdef TARGET_OS_Windows

#include "ovasCConfigurationDriverMensiaAcquisition.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace OpenViBEAcquisitionServer;
using namespace std;

//TODO_JL Add the URL as parameter for configuration so it can be saved and loaded

CConfigurationDriverMensiaAcquisition::CConfigurationDriverMensiaAcquisition(IDriverContext& rDriverContext, const char* sGtkBuilderFileName)
	: CConfigurationBuilder(sGtkBuilderFileName)
	  , m_rDriverContext(rDriverContext) {}

bool CConfigurationDriverMensiaAcquisition::preConfigure()
{
	if (!CConfigurationBuilder::preConfigure())
	{
		return false;
	}

	//TODO_JL call preConfigure from DLL

	return true;
}

bool CConfigurationDriverMensiaAcquisition::postConfigure()
{
	if (m_bApplyConfiguration)
	{
		//TODO_JL call postConfigure from DLL
	}

	if (!CConfigurationBuilder::postConfigure())
	{
		return false;
	}

	return true;
}

#endif // TARGET_OS_Windows
