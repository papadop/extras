/*
 * Brain Products Brainamp Series driver for OpenViBE
 * Copyright (C) 2010 INRIA - Author : Yann Renard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __OpenViBE_AcquisitionServer_CDriverBrainProductsBrainampSeries_H__
#define __OpenViBE_AcquisitionServer_CDriverBrainProductsBrainampSeries_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows

#include "ovas_defines_brainamp_series.h"

#include "ovasCConfigurationBrainProductsBrainampSeries.h"
#include "ovasCHeaderAdapterBrainProductsBrainampSeries.h"

#include <vector>
#include <list>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverBrainProductsBrainampSeries
	 * \author Mensia Technologies
	 */
	class CDriverBrainProductsBrainampSeries : public IDriver
	{
	public:

		friend class CConfigurationBrainProductsBrainampSeries;

		static char* getErrorMessage(uint32_t ui32Error);

	public:

		CDriverBrainProductsBrainampSeries(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool start() override;
		bool loop() override;
		bool stop() override;
		bool uninitialize() override;

		virtual bool startImpedanceCheck();
		virtual bool stopImpedanceCheck();

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeaderAdapter; }

	protected:

		bool getDeviceDetails(const uint32_t ui32Index, uint32_t* pAmplifierCount, uint32_t* pAmplifierType);

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;
		CHeaderAdapterBrainProductsBrainampSeries m_oHeaderAdapter;

		void* m_pDevice;
		CBrainampSetup* m_pDeviceSetup;
		CBrainampCalibrationSettings* m_pDeviceCalibrationSettings;

		uint32_t m_ui32ImpedanceCheckSignalFrequency;
		uint32_t m_ui32DecimationFactor;
		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;
		int16_t* m_pBuffer;
		uint16_t m_ui16Marker;

		std::list<std::vector<double>> m_vImpedanceBuffer;
		std::vector<double> m_vImpedance;

		unsigned long m_ui32BytesReturned;

		uint32_t m_ui32USBIndex;
		float m_pf32ResolutionFactor[256];
		EParameter m_peChannelSelected[256];
		EParameter m_peLowPassFilterFull[256];
		EParameter m_peResolutionFull[256];
		EParameter m_peDCCouplingFull[256];
		EParameter m_eLowPass;
		EParameter m_eResolution;
		EParameter m_eDCCoupling;
		EParameter m_eImpedance;
	};

	inline std::ostream& operator<<(std::ostream& out, const EParameter& var)
	{
		out << (int)var;

		return out;
	}

	inline std::istream& operator>>(std::istream& in, EParameter& var)
	{
		int l_iTmp;

		in >> l_iTmp;

		var = (EParameter)l_iTmp;

		return in;
	}

	inline std::ostream& operator<<(std::ostream& out, const EParameter var[256])
	{
		for (int i = 0; i < 256; i++)
		{
			out << (int)var[i];
			out << " ";
		}

		return out;
	}

	inline std::istream& operator>>(std::istream& in, EParameter var[256])
	{
		int l_iTmp;

		for (int i = 0; i < 256; i++)
		{
			in >> l_iTmp;
			var[i] = (EParameter)l_iTmp;
		}

		return in;
	}
};

#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CDriverBrainProductsBrainampSeries_H__
