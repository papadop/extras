/*
 * Brain Products Brainamp Series driver for OpenViBE
 * Copyright (C) 2010 INRIA - Author : Yann Renard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __OpenViBE_AcquisitionServer_CHeaderAdapterBrainProductsBrainampSeries_H__
#define __OpenViBE_AcquisitionServer_CHeaderAdapterBrainProductsBrainampSeries_H__

#include "../ovasCHeader.h"

#if defined TARGET_OS_Windows

#include "ovas_defines_brainamp_series.h"

namespace OpenViBEAcquisitionServer
{
	class CHeaderAdapterBrainProductsBrainampSeries : public CHeaderAdapter
	{
	public:

		CHeaderAdapterBrainProductsBrainampSeries(
			IHeader& rAdpatedHeader,
			EParameter* peChannelSelected);

		bool setChannelCount(const uint32_t ui32ChannelCount) override;
		bool setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName) override;

		uint32_t getChannelCount() const override;
		const char* getChannelName(const uint32_t ui32ChannelIndex) const override;

		EParameter* m_peChannelSelected;
	};
}

#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CHeaderAdapterBrainProductsBrainampSeries_H__
