#include "ovasCConfigurationEGIAmpServer.h"

#include <toolkit/ovtk_all.h>

#include <iostream>
#include <fstream>
#include <list>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace std;

//___________________________________________________________________//
//                                                                   //

CConfigurationEGIAmpServer::CConfigurationEGIAmpServer(const char* sGtkBuilderFileName)
	: CConfigurationBuilder(sGtkBuilderFileName)
	  , m_sHostName("localhost")
	  , m_ui32CommandPort(9877)
	  , m_ui32StreamPort(9879) {}

CConfigurationEGIAmpServer::~CConfigurationEGIAmpServer() {}

//___________________________________________________________________//
//                                                                   //

bool CConfigurationEGIAmpServer::setHostName(const CString& sHostName)
{
	m_sHostName = sHostName;
	return true;
}

bool CConfigurationEGIAmpServer::setCommandPort(const uint32_t ui32CommandPort)
{
	m_ui32CommandPort = ui32CommandPort;
	return true;
}

bool CConfigurationEGIAmpServer::setStreamPort(const uint32_t ui32StreamPort)
{
	m_ui32StreamPort = ui32StreamPort;
	return true;
}

//___________________________________________________________________//
//                                                                   //

CString CConfigurationEGIAmpServer::getHostName() const
{
	return m_sHostName;
}

uint32_t CConfigurationEGIAmpServer::getCommandPort() const
{
	return m_ui32CommandPort;
}

uint32_t CConfigurationEGIAmpServer::getStreamPort() const
{
	return m_ui32StreamPort;
}

//___________________________________________________________________//
//                                                                   //

bool CConfigurationEGIAmpServer::preConfigure()
{
	bool l_bParentResult = CConfigurationBuilder::preConfigure();

	m_pHostName = GTK_WIDGET(gtk_builder_get_object(m_pBuilderConfigureInterface, "entry_host_name"));
	m_pCommandPort = GTK_WIDGET(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_command_port"));
	m_pStreamPort = GTK_WIDGET(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_stream_port"));

	gtk_spin_button_set_value(
		GTK_SPIN_BUTTON(m_pCommandPort),
		m_ui32CommandPort);

	gtk_spin_button_set_value(
		GTK_SPIN_BUTTON(m_pStreamPort),
		m_ui32StreamPort);

	gtk_entry_set_text(
		GTK_ENTRY(m_pHostName),
		m_sHostName.toASCIIString());

	return l_bParentResult;
}

bool CConfigurationEGIAmpServer::postConfigure()
{
	if (m_bApplyConfiguration)
	{
		gtk_spin_button_update(GTK_SPIN_BUTTON(m_pStreamPort));
		gtk_spin_button_update(GTK_SPIN_BUTTON(m_pCommandPort));

		m_ui32StreamPort = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(m_pStreamPort));
		m_ui32CommandPort = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(m_pCommandPort));
		m_sHostName = gtk_entry_get_text(GTK_ENTRY(m_pHostName));
	}

	return CConfigurationBuilder::postConfigure();
}
