#ifndef __OpenViBE_AcquisitionServer_CConfigurationEGIAmpServer_H__
#define __OpenViBE_AcquisitionServer_CConfigurationEGIAmpServer_H__

#include "../ovasCConfigurationBuilder.h"

namespace OpenViBEAcquisitionServer
{
	class CConfigurationEGIAmpServer : public CConfigurationBuilder
	{
	public:

		CConfigurationEGIAmpServer(const char* sGtkBuilderFileName);
		virtual ~CConfigurationEGIAmpServer();

		virtual bool setHostName(const OpenViBE::CString& sHostName);
		virtual bool setCommandPort(const uint32_t ui32CommandPort);
		virtual bool setStreamPort(const uint32_t ui32StreamPort);

		virtual OpenViBE::CString getHostName() const;
		virtual uint32_t getCommandPort() const;
		virtual uint32_t getStreamPort() const;

	protected:

		bool preConfigure() override;
		bool postConfigure() override;

	private:

		CConfigurationEGIAmpServer();

	protected:

		GtkWidget* m_pHostName;
		GtkWidget* m_pCommandPort;
		GtkWidget* m_pStreamPort;

		OpenViBE::CString m_sHostName;
		uint32_t m_ui32CommandPort;
		uint32_t m_ui32StreamPort;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationEGIAmpServer_H__
