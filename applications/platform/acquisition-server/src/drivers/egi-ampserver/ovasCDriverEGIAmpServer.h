#ifndef __OpenViBE_AcquisitionServer_CDriverEGIAmpServer_H__
#define __OpenViBE_AcquisitionServer_CDriverEGIAmpServer_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <socket/IConnectionClient.h>

namespace OpenViBEAcquisitionServer
{
	class CCommandConnectionHandler;

	/**
	 * \class CDriverEGIAmpServer
	 * \author Yann Renard (Inria)
	 */
	class CDriverEGIAmpServer : public IDriver
	{
	public:

		friend class CCommandConnectionHandler;

		CDriverEGIAmpServer(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			return eFlag == DriverFlag_IsUnstable;
		}

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		virtual bool execute(const char* sScriptFilename);

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		Socket::IConnectionClient* m_pCommand;
		Socket::IConnectionClient* m_pStream;
		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32SampleIndex;
		uint32_t m_ui32ChannelCount;
		float* m_pBuffer;

		OpenViBE::CString m_sAmpServerHostName;
		uint32_t m_ui32CommandPort;
		uint32_t m_ui32StreamPort;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverEGIAmpServer_H__
