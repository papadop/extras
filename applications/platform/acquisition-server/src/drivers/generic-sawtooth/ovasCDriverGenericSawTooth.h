#ifndef __OpenViBE_AcquisitionServer_CDriverGenericSawTooth_H__
#define __OpenViBE_AcquisitionServer_CDriverGenericSawTooth_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGenericSawTooth 
	 * \author Yann Renard (INRIA)
	 */
	class CDriverGenericSawTooth : public IDriver
	{
	public:

		CDriverGenericSawTooth(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32ExternalBlockSize;
		std::vector<float> m_vSample;

		uint64_t m_ui64TotalSampleCount;

		uint64_t m_ui64StartTime;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverGenericSawTooth_H__
