#include "ovasCConfigurationGenericTimeSignal.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace OpenViBEAcquisitionServer;
using namespace std;

CConfigurationGenericTimeSignal::CConfigurationGenericTimeSignal(IDriverContext& rDriverContext, const char* sGtkBuilderFileName)
	: CConfigurationBuilder(sGtkBuilderFileName)
	  , m_rDriverContext(rDriverContext) {}

bool CConfigurationGenericTimeSignal::preConfigure()
{
	if (! CConfigurationBuilder::preConfigure())
	{
		return false;
	}

	return true;
}

bool CConfigurationGenericTimeSignal::postConfigure()
{
	if (m_bApplyConfiguration)
	{
		// 
	}

	if (! CConfigurationBuilder::postConfigure()) // normal header is filled (Subject ID, Age, Gender, channels, sampling frequency)
	{
		return false;
	}

	return true;
}
