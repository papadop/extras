#ifndef __OpenViBE_AcquisitionServer_CDriverGenericTimeSignal_H__
#define __OpenViBE_AcquisitionServer_CDriverGenericTimeSignal_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGenericTimeSignal 
	 * \author Jussi Lindgren (Inria)
	 *
	 * This driver may have some utility in debugging. For each sample, it returns the 
	 * current time as obtained from openvibe's System::Time:zgettime() converted to float seconds.
	 *
	 */
	class CDriverGenericTimeSignal : public IDriver
	{
	public:

		CDriverGenericTimeSignal(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		SettingsHelper m_oSettings;

		uint64_t m_ui64TotalSampleCount;
		uint64_t m_ui64StartTime;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverGenericTimeSignal_H__
