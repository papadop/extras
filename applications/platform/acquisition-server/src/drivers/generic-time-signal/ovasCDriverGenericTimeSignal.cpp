#include "ovasCDriverGenericTimeSignal.h"
#include "../ovasCConfigurationBuilder.h"

#include "ovasCConfigurationGenericTimeSignal.h"

#include <toolkit/ovtk_all.h>
#include <openvibe/ovITimeArithmetics.h>

#include <system/ovCTime.h>

#include <cmath>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace Kernel;

//___________________________________________________________________//
//                                                                   //

CDriverGenericTimeSignal::CDriverGenericTimeSignal(IDriverContext& rDriverContext)
	: IDriver(rDriverContext)
	  , m_pCallback(nullptr)
	  , m_oSettings("AcquisitionServer_Driver_GenericTimeSignal", m_rDriverContext.getConfigurationManager())
	  , m_ui64TotalSampleCount(0)
	  , m_ui64StartTime(0)
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::CDriverGenericTimeSignal\n";

	m_oHeader.setSamplingFrequency(512);
	m_oHeader.setChannelCount(1);
	m_oHeader.setChannelName(0, "Time(s)");
	m_oHeader.setChannelUnits(0, OVTK_UNIT_Second, OVTK_FACTOR_Base);

	m_oSettings.add("Header", &m_oHeader);
	m_oSettings.load();
}

void CDriverGenericTimeSignal::release()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::release\n";

	delete this;
}

const char* CDriverGenericTimeSignal::getName()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::getName\n";

	return "Generic Time Signal";
}

//___________________________________________________________________//
//                                                                   //

bool CDriverGenericTimeSignal::initialize(
	const uint32_t ui32SampleCountPerSentBlock,
	IDriverCallback& rCallback)
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::initialize\n";

	if (m_rDriverContext.isConnected()) { return false; }

	if (!m_oHeader.isChannelCountSet()
		|| !m_oHeader.isSamplingFrequencySet())
	{
		return false;
	}

	m_pCallback = &rCallback;

	return true;
}

bool CDriverGenericTimeSignal::start()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::start\n";

	if (!m_rDriverContext.isConnected()) { return false; }
	if (m_rDriverContext.isStarted()) { return false; }

	m_ui64TotalSampleCount = 0;
	m_ui64StartTime = System::Time::zgetTime();

	return true;
}

#include <iostream>

bool CDriverGenericTimeSignal::loop()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverGenericTimeSignal::loop\n";

	if (!m_rDriverContext.isConnected()) { return false; }
	if (!m_rDriverContext.isStarted()) { return true; }

	const uint64_t ui64TimeNow = System::Time::zgetTime();

	// Find out how many samples to send; note that we always just send 1 at a time with this driver
	const uint64_t l_ui64Elapsed = ui64TimeNow - m_ui64StartTime;
	const uint64_t l_ui64SamplesNeededSoFar = (m_oHeader.getSamplingFrequency() * l_ui64Elapsed) >> 32;
	if (l_ui64SamplesNeededSoFar <= m_ui64TotalSampleCount)
	{
		// Too early
		return true;
	}

	const float32 timeNow = static_cast<float>(ITimeArithmetics::timeToSeconds(ui64TimeNow));

	m_pCallback->setSamples(&timeNow, 1);

	m_ui64TotalSampleCount++;

	m_rDriverContext.correctDriftSampleCount(m_rDriverContext.getSuggestedDriftCorrectionSampleCount());

	return true;
}

bool CDriverGenericTimeSignal::stop()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::stop\n";

	if (!m_rDriverContext.isConnected()) { return false; }
	if (!m_rDriverContext.isStarted()) { return false; }
	return true;
}

bool CDriverGenericTimeSignal::uninitialize()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::uninitialize\n";

	if (!m_rDriverContext.isConnected()) { return false; }
	if (m_rDriverContext.isStarted()) { return false; }

	m_pCallback = nullptr;

	return true;
}

//___________________________________________________________________//
//                                                                   //

bool CDriverGenericTimeSignal::isConfigurable()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::isConfigurable\n";

	return true;
}

bool CDriverGenericTimeSignal::configure()
{
	m_rDriverContext.getLogManager() << LogLevel_Trace << "CDriverGenericTimeSignal::configure\n";

	CConfigurationGenericTimeSignal m_oConfiguration(m_rDriverContext,
													 Directories::getDataDir() + "/applications/acquisition-server/interface-Generic-TimeSignal.ui");

	if (!m_oConfiguration.configure(m_oHeader))
	{
		return false;
	}

	m_oSettings.save();

	return true;
}
