#ifndef __OpenViBE_AcquisitionServer_CConfigurationGenericTimeSignal_H__
#define __OpenViBE_AcquisitionServer_CConfigurationGenericTimeSignal_H__

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationGenericTimeSignal
	 * \sa CDriverGenericTimeSignal
	 */
	class CConfigurationGenericTimeSignal : public CConfigurationBuilder
	{
	public:

		CConfigurationGenericTimeSignal(IDriverContext& rDriverContext, const char* sGtkBuilderFileName);

		bool preConfigure() override;
		bool postConfigure() override;

	protected:

		IDriverContext& m_rDriverContext;

	private:
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationGenericTimeSignal_H__
