#ifndef __OpenViBE_AcquisitionServer_CDriverTMSiRefa32B_H__
#define __OpenViBE_AcquisitionServer_CDriverTMSiRefa32B_H__

#if defined TARGET_HAS_ThirdPartyNeXus

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include "ovas_base.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include "ovasCConfigurationTMSIRefa32B.h"

// This dll comes from the sdk-nexus.zip dependency archive
#define RTLOADER "RTINST.Dll"
#include <gtk/gtk.h>
// Get Signal info

#define SIGNAL_NAME 40
#define MAX_BUFFER_SIZE 0xFFFFFFFF

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverTMSiRefa32B
	 * \author Baptiste Payan (INRIA)
	 */
	class CDriverTMSiRefa32B : virtual public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverTMSiRefa32B(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		~CDriverTMSiRefa32B();
		virtual void release();
		virtual const char* getName();

		virtual bool isFlagSet(
			const OpenViBEAcquisitionServer::EDriverFlag eFlag) const
		{
			return (eFlag==DriverFlag_IsUnstable) || (eFlag==DriverFlag_IsDeprecated);
		}

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }
		virtual bool CDriverTMSiRefa32B::measureMode(uint32_t mode,uint32_t info );
	protected:
		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float *m_pSample;
		bool m_bValid;

		uint32_t m_ui32SampleIndex;
		uint32_t m_ui32TotalSampleReceived;
		OpenViBE::CStimulationSet m_oStimulationSet;

		bool refreshDevicePath();
		bool m_bCheckImpedance;
		int32_t m_i32NumOfTriggerChannel;
		uint32_t m_ui32LastTriggerValue;

		//----------- TYPE ---------------------
		//constants used by set chantype
		#define EXG (ULONG) 0x0001
		#define AUX (ULONG) 0x0002
		#define DEVICE_FEATURE_TYPE 		0x0303
		//------------ MODE ---------------------
		#define DEVICE_FEATURE_MODE		    0x0302
		//--------------- RTC ----------------------
		#define DEVICE_FEATURE_RTC		    0x0301
		//---------- HIGHPASS ------------------
		#define DEVICE_FEATURE_HIGHPASS 	0x0401
		//------------- LOWPASS ----------------
		#define DEVICE_FEATURE_LOWPASS 	0x0402
		//--------------- GAIN ------------------
		#define DEVICE_FEATURE_GAIN		0x0403
		//--------------- OFFSET -------------------
		#define DEVICE_FEATURE_OFFSET	0x0404
		//------------------ IO ----------------------
		#define DEVICE_FEATURE_IO 0x0500
		//----------------- MEMORY ----------------------
		#define DEVICE_FEATURE_MEMORY 0x0501
		//------------------- STORAGE ---------------------
		#define DEVICE_FEATURE_STORAGE 0x0502
		//------------------ CORRECTION --------------------
		#define DEVICE_FEATURE_CORRECTION 0x0503
		//--------------------- ID ---------------------------
		#define DEVICE_FEATURE_ID 0x0504

		#define MEASURE_MODE_NORMAL			((ULONG)0x0)
		#define MEASURE_MODE_IMPEDANCE		((ULONG)0x1)
		#define MEASURE_MODE_CALIBRATION	((ULONG)0x2)
		#define MEASURE_MODE_IMPEDANCE_EX	((ULONG)0x3)
		#define MEASURE_MODE_CALIBRATION_EX	((ULONG)0x4)
		//for MEASURE_MODE_IMPEDANCE
		#define IC_OHM_002 0	// 2K Impedance limit
		#define IC_OHM_005 1	// 5K Impedance limit
		#define IC_OHM_010 2	// 10K Impedance limit
		#define IC_OHM_020 3	// 20K Impedance limit
		#define IC_OHM_050 4	// 50K Impedance limit
		#define IC_OHM_100 5	// 100K Impedance limit
		//for MEASURE_MODE_CALIBRATION
		#define IC_VOLT_050 0	//50 uV t-t Calibration voltage
		#define IC_VOLT_100 1	//100 uV t-t
		#define IC_VOLT_200 2	//200 uV t-t
		#define IC_VOLT_500 3	//500 uV t-t
	};
};

#endif // TARGET_OS_Windows

#endif // __OpenViBE_AcquisitionServer_CDriverTMSiRefa32B_H__
