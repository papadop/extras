#ifndef __OpenViBE_AcquisitionServer_CConfigurationLabStreamingLayer_H__
#define __OpenViBE_AcquisitionServer_CConfigurationLabStreamingLayer_H__

#if defined(TARGET_HAS_ThirdPartyLSL)

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"
#include "ovasIHeader.h"

#include <gtk/gtk.h>
#include <lsl_cpp.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationLabStreamingLayer
	 * \author Jussi T. Lindgren / Inria
	 * \date Wed Oct 15 09:41:18 2014
	 * \brief The CConfigurationLabStreamingLayer handles the configuration dialog specific to the LabStreamingLayer (LSL) device.
	 *
	 * \sa CDriverLabStreamingLayer
	 */
	class CConfigurationLabStreamingLayer : public CConfigurationBuilder
	{
	public:

		CConfigurationLabStreamingLayer(IDriverContext& rDriverContext, const char* sGtkBuilderFileName,
										IHeader& rHeader,
										bool& rLimitSpeed,
										OpenViBE::CString& rSignalStream,
										OpenViBE::CString& rSignalStreamID,
										OpenViBE::CString& rMarkerStream,
										OpenViBE::CString& rMarkerStreamID,
										uint32_t& ui32FallbackSamplingRate);

		bool preConfigure() override;
		bool postConfigure() override;

	protected:

		IDriverContext& m_rDriverContext;

	private:

		IHeader& m_rHeader;

		bool& m_rLimitSpeed;
		OpenViBE::CString& m_rSignalStream;
		OpenViBE::CString& m_rSignalStreamID;
		OpenViBE::CString& m_rMarkerStream;
		OpenViBE::CString& m_rMarkerStreamID;
		uint32_t& m_ui32FallbackSamplingRate;

		std::vector<lsl::stream_info> m_vStreams;
		std::vector<int32_t> m_vSignalIndex;
		std::vector<int32_t> m_vMarkerIndex;
	};
};

#endif // TARGET_HAS_ThirdPartyLSL

#endif // __OpenViBE_AcquisitionServer_CConfigurationLabStreamingLayer_H__
