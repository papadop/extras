#ifndef __OpenViBE_AcquisitionServer_CDriverLabStreamingLayer_H__
#define __OpenViBE_AcquisitionServer_CDriverLabStreamingLayer_H__

#if defined(TARGET_HAS_ThirdPartyLSL)

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include <lsl_cpp.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverLabStreamingLayer
	 * \author Jussi T. Lindgren / Inria
	 * \date Wed Oct 15 09:41:18 2014
	 * \brief The CDriverLabStreamingLayer allows the acquisition server to acquire data from a LabStreamingLayer (LSL) device.
	 *
	 * \sa CConfigurationLabStreamingLayer
	 */
	class CDriverLabStreamingLayer : public IDriver
	{
	public:

		CDriverLabStreamingLayer(IDriverContext& rDriverContext);
		virtual ~CDriverLabStreamingLayer();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			return eFlag == DriverFlag_IsUnstable;
		}

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;

		CHeader m_oHeader;

	private:

		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32LSLFallbackSamplingRate;
		float* m_pSample;
		float* m_pBuffer;

		uint64_t m_ui64StartTime;
		uint64_t m_ui64SampleCount;

		lsl::stream_info m_oSignalStream;
		lsl::stream_inlet* m_pSignalInlet;

		lsl::stream_info m_oMarkerStream;
		lsl::stream_inlet* m_pMarkerInlet;

		bool m_bLimitSpeed;
		OpenViBE::CString m_sSignalStream;
		OpenViBE::CString m_sSignalStreamID;
		OpenViBE::CString m_sMarkerStream;
		OpenViBE::CString m_sMarkerStreamID;
	};
};

#endif // TARGET_HAS_ThirdPartyLSL

#endif // __OpenViBE_AcquisitionServer_CDriverLabStreamingLayer_H__
