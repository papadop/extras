#ifndef __OpenViBE_AcquisitionServer_CDriverBrainProductsVAmp_H__
#define __OpenViBE_AcquisitionServer_CDriverBrainProductsVAmp_H__

#if defined TARGET_HAS_ThirdPartyUSBFirstAmpAPI

#include "ovasIDriver.h"
#include "ovasCHeaderBrainProductsVAmp.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <vector>
#include <deque>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverBrainProductsVAmp
	 * \author Laurent Bonnet (INRIA)
	 * \date 16 nov 2009
	 * \erief The CDriverBrainProductsVAmp allows the acquisition server to acquire data from a USB-VAmp-16 amplifier (BrainProducts GmbH).
	 *
	 * The driver allows 2 different acquisition modes: normal (2kHz sampling frequency - max 16 electrodes)
	 * or fast (20kHz sampling frequency, 4 monopolar or differential channels).
	 * The driver uses a dedicated Header.
	 *
	 * \sa CHeaderBrainProductsVAmp
	 */
	class CDriverBrainProductsVAmp : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverBrainProductsVAmp(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual ~CDriverBrainProductsVAmp();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }

	protected:

		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;

		bool m_bAcquireAuxiliaryAsEEG;
		bool m_bAcquireTriggerAsEEG;

		OpenViBEAcquisitionServer::CHeaderBrainProductsVAmp m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32TotalSampleCount;
		uint32_t m_ui32AcquisitionMode;
		uint32_t m_ui32EEGChannelCount;
		uint32_t m_ui32AuxiliaryChannelCount;
		uint32_t m_ui32TriggerChannelCount;

		std::vector<uint32_t> m_vStimulationIdentifier;
		std::vector<uint64_t> m_vStimulationDate;
		std::vector<uint64_t> m_vStimulationSample;
		OpenViBE::CStimulationSet m_oStimulationSet;
		uint32_t m_ui32LastTrigger;

		std::deque < std::vector < float > > m_vSampleCache;
		std::vector < float > m_vSample;
		std::vector < double > m_vFilter;
		std::vector < float > m_vResolution;

		int64_t m_i64DriftOffsetSampleCount;
		uint32_t m_ui32PhysicalSampleRateHz;
		uint64_t m_ui64CounterStep;
		uint64_t m_ui64Counter;

	private:

		bool m_bFirstStart;

	};
};

#endif // TARGET_HAS_ThirdPartyUSBFirstAmpAPI

#endif // __OpenViBE_AcquisitionServer_CDriverBrainProductsVAmp_H__
