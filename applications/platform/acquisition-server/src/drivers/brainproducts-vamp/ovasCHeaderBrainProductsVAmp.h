#ifndef __OpenViBEAcquisitionServer_CHeaderBrainProductsVAmp_H__
#define __OpenViBEAcquisitionServer_CHeaderBrainProductsVAmp_H__

#if defined TARGET_HAS_ThirdPartyUSBFirstAmpAPI

#include "../ovasCHeader.h"

#include <windows.h>
#include <FirstAmp.h>

#include<map>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CHeaderBrainProductsVAmp
	 * \author Laurent Bonnet (INRIA)
	 * \date 16 nov 2009
	 * \erief The CHeaderBrainProductsVAmp is an Adaptator for the VAmp device.
	 *
	 * It contains basic functions using the basic header behaviour, and a set of specific functions to handle the Fast Mode data.
	 *
	 * \sa CDriverBrainProductsVAmp
	 */
	class CHeaderBrainProductsVAmp : public OpenViBEAcquisitionServer::IHeader
	{
	public:

		CHeaderBrainProductsVAmp();
		virtual ~CHeaderBrainProductsVAmp();
		virtual void reset();

		// Experiment information
		virtual bool setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier);
		virtual bool setSubjectAge(const uint32_t ui32SubjectAge);
		virtual bool setSubjectGender(const uint32_t ui32SubjectGender);

		virtual uint32_t getExperimentIdentifier() const;
		virtual uint32_t getSubjectAge() const;
		virtual uint32_t getSubjectGender() const;

		virtual bool isExperimentIdentifierSet() const;
		virtual bool isSubjectAgeSet() const;
		virtual bool isSubjectGenderSet() const;

		virtual bool setImpedanceCheckRequested(const bool bImpedanceCheckRequested);
		virtual bool isImpedanceCheckRequested() const;
		virtual uint32_t getImpedanceLimit() const { return m_ui32ImpedanceLimit; };
		virtual bool setImpedanceLimit(const uint32_t ui32ImpedanceLimit) { m_ui32ImpedanceLimit = ui32ImpedanceLimit; return true; };

		// Channel information
		virtual bool setChannelCount(const uint32_t ui32ChannelCount);
		virtual bool setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName);
		virtual bool setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain);
		virtual bool setChannelUnits(const uint32_t ui32ChannelIndex, const uint32_t ui32ChannelUnit, const uint32_t ui32ChannelFactor);

		virtual uint32_t getChannelCount() const;
		virtual const char* getChannelName(const uint32_t ui32ChannelIndex) const;
		virtual float getChannelGain(const uint32_t ui32ChannelIndex) const;
		virtual bool getChannelUnits(const uint32_t ui32ChannelIndex, uint32_t& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const;

		virtual bool isChannelCountSet() const;
		virtual bool isChannelNameSet() const;
		virtual bool isChannelGainSet() const;
		virtual bool isChannelUnitSet() const;

		// Samples information
		virtual bool setSamplingFrequency(const uint32_t ui32SamplingFrequency);

		virtual uint32_t getSamplingFrequency() const;

		virtual bool isSamplingFrequencySet() const;

		//------------- SPECIFIC FUNCTIONS -------------

		virtual uint32_t getAcquisitionMode() { return m_ui32AcquisitionMode; }
		virtual bool setAcquisitionMode(uint32_t ui32AcquisitionMode) { m_ui32AcquisitionMode = ui32AcquisitionMode; return true; }

		virtual uint32_t getEEGChannelCount(uint32_t ui32AcquisitionMode);
		virtual uint32_t getAuxiliaryChannelCount(uint32_t ui32AcquisitionMode);
		virtual uint32_t getTriggerChannelCount(uint32_t ui32AcquisitionMode);

		// Pair information
		virtual bool setPairCount(const uint32_t ui32PairCount);
		virtual bool setPairName(const uint32_t ui32PairIndex, const char* sPairName);
		virtual bool setPairGain(const uint32_t ui32PairIndex, const float f32PairGain);
		virtual bool setPairUnits(const uint32_t ui32PairIndex, const uint32_t ui32PairUnit, const uint32_t ui32PairFactor);
		virtual bool setDeviceId(int32_t i32DeviceId);
		virtual bool setFastModeSettings(t_faDataModeSettings tFastModeSettings);

		virtual uint32_t getPairCount() const;
		virtual const char* getPairName(const uint32_t ui32PairIndex) const;
		virtual float getPairGain(const uint32_t ui32PairIndex) const;
		virtual bool getPairUnits(const uint32_t ui32PairIndex, uint32_t& ui32PairUnit, uint32_t& ui32PairFactor) const;
		virtual int32_t getDeviceId() const;
		virtual t_faDataModeSettings getFastModeSettings() const;

		virtual bool isPairCountSet() const;
		virtual bool isPairNameSet() const;
		virtual bool isPairGainSet() const;
		virtual bool isPairUnitSet() const;
		virtual bool isDeviceIdSet() const;
		virtual bool isFastModeSettingsSet() const;

		OpenViBEAcquisitionServer::CHeader* getBasicHeader() const { return m_pBasicHeader; }

	protected:

		OpenViBEAcquisitionServer::CHeader* m_pBasicHeader; // the basic header

		// additional information
		int32_t m_i32DeviceId;
		uint32_t m_ui32AcquisitionMode;
		t_faDataModeSettings m_tFastModeSettings;

		// Pair information
		uint32_t m_ui32PairCount;
		std::map<uint32_t, std::string> m_vPairName;
		std::map<uint32_t, float> m_vPairGain;
		std::map<uint32_t, std::pair<uint32_t, uint32_t> > m_vPairUnit;

		uint32_t m_ui32ImpedanceLimit = 5000; // 5kOhm
	};
}

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI

#endif // __OpenViBEAcquisitionServer_CHeaderBrainProductsVAmp_H__
