#include "ovasCHeaderBrainProductsVAmp.h"
#include "ovasCConfigurationBrainProductsVAmp.h"
#include "../ovasCHeader.h"

#include <system/ovCMemory.h>

#if defined TARGET_HAS_ThirdPartyUSBFirstAmpAPI

#include <windows.h>
#include <FirstAmp.h>

#include <map>
#include <string>

#define _NoValueI_ 0xffffffff


using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace std;

namespace
{
	uint32_t g_vEEGChannelCount[] = { 16, 8, 4 };
	uint32_t g_vAuxiliaryChannelCount[] = { 2, 2, 0 };
	uint32_t g_vTriggerChannelCount[] = { 1, 1, 1 };
};

//___________________________________________________________________//
//                                                                   //

CHeaderBrainProductsVAmp::CHeaderBrainProductsVAmp()
{
	m_pBasicHeader = new CHeader();

	// additional information
	m_i32DeviceId = -1;
	m_ui32AcquisitionMode = AcquisitionMode_VAmp16;

	// Pair information
	m_ui32PairCount = 0;
}

CHeaderBrainProductsVAmp::~CHeaderBrainProductsVAmp()
{
	delete m_pBasicHeader;
}

void CHeaderBrainProductsVAmp::reset()
{
	m_pBasicHeader->reset();
	m_i32DeviceId = FA_ID_INVALID;

	// Pair information
	m_ui32PairCount = 0;
}

//___________________________________________________________________//
//                                                                   //

uint32 CHeaderBrainProductsVAmp::getEEGChannelCount(uint32_t ui32AcquisitionMode)
{
	return g_vEEGChannelCount[ui32AcquisitionMode];
}

uint32 CHeaderBrainProductsVAmp::getAuxiliaryChannelCount(uint32_t ui32AcquisitionMode)
{
	return g_vAuxiliaryChannelCount[ui32AcquisitionMode];
}

uint32 CHeaderBrainProductsVAmp::getTriggerChannelCount(uint32_t ui32AcquisitionMode)
{
	return g_vTriggerChannelCount[ui32AcquisitionMode];
}

// Pair information

bool CHeaderBrainProductsVAmp::setPairCount(const uint32_t ui32PairCount)
{
	m_ui32PairCount=ui32PairCount;
	m_vPairName.clear();
	m_vPairGain.clear();
	return m_ui32PairCount!=_NoValueI_;
}

bool CHeaderBrainProductsVAmp::setPairName(const uint32_t ui32PairIndex, const char* sPairName)
{
	m_vPairName[ui32PairIndex]=sPairName;
	return ui32PairIndex<m_ui32PairCount;
}

bool CHeaderBrainProductsVAmp::setPairGain(const uint32_t ui32PairIndex, const float f32PairGain)
{
	m_vPairGain[ui32PairIndex]=f32PairGain;
	return ui32PairIndex<m_ui32PairCount;
}

bool CHeaderBrainProductsVAmp::setPairUnits(const uint32 ui32PairIndex, const uint32 ui32PairUnit, const uint32_t ui32PairFactor)
{
	m_vPairUnit[ui32PairIndex]=std::pair<uint32,uint32_t>(ui32PairUnit, ui32PairFactor);
	return ui32PairIndex<m_ui32PairCount;
}

bool CHeaderBrainProductsVAmp::setFastModeSettings(t_faDataModeSettings tFastModeSettings)
{
	m_tFastModeSettings=tFastModeSettings;
	return isFastModeSettingsSet();
}

uint32_t CHeaderBrainProductsVAmp::getPairCount() const
{
	return m_ui32PairCount;
}

const char* CHeaderBrainProductsVAmp::getPairName(const uint32_t ui32PairIndex) const
{
	map<uint32_t, string>::const_iterator i=m_vPairName.find(ui32PairIndex);
	if(i==m_vPairName.end())
	{
		return "";
	}
	return i->second.c_str();
}

float CHeaderBrainProductsVAmp::getPairGain(const uint32_t ui32PairIndex) const
{
	map<uint32_t, float>::const_iterator i=m_vPairGain.find(ui32PairIndex);
	if(i==m_vPairGain.end())
	{
		return(ui32PairIndex<m_ui32PairCount?1.0f:0.0f);
	}
	return i->second;
}

bool CHeaderBrainProductsVAmp::getPairUnits(const uint32 ui32PairIndex, uint32& ui32PairUnit, uint32_t& ui32PairFactor) const
{
	map<uint32, std::pair<uint32,uint32_t> >::const_iterator i=m_vPairUnit.find(ui32PairIndex);
	if(i==m_vPairUnit.end())
	{
		ui32PairUnit = OVTK_UNIT_Unspecified;
		ui32PairFactor = OVTK_FACTOR_Base;

		return false;
	}
	ui32PairUnit = (i->second).first;
	ui32PairFactor = (i->second).second;

	return true;
}

t_faDataModeSettings CHeaderBrainProductsVAmp::getFastModeSettings() const
{
	return m_tFastModeSettings;
}

bool CHeaderBrainProductsVAmp::isPairCountSet() const
{
	return m_ui32PairCount!=_NoValueI_ && m_ui32PairCount!=0;
}

bool CHeaderBrainProductsVAmp::isPairNameSet() const
{
	return isPairCountSet();
}

bool CHeaderBrainProductsVAmp::isPairGainSet() const
{
	return isPairCountSet();
}

bool CHeaderBrainProductsVAmp::isPairUnitSet() const
{
	return isPairCountSet();
}


//___________________________________________________________________//
//                                                                   //

// Experiment information
bool CHeaderBrainProductsVAmp::setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier)
{
	return m_pBasicHeader->setExperimentIdentifier(ui32ExperimentIdentifier);
}

bool CHeaderBrainProductsVAmp::setSubjectAge(const uint32_t ui32SubjectAge)
{
	return m_pBasicHeader->setSubjectAge(ui32SubjectAge);
}

bool CHeaderBrainProductsVAmp::setSubjectGender(const uint32_t ui32SubjectGender)
{
	return m_pBasicHeader->setSubjectGender(ui32SubjectGender);
}
boolean CHeaderBrainProductsVAmp::setImpedanceCheckRequested(const bool bImpedanceCheckRequested)
{
	return m_pBasicHeader->setImpedanceCheckRequested(bImpedanceCheckRequested);
}

bool CHeaderBrainProductsVAmp::isImpedanceCheckRequested() const
{
	return m_pBasicHeader->isImpedanceCheckRequested();
}

uint32_t CHeaderBrainProductsVAmp::getExperimentIdentifier() const
{
	return m_pBasicHeader->getExperimentIdentifier();
}

uint32_t CHeaderBrainProductsVAmp::getSubjectAge() const
{
	return m_pBasicHeader->getSubjectAge();
}

uint32_t CHeaderBrainProductsVAmp::getSubjectGender() const
{
	return m_pBasicHeader->getSubjectGender();
}

bool CHeaderBrainProductsVAmp::isExperimentIdentifierSet() const
{
	return m_pBasicHeader->isExperimentIdentifierSet();
}

bool CHeaderBrainProductsVAmp::isSubjectAgeSet() const
{
	return m_pBasicHeader->isSubjectAgeSet();
}

bool CHeaderBrainProductsVAmp::isSubjectGenderSet() const
{
	return m_pBasicHeader->isSubjectGenderSet();
}

//___________________________________________________________________//
//                                                                   //

bool CHeaderBrainProductsVAmp::setDeviceId(int32_t i32DeviceId)
{
	m_i32DeviceId=i32DeviceId;
	return m_i32DeviceId!=_NoValueI_;
}

int32_t CHeaderBrainProductsVAmp::getDeviceId() const
{
	return m_i32DeviceId;
}

bool CHeaderBrainProductsVAmp::isDeviceIdSet() const
{
	return m_i32DeviceId != _NoValueI_;
}

bool CHeaderBrainProductsVAmp::isFastModeSettingsSet() const
{
	return (m_tFastModeSettings.Mode20kHz4Channels.ChannelsPos[0] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsNeg[0] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsPos[1] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsNeg[1] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsPos[2] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsNeg[2] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsPos[3] == _NoValueI_
	|| m_tFastModeSettings.Mode20kHz4Channels.ChannelsNeg[3] == _NoValueI_);
}

//___________________________________________________________________//
//                                                                   //

// Channel information

bool CHeaderBrainProductsVAmp::setChannelCount(const uint32_t ui32ChannelCount)
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->setPairCount(ui32ChannelCount);
	}
	else
	{
		return m_pBasicHeader->setChannelCount(ui32ChannelCount);
	}
}

bool CHeaderBrainProductsVAmp::setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName)
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->setPairName(ui32ChannelIndex, sChannelName);
	}
	else
	{
		return m_pBasicHeader->setChannelName(ui32ChannelIndex, sChannelName);
	}

}

bool CHeaderBrainProductsVAmp::setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain)
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->setPairGain(ui32ChannelIndex,f32ChannelGain);
	}
	else
	{
		return m_pBasicHeader->setChannelGain(ui32ChannelIndex,f32ChannelGain);
	}
}

bool CHeaderBrainProductsVAmp::setChannelUnits(const uint32 ui32ChannelIndex, const uint32 ui32ChannelUnit, const uint32_t ui32ChannelFactor)
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->setPairUnits(ui32ChannelIndex,ui32ChannelUnit, ui32ChannelFactor);
	}
	else
	{
		return m_pBasicHeader->setChannelUnits(ui32ChannelIndex,ui32ChannelUnit, ui32ChannelFactor);
	}
}

uint32_t CHeaderBrainProductsVAmp::getChannelCount() const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->getPairCount();
	}
	else
	{
		return m_pBasicHeader->getChannelCount();
	}
}

const char* CHeaderBrainProductsVAmp::getChannelName(const uint32_t ui32ChannelIndex) const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->getPairName(ui32ChannelIndex);
	}
	else
	{
		return m_pBasicHeader->getChannelName(ui32ChannelIndex);
	}
}

float CHeaderBrainProductsVAmp::getChannelGain(const uint32_t ui32ChannelIndex) const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->getPairGain(ui32ChannelIndex);
	}
	else
	{
		return m_pBasicHeader->getChannelGain(ui32ChannelIndex);
	}
}
	
bool CHeaderBrainProductsVAmp::getChannelUnits(const uint32_t ui32ChannelIndex, uint32_t& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		return this->getPairUnits(ui32ChannelIndex, ui32ChannelUnit, ui32ChannelFactor);
	}
	else
	{
		return m_pBasicHeader->getChannelUnits(ui32ChannelIndex, ui32ChannelUnit, ui32ChannelFactor);
	}
}

bool CHeaderBrainProductsVAmp::isChannelCountSet() const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->isPairCountSet();
	}
	else
	{
		return m_pBasicHeader->isChannelCountSet();
	}
}

bool CHeaderBrainProductsVAmp::isChannelNameSet() const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->isPairNameSet();
	}
	else
	{
		return m_pBasicHeader->isChannelNameSet();
	}
}

bool CHeaderBrainProductsVAmp::isChannelGainSet() const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->isPairGainSet();
	}
	else
	{
		return m_pBasicHeader->isChannelGainSet();
	}
}

bool CHeaderBrainProductsVAmp::isChannelUnitSet() const
{
	if(m_ui32AcquisitionMode == AcquisitionMode_VAmp4Fast)
	{
		// in fast mode the channel count is the pair count (to display in the designer as a "channel")
		return this->isPairUnitSet();
	}
	else
	{
		return m_pBasicHeader->isChannelUnitSet();
	}
}

//___________________________________________________________________//
//                                                                   //

// Samples information
bool CHeaderBrainProductsVAmp::setSamplingFrequency(const uint32_t ui32SamplingFrequency)
{
	return m_pBasicHeader->setSamplingFrequency(ui32SamplingFrequency);
}

uint32_t CHeaderBrainProductsVAmp::getSamplingFrequency() const
{
	return m_pBasicHeader->getSamplingFrequency();
}

bool CHeaderBrainProductsVAmp::isSamplingFrequencySet() const
{
	return m_pBasicHeader->isSamplingFrequencySet();
}

#endif // TARGET_HAS_ThirdPartyUSBFirstAmpAPI
