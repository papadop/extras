#ifndef __OpenViBE_AcquisitionServer_CConfigurationDriverGenericOscillator_H__
#define __OpenViBE_AcquisitionServer_CConfigurationDriverGenericOscillator_H__

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationDriverGenericOscillator
	 * \author Jozef Legeny (Inria)
	 * \date 28 jan 2013
	 * \brief The CConfigurationDriverGenericOscillator handles the configuration dialog specific to the Generic Oscillator driver
	 *
	 * \sa CDriverGenericOscillator
	 */

	class CConfigurationDriverGenericOscillator : public CConfigurationBuilder
	{
	public:
		CConfigurationDriverGenericOscillator(IDriverContext& rDriverContext,
											  const char* sGtkBuilderFileName, bool& rSendPeriodicStimulations,
											  double& rStimulationInterval);

		bool preConfigure() override;
		bool postConfigure() override;

	protected:

		IDriverContext& m_rDriverContext;

		bool& m_rSendPeriodicStimulations;
		double& m_rStimulationInterval;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationDriverGenericOscillator_H__
