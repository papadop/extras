#ifndef __OpenViBE_AcquisitionServer_CDriverGenericOscillator_H__
#define __OpenViBE_AcquisitionServer_CDriverGenericOscillator_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGenericOscillator
	 * \author Yann Renard (INRIA)
	 */
	class CDriverGenericOscillator : public IDriver
	{
	public:

		CDriverGenericOscillator(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		std::vector<float> m_vSample;

		uint32_t m_ui64TotalSampleCount;
		uint64_t m_ui64TotalStimCount;

		uint64_t m_ui64StartTime;

		OpenViBE::CStimulationSet m_oStimulationSet;

	private:
		bool m_bSendPeriodicStimulations;
		double m_f64StimulationInterval;	// Seconds
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverGenericOscillator_H__
