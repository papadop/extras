#ifndef __OpenViBE_AcquisitionServer_CConfigurationMKSNVXDriver_H__
#define __OpenViBE_AcquisitionServer_CConfigurationMKSNVXDriver_H__

#include "../../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationMKSNVXDriver
	 * \author mkochetkov (MKS)
	 * \date Tue Jan 21 23:21:03 2014
	 * \brief The CConfigurationMKSNVXDriver handles the configuration dialog specific to the MKSNVXDriver device.
	 *
	 * TODO: details
	 *
	 * \sa CDriverMKSNVXDriver
	 */
	class CConfigurationMKSNVXDriver : public CConfigurationBuilder
	{
		uint32_t& dataMode_;
		bool& showAuxChannels_;
	protected:
		IDriverContext& m_rDriverContext;
	public:
		// you may have to add to your constructor some reference parameters
		// for example, a connection ID:
		//CConfigurationMKSNVXDriver(OpenViBEAcquisitionServer::IDriverContext& rDriverContext, const char* sGtkBuilderFileName, uint32_t& rConnectionId);
		CConfigurationMKSNVXDriver(IDriverContext& rDriverContext, const char* sGtkBuilderFileName, uint32_t& dataMode, bool& auxChannels);

		bool preConfigure() override;
		bool postConfigure() override;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationMKSNVXDriver_H__
