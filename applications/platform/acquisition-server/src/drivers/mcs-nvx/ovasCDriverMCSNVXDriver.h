#if defined(TARGET_HAS_ThirdPartyMCS)

#ifndef __OpenViBE_AcquisitionServer_CDriverMKSNVXDriver_H__
#define __OpenViBE_AcquisitionServer_CDriverMKSNVXDriver_H__

#include <vector>
#include "ovasIDriver.h"
#include "../../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../../ovasCSettingsHelper.h"
#include "../../ovasCSettingsHelperOperators.h"
#include "NVX.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverMKSNVXDriver
	 * \author mkochetkov (MKS)
	 * \date Tue Jan 21 23:21:03 2014
	 * \brief The CDriverMKSNVXDriver allows the acquisition server to acquire data from a MKSNVXDriver device.
	 *
	 * TODO: details
	 *
	 * \sa CConfigurationMKSNVXDriver
	 */
	const size_t maxNumberOfChannels = 36;
	class CDriverMKSNVXDriver : public OpenViBEAcquisitionServer::IDriver
	{
		uint8_t deviceBuffer_[1024*maxNumberOfChannels];
		int nvxDeviceId_;
		t_NVXDataSettins nvxDataSettings_;
		int nvxDataModel_;
		uint32_t dataMode_; // normal, test, impedance
		t_NVXProperty nvxProperty_;
		unsigned int samplesCounter_; // previous t_NVXDataModelXX.Counter
		uint32_t triggerStates_;
		bool showAuxChannels_;
		t_NVXInformation nvxInfo_;
	public:

		CDriverMKSNVXDriver(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		size_t getDeviceBufferSamplesCapacity() const {
			return sizeof(deviceBuffer_) / sizeof(t_NVXDataModel36) * maxNumberOfChannels; // I really do not know if it is accurate for all modes.
		}
		virtual ~CDriverMKSNVXDriver();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }
		
		virtual bool isFlagSet(
			const OpenViBEAcquisitionServer::EDriverFlag eFlag) const
		{
			return eFlag==DriverFlag_IsUnstable;
		}

	protected:
		
		SettingsHelper m_oSettings;
		
		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;

		// Replace this generic Header with any specific header you might have written
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		std::vector<float> sampleData_;
	private:

		/*
		 * Insert here all specific attributes, such as USB port number or device ID.
		 * Example :
		 */
		// uint32_t m_ui32ConnectionID;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverMKSNVXDriver_H__

#endif
