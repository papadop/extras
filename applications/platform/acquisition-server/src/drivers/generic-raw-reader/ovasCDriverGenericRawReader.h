#ifndef __OpenViBE_AcquisitionServer_CDriverGenericRawReader_H__
#define __OpenViBE_AcquisitionServer_CDriverGenericRawReader_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGenericRawReader
	 * \author Yann Renard (INRIA)
	 */
	class CDriverGenericRawReader : public IDriver
	{
	public:

		enum
		{
			Endian_Little=0,
			Endian_Big=1,

			Format_UnsignedInteger8=0,
			Format_UnsignedInteger16=1,
			Format_UnsignedInteger32=2,
			Format_SignedInteger8=3,
			Format_SignedInteger16=4,
			Format_SignedInteger32=5,
			Format_Float32=6,
			Format_Float64=7,
		} EParameter;

		CDriverGenericRawReader(IDriverContext& rDriverContext);
		virtual void release();

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		virtual bool open() =0;
		virtual bool close() =0;
		virtual bool read() =0;

	protected:

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;
		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32SampleSize;
		uint32_t m_ui32SampleFormat;
		uint32_t m_ui32SampleEndian;
		uint32_t m_ui32StartSkip;
		uint32_t m_ui32HeaderSkip;
		uint32_t m_ui32FooterSkip;
		uint32_t m_ui32DataFrameSize;
		bool m_bLimitSpeed;
		uint8_t* m_pDataFrame;
		float* m_pSample;
		uint64_t m_ui64StartTime;
		uint64_t m_ui64TotalSampleCount;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverGenericRawReader_H__
