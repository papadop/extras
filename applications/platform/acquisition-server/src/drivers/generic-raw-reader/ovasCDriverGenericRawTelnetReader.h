#ifndef __OpenViBE_AcquisitionServer_CDriverGenericRawTelnetReader_H__
#define __OpenViBE_AcquisitionServer_CDriverGenericRawTelnetReader_H__

#include "ovasCDriverGenericRawReader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <socket/IConnectionClient.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGenericRawTelnetReader
	 * \author Yann Renard (INRIA)
	 */
	class CDriverGenericRawTelnetReader : public CDriverGenericRawReader
	{
	public:

		CDriverGenericRawTelnetReader(IDriverContext& rDriverContext);

		const char* getName() override { return "Generic Raw Telnet Reader"; }
		bool isConfigurable() override { return true; }
		bool configure() override;

	protected:

		bool open() override;
		bool close() override;
		bool read() override;

	protected:

		SettingsHelper m_oSettings;

		Socket::IConnectionClient* m_pConnection;
		OpenViBE::CString m_sHostName;
		uint32_t m_ui32HostPort;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverGenericRawTelnetReader_H__
