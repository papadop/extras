#ifndef __OpenViBE_AcquisitionServer_CDriverGenericRawFileReader_H__
#define __OpenViBE_AcquisitionServer_CDriverGenericRawFileReader_H__

#include "ovasCDriverGenericRawReader.h"


#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"


#include <cstdio>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGenericRawFileReader
	 * \author Yann Renard (INRIA)
	 */
	class CDriverGenericRawFileReader : public CDriverGenericRawReader
	{
	public:

		CDriverGenericRawFileReader(IDriverContext& rDriverContext);

		const char* getName() override { return "Generic Raw File Reader"; }
		bool isConfigurable() override { return true; }
		bool configure() override;

	protected:

		bool open() override;
		bool close() override;
		bool read() override;

	protected:

		SettingsHelper m_oSettings;

		FILE* m_pFile;
		OpenViBE::CString m_sFilename;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverGenericRawFileReader_H__
