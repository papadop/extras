#ifndef __OpenViBE_AcquisitionServer_CConfigurationGenericRawReader_H__
#define __OpenViBE_AcquisitionServer_CConfigurationGenericRawReader_H__

#include "../ovasCConfigurationNetworkBuilder.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	class CConfigurationGenericRawReader : public CConfigurationNetworkBuilder
	{
	public:

		CConfigurationGenericRawReader(const char* sGtkBuilderFileName,
									   bool& rLimitSpeed,
									   uint32_t& rSampleFormat,
									   uint32_t& rSampleEndian,
									   uint32_t& rStartSkip,
									   uint32_t& rHeaderSkip,
									   uint32_t& rFooterSkip,
									   OpenViBE::CString& rFilename);

	protected:

		bool preConfigure() override;
		bool postConfigure() override;

	public:

		bool& m_rLimitSpeed;
		uint32_t& m_rSampleFormat;
		uint32_t& m_rSampleEndian;
		uint32_t& m_rStartSkip;
		uint32_t& m_rHeaderSkip;
		uint32_t& m_rFooterSkip;
		OpenViBE::CString& m_rFilename;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationGenericRawReader_H__
