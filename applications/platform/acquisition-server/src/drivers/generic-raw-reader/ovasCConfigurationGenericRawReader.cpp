#include "ovasCConfigurationGenericRawReader.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace OpenViBEAcquisitionServer;

CConfigurationGenericRawReader::CConfigurationGenericRawReader(const char* sGtkBuilderFileName,
															   bool& rLimitSpeed,
															   uint32_t& rSampleFormat,
															   uint32_t& rSampleEndian,
															   uint32_t& rStartSkip,
															   uint32_t& rHeaderSkip,
															   uint32_t& rFooterSkip,
															   CString& rFilename)
	: CConfigurationNetworkBuilder(sGtkBuilderFileName)
	  , m_rLimitSpeed(rLimitSpeed)
	  , m_rSampleFormat(rSampleFormat)
	  , m_rSampleEndian(rSampleEndian)
	  , m_rStartSkip(rStartSkip)
	  , m_rHeaderSkip(rHeaderSkip)
	  , m_rFooterSkip(rFooterSkip)
	  , m_rFilename(rFilename) {}

bool CConfigurationGenericRawReader::preConfigure()
{
	if (!CConfigurationNetworkBuilder::preConfigure())
	{
		return false;
	}

	GtkToggleButton* l_pToggleButtonSpeedLimit = GTK_TOGGLE_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "checkbutton_limit_speed"));
	GtkEntry* l_pEntryFilename = GTK_ENTRY(gtk_builder_get_object(m_pBuilderConfigureInterface, "entry_filename"));
	GtkComboBox* l_pComboBoxEndianness = GTK_COMBO_BOX(gtk_builder_get_object(m_pBuilderConfigureInterface, "combobox_endianness"));
	GtkComboBox* l_pComboBoxSampleType = GTK_COMBO_BOX(gtk_builder_get_object(m_pBuilderConfigureInterface, "combobox_sample_type"));
	GtkSpinButton* l_pSpinButtonStartSize = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_start_size"));
	GtkSpinButton* l_pSpinButtonHeaderSize = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_header_size"));
	GtkSpinButton* l_pSpinButtonFooterSize = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_footer_size"));

	gtk_toggle_button_set_active(l_pToggleButtonSpeedLimit, m_rLimitSpeed ? TRUE : FALSE);
	gtk_entry_set_text(l_pEntryFilename, m_rFilename.toASCIIString());
	gtk_combo_box_set_active(l_pComboBoxEndianness, m_rSampleEndian);
	gtk_combo_box_set_active(l_pComboBoxSampleType, m_rSampleFormat);
	gtk_spin_button_set_value(l_pSpinButtonStartSize, m_rStartSkip);
	gtk_spin_button_set_value(l_pSpinButtonHeaderSize, m_rHeaderSkip);
	gtk_spin_button_set_value(l_pSpinButtonFooterSize, m_rFooterSkip);

	return true;
}

bool CConfigurationGenericRawReader::postConfigure()
{
	if (m_bApplyConfiguration)
	{
		GtkToggleButton* l_pToggleButtonSpeedLimit = GTK_TOGGLE_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "checkbutton_limit_speed"));
		GtkEntry* l_pEntryFilename = GTK_ENTRY(gtk_builder_get_object(m_pBuilderConfigureInterface, "entry_filename"));
		GtkComboBox* l_pComboBoxEndianness = GTK_COMBO_BOX(gtk_builder_get_object(m_pBuilderConfigureInterface, "combobox_endianness"));
		GtkComboBox* l_pComboBoxSampleType = GTK_COMBO_BOX(gtk_builder_get_object(m_pBuilderConfigureInterface, "combobox_sample_type"));
		GtkSpinButton* l_pSpinButtonStartSize = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_start_size"));
		GtkSpinButton* l_pSpinButtonHeaderSize = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_header_size"));
		GtkSpinButton* l_pSpinButtonFooterSize = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_footer_size"));

		gtk_spin_button_update(l_pSpinButtonStartSize);
		gtk_spin_button_update(l_pSpinButtonHeaderSize);
		gtk_spin_button_update(l_pSpinButtonFooterSize);

		m_rLimitSpeed = gtk_toggle_button_get_active(l_pToggleButtonSpeedLimit) ? true : false;
		m_rFilename = gtk_entry_get_text(l_pEntryFilename);
		m_rSampleEndian = (uint32_t)gtk_combo_box_get_active(l_pComboBoxEndianness);
		m_rSampleFormat = (uint32_t)gtk_combo_box_get_active(l_pComboBoxSampleType);
		m_rStartSkip = (uint32_t)gtk_spin_button_get_value(l_pSpinButtonStartSize);
		m_rHeaderSkip = (uint32_t)gtk_spin_button_get_value(l_pSpinButtonHeaderSize);
		m_rFooterSkip = (uint32_t)gtk_spin_button_get_value(l_pSpinButtonFooterSize);
	}

	if (!CConfigurationNetworkBuilder::postConfigure())
	{
		return false;
	}
	return true;
}
