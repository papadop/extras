/*
 * The raw reader expects the data to be formatted as follows
 *
 * [START][BLOCK0][BLOCK1][BLOCK2]...
 *  skip   parse   parse   parse  ...
 *
 * where each block is          [===========BLOCKX=================]
 *                  is read as  [===========dataFrameSize==========]
 *                  breaks to   [header====][sample====][footer====]
 *                  equals      [headerSize][sampleSize][footerSize]
 *                  means          skip        keep         skip
 *
 * For correct parsing, user must provide the exact sizes of the skipped parts "start", "header" and "footer" in bytes. 
 *
 */

#include "ovasCDriverGenericRawReader.h"
#include "../ovasCConfigurationBuilder.h"

#include <toolkit/ovtk_all.h>
#include <openvibe/ovITimeArithmetics.h>

#include <system/ovCMemory.h>
#include <system/ovCTime.h>

#include <cmath>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace Kernel;

// #define OPENVIBE_DEBUG_RAW_READER

namespace
{
	template <class T>
	float decode_little_endian(const uint8_t* pBuffer)
	{
		T t;
		System::Memory::littleEndianToHost(pBuffer, &t);
		return float(t);
	}

	template <class T>
	float decode_big_endian(const uint8_t* pBuffer)
	{
		T t;
		System::Memory::bigEndianToHost(pBuffer, &t);
		return float(t);
	}
};

//___________________________________________________________________//
//                                                                   //

CDriverGenericRawReader::CDriverGenericRawReader(IDriverContext& rDriverContext)
	: IDriver(rDriverContext)
	  , m_pCallback(nullptr)
	  , m_ui32SampleCountPerSentBlock(0)
	  , m_ui32SampleFormat(Format_SignedInteger32)
	  , m_ui32SampleEndian(Endian_Little)
	  , m_ui32StartSkip(0)
	  , m_ui32HeaderSkip(0)
	  , m_ui32FooterSkip(20)
	  , m_bLimitSpeed(false)
	  , m_pDataFrame(nullptr)
	  , m_pSample(nullptr)
{
	m_oHeader.setSamplingFrequency(512);
	m_oHeader.setChannelCount(16);
}

void CDriverGenericRawReader::release()
{
	delete this;
}

//___________________________________________________________________//
//                                                                   //

bool CDriverGenericRawReader::initialize(
	const uint32_t ui32SampleCountPerSentBlock,
	IDriverCallback& rCallback)
{
	if (m_rDriverContext.isConnected()) { return false; }

	switch (m_ui32SampleFormat)
	{
		case Format_UnsignedInteger8:
		case Format_SignedInteger8:
			m_ui32SampleSize = 1;
			break;
		case Format_UnsignedInteger16:
		case Format_SignedInteger16:
			m_ui32SampleSize = 2;
			break;
		case Format_UnsignedInteger32:
		case Format_SignedInteger32:
		case Format_Float32:
			m_ui32SampleSize = 4;
			break;
		case Format_Float64:
			m_ui32SampleSize = 8;
			break;
		default:
			m_rDriverContext.getLogManager() << LogLevel_Error << "Unsupported data format " << m_ui32SampleFormat << "\n";
			return false;
	}

	m_ui32DataFrameSize = m_ui32SampleSize * m_oHeader.getChannelCount();
	m_ui32DataFrameSize += m_ui32HeaderSkip;
	m_ui32DataFrameSize += m_ui32FooterSkip;

	m_pSample = new float[m_oHeader.getChannelCount()];
	m_pDataFrame = new uint8_t[m_ui32DataFrameSize];
	if (!m_pSample || !m_pDataFrame)
	{
		m_rDriverContext.getLogManager() << LogLevel_Error << "Could not allocate memory !\n";
		return false;
	}

	// open() should skip m_ui32StartSkip worth of bytes already
	if (!this->open())
	{
		return false;
	}

	m_pCallback = &rCallback;
	m_ui32SampleCountPerSentBlock = ui32SampleCountPerSentBlock;

	return true;
}

bool CDriverGenericRawReader::start()
{
	if (!m_rDriverContext.isConnected()) { return false; }
	if (m_rDriverContext.isStarted()) { return false; }
	m_ui64TotalSampleCount = 0;
	m_ui64StartTime = System::Time::zgetTime();
	return true;
}

bool CDriverGenericRawReader::loop()
{
	if (!m_rDriverContext.isConnected()) { return false; }
	// if(!m_rDriverContext.isStarted()) { return true; }

	uint64_t l_ui64SampleTime = ITimeArithmetics::sampleCountToTime(m_oHeader.getSamplingFrequency(), m_ui64TotalSampleCount);

	if (m_bLimitSpeed && (l_ui64SampleTime > System::Time::zgetTime() - m_ui64StartTime))
	{
		return true;
	}

#ifdef OPENVIBE_DEBUG_RAW_READER
	m_rDriverContext.getLogManager() << LogLevel_Info << "Decoded : ";
#endif

	for (uint32_t j = 0; j < m_ui32SampleCountPerSentBlock; j++)
	{
		if (!this->read())
		{
			return false;
		}

		for (uint32_t i = 0; i < m_oHeader.getChannelCount(); i++)
		{
			uint8_t* l_pDataFrame = m_pDataFrame + m_ui32HeaderSkip + i * m_ui32SampleSize;
			switch (m_ui32SampleEndian)
			{
				case Endian_Little:
					switch (m_ui32SampleFormat)
					{
						case Format_UnsignedInteger8: m_pSample[i] = *l_pDataFrame;
							break;
						case Format_UnsignedInteger16: m_pSample[i] = decode_little_endian<uint16_t>(l_pDataFrame);
							break;
						case Format_UnsignedInteger32: m_pSample[i] = decode_little_endian<uint32_t>(l_pDataFrame);
							break;
						case Format_SignedInteger8: m_pSample[i] = *l_pDataFrame;
							break;
						case Format_SignedInteger16: m_pSample[i] = decode_little_endian<int16_t>(l_pDataFrame);
							break;
						case Format_SignedInteger32: m_pSample[i] = decode_little_endian<int32_t>(l_pDataFrame);
							break;
						case Format_Float32: m_pSample[i] = decode_little_endian<float>(l_pDataFrame);
							break;
						case Format_Float64: m_pSample[i] = decode_little_endian<double>(l_pDataFrame);
							break;
						default: break;
					}
					break;

				case Endian_Big:
					switch (m_ui32SampleFormat)
					{
						case Format_UnsignedInteger8: m_pSample[i] = *l_pDataFrame;
							break;
						case Format_UnsignedInteger16: m_pSample[i] = decode_big_endian<uint16_t>(l_pDataFrame);
							break;
						case Format_UnsignedInteger32: m_pSample[i] = decode_big_endian<uint32_t>(l_pDataFrame);
							break;
						case Format_SignedInteger8: m_pSample[i] = *l_pDataFrame;
							break;
						case Format_SignedInteger16: m_pSample[i] = decode_big_endian<int16_t>(l_pDataFrame);
							break;
						case Format_SignedInteger32: m_pSample[i] = decode_big_endian<int32_t>(l_pDataFrame);
							break;
						case Format_Float32: m_pSample[i] = decode_big_endian<float>(l_pDataFrame);
							break;
						case Format_Float64: m_pSample[i] = decode_big_endian<double>(l_pDataFrame);
							break;
						default: break;
					}
					break;
				default:
					break;
			}
#ifdef OPENVIBE_DEBUG_RAW_READER
			m_rDriverContext.getLogManager() << m_pSample[i] << " ";
#endif
		}

		if (m_rDriverContext.isStarted())
		{
			m_pCallback->setSamples(m_pSample, 1);
		}
	}
#ifdef OPENVIBE_DEBUG_RAW_READER
	m_rDriverContext.getLogManager() << "\n";
#endif

	if (m_rDriverContext.isStarted())
	{
		m_rDriverContext.correctDriftSampleCount(m_rDriverContext.getSuggestedDriftCorrectionSampleCount());
	}

	m_ui64TotalSampleCount += m_ui32SampleCountPerSentBlock;

	return true;
}

bool CDriverGenericRawReader::stop()
{
	if (!m_rDriverContext.isConnected()) { return false; }
	if (!m_rDriverContext.isStarted()) { return false; }

	return true;
}

bool CDriverGenericRawReader::uninitialize()
{
	if (!m_rDriverContext.isConnected()) { return false; }
	if (m_rDriverContext.isStarted()) { return false; }

	if (!this->close())
	{
		return false;
	}

	delete [] m_pSample;
	delete [] m_pDataFrame;
	m_pSample = nullptr;
	m_pDataFrame = nullptr;
	m_pCallback = nullptr;

	return true;
}
