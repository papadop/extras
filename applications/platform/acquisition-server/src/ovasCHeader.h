#ifndef __OpenViBEAcquisitionServer_CHeader_H__
#define __OpenViBEAcquisitionServer_CHeader_H__

#include "ovasIHeader.h"

namespace OpenViBEAcquisitionServer
{
	class CHeader : public IHeader
	{
	public:

		CHeader();
		virtual ~CHeader();
		void reset() override;

		// Experiment information
		bool setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier) override;
		bool setSubjectAge(const uint32_t ui32SubjectAge) override;
		bool setSubjectGender(const uint32_t ui32SubjectGender) override;

		uint32_t getExperimentIdentifier() const override;
		uint32_t getSubjectAge() const override;
		uint32_t getSubjectGender() const override;

		bool isExperimentIdentifierSet() const override;
		bool isSubjectAgeSet() const override;
		bool isSubjectGenderSet() const override;

		bool setImpedanceCheckRequested(const bool bImpedanceCheckRequested) override;
		bool isImpedanceCheckRequested() const override;
		uint32_t getImpedanceLimit() const override;
		bool setImpedanceLimit(const uint32_t ui32ImpedanceLimit) override;

		// Chanel information
		bool setChannelCount(const uint32_t ui32ChannelCount) override;
		bool setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName) override;
		bool setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain) override;
		bool setChannelUnits(const uint32_t ui32ChannelIndex, const uint32_t ui32ChannelUnit, const uint32_t ui32ChannelFactor) override;
		// virtual OpenViBE::boolean setChannelLocation(const uint32_t ui32ChannelIndex, const OpenViBE::float32 ui32ChannelLocationX, const OpenViBE::float32 ui32ChannelLocationY, const OpenViBE::float32 ui32ChannelLocationZ);

		uint32_t getChannelCount() const override;
		const char* getChannelName(const uint32_t ui32ChannelIndex) const override;
		float getChannelGain(const uint32_t ui32ChannelIndex) const override;
		bool getChannelUnits(const uint32_t ui32ChannelIndex, uint32_t& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const override;
		// virtual getChannelLocation(const uint32_t ui32ChannelIndex) const;

		bool isChannelCountSet() const override;
		bool isChannelNameSet() const override;
		bool isChannelGainSet() const override;
		// virtual OpenViBE::boolean isChannelLocationSet() const;
		bool isChannelUnitSet() const override;

		// Samples information
		bool setSamplingFrequency(const uint32_t ui32SamplingFrequency) override;

		uint32_t getSamplingFrequency() const override;

		bool isSamplingFrequencySet() const override;

	protected:

		IHeader* m_pHeaderImpl;
	};

	class CHeaderAdapter : public IHeader
	{
	public:

		CHeaderAdapter(IHeader& rAdaptedHeader) : m_rAdaptedHeader(rAdaptedHeader) { }

		void reset() override { return m_rAdaptedHeader.reset(); }

		// Experiment information
		bool setExperimentIdentifier(const uint32_t ui32ExperimentIdentifier) override { return m_rAdaptedHeader.setExperimentIdentifier(ui32ExperimentIdentifier); }
		bool setSubjectAge(const uint32_t ui32SubjectAge) override { return m_rAdaptedHeader.setSubjectAge(ui32SubjectAge); }
		bool setSubjectGender(const uint32_t ui32SubjectGender) override { return m_rAdaptedHeader.setSubjectGender(ui32SubjectGender); }

		uint32_t getExperimentIdentifier() const override { return m_rAdaptedHeader.getExperimentIdentifier(); }
		uint32_t getSubjectAge() const override { return m_rAdaptedHeader.getSubjectAge(); }
		uint32_t getSubjectGender() const override { return m_rAdaptedHeader.getSubjectGender(); }

		bool isExperimentIdentifierSet() const override { return m_rAdaptedHeader.isExperimentIdentifierSet(); }
		bool isSubjectAgeSet() const override { return m_rAdaptedHeader.isSubjectAgeSet(); }
		bool isSubjectGenderSet() const override { return m_rAdaptedHeader.isSubjectGenderSet(); }

		bool setImpedanceCheckRequested(const bool bImpedanceCheckRequested) override { return m_rAdaptedHeader.setImpedanceCheckRequested(bImpedanceCheckRequested); }
		bool isImpedanceCheckRequested() const override { return m_rAdaptedHeader.isImpedanceCheckRequested(); }
		uint32_t getImpedanceLimit() const override { return m_rAdaptedHeader.getImpedanceLimit(); }
		bool setImpedanceLimit(const uint32_t ui32ImpedanceLimit) override { return m_rAdaptedHeader.setImpedanceLimit(ui32ImpedanceLimit); }

		// Channel information
		bool setChannelCount(const uint32_t ui32ChannelCount) override { return m_rAdaptedHeader.setChannelCount(ui32ChannelCount); }
		bool setChannelName(const uint32_t ui32ChannelIndex, const char* sChannelName) override { return m_rAdaptedHeader.setChannelName(ui32ChannelIndex, sChannelName); }
		bool setChannelGain(const uint32_t ui32ChannelIndex, const float f32ChannelGain) override { return m_rAdaptedHeader.setChannelGain(ui32ChannelIndex, f32ChannelGain); }
		bool setChannelUnits(const uint32_t ui32ChannelIndex, const uint32_t ui32ChannelUnit, const uint32_t ui32ChannelFactor) override { return m_rAdaptedHeader.setChannelUnits(ui32ChannelIndex, ui32ChannelUnit, ui32ChannelFactor); } ;

		uint32_t getChannelCount() const override { return m_rAdaptedHeader.getChannelCount(); }
		const char* getChannelName(const uint32_t ui32ChannelIndex) const override { return m_rAdaptedHeader.getChannelName(ui32ChannelIndex); }
		float getChannelGain(const uint32_t ui32ChannelIndex) const override { return m_rAdaptedHeader.getChannelGain(ui32ChannelIndex); }
		bool getChannelUnits(const uint32_t ui32ChannelIndex, uint32_t& ui32ChannelUnit, uint32_t& ui32ChannelFactor) const override { return m_rAdaptedHeader.getChannelUnits(ui32ChannelIndex, ui32ChannelUnit, ui32ChannelFactor); } ;

		bool isChannelCountSet() const override { return m_rAdaptedHeader.isChannelCountSet(); }
		bool isChannelNameSet() const override { return m_rAdaptedHeader.isChannelNameSet(); }
		bool isChannelGainSet() const override { return m_rAdaptedHeader.isChannelGainSet(); }
		bool isChannelUnitSet() const override { return m_rAdaptedHeader.isChannelUnitSet(); }

		// Samples information
		bool setSamplingFrequency(const uint32_t ui32SamplingFrequency) override { return m_rAdaptedHeader.setSamplingFrequency(ui32SamplingFrequency); }
		uint32_t getSamplingFrequency() const override { return m_rAdaptedHeader.getSamplingFrequency(); }
		bool isSamplingFrequencySet() const override { return m_rAdaptedHeader.isSamplingFrequencySet(); }

	protected:

		IHeader& m_rAdaptedHeader;
	};
};

#endif // __OpenViBEAcquisitionServer_CHeader_H__
