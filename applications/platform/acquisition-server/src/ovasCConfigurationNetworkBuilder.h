#ifndef __OpenViBE_AcquisitionServer_CConfigurationNetworkBuilder_H__
#define __OpenViBE_AcquisitionServer_CConfigurationNetworkBuilder_H__

#include "ovasCConfigurationBuilder.h"

namespace OpenViBEAcquisitionServer
{
	class CConfigurationNetworkBuilder : public CConfigurationBuilder
	{
	public:

		CConfigurationNetworkBuilder(const char* sGtkBuilderFileName);
		virtual ~CConfigurationNetworkBuilder();

		virtual bool setHostName(const OpenViBE::CString& sHostName);
		virtual bool setHostPort(const uint32_t ui32HostPort);

		virtual OpenViBE::CString getHostName() const;
		virtual uint32_t getHostPort() const;

	protected:

		bool preConfigure() override;
		bool postConfigure() override;

	private:

		CConfigurationNetworkBuilder();

	protected:

		GtkWidget* m_pHostName;
		GtkWidget* m_pHostPort;

		OpenViBE::CString m_sHostName;
		uint32_t m_ui32HostPort;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationNetworkBuilder_H__
