#ifndef __OpenViBESkeletonGenerator_CDriverSkeletonGenerator_H__
#define __OpenViBESkeletonGenerator_CDriverSkeletonGenerator_H__

#include "ovsgCSkeletonGenerator.h"

#include <vector>
#include <map>

namespace OpenViBESkeletonGenerator
{
	class CDriverSkeletonGenerator : public CSkeletonGenerator
	{
	public:

		CDriverSkeletonGenerator(OpenViBE::Kernel::IKernelContext& rKernelContext, GtkBuilder* pBuilderInterface);
		virtual ~CDriverSkeletonGenerator();

		bool initialize() override;
		bool save(OpenViBE::CString sFileName) override;
		bool load(OpenViBE::CString sFileName) override;
		void getCurrentParameters() override;

		OpenViBE::CString m_sDriverName;
		OpenViBE::CString m_sClassName;
		OpenViBE::CString m_sSamplingFrequencies;
		std::vector<OpenViBE::CString> m_vSamplingFrequencies;
		OpenViBE::CString m_sMinChannel;
		OpenViBE::CString m_sMaxChannel;

		void buttonCheckCB();
		void buttonOkCB();
		void buttonTooltipCB(GtkButton* pButton);
		void buttonExitCB();

	private:

		OpenViBE::Kernel::ILogManager& getLogManager() const override { return m_rKernelContext.getLogManager(); }
		OpenViBE::Kernel::IErrorManager& getErrorManager() const override { return m_rKernelContext.getErrorManager(); }

		typedef enum
		{
			WidgetName_DriverName,
			WidgetName_ClassName,
			WidgetName_ChannelCount,
			WidgetName_SamplingFrequencies,
			WidgetName_TargetDirectory,
		} EWidgetName;

		std::map<GtkButton*, EWidgetName> m_vWidgetName;
	};
}

#endif //__OpenViBESkeletonGenerator_CDriverSkeletonGenerator_H__
