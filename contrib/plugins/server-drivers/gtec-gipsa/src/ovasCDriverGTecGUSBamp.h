#ifndef __OpenViBE_AcquisitionServer_CDriverGTecGUSBamp_H__
#define __OpenViBE_AcquisitionServer_CDriverGTecGUSBamp_H__

#if defined TARGET_HAS_ThirdPartyGUSBampCAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <Windows.h>

#include "ringbuffer.h"

#include <gtk/gtk.h>
#include <vector>

//threading
#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory> // unique_ptr

#include <deque>
using namespace std;

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGTecGUSBamp
	 * \author Anton Andreev, Gipsa-lab, VIBS team
	 * \date 19/07/2012
	 * \brief GTEC driver
	 *
	 * This driver was rewritten to match the code provided by Guger as much as possible. There are several things
	 * that all must work together so that higher frequencies are supported and no hardware triggers are lost.
	 *
	 * This driver supports several buffers so that the more than one GT_GetData can be executed in the beginning (QUEUE_SIZE)
	 * and then calls to GT_GetData are queued. This allows data to be processed by OpenVibe while waiting for the next result of
	 * a previously issued GT_GetData. The extra thread is added to support this and to allow for async IO. 
	 *
	 * Hardware triggers on the parallel port are supported.
	 *
	 * The driver supports several g.tec devices working with the provided async cables. There are several requirements for async
	 * acquisition to work properly and these are checked in verifySyncMode().
	 */

	struct GDevice
	{
		HANDLE handle;
		std::string   serial;
	};

	class CDriverGTecGUSBamp : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverGTecGUSBamp(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);

		virtual void release();
		virtual const char* getName();

		virtual bool initialize(
		    const uint32_t ui32SampleCountPerSentBlock,
		    OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }

		bool CDriverGTecGUSBamp::acquire();

		void ConfigFiltering(HANDLE o_pDevice);

	protected:

		static const int BUFFER_SIZE_SECONDS = 2;         //the size of the GTEC ring buffer in seconds
		static const int GTEC_NUM_CHANNELS = 16;          //the number of channels without countig the trigger channel
		static const int QUEUE_SIZE = 8;//4 default		  //the number of GT_GetData calls that will be queued during acquisition to avoid loss of data
		static const int NUMBER_OF_SCANS = 32;            //the number of scans that should be received simultaneously (depending on the _sampleRate; see C-API documentation for this value!)
		
		uint32_t NumDevices();

		static const uint32_t nPoints = NUMBER_OF_SCANS * (GTEC_NUM_CHANNELS + 1);
		int validPoints;
		static const DWORD bufferSizeBytes;

		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;

		float* m_pSample;

		uint32_t m_ui32GlobalImpedanceIndex;

		uint8_t m_ui8CommonGndAndRefBitmap;

		int32_t m_i32NotchFilterIndex;
		int32_t m_i32BandPassFilterIndex;

		bool m_bTriggerInputEnabled;
		bool m_bBipolarEnabled; //electrodes are substracted in sepecific sequence 1-2=1, ... 15-16=15 which results in 8 instead of 16 electrodes - used for EMG
		bool m_bCalibrationSignalEnabled;
		bool m_bShowDeviceName; //adds the amplifier serial number to the name of the channel 

		bool m_bReconfigurationRequired; // After some gt calls, we may need reconfig

		uint32_t m_ui32AcquiredChannelCount;      //number of channels specified by the user, never counts the event channels

		uint32_t m_ui32TotalHardwareStimulations; //since start button clicked
		uint32_t m_ui32TotalDriverChunksLost;     //since start button clicked
		uint32_t m_ui32TotalDriverTimeouts;		  //since start button clicked
		uint32_t m_ui32TotalRingBufferOverruns;
		uint32_t m_ui32TotalDataUnavailable; 

		//contains buffer per device and then QUEUE_SIZE buffers so that several calls to GT_GetData can be supported
		BYTE*** m_buffers;
	    OVERLAPPED** m_overlapped;

		bool m_flagIsFirstLoop;
		bool m_bufferOverrun;

		//ring buffer provided by Guger
		CRingBuffer<float> m_RingBuffer;	

		uint32_t m_ui32CurrentQueueIndex;

		std::unique_ptr<std::thread> m_ThreadPtr;
		bool m_bIsThreadRunning;

		std::mutex m_io_mutex;

		float *m_bufferReceivedData;
		std::condition_variable  m_itemAvailable;

		bool ConfigureDevice(uint32_t deviceNumber);

		bool verifySyncMode();//Checks if devices are configured correctly when acquiring data from multiple devices

		//Selects which device to become the new master, used only when more than 1 device is available
		bool setMasterDevice(string targetMasterSerial); //0 first device
		void detectDevices();

	    uint32_t m_mastersCnt;
	    uint32_t m_slavesCnt;
	    string m_masterSerial;

		void remapChannelNames();   // Converts channel names while appending the device name and handling event channels
		void restoreChannelNames(); // Restores channel names without the device name

		vector<std::string> m_vOriginalChannelNames;        // Channel names without the device name inserted
		vector<GDevice> m_vGDevices;                        // List of amplifiers

		std::string CDriverGTecGUSBamp::getSerialByHandler(HANDLE o_pDevice);

		// Stores information related to each channel available in the recording system
		struct Channel
		{
			int32_t m_i32Index;             // Channel index in openvibe Designer, -1 is unused
			int32_t m_i32OldIndex;          // Channel index in the user-settable channel name list
			int32_t m_i32GtecDeviceIndex;   // Device index of the gtec amplifier this channel is in
			int32_t m_i32GtecChannelIndex;  // Channel index in the device-specific numbering
			bool m_bIsEventChannel;                 // Is this the special digital channel?
		};

		// Channel indexes are seen as a sequence [dev1chn1, dev1chn2,...,dev1chnN, dev2chn1, dev2chn2, ..., dev2chnN, ...]
		// The following vector is used to map these 'system indexes' to openvibe channels
		vector<Channel> m_vChannelMap;
	};
};

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI

#endif // __OpenViBE_AcquisitionServer_CDriverGTecGUSBamp_H__
