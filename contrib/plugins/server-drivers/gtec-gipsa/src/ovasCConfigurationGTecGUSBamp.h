#ifndef __OpenViBE_AcquisitionServer_CConfigurationGTecGUSBamp_H__
#define __OpenViBE_AcquisitionServer_CConfigurationGTecGUSBamp_H__

#if defined TARGET_HAS_ThirdPartyGUSBampCAPI

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>
#include <string.h>
#include <vector>
using namespace std;

namespace OpenViBEAcquisitionServer
{
	class CConfigurationGTecGUSBamp : public OpenViBEAcquisitionServer::CConfigurationBuilder
	{
	public:
		CConfigurationGTecGUSBamp(const char* sGtkBuilderFileName,
								  uint8_t& rCommonGndAndRefBitmap,
								  int32_t& rNotchFilterIndex, 
								  int32_t& rBandPassFilterIndex,
								  bool& rTriggerInput,
								  vector<string> rDevicesSerials,
								  string& rMasterDeviceIndex,
								  bool& rBipolar,
								  bool& rCalibrationSignalEnabled,
								  bool& rShowDeviceName
								  );

		virtual bool preConfigure();
		virtual bool postConfigure();

		void buttonCalibratePressedCB();
		void idleCalibrateCB();

		void buttonCommonGndRefPressedCB();
		void buttonFiltersPressedCB();
		void setHardwareFiltersDialog();
		void buttonFiltersApplyPressedCB();

	protected:
		uint8_t& m_rCommonGndAndRefBitmap;

		int32_t& m_rNotchFilterIndex;
		int32_t& m_rBandPassFilterIndex;
		bool& m_rTriggerInput;
		vector<string> m_rDevicesSerials;
		string& m_rMasterDeviceIndex;
		vector<uint32_t> m_vComboBoxBandPassFilterIndex;
		vector<uint32_t> m_vComboBoxNotchFilterIndex;
		bool& m_rBipolarEnabled;
		bool& m_rCalibrationSignalEnabled;
		bool& m_rShowDeviceName;
		
	private:
		::GtkWidget* m_pCalibrateDialog;
		bool m_bCalibrationDone;
	};
};

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI

#endif // __OpenViBE_AcquisitionServer_CConfigurationGTecGUSBamp_H__
