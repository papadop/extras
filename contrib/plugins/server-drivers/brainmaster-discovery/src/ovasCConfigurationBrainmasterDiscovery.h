/*
 * ovasCConfigurationBrainmasterDiscovery.h
 *
 * Copyright (c) 2012, Yann Renard. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __OpenViBE_AcquisitionServer_CConfigurationBrainmasterDiscovery_H__
#define __OpenViBE_AcquisitionServer_CConfigurationBrainmasterDiscovery_H__

#if defined TARGET_HAS_ThirdPartyBrainmasterCodeMakerAPI

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>
#include <vector>
#include <string>
#include <map>

namespace OpenViBEAcquisitionServer
{
	class CConfigurationBrainmasterDiscovery : public OpenViBEAcquisitionServer::CConfigurationBuilder
	{
	public:
		CConfigurationBrainmasterDiscovery(const char* sGtkBuilderFileName,
			uint32_t& rPort,
			uint32_t& rPreset,
			uint32_t& rType,
			uint32_t& rBaudRate,
			uint32_t& rSamplingFrequency,
			uint32_t& rBitDepth,
			uint32_t& rNotchFilters,
			std::map < uint32_t, uint32_t >& rvChannelType,
			std::string& sDeviceSerial,
			std::string& sDevicePasskey);
		virtual ~CConfigurationBrainmasterDiscovery();

		virtual bool preConfigure();
		virtual bool postConfigure();

		::GtkSpinButton* m_pSpinButtonChannelCount;
		::GtkComboBox* m_pComboBoxDevice;
		::GtkComboBox* m_pComboBoxPreset;
		::GtkComboBox* m_pComboBoxType;
		::GtkComboBox* m_pComboBoxBaudRate;
		::GtkComboBox* m_pComboBoxSamplingRate;
		::GtkComboBox* m_pComboBoxBitDepth;
		::GtkComboBox* m_pComboBoxNotchFilters;
		::GtkEntry* m_pEntryDeviceSerial;
		::GtkEntry* m_pEntryDevicePasskey;

		using OpenViBEAcquisitionServer::CConfigurationBuilder::m_vChannelName;


		::GtkListStore* m_pListStore;

		uint32_t& m_rPort;
		uint32_t& m_rPreset;
		uint32_t& m_rType;
		uint32_t& m_rBaudRate;
		uint32_t& m_rSamplingRate;
		uint32_t& m_rBitDepth;
		uint32_t& m_rNotchFilters;
		std::map < uint32_t, uint32_t >& m_rvChannelType;
		std::string& m_sDeviceSerial;
		std::string& m_sDevicePasskey;

		std::vector < ::GtkWidget* > m_vSensitiveWidget;
	};
};

#endif // TARGET_HAS_ThirdPartyBrainmasterCodeMakerAPI

#endif // __OpenViBE_AcquisitionServer_CConfigurationBrainmasterDiscovery_H__
