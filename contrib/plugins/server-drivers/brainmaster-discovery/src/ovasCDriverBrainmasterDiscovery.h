/*
 * ovasCDriverBrainmasterDiscovery.h
 *
 * Copyright (c) 2012, Yann Renard. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

#ifndef __OpenViBE_AcquisitionServer_CDriverBrainmasterDiscovery_H__
#define __OpenViBE_AcquisitionServer_CDriverBrainmasterDiscovery_H__

#if defined TARGET_HAS_ThirdPartyBrainmasterCodeMakerAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include <vector>
#include <map>
#include <string>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverBrainmasterDiscovery
	 * \author Yann Renard
	 */
	class CDriverBrainmasterDiscovery : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverBrainmasterDiscovery(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual void release();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();
		virtual bool read(unsigned char* pFrame, uint32_t ui32Count);
		virtual uint32_t sync();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }

	protected:

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;
		uint32_t m_ui32SampleCountPerSentBlock;

		std::string m_sDeviceSerial;
		std::string m_sDevicePasskey;
		std::string m_sFrameDumpFilename;
		bool m_bFrameDumpFlag;

		uint32_t m_ui32Port;
		uint32_t m_ui32PortReal;

		uint32_t m_ui32Preset;
		uint32_t m_ui32Type;
		uint32_t m_ui32BaudRate;
		uint32_t m_ui32BaudRateReal;
		uint32_t m_ui32SamplingRate;
		uint32_t m_ui32SamplingRateReal;
		uint32_t m_ui32BitDepth;
		uint32_t m_ui32BitDepthReal;
		uint32_t m_ui32NotchFilters;
		uint32_t m_ui32NotchFiltersReal;

		uint32_t m_ui32FrameSize;
		uint32_t m_ui32DataOffset;

		std::map < uint32_t, uint32_t > m_vBaudRate;
		std::map < uint32_t, uint32_t > m_vBaudRateValue;
		std::map < uint32_t, uint32_t > m_vNotchFilters;
		std::map < uint32_t, uint32_t > m_vNotchFiltersValue;
		std::map < uint32_t, uint32_t > m_vBitDepth;
		std::map < uint32_t, uint32_t > m_vBitDepthValue;
		std::map < uint32_t, uint32_t > m_vChannelType;
		std::map < uint32_t, uint32_t > m_vChannelSelectionMask;
		std::map < uint32_t, std::map < uint32_t, float (*)(unsigned char*&) > > m_vBitDepthDecoder;
		std::map < uint32_t, int (*)() > m_vStartModule;
		std::map < uint32_t, int (*)() > m_vStopModule;

		std::vector < float > m_vSample;
		std::vector < unsigned char > m_vBuffer;

		uint32_t m_ui32SyncByte;
		bool m_bWasLastFrameCorrect;
	};
};

#endif // TARGET_HAS_ThirdPartyBrainmasterCodeMakerAPI

#endif // __OpenViBE_AcquisitionServer_CDriverBrainmasterDiscovery_H__
