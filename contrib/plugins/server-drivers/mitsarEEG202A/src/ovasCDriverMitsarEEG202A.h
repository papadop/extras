#ifndef __OpenViBE_AcquisitionServer_CDriverMitsarEEG202A_H__
#define __OpenViBE_AcquisitionServer_CDriverMitsarEEG202A_H__

#if defined(TARGET_HAS_ThirdPartyMitsar)
#if defined TARGET_OS_Windows

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverMitsarEEG202A
	 * \author Gelu Ionescu (GIPSA-lab)
	 * \date 26 April 2012
	 * \erief The CDriverMitsarEEG202A allows the acquisition server to acquire data from a Mitsar EEG 202A amplifier.
	 *
	 * submitted by Anton Andreev (GIPSA-lab)
	 */
	class CDriverMitsarEEG202A : public OpenViBEAcquisitionServer::IDriver
	{
	private:
		typedef enum
		{
			CHANNEL_NB      = 33,
			SAMPLING_RATE	= 500,
			SAMPLES_NB		= 33,
			STIMULATION_0	= 0,
			STIMULATION_128	= 128, 
			STIMULATION_64	= 64, 
			STIMULATION_192	= (STIMULATION_128 + STIMULATION_64), //both buttons pressed
		} misc_t;
	public:

		CDriverMitsarEEG202A(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);//modif new Idriver
		virtual void release();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }

		//??? virtual void processData (cf neXus)

	protected:
		
		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback*  m_pCallback;
		OpenViBEAcquisitionServer::CHeader           m_oHeader;

		uint32_t                             m_ui32RefIndex;
		bool                            m_bEventAndBioChannelsState;

		uint32_t                             m_ui32LastStimulation;
		float*                           m_pfl32StimulationChannel;
		std::vector<uint32_t>                m_vStimulationIdentifier;
		std::vector<uint32_t>                m_vStimulationDate;
	
		std::vector<float>               m_vInputSamples;
		std::vector<float>               m_vOutputSamples;

	};
};

#endif
#endif

#endif // __OpenViBE_AcquisitionServer_CDriverMitsarEEG202A_H__
