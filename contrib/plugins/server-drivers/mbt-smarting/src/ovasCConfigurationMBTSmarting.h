#ifndef __OpenViBE_AcquisitionServer_CConfigurationMBTSmarting_H__
#define __OpenViBE_AcquisitionServer_CConfigurationMBTSmarting_H__

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationMBTSmarting
	 * \author mBrainTrain dev team (mBrainTrain)
	 * \date Tue Oct 14 16:09:43 2014
	 * \brief The CConfigurationMBTSmarting handles the configuration dialog specific to the MBTSmarting device.
	 *
	 * TODO: details
	 *
	 * \sa CDriverMBTSmarting
	 */
	class CConfigurationMBTSmarting : public CConfigurationBuilder
	{
	public:

		// you may have to add to your constructor some reference parameters
		// for example, a connection ID:
		CConfigurationMBTSmarting(IDriverContext& rDriverContext, const char* sGtkBuilderFileName, uint32_t& rConnectionId);
		//CConfigurationMBTSmarting(OpenViBEAcquisitionServer::IDriverContext& rDriverContext, const char* sGtkBuilderFileName);

		bool preConfigure() override;
		bool postConfigure() override;

		virtual ~CConfigurationMBTSmarting();

	protected:

		IDriverContext& m_rDriverContext;

	private:

		/*
		 * Insert here all specific attributes, such as a connection ID.
		 * use references to directly modify the corresponding attribute of the driver
		 * Example:
		 */
		uint32_t& m_ui32ConnectionID;
		//::GtkListStore* m_pListStore;
	};
};


#endif // __OpenViBE_AcquisitionServer_CConfigurationMBTSmarting_H__
