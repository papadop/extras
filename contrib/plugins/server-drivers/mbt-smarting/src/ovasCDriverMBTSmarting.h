#ifndef __OpenViBE_AcquisitionServer_CDriverMBTSmarting_H__
#define __OpenViBE_AcquisitionServer_CDriverMBTSmarting_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include "ovasCSmartingAmp.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverMBTSmarting
	 * \author mBrainTrain dev team (mBrainTrain)
	 * \date Tue Oct 14 16:09:43 2014
	 * \brief The CDriverMBTSmarting allows the acquisition server to acquire data from a MBTSmarting device.
	 *
	 * TODO: details
	 *
	 * \sa CConfigurationMBTSmarting
	 */
	class CDriverMBTSmarting : public IDriver
	{
	public:

		CDriverMBTSmarting(IDriverContext& rDriverContext);
		virtual ~CDriverMBTSmarting();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
#ifdef TARGET_OS_Windows
			return false;
#elif defined TARGET_OS_Linux
		return true;
#endif
		}

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;

		// Replace this generic Header with any specific header you might have written
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;

	private:

		/*
		 * Insert here all specific attributes, such as USB port number or device ID.
		 * Example :
		 */
		uint32_t m_ui32ConnectionID;
		std::shared_ptr<SmartingAmp> m_pSmartingAmp;
		std::vector<unsigned char> m_byteArray;
		int sample_number;
		int latency;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverMBTSmarting_H__
