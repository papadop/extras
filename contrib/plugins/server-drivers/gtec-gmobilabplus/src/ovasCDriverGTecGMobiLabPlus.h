/**
 * The gMobilab driver was contributed
 * by Lucie Daubigney from Supelec Metz
 */

#ifndef __OpenViBE_AcquisitionServer_CDriverGTecGMobiLabPlus_H__
#define __OpenViBE_AcquisitionServer_CDriverGTecGMobiLabPlus_H__

#if defined TARGET_HAS_ThirdPartyGMobiLabPlusAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <string>
#ifdef TARGET_OS_Windows
#include <Windows.h>
#endif

namespace OpenViBEAcquisitionServer
{
	class CDriverGTecGMobiLabPlusPrivate; // fwd declare

	/**
	 * \class CDriverGTecGMobiLabPlus
	 * \author Lucie Daubigney (Supelec Metz)
	 */
	class CDriverGTecGMobiLabPlus : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverGTecGMobiLabPlus(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual ~CDriverGTecGMobiLabPlus();
		virtual void release();
		virtual const char* getName();

		virtual bool isFlagSet(
			const OpenViBEAcquisitionServer::EDriverFlag eFlag) const
		{
			return eFlag==DriverFlag_IsUnstable;
		}

		//configuration
		virtual bool isConfigurable();
		virtual bool configure();
		//initialisation
		virtual bool initialize(const uint32_t ui32SampleCountPerChannel, OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader();

		//acquisition
		virtual bool start();
		virtual bool stop();
		virtual bool loop();

	protected:

		SettingsHelper m_oSettings;

		//usefull data to communicate with OpenViBE
		OpenViBEAcquisitionServer::IHeader* m_pHeader;
		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		uint32_t m_ui32SampleCountPerSentBlock;//number of sample you want to send in a row
		float* m_pSample;//array containing the data to sent to OpenViBE once they had been recovered from the gTec module

		//params
		std::string m_oPortName;
		bool m_bTestMode;
				
		// Pointers do gtec-specific data and function pointers
		OpenViBEAcquisitionServer::CDriverGTecGMobiLabPlusPrivate* m_pGtec;
		
		// Register the function pointers from the dll. (The dll approach
		// is used with gMobilab to avoid conflicts with the gUSBAmp lib)
		bool registerLibraryFunctions();
		
#if defined(TARGET_OS_Windows)
		HINSTANCE m_pLibrary;
#elif defined(TARGET_OS_Linux)
		void* m_pLibrary;
#endif
	private:

		void allowAnalogInputs(uint32_t ui32ChannelIndex);
	};
};

#endif // TARGET_HAS_ThirdPartyGMobiLabPlusAPI

#endif // __OpenViBE_AcquisitionServer_CDriverGTecGMobiLabPlus_H__
