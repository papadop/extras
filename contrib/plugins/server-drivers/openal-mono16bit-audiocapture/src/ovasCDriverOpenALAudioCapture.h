#ifndef __OpenViBE_AcquisitionServer_CDriverOpenALAudioCapture_H__
#define __OpenViBE_AcquisitionServer_CDriverOpenALAudioCapture_H__

#if defined TARGET_HAS_ThirdPartyOpenAL

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows
#include <al.h>
#include <alc.h>
#elif defined TARGET_OS_Linux
	#include <AL/al.h>
	#include <AL/alc.h>
#else
#endif

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverOpenALAudioCapture
	 * \author Aurelien Van Langhenhove (CIC-IT Garches)
	 * \date Mon May 16 16:55:49 2011
	 * \erief The CDriverOpenALAudioCapture allows the acquisition server to acquire data from a OpenAL audio capture device.
	 *
	 * TODO: details
	 *
	 * \sa CConfigurationOpenALAudioCapture
	 */
	class CDriverOpenALAudioCapture : public IDriver
	{
	public:

		CDriverOpenALAudioCapture(IDriverContext& rDriverContext);
		virtual ~CDriverOpenALAudioCapture();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			return eFlag == DriverFlag_IsUnstable;
		}

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;

		// Replace this generic Header with any specific header you might have written
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;
		ALshort* Samples;

	private:

		/*
		 * Insert here all specific attributes, such as USB port number or device ID.
		 * Example :
		 */
		// uint32_t m_ui32ConnectionID;
		ALCdevice* Device;
		ALCcontext* Context;
		ALCdevice* CaptureDevice;
	};
};

#endif //TARGET_HAS_ThirdPartyOpenAL
#endif // __OpenViBE_AcquisitionServer_CDriverOpenALAudioCapture_H__
