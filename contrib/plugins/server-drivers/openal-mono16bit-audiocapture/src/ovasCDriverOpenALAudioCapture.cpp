#if defined TARGET_HAS_ThirdPartyOpenAL
#include "ovasCDriverOpenALAudioCapture.h"
#include "ovasCConfigurationOpenALAudioCapture.h"

#include <toolkit/ovtk_all.h>
#include <system/ovCTime.h>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace Kernel;
using namespace std;

//___________________________________________________________________//
//                                                                   //

CDriverOpenALAudioCapture::CDriverOpenALAudioCapture(IDriverContext& rDriverContext)
	: IDriver(rDriverContext)
	  , m_oSettings("AcquisitionServer_Driver_OpenALAudioCapture", m_rDriverContext.getConfigurationManager())
	  , m_pCallback(nullptr)
	  , m_ui32SampleCountPerSentBlock(0)
	  , m_pSample(nullptr)
{
	m_oHeader.setSamplingFrequency(8192);
	m_oHeader.setChannelCount(1);
	Device = nullptr;
	Context = nullptr;
	CaptureDevice = nullptr;

	m_oSettings.add("Header", &m_oHeader);
	m_oSettings.load();
}

CDriverOpenALAudioCapture::~CDriverOpenALAudioCapture() {}

const char* CDriverOpenALAudioCapture::getName()
{
	return "OpenAL audio capture";
}

//___________________________________________________________________//
//                                                                   //

bool CDriverOpenALAudioCapture::initialize(
	const uint32_t ui32SampleCountPerSentBlock,
	IDriverCallback& rCallback)
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::initialize\n";

	if (m_rDriverContext.isConnected()) return false;
	if (!m_oHeader.isChannelCountSet() || !m_oHeader.isSamplingFrequencySet()) return false;

	m_pSample = new float[ui32SampleCountPerSentBlock];
	if (!m_pSample)
	{
		delete [] m_pSample;
		m_pSample = nullptr;
		return false;
	}

	Samples = new ALshort[ui32SampleCountPerSentBlock];
	if (!Samples)
	{
		delete [] Samples;
		Samples = nullptr;
		return false;
	}
	
	// Open default audio device
	Device = alcOpenDevice(nullptr);
	if (!Device)
	{
		m_rDriverContext.getLogManager() << LogLevel_Warning << "Default audio device opening failed.\n";
		return false;
	}
	
	// Create an audio context
	Context = alcCreateContext(Device, nullptr);
	if (!Context)
	{
		m_rDriverContext.getLogManager() << LogLevel_Warning << "Audio context creation failed.\n";
		return false;
	}
	
	// Activate context
	if (!alcMakeContextCurrent(Context))
	{
		m_rDriverContext.getLogManager() << LogLevel_Warning << "Audio context activation failed.\n";
		return false;
	}

	// Verify if audio capture is supported by the computer
	if (alcIsExtensionPresent(Device, "ALC_EXT_CAPTURE") == AL_FALSE)
	{
		m_rDriverContext.getLogManager() << LogLevel_Warning << "Default audio device does not support audio capture.\n";
		return false;
	}

	// Open capture device
	CaptureDevice = alcCaptureOpenDevice(nullptr, (ALCsizei)m_oHeader.getSamplingFrequency(), AL_FORMAT_MONO16, (ALCsizei)m_oHeader.getSamplingFrequency());
	if (!CaptureDevice)
	{
		m_rDriverContext.getLogManager() << LogLevel_Warning << "Default capture device opening failed.\n";
		return false;
	}

	// Saves parameters
	m_pCallback = &rCallback;
	m_ui32SampleCountPerSentBlock = ui32SampleCountPerSentBlock;
	return true;
}

bool CDriverOpenALAudioCapture::start()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::start\n";

	if (!m_rDriverContext.isConnected()) return false;
	if (m_rDriverContext.isStarted()) return false;

	// ...
	// request hardware to start
	// sending data
	// ...
	alcCaptureStart(CaptureDevice);

	return true;
}

bool CDriverOpenALAudioCapture::loop()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::loop\n";

	if (!m_rDriverContext.isConnected()) return false;
	if (!m_rDriverContext.isStarted()) return true;
	
	// Activate context
	if (!alcMakeContextCurrent(Context))
	{
		m_rDriverContext.getLogManager() << LogLevel_Warning << "Audio context activation failed.\n";
	}

	ALCint SamplesAvailable;

	do
	{
		alcGetIntegerv(CaptureDevice, ALC_CAPTURE_SAMPLES, 1, &SamplesAvailable);
		if ((uint32_t)SamplesAvailable >= m_ui32SampleCountPerSentBlock)
		{
			alcCaptureSamples(CaptureDevice, &Samples[0], (ALCsizei)m_ui32SampleCountPerSentBlock);
			for (uint32_t i = 0; i < m_ui32SampleCountPerSentBlock; i++)
			{
				m_pSample[i] = (float)Samples[i];
			}
			m_pCallback->setSamples(m_pSample);
			m_rDriverContext.correctDriftSampleCount(m_rDriverContext.getSuggestedDriftCorrectionSampleCount());
		}
	} while ((uint32_t)SamplesAvailable >= m_ui32SampleCountPerSentBlock);

	return true;
}

bool CDriverOpenALAudioCapture::stop()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::start\n";

	if (!m_rDriverContext.isConnected()) return false;
	if (!m_rDriverContext.isStarted()) return false;

	alcCaptureStop(CaptureDevice);

	return true;
}

bool CDriverOpenALAudioCapture::uninitialize()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::start\n";

	if (!m_rDriverContext.isConnected()) return false;
	if (m_rDriverContext.isStarted()) return false;
	
	// Close capture device
	alcCaptureCloseDevice(CaptureDevice);

	// Context desactivation
	alcMakeContextCurrent(nullptr);

	// Context destruction
	alcDestroyContext(Context);

	// Close device
	alcCloseDevice(Device);

	delete [] m_pSample;
	m_pSample = nullptr;

	delete [] Samples;
	Samples = nullptr;

	m_pCallback = nullptr;

	return true;
}

//___________________________________________________________________//
//                                                                   //
bool CDriverOpenALAudioCapture::isConfigurable()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::isConfigurable\n";

	return true;
}

bool CDriverOpenALAudioCapture::configure()
{
	m_rDriverContext.getLogManager() << LogLevel_Debug << "CDriverOpenALAudioCapture::start\n";
	
	// Change this line if you need to specify some references to your driver attribute that need configuration, e.g. the connection ID.
	CConfigurationOpenALAudioCapture m_oConfiguration(m_rDriverContext,
													  Directories::getDataDir() + "/applications/acquisition-server/interface-OpenALAudioCapture.ui");

	if (!m_oConfiguration.configure(m_oHeader))
	{
		return false;
	}

	m_oSettings.save();

	return true;
}
#endif //TARGET_HAS_ThirdPartyOpenAL
