/*
 * \author Christoph Veigl, Yann Renard
 *
 * \copyright AGPL3
 *
 */
#ifndef __OpenViBE_AcquisitionServer_CConfigurationOpenEEGModularEEG_H__
#define __OpenViBE_AcquisitionServer_CConfigurationOpenEEGModularEEG_H__

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	class CConfigurationOpenEEGModularEEG : public CConfigurationBuilder
	{
	public:
		CConfigurationOpenEEGModularEEG(const char* sGtkBuilderFileName, uint32_t& rUSBIndex);
		virtual ~CConfigurationOpenEEGModularEEG();

		bool preConfigure() override;
		bool postConfigure() override;

	protected:
		uint32_t& m_rUSBIndex;
		GtkListStore* m_pListStore;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationOpenEEGModularEEG_H__
