/*
 * \author Christoph Veigl, Yann Renard
 *
 * \copyright AGPL3
 */
#ifndef __OpenViBE_AcquisitionServer_CDriverOpenEEGModularEEG_H__
#define __OpenViBE_AcquisitionServer_CDriverOpenEEGModularEEG_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows
typedef void* FD_TYPE;
#elif defined TARGET_OS_Linux
 typedef int32_t FD_TYPE;
#else
#endif

#include <vector>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverOpenEEGModularEEG
	 * \author Christoph Veigl, Yann Renard
	 */
	class CDriverOpenEEGModularEEG : public IDriver
	{
	public:

		CDriverOpenEEGModularEEG(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		// void logPacket();
		bool parseByteP2(uint8_t ui8Actbyte);

		bool initTTY(FD_TYPE* pFileDescriptor, uint32_t ui32TtyNumber);
		int32_t readPacketFromTTY(FD_TYPE i32FileDescriptor);
		void closeTTY(FD_TYPE i32FileDescriptor);

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32ChannelCount;
		uint32_t m_ui32DeviceIdentifier;
		float* m_pSample;

		FD_TYPE m_i32FileDescriptor;
		uint16_t m_ui16Readstate;
		uint16_t m_ui16ExtractPosition;
		uint8_t m_ui8PacketNumber;
		uint8_t m_ui8LastPacketNumber;
		uint16_t m_ui16Switches;

		std::vector<std::vector<int32_t>> m_vChannelBuffer;
		std::vector<int32_t> m_vChannelBuffer2;

		OpenViBE::CString m_sTTYName;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverOpenEEGModularEEG_H__
