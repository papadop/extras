#include "ovasCConfigurationEncephalan.h"
#include <gtk/gtk.h>

using namespace OpenViBE;
using namespace Kernel;
using namespace OpenViBEAcquisitionServer;
using namespace std;


//---------------------------------------------------------------------------------------------------
CConfigurationEncephalan::CConfigurationEncephalan(IDriverContext& driverContext, const char* gtkFileName, uint32_t& connectionPort, char* connectionIp)
	: CConfigurationBuilder(gtkFileName), m_driverContext(driverContext),
	m_connectionPort(connectionPort), m_connectionIp(connectionIp) { }
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CConfigurationEncephalan::preConfigure()
{
	if (!CConfigurationBuilder::preConfigure()) { return false; }

	GtkSpinButton* l_pConnectionPort = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_port"));
	gtk_spin_button_set_value(l_pConnectionPort, m_connectionPort);
	GtkEntry* l_pConnectionIp = GTK_ENTRY(gtk_builder_get_object(m_pBuilderConfigureInterface, "entry_ip"));
	gtk_entry_set_text(l_pConnectionIp, m_connectionIp);

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CConfigurationEncephalan::postConfigure()
{
	if (m_bApplyConfiguration)
	{
		GtkSpinButton* l_pConnectionPort = GTK_SPIN_BUTTON(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_port"));
		m_connectionPort = uint32_t(::gtk_spin_button_get_value_as_int(l_pConnectionPort));
		GtkEntry* l_pConnectionIp = GTK_ENTRY(gtk_builder_get_object(m_pBuilderConfigureInterface, "entry_ip"));
		m_connectionIp = const_cast<char*>(gtk_entry_get_text(l_pConnectionIp));
	}

	return CConfigurationBuilder::postConfigure();
}
//---------------------------------------------------------------------------------------------------
