#ifndef __OpenViBE_AcquisitionServer_CDrivergNautilusInterface_H__
#define __OpenViBE_AcquisitionServer_CDrivergNautilusInterface_H__

#if defined(TARGET_HAS_ThirdPartyGNEEDaccessAPI)

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <GDSClientAPI.h>
#include <GDSClientAPI_gNautilus.h>
#include <string>
#include <algorithm>
#include <Windows.h>

using namespace std;

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDrivergNautilusInterface
	 * \author g.tec medical engineering GmbH (g.tec medical engineering GmbH)
	 * \date Wed Aug 12 16:37:18 2015
	 * \brief The CDrivergNautilusInterface allows the acquisition server to acquire data from a g.NEEDaccess device.
	 *
	 * TODO: details
	 *
	 * \sa CConfigurationgNautilusInterface
	 */
	class CDrivergNautilusInterface : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDrivergNautilusInterface(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual ~CDrivergNautilusInterface();
		virtual const char* getName();

		virtual bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }
		
		virtual bool isFlagSet(
			const OpenViBEAcquisitionServer::EDriverFlag eFlag) const
		{
			return eFlag==DriverFlag_IsUnstable;
		}

	protected:
		
		SettingsHelper m_oSettings;
		
		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;

		// Replace this generic Header with any specific header you might have written
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;
		uint32_t m_ui32DeviceIndex;
		uint32_t m_ui32ActualDeviceIndex;
		uint32_t m_ui32BufferSize;
		size_t m_ui32AvailableScans;
		float* m_pBuffer;

		int32_t m_i32NotchFilterIndex;
		int32_t m_i32BandPassFilterIndex;

		double m_f64Sensitivity;
		int32_t m_i32InputSource;
		uint32_t m_ui32NetworkChannel;
		bool m_bDigitalInputEnabled;
		bool m_bNoiseReductionEnabled;
		bool m_bCAREnabled;
		bool m_bAccelerationDataEnabled;
		bool m_bCounterEnabled;
		bool m_bLinkQualityEnabled;
		bool m_bBatteryLevelEnabled;
		bool m_bValidationIndicatorEnabled;
		vector<uint16_t> m_vSelectedChannels;
		vector<int32_t> m_vBipolarChannels;
		vector<bool> m_vNoiseReduction;
		vector<bool> m_vCAR;
		uint32_t m_ui32AcquiredChannelCount;

		GDS_HANDLE m_pDevice;
		GDS_RESULT m_oGdsResult;
		GDS_GNAUTILUS_CONFIGURATION m_oNautilusDeviceCfg;
		std::string m_sDeviceSerial;
		uint32_t m_ui32DeviceCount;

	private:

		/*
		 * Insert here all specific attributes, such as USB port number or device ID.
		 * Example :
		 */
		// uint32_t m_ui32ConnectionID;
	};
};

#endif // TARGET_HAS_ThirdPartyGNEEDaccessAPI

#endif // __OpenViBE_AcquisitionServer_CDrivergNautilusInterface_H__
