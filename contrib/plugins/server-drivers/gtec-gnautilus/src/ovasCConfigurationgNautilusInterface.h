#ifndef __OpenViBE_AcquisitionServer_CConfigurationgNautilusInterface_H__
#define __OpenViBE_AcquisitionServer_CConfigurationgNautilusInterface_H__

#if defined(TARGET_HAS_ThirdPartyGNEEDaccessAPI)

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

#include <gtk/gtk.h>
#include <GDSClientAPI.h>
#include <GDSClientAPI_gNautilus.h>

#include <sstream>
#include <algorithm>

using namespace std;

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CConfigurationgNautilusInterface
	 * \author g.tec medical engineering GmbH (g.tec medical engineering GmbH)
	 * \date Wed Aug 12 16:37:18 2015
	 * \brief The CConfigurationgNautilusInterface handles the configuration dialog specific to the g.NEEDaccess device.
	 *
	 * TODO: details
	 *
	 * \sa CDrivergNautilusInterface
	 */
	class CConfigurationgNautilusInterface : public OpenViBEAcquisitionServer::CConfigurationBuilder
	{
	public:

		// you may have to add to your constructor some reference parameters
// for example, a connection ID:
		//CConfigurationgNautilusInterface(OpenViBEAcquisitionServer::IDriverContext& rDriverContext, const char* sGtkBuilderFileName, uint32_t& rConnectionId);
		CConfigurationgNautilusInterface(OpenViBEAcquisitionServer::IDriverContext& rDriverContext,
										 const char* sGtkBuilderFileName,
										 string& rDeviceSerial,
										 int32_t& rInputSource,
										 uint32_t& rNetworkChannel,
										 int32_t& rBandpassFilterIndex,
										 int32_t& rNotchFilterIndex,
										 double& rSensitivity,
										 bool& rDigitalInputEnabled,
										 bool& rNoiseReductionEnabled,
										 bool& rCAREnabled,
										 bool& rAccelerationDataEnabled,
										 bool& rCounterEnabled,
										 bool& rLinkQualityEnabled,
										 bool& rBatteryLevelEnabled,
										 bool& rValidationIndicatorEnabled,
										 vector<uint16_t>& rSelectedChannels,
										 vector<int32_t>& rBipolarChannels,
										 vector<bool>& rCAR,
										 vector<bool>& rNoiseReduction
										 );

		virtual bool preConfigure();
		virtual bool postConfigure();
		
		//button callback functions
		void buttonChannelSettingsPressedCB();
		void buttonChannelSettingsApplyPressedCB();
		void buttonSensitivityFiltersPressedCB();
		void buttonSensitivityFiltersApplyPressedCB();
		bool getHardwareSettings();
		bool getChannelNames();
		bool getAvailableChannels();
		bool getFiltersForNewSamplingRate();
		void comboboxSampleRateChangedCB();
		void checkbuttonNoiseReductionChangedCB();
		void checkbuttonCARChangedCB();

	protected:

		OpenViBEAcquisitionServer::IDriverContext& m_rDriverContext;
		string& m_rDeviceSerial;
		int32_t& m_rInputSource;
		uint32_t& m_rNetworkChannel;
		int32_t& m_rBandpassFilterIndex;
		int32_t& m_rNotchFilterIndex;
		double& m_rSensitivity;
		bool& m_rDigitalInputEnabled;
		bool& m_rNoiseReductionEnabled;
		bool& m_rCAREnabled;
		bool& m_rAccelerationDataEnabled;
		bool& m_rCounterEnabled;
		bool& m_rLinkQualityEnabled;
		bool& m_rBatteryLevelEnabled;
		bool& m_rValidationIndicatorEnabled;
		vector<uint16_t>& m_vSelectedChannels;
		vector<int32_t>& m_vBipolarChannels;
		vector<bool>& m_vCAR;
		vector<bool>& m_vNoiseReduction;
		vector<int32_t> m_vComboBoxBandpassFilterIndex;
		vector<int32_t> m_vComboBoxNotchFilterIndex;
		vector<double> m_vComboBoxSensitivityValues;
		vector<int32_t> m_vComboBoxInputSources;
		vector<uint32_t> m_vComboBoxNetworkChannels;

	private:

		/*
		 * Insert here all specific attributes, such as a connection ID.
		 * use references to directly modify the corresponding attribute of the driver
		 * Example:
		 */
		// uint32_t& m_ui32ConnectionID;
		bool openDevice();
		bool closeDevice();
		GDS_HANDLE m_ui64DeviceHandle;
		GDS_RESULT m_oGdsResult;
		vector<bool> m_vAvailableChannels;
		char (*m_sDeviceNames)[DEVICE_NAME_LENGTH_MAX];
		bool m_bConnectionOpen;
	};
};

#endif // TARGET_HAS_ThirdPartyGNEEDaccessAPI

#endif // __OpenViBE_AcquisitionServer_CConfigurationgNautilusInterface_H__
