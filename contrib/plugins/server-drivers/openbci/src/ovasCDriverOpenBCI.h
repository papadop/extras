/*
 * OpenBCI driver for OpenViBE
 *
 * \author Jeremy Frey
 * \author Yann Renard
 *
 */
#ifndef __OpenViBE_AcquisitionServer_CDriverOpenBCI_H__
#define __OpenViBE_AcquisitionServer_CDriverOpenBCI_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#if defined TARGET_OS_Windows
typedef void* FD_TYPE;
#elif defined TARGET_OS_Linux
 typedef int32_t FD_TYPE;
#else
#endif

#include <vector>
#include <deque>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverOpenBCI
	 * \author Jeremy Frey
	 * \author Yann Renard
	 */
	class CDriverOpenBCI : public IDriver
	{
	public:

		CDriverOpenBCI(IDriverContext& rDriverContext);
		virtual void release();
		const char* getName() override;

		bool initialize(const uint32_t ui32SampleCountPerSentBlock, IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

	public:

		typedef enum
		{
			ParserAutomaton_Default,
			ParserAutomaton_StartByteReceived,
			ParserAutomaton_SampleNumberReceived,
			ParserAutomaton_EEGSamplesReceived,
			ParserAutomaton_AccelerometerSamplesReceived,
		} EParserAutomaton;

	protected:

		int32_t interpret24bitAsInt32(const std::vector<uint8_t>& byteBuffer);
		int32_t interpret16bitAsInt32(const std::vector<uint8_t>& byteBuffer);
		int16_t parseByte(uint8_t ui8Actbyte);

		OpenViBE::boolean sendCommand(FD_TYPE i32FileDescriptor, const char* sCommand, bool bWaitForResponse, bool bLogResponse, uint32_t ui32Timeout, std::string& sReply);
		bool resetBoard(FD_TYPE i32FileDescriptor, bool bRegularInitialization);
		bool handleCurrentSample(int32_t packetNumber); // will take car of samples fetch from OpenBCI board, dropping/merging packets if necessary
		void updateDaisy(bool bQuietLogging); // update internal state regarding daisy module

	protected:

		bool openDevice(FD_TYPE* pFileDescriptor, uint32_t ui32TtyNumber);
		void closeDevice(FD_TYPE i32FileDescriptor);
		uint32_t writeToDevice(FD_TYPE i32FileDescriptor, const void* pBuffer, const uint32_t ui32BufferSize);
		uint32_t readFromDevice(FD_TYPE i32FileDescriptor, void* pBuffer, const uint32_t ui32BufferSize, const uint64_t ui64TimeOut = 0);

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		FD_TYPE m_i32FileDescriptor;

		OpenViBE::CString m_sDriverName;
		OpenViBE::CString m_sTTYName;
		OpenViBE::CString m_sAdditionalCommands; // string to send possibly upon initialisation
		uint32_t m_ui32ChannelCount;
		uint32_t m_ui32DeviceIdentifier;
		uint32_t m_ui32ReadBoardReplyTimeout; // parameter com init string
		uint32_t m_ui32FlushBoardReplyTimeout;  // parameter com init string
		bool m_bDaisyModule; // daisy module attached or not

		// OpenBCI protocol related
		int16_t m_i16SampleNumber; // returned by the board
		uint32_t m_ui32ReadState; // position in the sample (see doc)
		uint8_t m_ui8SampleBufferPosition;// position in the buffer
		std::vector<uint8_t> m_vEEGValueBuffer; // buffer for one EEG value (int24)
		std::vector<uint8_t> m_vAccValueBuffer; // buffer for one accelerometer value (int16_t)
		const static uint8_t EEGValueBufferSize = 3; // int24 == 3 bytes
		const static uint8_t AccValueBufferSize = 2; // int16_t == 2 bytes
		const static uint8_t EEGValueCountPerSample = 8; // the board send EEG values 8 by 8 (will concatenate 2 samples with daisy module)
		const static uint8_t AccValueCountPerSample = 3; // 3 accelerometer data per sample

		uint32_t MissingSampleDelayBeforeReset; // in ms - value acquired from configuration manager
		uint32_t DroppedSampleCountBeforeReset; // in samples - value acquired from configuration manager
		uint32_t DroppedSampleSafetyDelayBeforeReset; // in ms - value acquired from configuration manager

		std::deque<uint32_t> m_vDroppedSampleTime;

		float m_f32UnitsToMicroVolts; // convert from int32_t to microvolt
		float m_f32UnitsToRadians; // converts from int16_t to radians
		uint32_t m_ui32ExtractPosition; // used to situate sample reading both with EEG and accelerometer data
		uint32_t m_ui32ValidAccelerometerCount;
		int32_t m_i32LastPacketNumber; // used to detect consecutive packets when daisy module is used

		// buffer for multibyte reading over serial connection
		std::vector<uint8_t> m_vReadBuffer;
		// buffer to store sample coming from OpenBCI -- filled by parseByte(), passed to handleCurrentSample()
		std::vector<float> m_vSampleEEGBuffer;
		std::vector<float> m_vSampleEEGBufferDaisy;
		std::vector<float> m_vSampleAccBuffer;
		std::vector<float> m_vSampleAccBufferTemp;

		// buffer to store aggregated samples
		std::vector<std::vector<float>> m_vChannelBuffer; // buffer to store channels & chunks
		std::vector<float> m_vSampleBuffer;

		bool m_bSeenPacketFooter; // extra precaution to sync packets

		// mechanism to call resetBoard() if no data are received
		uint32_t m_ui32Tick; // last tick for polling
		uint32_t m_ui32StartTime; // actual time since connection

		// sample storing for device callback and serial i/o
		std::vector<float> m_vCallbackSample;

	private:

		typedef struct
		{
			uint32_t ui32DeviceVersion;
			uint32_t ui32DeviceChannelCount;

			uint32_t ui32BoardId;
			uint32_t ui32DaisyId;
			uint32_t ui32MocapId;

			char sBoardChipset[64];
			char sDaisyChipset[64];
			char sMocapChipset[64];
		} SDeviceInformation;

		SDeviceInformation m_oDeviceInformation;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverOpenBCI_H__
