/*
 * OpenBCI driver for OpenViBE
 *
 * \author Jeremy Frey
 * \author Yann Renard
 */
#ifndef __OpenViBE_AcquisitionServer_CConfigurationOpenBCI_H__
#define __OpenViBE_AcquisitionServer_CConfigurationOpenBCI_H__

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>
#include <map>

namespace OpenViBEAcquisitionServer
{
	class CConfigurationOpenBCI : public CConfigurationBuilder
	{
	public:

		const static uint16_t DefaultSamplingRate = 250; // sampling rate with no daisy module (divided by 2 with daisy module)
		const static uint16_t DefaultEEGChannelCount = 8; // number of EEG channels with no daisy module (multiplied by 2 with daisy module)
		const static uint16_t DefaultAccChannelCount = 3; // number of Acc channels (daisy module does not have an impact)

		typedef enum
		{
			DaisyStatus_Active,
			DaisyStatus_Inactive,
		} EDaisyStatus;

		typedef struct
		{
			int iEEGChannelCount;
			int iAccChannelCount;
			int iSamplingRate;
		} SDaisyInformation;

		static uint32_t getMaximumTTYCount();
		static OpenViBE::CString getTTYFileName(const uint32_t ui32TTYNumber);
		static bool isTTYFile(const OpenViBE::CString& sTTYFileName);

		CConfigurationOpenBCI(const char* sGtkBuilderFileName, uint32_t& rUSBIndex);
		virtual ~CConfigurationOpenBCI();

		bool preConfigure() override;
		bool postConfigure() override;

		bool setAdditionalCommands(const OpenViBE::CString& sAdditionalCommands);
		OpenViBE::CString getAdditionalCommands() const;

		bool setReadBoardReplyTimeout(const uint32_t iReadBoardReplyTimeout);
		uint32_t getReadBoardReplyTimeout() const;

		bool setFlushBoardReplyTimeout(const uint32_t iFlushBoardReplyTimeout);
		uint32_t getFlushBoardReplyTimeout() const;

		bool setDaisyModule(const bool sDaisyModule);
		bool getDaisyModule() const;

		void checkbuttonDaisyModuleCB(EDaisyStatus eStatus);

		static SDaisyInformation getDaisyInformation(EDaisyStatus eStatus);

	protected:

		std::map<int, int> m_vComboSlotIndexToSerialPort;
		uint32_t& m_rUSBIndex;
		GtkListStore* m_pListStore;
		GtkEntry* l_pEntryComInit;
		OpenViBE::CString m_sAdditionalCommands;
		uint32_t m_iReadBoardReplyTimeout;
		uint32_t m_iFlushBoardReplyTimeout;
		bool m_bDaisyModule;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationOpenBCI_H__
