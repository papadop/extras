#if defined(TARGET_HAS_ThirdPartyEEGOAPI)

#ifndef __OpenViBE_AcquisitionServer_CDriverEEGO_H__
#define __OpenViBE_AcquisitionServer_CDriverEEGO_H__

#include <memory>

#include "ovasIDriver.h"
#include "ovasCHeaderEEGO.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

// forward declarations
namespace eemagine
{
	namespace sdk
	{
		class amplifier;
		class stream;
		class factory;
	}
}

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverEEGO
	 * \author Steffen Heimes (eemagine GmbH)
	 * \date Mon Oct 20 14:40:33 2014
	 * \brief The CDriverEEGO allows the acquisition server to acquire data from an EEGO device.
	 *
	 * \sa CConfigurationEEGO
	 */
	class CDriverEEGO : public IDriver
	{
	public:

		CDriverEEGO(IDriverContext& rDriverContext);
		virtual ~CDriverEEGO();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(const EDriverFlag eFlag) const override
		{
			return false; // The only currently used flag is for checking for unstability. eego is stable now.
		}

	private:


		bool loop_wrapped();

		/**
		 * Check if the configuration makes sense and tries to fix it, informing the user.
		 */
		bool check_configuration();
		uint64_t getRefChannelMask() const;
		uint64_t getBipChannelMask() const;
		eemagine::sdk::factory& factory();

	protected:

		SettingsHelper m_oSettings;
		IDriverCallback* m_pCallback;
		CHeaderEEGO m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		std::unique_ptr<float[]> m_pSample;

		std::unique_ptr<eemagine::sdk::factory> m_pFactory;
		std::unique_ptr<eemagine::sdk::amplifier> m_pAmplifier;
		std::unique_ptr<eemagine::sdk::stream> m_pStream;

	private:

		uint32_t m_ui32SamplesInBuffer;
		uint32_t m_i32TriggerChannel;
		OpenViBE::CStimulationSet m_oStimulationSet; // Storing the samples over time

		// To detect flanks in the trigger signal. The last state on the trigger input.
		uint32_t m_ui32LastTriggerValue;

		// For setting store/load
		uint32_t m_iBIPRange; // [mV]
		uint32_t m_iEEGRange; // [mV]
		OpenViBE::CString m_sEEGMask; // String interpreted as value to be interpreted as bitfield
		OpenViBE::CString m_sBIPMask; // String interpreted as value to be interpreted as bitfield
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverEEGO_H__

#endif
