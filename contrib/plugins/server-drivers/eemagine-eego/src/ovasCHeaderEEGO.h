#ifndef __OpenViBE_AcquisitionServer_CHeaderEEGO_H__
#define __OpenViBE_AcquisitionServer_CHeaderEEGO_H__

#if defined TARGET_HAS_ThirdPartyEEGOAPI

#include "../ovasCHeader.h"

namespace OpenViBEAcquisitionServer
{
	class CHeaderEEGO : public CHeader
	{
	public:

		CHeaderEEGO();

		// EEG, referential channels
		// range
		uint32_t getEEGRange() const;

		void setEEGRange(uint32_t range);
		bool isEEGRangeSet() const;

		// mask
		OpenViBE::CString getEEGMask() const;
		uint64_t getEEGMaskInt() const; // Same as method above. Only string parsing has been done
		void setEEGMask(const OpenViBE::CString mask);
		bool isEEGMaskSet() const;

		// Bipolar channels
		// range
		uint32_t getBIPRange() const;
		uint64_t getBIPMaskInt() const; // Same as method above. Only string parsing has been done
		void setBIPRange(uint32_t range);
		bool isBIPRangeSet() const;

		// mask
		OpenViBE::CString getBIPMask() const;
		void setBIPMask(const OpenViBE::CString mask);
		bool isBIPMaskSet() const;

	public:
		// Converts a string representing a number to this number as unsigned 64 bit value.
		// Accepts 0x, 0b and 0 notation for hexadecimal, binary and octal notation.
		// Otherwise it is interpreted as decimal.
		// Returns true if the conversion was successfull, false on error.
		// Please note that the error checking goes beyond the parsing  strtoull etc.:
		// The strto* methods stop parsing at the first character which could not be interpreted.
		// Here the string is checked against all invalid chars and an error will be returned.
		static bool convertMask(char const* str, uint64_t& r_oOutValue);

		// data
	protected:
		uint32_t m_iEEGRange;
		uint32_t m_iBIPRange;
		OpenViBE::CString m_sEEGMask;
		OpenViBE::CString m_sBIPMask;
		bool m_bEEGRangeSet;
		bool m_bBIPRangeSet;
		bool m_bEEGMaskSet;
		bool m_bBIPMaskSet;
	};
}

#endif

#endif // Header Guard
