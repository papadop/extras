#ifndef __OpenViBE_AcquisitionServer_CConfigurationGTecGUSBampLegacy_H__
#define __OpenViBE_AcquisitionServer_CConfigurationGTecGUSBampLegacy_H__

#if defined TARGET_HAS_ThirdPartyGUSBampCAPI

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>

namespace OpenViBEAcquisitionServer
{
	class CConfigurationGTecGUSBampLegacy : public OpenViBEAcquisitionServer::CConfigurationBuilder
	{
	public:
		CConfigurationGTecGUSBampLegacy(const char* sGtkBuilderFileName,uint32_t& rUSBIndex,uint8_t& rCommonGndAndRefBitmap, OpenViBE::int32& rNotchFilterIndex, int32_t& rBandPassFilterIndex,bool& rTriggerInput);

		virtual bool preConfigure();
		virtual bool postConfigure();

		void buttonCalibratePressedCB();
		void idleCalibrateCB();

		void buttonCommonGndRefPressedCB();
		void buttonFiltersPressedCB();

	protected:
		uint32_t& m_rUSBIndex;
		uint8_t& m_rCommonGndAndRefBitmap;

		int32_t& m_rNotchFilterIndex;
		int32_t& m_rBandPassFilterIndex;
		bool& m_rTriggerInput;
		
	private:
		::GtkWidget* m_pCalibrateDialog;
		bool m_bCalibrationDone;
	};
};

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI

#endif // __OpenViBE_AcquisitionServer_CConfigurationGTecGUSBampLegacy_H__
