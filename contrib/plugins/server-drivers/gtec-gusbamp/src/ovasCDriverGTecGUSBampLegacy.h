#ifndef __OpenViBE_AcquisitionServer_CDriverGTecGUSBampLegacy_H__
#define __OpenViBE_AcquisitionServer_CDriverGTecGUSBampLegacy_H__

#if defined TARGET_HAS_ThirdPartyGUSBampCAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <gtk/gtk.h>
#include <vector>

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverGTecGUSBampLegacy
	 * \author Yann Renard (Inria)
	 * \date unknown
	 * \brief GTEC driver 
	 *
	 */
	class CDriverGTecGUSBampLegacy : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverGTecGUSBampLegacy(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual void release();
		virtual const char* getName();

		virtual bool initialize(
		const uint32_t ui32SampleCountPerSentBlock,
		OpenViBEAcquisitionServer::IDriverCallback& rCallback);
		virtual bool uninitialize();

		virtual bool start();
		virtual bool stop();
		virtual bool loop();

		virtual bool isConfigurable();
		virtual bool configure();
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader() { return &m_oHeader; }
	
		virtual bool isFlagSet(
			const OpenViBEAcquisitionServer::EDriverFlag eFlag) const
		{
			return eFlag==DriverFlag_IsDeprecated;
		}

	protected:

		SettingsHelper m_oSettings;

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		uint32_t m_ui32DeviceIndex;
		uint32_t m_ui32ActualDeviceIndex;
		uint32_t m_ui32BufferSize;
		uint8_t* m_pBuffer;
		float* m_pSampleTranspose;
		float* m_pSample;
		void* m_pDevice;
		void* m_pEvent;
		void* m_pOverlapped;

		uint32_t m_ui32ActualImpedanceIndex;

		uint8_t m_ui8CommonGndAndRefBitmap;

		int32_t m_i32NotchFilterIndex;
		int32_t m_i32BandPassFilterIndex;

		bool m_bTriggerInputEnabled;
		uint32_t m_ui32LastStimulation;

		// EVENT CHANNEL : contribution Anton Andreev (Gipsa-lab) - 0.14.0
		typedef enum
		{
			STIMULATION_0	= 0,
			STIMULATION_64	= 64,
			STIMULATION_128	= 128,
			STIMULATION_192	= 192
		} gtec_triggers_t;

		uint32_t m_ui32TotalHardwareStimulations; //since start button clicked
		uint32_t m_ui32TotalDriverChunksLost; //since start button clicked
		uint32_t m_ui32AcquiredChannelCount; //number of channels 1..16 specified bu user

	};
};

#endif // TARGET_HAS_ThirdPartyGUSBampCAPI

#endif // __OpenViBE_AcquisitionServer_CDriverGTecGUSBampLegacy_H__
