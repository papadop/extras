#ifndef __OpenViBE_AcquisitionServer_CDriverCognionics_H__
#define __OpenViBE_AcquisitionServer_CDriverCognionics_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverCognionics
	 * \author Mike Chi (Cognionics, Inc.)
	 * \copyright AGPL3
	 * \date Thu Apr 18 21:19:49 2013
	 
	 * \brief The CDriverCognionics allows the acquisition server to acquire data from a Cognionics device.
	 *
	 * TODO: details
	 *
	 * \sa CConfigurationCognionics
	 */
	class CDriverCognionics : public IDriver
	{
	public:

		CDriverCognionics(IDriverContext& rDriverContext);
		virtual ~CDriverCognionics();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			return eFlag == DriverFlag_IsUnstable;
		}

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;

		// Replace this generic Header with any specific header you might have written
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;

	private:

		/*
		 * Insert here all specific attributes, such as USB port number or device ID.
		 * Example :
		 */
		uint32_t m_ui32ComPort;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverCognionics_H__
