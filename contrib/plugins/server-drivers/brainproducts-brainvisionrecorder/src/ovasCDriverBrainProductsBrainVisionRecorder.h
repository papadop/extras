#ifndef __OpenViBE_AcquisitionServer_CDriverBrainProductsBrainVisionRecorder_H__
#define __OpenViBE_AcquisitionServer_CDriverBrainProductsBrainVisionRecorder_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <vector>

#include <socket/IConnectionClient.h>

#ifndef M_DEFINE_GUID
#define M_DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
	GUID name = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }
#endif

#ifndef M_COMPARE_GUID
#define M_COMPARE_GUID(name1, name2) \
		(	name1.Data1 == name2.Data1 && name1.Data2 == name2.Data2 && \
			name1.Data3 == name2.Data3 && name1.Data4[0] == name2.Data4[0] && \
			name1.Data4[1] == name2.Data4[1] && name1.Data4[2] == name2.Data4[2] &&\
			name1.Data4[3] == name2.Data4[3] && name1.Data4[4] == name2.Data4[4] &&\
			name1.Data4[5] == name2.Data4[5] && name1.Data4[6] == name2.Data4[6] &&\
			name1.Data4[7] == name2.Data4[7] \
		)
#endif

namespace OpenViBEAcquisitionServer
{
	class CDriverBrainProductsBrainVisionRecorder : public IDriver
	{
	public:

		CDriverBrainProductsBrainVisionRecorder(IDriverContext& rDriverContext);
		virtual ~CDriverBrainProductsBrainVisionRecorder();
		const char* getName() override;

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;

		const IHeader* getHeader() override { return &m_oHeader; }

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		Socket::IConnectionClient* m_pConnectionClient;
		OpenViBE::CString m_sServerHostName;
		uint32_t m_ui32ServerHostPort;

		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;

		uint32_t m_ui32IndexIn;
		uint32_t m_ui32IndexOut;
		uint32_t m_ui32BuffDataIndex;

		uint32_t m_ui32MarkerCount;
		uint32_t m_ui32NumberOfMarkers;

		std::vector<uint32_t> m_vStimulationIdentifier;
		std::vector<uint64_t> m_vStimulationDate;
		std::vector<uint64_t> m_vStimulationSample;

#pragma pack(push)
#pragma pack(1)

		typedef struct
		{
			uint32_t Data1;
			unsigned short Data2;
			unsigned short Data3;
			uint8_t Data4[8];
		} GUID;

		struct RDA_Marker
			//; A single marker in the marker array of RDA_MessageData
		{
			uint32_t nSize;              // Size of this marker.
			uint32_t nPosition;          // Relative position in the data block.
			uint32_t nPoints;            // Number of points of this marker
			int32_t nChannel;           // Associated channel number (-1 = all channels).
			char sTypeDesc[1];       // Type, description in ASCII delimited by '\0'.
		};

		struct RDA_MessageHeader
			//; Message header
		{
			GUID guid;               // Always GUID_RDAHeader
			uint32_t nSize;              // Size of the message block in bytes including this header
			uint32_t nType;              // Message type.
		};

		// **** Messages sent by the RDA server to the clients. ****
		struct RDA_MessageStart : RDA_MessageHeader
			//; Setup / Start infos, Header -> nType = 1
		{
			uint32_t nChannels;          // Number of channels
			double dSamplingInterval;  // Sampling interval in microseconds
			double dResolutions[1];    // Array of channel resolutions -> double dResolutions[nChannels] coded in microvolts. i.e. RealValue = resolution * A/D value
			char sChannelNames[1];   // Channel names delimited by '\0'. The real size is larger than 1.
		};

		struct RDA_MessageStop : RDA_MessageHeader
			//; Data acquisition has been stopped. // Header -> nType = 3
		{ };

		struct RDA_MessageData32 : RDA_MessageHeader
			//; Block of 32-bit floating point data, Header -> nType = 4, sent only from port 51244
		{
			uint32_t nBlock;             // Block number, i.e. acquired blocks since acquisition started.
			uint32_t nPoints;            // Number of data points in this block
			uint32_t nMarkers;           // Number of markers in this data block
			float fData[1];           // Data array -> float fData[nChannels * nPoints], multiplexed
			RDA_Marker Markers[1];         // Array of markers -> RDA_Marker Markers[nMarkers]
		};
#pragma pack(pop)

		RDA_MessageHeader* m_pStructRDA_MessageHeader;
		char* m_pcharStructRDA_MessageHeader;
		size_t m_uHeaderBufferSize;

		RDA_MessageStart* m_pStructRDA_MessageStart;
		RDA_MessageStop* m_pStructRDA_MessageStop;
		RDA_MessageData32* m_pStructRDA_MessageData32;
		RDA_Marker* m_pStructRDA_Marker;

		std::vector<float> m_vSignalBuffer;

	private:

		bool reallocateHeaderBuffer(size_t newSize);
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverBrainProductsBrainVisionRecorder_H__
