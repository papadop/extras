#ifndef __OpenViBE_AcquisitionServer_CDriverCtfVsmMeg_H__
#define __OpenViBE_AcquisitionServer_CDriverCtfVsmMeg_H__

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <openvibe/ov_all.h>

#include <socket/IConnectionClient.h>

#define NB_CHAN_RECORDED_MAX 410
#define NB_SAMP_ACQ_Packet   28160

namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverCtfVsmMeg
	 * \author Pierre-Emmanuel Aguera (INSERM)
	 */
	class CDriverCtfVsmMeg : public IDriver
	{
	public:

		typedef char str32[32];
		typedef char str100[100];

		CDriverCtfVsmMeg(IDriverContext& rDriverContext);
		virtual ~CDriverCtfVsmMeg();
		const char* getName() override;

		bool isFlagSet(
			const EDriverFlag eFlag) const override
		{
			return eFlag == DriverFlag_IsUnstable;
		}

		bool initialize(
			const uint32_t ui32SampleCountPerSentBlock,
			IDriverCallback& rCallback) override;
		bool uninitialize() override;

		bool start() override;
		bool stop() override;
		bool loop() override;

		bool isConfigurable() override;
		bool configure() override;
		const IHeader* getHeader() override { return &m_oHeader; }

		Socket::IConnectionClient* m_pConnectionClient;
		OpenViBE::CString m_sServerHostName;
		uint32_t m_ui32ServerHostPort;

		struct
		{
			int nbCharExperimentId;
			str100 experimentId;
			int nbCharExperimentDate;
			str32 experimentDate;

			int nbCharSubjectName;
			str32 subjectName;
			int subjectAge;
			char subjectGender[1]; /**F: female or M: Male*/

			int labId;
			str32 labName;
			int technicianId;
			str32 technicianName;

			float samplingRate;
			int numberOfChannels;
			str32 channelLabel[NB_CHAN_RECORDED_MAX];
			int channelTypeIndex[NB_CHAN_RECORDED_MAX];

			float properGain[NB_CHAN_RECORDED_MAX];
			float qGain[NB_CHAN_RECORDED_MAX];
			float ioGain[NB_CHAN_RECORDED_MAX];

			int numberOfCoils[NB_CHAN_RECORDED_MAX];
			int gradOrderNum[NB_CHAN_RECORDED_MAX];
		} m_structHeader;

		char* m_pStructHeader;

		struct
		{
			int sampleNumber;
			int nbSamplesPerChanPerBlock;
			int nbSamplesTotPerBlock;
			signed int data[NB_SAMP_ACQ_Packet];
		} m_structBuffData;

		char* m_pStructBuffData;

	protected:

		SettingsHelper m_oSettings;

		IDriverCallback* m_pCallback;
		CHeader m_oHeader;

		uint32_t m_ui32SampleCountPerSentBlock;
		float* m_pSample;

		uint32_t m_ui32IndexIn;
		uint32_t m_ui32IndexOut;
		uint32_t m_ui32SocketFlag;
		uint32_t m_ui32BuffDataIndex;
	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverCtfVsmMeg_H__
