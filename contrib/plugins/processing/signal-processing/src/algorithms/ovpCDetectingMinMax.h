#ifndef __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CDetectingMinMax_H__
#define __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CDetectingMinMax_H__

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CDetectingMinMax : virtual public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_DetectingMinMax);

		protected:

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pSignalMatrix;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SamplingFrequency;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64TimeWindowStart;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64TimeWindowEnd;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pSignalMatrix;
		};

		class CDetectingMinMaxDesc : virtual public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Detects Min or Max of input buffer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Algorithm/Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_DetectingMinMax; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CDetectingMinMax(); }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmProto) const override
			{
				rAlgorithmProto.addInputParameter(OVP_Algorithm_DetectingMinMax_InputParameterId_SignalMatrix, "Signal input matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_DetectingMinMax_InputParameterId_TimeWindowStart, "Time window start", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_DetectingMinMax_InputParameterId_TimeWindowEnd, "Time window end", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_DetectingMinMax_InputParameterId_SamplingFrequency, "Sampling frequency", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addOutputParameter(OVP_Algorithm_DetectingMinMax_OutputParameterId_SignalMatrix, "Signal output matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_DetectingMinMax_InputTriggerId_Initialize, "Initialize");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_DetectingMinMax_InputTriggerId_DetectsMin, "Detects min");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_DetectingMinMax_InputTriggerId_DetectsMax, "Detects max");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_DetectingMinMaxDesc);
		};
	};
};

#endif // __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CDetectingMinMax_H__
