#ifndef __OpenViBEPlugins_SignalProcessing_Algorithms_Filter_CComputeTemporalFilterCoefficients_H__
#define __OpenViBEPlugins_SignalProcessing_Algorithms_Filter_CComputeTemporalFilterCoefficients_H__

#if defined TARGET_HAS_ThirdPartyITPP

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

#include <itpp/itstat.h>
#include <itpp/itsignal.h>

#define PREC 27
#define MAXEXP 1024
#define MINEXP -1077
#define MAXNUM 1.79769313486231570815E308

typedef struct
{
	double real;
	double imag;
} cmplex;

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CComputeTemporalFilterCoefficients : virtual public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_ComputeTemporalFilterCoefficients);

		public:

			// Functions for Butterworth and Chebychev filters
			void findSPlanePolesAndZeros();
			void convertSPlanePolesAndZerosToZPlane();
			// Functions for Complex arithmetic
			double absComplex(cmplex* z);
			void divComplex(cmplex* a, cmplex* b, cmplex* c);
			void sqrtComplex(cmplex* z, cmplex* w);
			void addComplex(cmplex* a, cmplex* b, cmplex* c);
			void mulComplex(cmplex* a, cmplex* b, cmplex* c);
			void subComplex(cmplex* a, cmplex* b, cmplex* c);

		protected:

			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SamplingFrequency;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64FilterMethod;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64FilterType;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64FilterOrder;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64LowCutFrequency;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64HighCutFrequency;
			OpenViBE::Kernel::TParameterHandler<double> ip_f64BandPassRipple;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pOutputMatrix;

			uint32_t m_ui32FilterOrder;
			uint64_t m_ui64FilterMethod;
			uint64_t m_ui64FilterType;
			double m_LowPassBandEdge;
			double m_HighPassBandEdge;
			double m_PassBandRipple;
			uint32_t m_ui32ArraySize;
			itpp::vec m_vecNumCoefFilter;
			itpp::vec m_vecDenomCoefFilter;
			double m_Phi;
			double m_Scale;
			double m_TanAng;
			double m_CosGam;
			double m_cbp;

			uint32_t m_ui32SamplingRate;
			uint32_t m_ui32NyquistFrequency;

			uint32_t m_ui32NbPoles;
			uint32_t m_ui32NbZeros;

			itpp::vec m_vecZs;
			uint32_t m_ui32zord;

			double m_Rho;
			double m_Eps;

			uint32_t m_ui32DimensionSize;
		};

		class CComputeTemporalFilterCoefficientsDesc : virtual public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Compute Filter Coefficients"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Algorithm/Signal processing/Filter"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_ComputeTemporalFilterCoefficients; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CComputeTemporalFilterCoefficients(); }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmProto) const override
			{
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_SamplingFrequency, "Sampling frequency", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_FilterMethod, "Filter method", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_FilterType, "Filter type", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_FilterOrder, "Filter order", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_LowCutFrequency, "Low cut frequency", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_HighCutFrequency, "High cut frequency", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_BandPassRipple, "Band pass ripple", OpenViBE::Kernel::ParameterType_Float);
				rAlgorithmProto.addOutputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_OutputParameterId_Matrix, "Matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputTriggerId_Initialize, "Initialize");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputTriggerId_ComputeCoefficients, "Compute coefficients");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_ComputeTemporalFilterCoefficientsDesc);
		};
	};
};

#endif // TARGET_HAS_ThirdPartyITPP

#endif // __OpenViBEPlugins_SignalProcessing_Algorithms_Filter_CComputeTemporalFilterCoefficients_H__
