#ifndef __OpenViBEPlugins_SignalProcessing_Algorithms_Filter_CApplyTemporalFilter_H__
#define __OpenViBEPlugins_SignalProcessing_Algorithms_Filter_CApplyTemporalFilter_H__

#if defined TARGET_HAS_ThirdPartyITPP

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

#include <itpp/itstat.h>
#include <itpp/itsignal.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CApplyTemporalFilter : virtual public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_ApplyTemporalFilter);

		protected:

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pSignalMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pFilterCoefficientsMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pSignalMatrix;

		protected:

			itpp::vec m_vecDenomCoefFilter;
			itpp::vec m_vecNumCoefFilter;
			std::vector<itpp::vec> m_oCurrentStates;

			bool m_bFlagInitialize;
		};

		class CApplyTemporalFilterDesc : virtual public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Temporal Filter (INSERM contrib)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Algorithm/Signal processing/Filter"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_ApplyTemporalFilter; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CApplyTemporalFilter(); }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmProto) const override
			{
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ApplyTemporalFilter_InputParameterId_SignalMatrix, "Signal matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_ApplyTemporalFilter_InputParameterId_FilterCoefficientsMatrix, "Filter coefficients matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addOutputParameter(OVP_Algorithm_ApplyTemporalFilter_OutputParameterId_FilteredSignalMatrix, "Filtered signal matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_ApplyTemporalFilter_InputTriggerId_Initialize, "Initialize");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_ApplyTemporalFilter_InputTriggerId_ApplyFilter, "Apply filter");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_ApplyTemporalFilter_InputTriggerId_ApplyFilterWithHistoric, "Apply filter with historic");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_ApplyTemporalFilterDesc);
		};
	};
};

#endif // TARGET_HAS_ThirdPartyITPP

#endif // __OpenViBEPlugins_SignalProcessing_Algorithms_Filter_CApplyTemporalFilter_H__
