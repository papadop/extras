#if defined TARGET_HAS_ThirdPartyITPP

#include "ovpCComputeTemporalFilterCoefficients.h"

#include <limits>
#include <cmath>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace SignalProcessing;
using namespace itpp;
using namespace std;
// ________________________________________________________________________________________________________________
//

// Add 2complexes
void CComputeTemporalFilterCoefficients::addComplex(cmplex* a, cmplex* b, cmplex* c)
{
	c->real = b->real + a->real;
	c->imag = b->imag + a->imag;
}

// Substract 2 complex
void CComputeTemporalFilterCoefficients::subComplex(cmplex* a, cmplex* b, cmplex* c)
{
	c->real = b->real - a->real;
	c->imag = b->imag - a->imag;
}

// Multiply 2 complexes
void CComputeTemporalFilterCoefficients::mulComplex(cmplex* a, cmplex* b, cmplex* c)
{
	double y;
	y = b->real * a->real - b->imag * a->imag;
	c->imag = b->real * a->imag + b->imag * a->real;
	c->real = y;
}

// Divide 2 complex numbers
void CComputeTemporalFilterCoefficients::divComplex(cmplex* a, cmplex* b, cmplex* c)
{
	double y, p, q, w;

	y = a->real * a->real + a->imag * a->imag;
	p = b->real * a->real + b->imag * a->imag;
	q = b->imag * a->real - b->real * a->imag;

	if (y < 1.0)
	{
		w = MAXNUM * y;
		if ((fabs(p) > w) || (fabs(q) > w) || (y == 0.0))
		{
			c->real = MAXNUM;
			c->imag = MAXNUM;
			cout << "divCOMPLEX: OVERFLOW" << std::endl;
			return;
		}
	}
	c->real = p / y;
	c->imag = q / y;
}

// Compute abs of a complex
double CComputeTemporalFilterCoefficients::absComplex(cmplex* z)
{
	double x, y, b, re, im;
	int ex, ey, e;

	re = fabs(z->real);
	im = fabs(z->imag);

	if (re == 0.0)
	{
		return (im);
	}
	if (im == 0.0)
	{
		return (re);
	}

	// Get the exponents of the numbers
	x = frexp(re, &ex);
	y = frexp(im, &ey);

	// Check if one number is tiny compared to the other
	e = ex - ey;
	if (e > PREC)
	{
		return (re);
	}
	if (e < -PREC)
	{
		return (im);
	}

	// Find approximate exponent e of the geometric mean.
	e = (ex + ey) >> 1;

	// Rescale so mean is about 1
	x = ldexp(re, -e);
	y = ldexp(im, -e);

	// Hypotenuse of the right triangle
	b = sqrt(x * x + y * y);

	// Compute the exponent of the answer.
	y = frexp(b, &ey);
	ey = e + ey;

	// Check it for overflow and underflow.
	if (ey > MAXEXP)
	{
		cout << "absCOMPLEX: OVERFLOW" << std::endl;
		return (std::numeric_limits<double>::infinity());
	}
	if (ey < MINEXP)
	{
		return (0.0);
	}

	// Undo the scaling
	b = ldexp(b, e);
	return (b);
}

// Compute sqrt of a complex number
void CComputeTemporalFilterCoefficients::sqrtComplex(cmplex* z, cmplex* w)
{
	cmplex q, s;
	double x, y, r, t;

	x = z->real;
	y = z->imag;

	if (y == 0.0)
	{
		if (x < 0.0)
		{
			w->real = 0.0;
			w->imag = sqrt(-x);
			return;
		}
		else
		{
			w->real = sqrt(x);
			w->imag = 0.0;
			return;
		}
	}

	if (x == 0.0)
	{
		r = fabs(y);
		r = sqrt(0.5 * r);
		if (y > 0)
		{
			w->real = r;
		}
		else
		{
			w->real = -r;
		}
		w->imag = r;
		return;
	}

	// Approximate  sqrt(x^2+y^2) - x  =  y^2/2x - y^4/24x^3 + ... .
	// The relative error in the first term is approximately y^2/12x^2 .
	if ((fabs(y) < 2.e-4 * fabs(x)) && (x > 0))
	{
		t = 0.25 * y * (y / x);
	}
	else
	{
		r = absComplex(z);
		t = 0.5 * (r - x);
	}
	r = sqrt(t);
	q.imag = r;
	q.real = y / (2.0 * r);
	// Heron iteration in complex arithmetic
	divComplex(&q, z, &s);
	addComplex(&q, &s, w);
	w->real *= 0.5;
	w->imag *= 0.5;
}

// compute s plane poles and zeros
void CComputeTemporalFilterCoefficients::findSPlanePolesAndZeros()
{
	m_ui32NbPoles = (m_ui32FilterOrder + 1) / 2;
	m_ui32NbZeros = 0;
	m_vecZs = zeros(m_ui32ArraySize);

	double l_dm;
	uint32_t l_ui32lr = 0;
	uint32_t l_ui32ir = 0;
	uint32_t l_ui32ii = 0;
	double l_db;
	double l_da;

	if (m_ui64FilterMethod == OVP_TypeId_FilterMethod_Butterworth)//poles equally spaced around the unit circle
	{
		if (m_ui32FilterOrder & 1)
		{
			l_dm = 0.0;
		}
		else
		{
			l_dm = pi / (2.0 * (double)m_ui32FilterOrder);
		}

		for (uint32_t i = 0; i < m_ui32NbPoles; i++)// poles
		{
			l_ui32lr = i + i;
			m_vecZs[l_ui32lr] = -cos(l_dm);
			m_vecZs[l_ui32lr + 1] = sin(l_dm);
			l_dm += pi / (double)m_ui32FilterOrder;
		}

		if (m_ui64FilterType == OVP_TypeId_FilterType_HighPass || m_ui64FilterType == OVP_TypeId_FilterType_BandStop) // high pass or band reject
		{
			// map s => 1/s
			for (uint32_t j = 0; j < m_ui32NbPoles; j++)
			{
				l_ui32ir = j + j;
				l_ui32ii = l_ui32ir + 1;
				l_db = m_vecZs[l_ui32ir] * m_vecZs[l_ui32ir] + m_vecZs[l_ui32ii] * m_vecZs[l_ui32ii];
				m_vecZs[l_ui32ir] = m_vecZs[l_ui32ir] / l_db;
				m_vecZs[l_ui32ii] = m_vecZs[l_ui32ii] / l_db;
			}

			// The zeros at infinity map to the origin.
			m_ui32NbZeros = m_ui32NbPoles;
			if (m_ui64FilterType == OVP_TypeId_FilterType_BandStop)
			{
				m_ui32NbZeros += m_ui32FilterOrder / 2;
			}
			for (uint32_t j = 0; j < m_ui32NbZeros; j++)
			{
				l_ui32ir = l_ui32ii + 1;
				l_ui32ii = l_ui32ir + 1;
				m_vecZs[l_ui32ir] = 0.0;
				m_vecZs[l_ui32ii] = 0.0;
			}
		}
	}

	if (m_ui64FilterMethod == OVP_TypeId_FilterMethod_Chebychev)
	{
		//For Chebyshev, find radii of two Butterworth circles (See Gold & Rader, page 60)
		m_Rho = (m_Phi - 1.0) * (m_Phi + 1);  // m_Rho = m_Eps^2 = {sqrt(1+m_Eps^2)}^2 - 1
		m_Eps = sqrt(m_Rho);
		// sqrt( 1 + 1/m_Eps^2 ) + 1/m_Eps  = {sqrt(1 + m_Eps^2)  +  1} / m_Eps
		m_Phi = (m_Phi + 1.0) / m_Eps;
		m_Phi = pow(m_Phi, (double)1.0 / m_ui32FilterOrder);  // raise to the 1/n power
		l_db = 0.5 * (m_Phi + 1.0 / m_Phi); // y coordinates are on this circle
		l_da = 0.5 * (m_Phi - 1.0 / m_Phi); // x coordinates are on this circle
		if (m_ui32FilterOrder & 1)
		{
			l_dm = 0.0;
		}
		else
		{
			l_dm = pi / (2.0 * (double)m_ui32FilterOrder);
		}

		for (uint32_t i = 0; i < m_ui32NbPoles; i++)// poles
		{
			l_ui32lr = i + i;
			m_vecZs[l_ui32lr] = -l_da * cos(l_dm);
			m_vecZs[l_ui32lr + 1] = l_db * sin(l_dm);
			l_dm += pi / (double)m_ui32FilterOrder;
		}

		if (m_ui64FilterType == OVP_TypeId_FilterType_HighPass || m_ui64FilterType == OVP_TypeId_FilterType_BandStop)// high pass or band reject
		{
			// map s => 1/s
			for (uint32_t j = 0; j < m_ui32NbPoles; j++)
			{
				l_ui32ir = j + j;
				l_ui32ii = l_ui32ir + 1;
				l_db = m_vecZs[l_ui32ir] * m_vecZs[l_ui32ir] + m_vecZs[l_ui32ii] * m_vecZs[l_ui32ii];
				m_vecZs[l_ui32ir] = m_vecZs[l_ui32ir] / l_db;
				m_vecZs[l_ui32ii] = m_vecZs[l_ui32ii] / l_db;
			}
			// The zeros at infinity map to the origin.
			m_ui32NbZeros = m_ui32NbPoles;
			if (m_ui64FilterType == OVP_TypeId_FilterType_BandStop)
			{
				m_ui32NbZeros += m_ui32FilterOrder / 2;
			}

			for (uint32_t j = 0; j < m_ui32NbZeros; j++)
			{
				l_ui32ir = l_ui32ii + 1;
				l_ui32ii = l_ui32ir + 1;
				m_vecZs[l_ui32ir] = 0.0;
				m_vecZs[l_ui32ii] = 0.0;
			}
		}
	}
}

//convert s plane poles and zeros to the z plane.
void CComputeTemporalFilterCoefficients::convertSPlanePolesAndZerosToZPlane()
{
	// Vars
	cmplex l_complexR, l_complexCnum, l_complexCden, l_complexCwc, l_complexCa, l_complexCb, l_complexB4ac;
	cmplex l_complexCone = { 1.0, 0.0 };
	cmplex* l_complexZ = new cmplex[m_ui32ArraySize];
	float64* l_dpp = new double[m_ui32ArraySize];
	float64* l_dy = new double[m_ui32ArraySize];
	float64* l_daa = new double[m_ui32ArraySize];

	float64 l_dC = 0.0;
	float64 l_da = 0.0;
	float64 l_db = 0.0;
	float64 l_dpn = 0.0;
	float64 l_dan = 0.0;
	float64 l_dgam = 0.0;
	float64 l_dai = 0.0;
	float64 l_dcng = 0.0;
	float64 l_dgain = 0.0;
	uint32_t l_ui32nc = 0;
	uint32_t l_ui32jt = 0;
	uint32_t l_ui32ii = 0;
	uint32_t l_ui32ir = 0;
	uint32 l_ui32jj = 0;
	uint32 l_ui32jh = 0;
	uint32 l_ui32jl = 0;
	uint32 l_ui32mh = 0;

	l_dC = m_TanAng;

	for (uint32_t i = 0; i < m_ui32ArraySize; i++)
	{
		l_complexZ[i].real = 0.0;
		l_complexZ[i].imag = 0.0;
	}

	l_ui32nc = m_ui32NbPoles;
	l_ui32jt = -1;
	l_ui32ii = -1;

	for (uint32_t icnt = 0; icnt < 2; icnt++)
	{
		do
		{
			l_ui32ir = l_ui32ii + 1;
			l_ui32ii = l_ui32ir + 1;

			l_complexR.real = m_vecZs[l_ui32ir];
			l_complexR.imag = m_vecZs[l_ui32ii];

			if (m_ui64FilterType == OVP_TypeId_FilterType_LowPass || m_ui64FilterType == OVP_TypeId_FilterType_HighPass)
			{
				// Substitute  s - r  =  s/wc - r = (1/wc)(z-1)/(z+1) - r
				//
				//     1  1 - r wc (       1 + r wc )
				// =  --- -------- ( z  -  -------- )
				//    z+1    wc    (       1 - r wc )
				//
				// giving the root in the z plane.
				l_complexCnum.real = 1 + l_dC * l_complexR.real;
				l_complexCnum.imag = l_dC * l_complexR.imag;
				l_complexCden.real = 1 - l_dC * l_complexR.real;
				l_complexCden.imag = -l_dC * l_complexR.imag;
				l_ui32jt += 1;
				divComplex(&l_complexCden, &l_complexCnum, &l_complexZ[l_ui32jt]);

				if (l_complexR.imag != 0.0)
				{
					// fill in complex conjugate root
					l_ui32jt += 1;
					l_complexZ[l_ui32jt].real = l_complexZ[l_ui32jt - 1].real;
					l_complexZ[l_ui32jt].imag = -l_complexZ[l_ui32jt - 1].imag;
				}
			}

			if (m_ui64FilterType == OVP_TypeId_FilterType_BandPass || m_ui64FilterType == OVP_TypeId_FilterType_BandStop)
			{
				// Substitute  s - r  =>  s/wc - r
				//
				//     z^2 - 2 z cgam + 1
				// =>  ------------------  -  r
				//         (z^2 + 1) wc
				//
				//         1
				// =  ------------  [ (1 - r wc) z^2  - 2 cgam z  +  1 + r wc ]
				//    (z^2 + 1) wc
				//
				// and solve for the roots in  the z plane.

				if (m_ui64FilterMethod == OVP_TypeId_FilterMethod_Chebychev)
				{
					l_complexCwc.real = m_cbp;
				}
				else
				{
					l_complexCwc.real = m_TanAng;
				}
				l_complexCwc.imag = 0.0;

				// r * wc //
				mulComplex(&l_complexR, &l_complexCwc, &l_complexCnum);
				// a = 1 - r wc //
				subComplex(&l_complexCnum, &l_complexCone, &l_complexCa);
				// 1 - (r wc)^2 //
				mulComplex(&l_complexCnum, &l_complexCnum, &l_complexB4ac);
				subComplex(&l_complexB4ac, &l_complexCone, &l_complexB4ac);
				// 4ac //
				l_complexB4ac.real *= 4.0;
				l_complexB4ac.imag *= 4.0;
				// b //
				l_complexCb.real = -2.0 * m_CosGam;
				l_complexCb.imag = 0.0;
				// b^2 //
				mulComplex(&l_complexCb, &l_complexCb, &l_complexCnum);
				// b^2 - 4ac//
				subComplex(&l_complexB4ac, &l_complexCnum, &l_complexB4ac);
				// sqrt() //
				sqrtComplex(&l_complexB4ac, &l_complexB4ac);
				// -b //
				l_complexCb.real = -l_complexCb.real;
				l_complexCb.imag = -l_complexCb.imag;
				// 2a //
				l_complexCa.real *= 2.0;
				l_complexCa.imag *= 2.0;
				// -b +sqrt(b^2-4ac) //
				addComplex(&l_complexB4ac, &l_complexCb, &l_complexCnum);
				// ... /2a //
				divComplex(&l_complexCa, &l_complexCnum, &l_complexCnum);
				l_ui32jt += 1;

				l_complexZ[l_ui32jt].real = l_complexCnum.real;
				l_complexZ[l_ui32jt].imag = l_complexCnum.imag;

				if (l_complexCnum.imag != 0.0)
				{
					l_ui32jt += 1;
					l_complexZ[l_ui32jt].real = l_complexCnum.real;
					l_complexZ[l_ui32jt].imag = -l_complexCnum.imag;
				}
				if ((l_complexR.imag != 0.0) || l_complexCnum.imag == 0.0)
				{
					// -b - sqrt( b^2 - 4ac) //
					subComplex(&l_complexB4ac, &l_complexCb, &l_complexCnum);
					// ... /2a //
					divComplex(&l_complexCa, &l_complexCnum, &l_complexCnum);

					l_ui32jt += 1;
					l_complexZ[l_ui32jt].real = l_complexCnum.real;
					l_complexZ[l_ui32jt].imag = l_complexCnum.imag;

					if (l_complexCnum.imag != 0.0)
					{
						l_ui32jt += 1;
						l_complexZ[l_ui32jt].real = l_complexCnum.real;
						l_complexZ[l_ui32jt].imag = -l_complexCnum.imag;
					}
				}
			}
		} while (--l_ui32nc > 0);

		if (icnt == 0)
		{
			m_ui32zord = l_ui32jt + 1;
			if (m_ui32NbZeros <= 0)
			{
				icnt = 2;
			}
		}
		l_ui32nc = m_ui32NbZeros;
	}

	// Generate the remaining zeros
	while (2 * m_ui32zord - 1 > l_ui32jt)
	{
		if (m_ui64FilterType != OVP_TypeId_FilterType_HighPass)
		{
			l_ui32jt += 1;
			l_complexZ[l_ui32jt].real = -1.0;
			l_complexZ[l_ui32jt].imag = 0.0;
		}

		if (m_ui64FilterType == OVP_TypeId_FilterType_BandPass || m_ui64FilterType == OVP_TypeId_FilterType_HighPass)
		{
			l_ui32jt += 1;
			l_complexZ[l_ui32jt].real = 1.0;
			l_complexZ[l_ui32jt].imag = 0.0;
		}
	}

	// Expand the poles and zeros into numerator and denominator polynomials
	for (uint32_t j = 0; j < m_ui32ArraySize; j++)
	{
		l_daa[j] = 0.0;
	}
	for (uint32_t icnt = 0; icnt < 2; icnt++)
	{
		for (uint32_t j = 0; j < m_ui32ArraySize; j++)
		{
			l_dpp[j] = 0.0;
			l_dy[j] = 0.0;
		}
		l_dpp[0] = 1.0;

		for (uint32_t j = 0; j < m_ui32zord; j++)
		{
			l_ui32jj = j;
			if (icnt)
			{
				l_ui32jj += m_ui32zord;
			}

			l_da = l_complexZ[l_ui32jj].real;
			l_db = l_complexZ[l_ui32jj].imag;
			for (uint32_t k = 0; k <= j; k++)
			{
				l_ui32jh = j - k;
				l_dpp[l_ui32jh + 1] = l_dpp[l_ui32jh + 1] - l_da * l_dpp[l_ui32jh] + l_db * l_dy[l_ui32jh];
				l_dy[l_ui32jh + 1] = l_dy[l_ui32jh + 1] - l_db * l_dpp[l_ui32jh] - l_da * l_dy[l_ui32jh];
			}
		}

		if (icnt == 0)
		{
			for (uint32_t j = 0; j <= m_ui32zord; j++)
			{
				l_daa[j] = l_dpp[j];
			}
		}
	}

	// Scale factors of the pole and zero polynomials
	if (m_ui64FilterType == OVP_TypeId_FilterType_HighPass)
	{
		l_da = -1.0;
	}
	else
	{
		l_da = 1.0;
	}

	if (m_ui64FilterType == OVP_TypeId_FilterType_HighPass || m_ui64FilterType == OVP_TypeId_FilterType_LowPass || m_ui64FilterType == OVP_TypeId_FilterType_BandStop)
	{
		l_dpn = 1.0;
		l_dan = 1.0;
		for (uint32_t j = 1; j <= m_ui32zord; j++)
		{
			l_dpn = l_da * l_dpn + l_dpp[j];
			l_dan = l_da * l_dan + l_daa[j];
		}
	}

	if (m_ui64FilterType == OVP_TypeId_FilterType_BandPass)
	{
		l_dgam = pi / 2.0 - asin(m_CosGam);  // = acos( cgam ) //
		l_ui32mh = m_ui32zord / 2;
		l_dpn = l_dpp[l_ui32mh];
		l_dan = l_daa[l_ui32mh];
		l_dai = 0.0;
		if (l_ui32mh > ((m_ui32zord / 4) * 2))
		{
			l_dai = 1.0;
			l_dpn = 0.0;
			l_dan = 0.0;
		}
		for (uint32 j = 1; j <= l_ui32mh; j++)
		{
			l_da = l_dgam * j - l_dai * pi / 2.0;
			l_dcng = cos(l_da);
			l_ui32jh = l_ui32mh + j;
			l_ui32jl = l_ui32mh - j;
			l_dpn = l_dpn + l_dcng * (l_dpp[l_ui32jh] + (1.0 - 2.0 * l_dai) * l_dpp[l_ui32jl]);
			l_dan = l_dan + l_dcng * (l_daa[l_ui32jh] + (1.0 - 2.0 * l_dai) * l_daa[l_ui32jl]);
		}
	}

	l_dgain = l_dan / (l_dpn * m_Scale);
	if (l_dpn == 0.0)
	{
		l_dgain = 1.0;
	}

	for (uint32_t j = 0; j <= m_ui32zord; j++)
	{
		l_dpp[j] = l_dgain * l_dpp[j];
	}

	for (uint32_t j = 0; j <= m_ui32zord; j++)
	{
		m_vecDenomCoefFilter[j] = l_dpp[j];
		m_vecNumCoefFilter[j] = l_daa[j];
	}

	delete [] l_complexZ;
	delete [] l_dpp;
	delete [] l_dy;
	delete [] l_daa;
}

bool CComputeTemporalFilterCoefficients::initialize()
{
	ip_ui64SamplingFrequency.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_SamplingFrequency));
	ip_ui64FilterMethod.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_FilterMethod));
	ip_ui64FilterType.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_FilterType));
	ip_ui64FilterOrder.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_FilterOrder));
	ip_f64LowCutFrequency.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_LowCutFrequency));
	ip_f64HighCutFrequency.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_HighCutFrequency));
	ip_f64BandPassRipple.initialize(getInputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputParameterId_BandPassRipple));

	op_pOutputMatrix.initialize(getOutputParameter(OVP_Algorithm_ComputeTemporalFilterCoefficients_OutputParameterId_Matrix));

	return true;
}

bool CComputeTemporalFilterCoefficients::uninitialize()
{
	op_pOutputMatrix.uninitialize();

	ip_f64BandPassRipple.uninitialize();
	ip_f64HighCutFrequency.uninitialize();
	ip_f64LowCutFrequency.uninitialize();
	ip_ui64FilterOrder.uninitialize();
	ip_ui64FilterType.uninitialize();
	ip_ui64FilterMethod.uninitialize();
	ip_ui64SamplingFrequency.uninitialize();

	return true;
}

// ________________________________________________________________________________________________________________
//

bool CComputeTemporalFilterCoefficients::process()
{
	if (isInputTriggerActive(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputTriggerId_Initialize))
	{
		m_ui32SamplingRate = (uint32_t)ip_ui64SamplingFrequency;
		m_ui64FilterMethod = ip_ui64FilterMethod;
		m_ui64FilterType = ip_ui64FilterType;
		m_ui32FilterOrder = (uint32_t)ip_ui64FilterOrder;
		m_LowPassBandEdge = ip_f64LowCutFrequency;
		m_HighPassBandEdge = ip_f64HighCutFrequency;
		m_PassBandRipple = ip_f64BandPassRipple;

		m_ui32ArraySize = 4 * m_ui32FilterOrder; // Maximum size of array involved in computation
	}

	if (isInputTriggerActive(OVP_Algorithm_ComputeTemporalFilterCoefficients_InputTriggerId_ComputeCoefficients))
	{
		if (m_ui64FilterMethod == OVP_TypeId_FilterMethod_Butterworth || m_ui64FilterMethod == OVP_TypeId_FilterMethod_Chebychev)
		{
			if (m_ui64FilterType == OVP_TypeId_FilterType_LowPass || m_ui64FilterType == OVP_TypeId_FilterType_HighPass)
			{
				m_ui32DimensionSize = m_ui32FilterOrder + 1;
				m_vecDenomCoefFilter = zeros(m_ui32DimensionSize);
				m_vecNumCoefFilter = zeros(m_ui32DimensionSize);
			}
			else
			{
				m_ui32DimensionSize = 2 * m_ui32FilterOrder + 1;
				m_vecDenomCoefFilter = zeros(m_ui32DimensionSize);
				m_vecNumCoefFilter = zeros(m_ui32DimensionSize);
			}

			if (m_ui64FilterMethod == OVP_TypeId_FilterMethod_Chebychev)
			{
				// For Chebyshev filter, ripples go from 1.0 to 1/sqrt(1+m_Eps^2)
				m_Phi = exp(0.5 * m_PassBandRipple / (10.0 / log(10.0)));

				if ((m_ui32FilterOrder & 1) == 0)
				{
					m_Scale = m_Phi;
				}
				else
				{
					m_Scale = 1.0;
				}
			}

			m_ui32NyquistFrequency = m_ui32SamplingRate / 2;

			//local variables
			float64 l_BandWidth = 0.0;
			float64 l_HighFrequencyEdge = 0.0;
			float64 l_Ang;
			float64 l_dCosAng;

			//locate edges
			if (m_ui64FilterType == OVP_TypeId_FilterType_LowPass)
			{
				m_LowPassBandEdge = 0.0;
			}
			if (m_ui64FilterType == OVP_TypeId_FilterType_HighPass)
			{
				l_BandWidth = m_HighPassBandEdge;
				l_HighFrequencyEdge = (double)m_ui32NyquistFrequency;
			}
			else
			{
				l_BandWidth = m_HighPassBandEdge - m_LowPassBandEdge;
				l_HighFrequencyEdge = m_HighPassBandEdge;
			}

			//convert to Frequency correspondence for bilinear transformation
			// Wanalog = tan( 2 pi Fdigital T / 2 )
			// where T = 1/fs
			l_Ang = (float64)l_BandWidth * pi / (double)m_ui32SamplingRate;
			l_dCosAng = cos(l_Ang);
			m_TanAng = sin(l_Ang) / l_dCosAng; // Wanalog

			// Transformation from low-pass to band-pass critical frequencies
			// Center frequency
			//                      cos( 1/2 (Whigh+Wlow) T )
			//  cos( Wcenter T ) =  -------------------------
			//                      cos( 1/2 (Whigh-Wlow) T )
			//
			// Band edges
			//            cos( Wcenter T) - cos( Wdigital T )
			//  Wanalog = -----------------------------------
			//                     sin( Wdigital T )

			l_HighFrequencyEdge = pi * (l_HighFrequencyEdge + m_LowPassBandEdge) / (double)m_ui32SamplingRate;
			m_CosGam = cos(l_HighFrequencyEdge) / l_dCosAng;
			l_HighFrequencyEdge = 2.0 * pi * m_HighPassBandEdge / (double)m_ui32SamplingRate;
			m_cbp = (m_CosGam - cos(l_HighFrequencyEdge)) / sin(l_HighFrequencyEdge);

			if (m_ui64FilterMethod == OVP_TypeId_FilterMethod_Butterworth)
			{
				m_Scale = 1.0;
			}

			findSPlanePolesAndZeros();
			convertSPlanePolesAndZerosToZPlane();
		}

		IMatrix* l_pOutputMatrix = op_pOutputMatrix;
		l_pOutputMatrix->setDimensionCount(2);						// Push m_vecDenomCoefFilter and m_vecNumCoefFilter as two column vectors
		l_pOutputMatrix->setDimensionSize(0, m_ui32DimensionSize);	// m_ui32Dimension rows
		l_pOutputMatrix->setDimensionSize(1, 2);						// 2 columns 

		double* l_pFilterCoefMatrix = l_pOutputMatrix->getBuffer();
		for (uint32_t i = 0; i < m_ui32DimensionSize; i++)
		{
			l_pFilterCoefMatrix[i] = m_vecDenomCoefFilter[i];
			l_pFilterCoefMatrix[m_ui32DimensionSize + i] = m_vecNumCoefFilter[i];
		}
	}

	return true;
}

#endif // TARGET_HAS_ThirdPartyITPP
