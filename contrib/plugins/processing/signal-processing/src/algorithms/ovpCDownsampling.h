#ifndef __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CDownsampling_H__
#define __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CDownsampling_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CDownsampling : virtual public OpenViBEToolkit::TAlgorithm<OpenViBE::Plugins::IAlgorithm>
		{
		public:

			void release() override { delete this; }
			bool initialize() override;
			bool uninitialize() override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TAlgorithm < OpenViBE::Plugins::IAlgorithm >, OVP_ClassId_Algorithm_Downsampling);

		protected:

			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SamplingFrequency;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64NewSamplingFrequency;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pSignalMatrix;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pSignalMatrix;
			double* m_f64LastValueOrigSignal;
			double m_f64LastTimeOrigSignal;
			double m_f64LastTimeNewSignal;
			bool m_bFirst;
		};

		class CDownsamplingDesc : virtual public OpenViBE::Plugins::IAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Downsampling"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("G. Gibert - E. Maby - P.E. Aguera"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Downsamples input signal."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Downsamples input signal to the new sampling rate chosen by user."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing Gpl/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_Downsampling; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CDownsampling(); }

			bool getAlgorithmPrototype(
				OpenViBE::Kernel::IAlgorithmProto& rAlgorithmProto) const override
			{
				rAlgorithmProto.addInputParameter(OVP_Algorithm_Downsampling_InputParameterId_SamplingFrequency, "Sampling frequency", OpenViBE::Kernel::ParameterType_UInteger);
				rAlgorithmProto.addInputParameter(OVP_Algorithm_Downsampling_InputParameterId_NewSamplingFrequency, "New sampling frequency", OpenViBE::Kernel::ParameterType_UInteger);

				rAlgorithmProto.addInputParameter(OVP_Algorithm_Downsampling_InputParameterId_SignalMatrix, "Signal matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addOutputParameter(OVP_Algorithm_Downsampling_OutputParameterId_SignalMatrix, "Signal matrix", OpenViBE::Kernel::ParameterType_Matrix);
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_Downsampling_InputTriggerId_Initialize, "Initialize");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_Downsampling_InputTriggerId_Resample, "Resample");
				rAlgorithmProto.addInputTrigger(OVP_Algorithm_Downsampling_InputTriggerId_ResampleWithHistoric, "Resample with historic");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IAlgorithmDesc, OVP_ClassId_Algorithm_DownsamplingDesc);
		};
	};
};

#endif // __OpenViBEPlugins_SignalProcessing_Algorithms_Basic_CDownsampling_H__
