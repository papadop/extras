// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#ifndef __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CDetectingMinMaxBoxAlgorithm_H__
#define __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CDetectingMinMaxBoxAlgorithm_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CDetectingMinMaxBoxAlgorithm : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_Box_DetectingMinMaxBoxAlgorithm)

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pDetectingMinMax;

			uint64_t m_ui64LastStartTime;
			uint64_t m_ui64LastEndTime;

			bool m_bMinFlag;
			bool m_bMaxFlag;
		};

		class CDetectingMinMaxBoxAlgorithmDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Min/Max detection"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Outputs the minimum or the maximum value inside a time window"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Either min or max detection can be specified as a box parameter"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString(""); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Box_DetectingMinMaxBoxAlgorithm; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CDetectingMinMaxBoxAlgorithm(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input epochs", OV_TypeId_Signal);
				rPrototype.addOutput("Output epochs", OV_TypeId_StreamedMatrix);
				rPrototype.addSetting("Min/Max", OVP_TypeId_MinMax, "Max");
				rPrototype.addSetting("Time window start", OV_TypeId_Float, "300");
				rPrototype.addSetting("Time window end", OV_TypeId_Float, "500");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_Box_DetectingMinMaxBoxAlgorithmDesc)
		};
	};
};

#endif // __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CDetectingMinMaxBoxAlgorithm_H__
