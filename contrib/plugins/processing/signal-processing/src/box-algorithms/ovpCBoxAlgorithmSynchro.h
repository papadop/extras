#ifndef __OpenViBEPlugins_BoxAlgorithm_Synchro_H__
#define __OpenViBEPlugins_BoxAlgorithm_Synchro_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "ovpCInputChannel.h"
#include "ovpCOutputChannel.h"

#include <string>
#include <vector>

#define OVP_ClassId_BoxAlgorithm_Synchro     OpenViBE::CIdentifier(0x7D8C1A18, 0x4C273A91)
#define OVP_ClassId_BoxAlgorithm_SynchroDesc OpenViBE::CIdentifier(0x4E806E5E, 0x5035290D)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmSynchro : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_Synchro);

		protected:

			//Intern ressources
			bool m_bStimulationReceivedStart;

			// new
			CInputChannel m_oCInputChannel;
			COutputChannel m_oCOutputChannel;
		};

		class CBoxAlgorithmSynchroDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Stream Synchronization"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Gelu Ionescu & Matthieu Goyat"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("GIPSA-lab"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Synchronize two acquisition servers"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_Synchro; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmSynchro; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Input stimulation", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput("Output signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Output stimulation", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addSetting("Synchronisation stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStart");
				// rBoxAlgorithmPrototype.addFlag   (OpenViBE::Kernel::BoxFlag_CanModifyInput);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_SynchroDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_Synchro_H__
