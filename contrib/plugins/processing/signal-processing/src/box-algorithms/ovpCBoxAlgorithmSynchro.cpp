#include "ovpCBoxAlgorithmSynchro.h"

#include <iostream>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace SignalProcessing;

bool CBoxAlgorithmSynchro::initialize()
{
	m_oCInputChannel.initialize(this);

	m_oCOutputChannel.initialize(this);

	m_bStimulationReceivedStart = false;

	return true;
}

bool CBoxAlgorithmSynchro::uninitialize()
{
	m_oCInputChannel.uninitialize();

	m_oCOutputChannel.uninitialize();

	return true;
}

bool CBoxAlgorithmSynchro::processInput(uint32_t inputIndex)
{
	this->getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmSynchro::process()
{
	// FIXME is it necessary to keep next line uncomment ?
	//IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

	if (m_oCInputChannel.isWorking())
	{
		// process stimulations
		for (uint32_t index = 0, nb = m_oCInputChannel.getNbOfStimulationBuffers(); index < nb; index++)
		{
			uint64_t l_u64StartTimestamp, l_u64EndTimestamp;
			IStimulationSet* l_pIStimulationSet = m_oCInputChannel.getStimulation(l_u64StartTimestamp, l_u64EndTimestamp, index);

			if (!l_pIStimulationSet)
				break;

			m_oCOutputChannel.sendStimulation(l_pIStimulationSet, l_u64StartTimestamp, l_u64EndTimestamp);
		}
		// process signal
		for (uint32_t index = 0, nb = m_oCInputChannel.getNbOfSignalBuffers(); index < nb; index++)
		{
			uint64_t l_u64StartTimestamp, l_u64EndTimestamp;
			CMatrix* l_pCMatrix = m_oCInputChannel.getSignal(l_u64StartTimestamp, l_u64EndTimestamp, index++);

			if (!l_pCMatrix)
				break;

			m_oCOutputChannel.sendSignal(l_pCMatrix, l_u64StartTimestamp, l_u64EndTimestamp);
		}
	}
	else if (m_oCInputChannel.hasSynchro())
	{
		m_oCOutputChannel.processSynchroSignal(m_oCInputChannel.getStimulationPosition(), m_oCInputChannel.getSignalPosition());
		m_oCInputChannel.startWorking();
	}
	else if (m_oCInputChannel.hasHeader())
		m_oCInputChannel.waitForSynchro();
	else
	{
		if (m_oCInputChannel.waitForSignalHeader())
		{
			m_oCOutputChannel.sendHeader(m_oCInputChannel.getSamplingRate(), m_oCInputChannel.getMatrixPtr());
		}
	}

	return true;
}
