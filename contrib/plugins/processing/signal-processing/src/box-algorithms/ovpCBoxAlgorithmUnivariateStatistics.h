#ifndef __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_CUnivariateStatistic_H__
#define __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_CUnivariateStatistic_H__

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_UnivariateStatistic     OpenViBE::CIdentifier(0x6118159D, 0x600C40B9)
#define OVP_ClassId_BoxAlgorithm_UnivariateStatisticDesc OpenViBE::CIdentifier(0x36F742D9, 0x6D1477B2)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxUnivariateStatistic : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_UnivariateStatistic)

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderMean;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderVariance;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderRange;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderMedian;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderIQR;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderPercentile;
			OpenViBE::Kernel::IAlgorithmProxy* m_pMatrixStatistic;

			OpenViBE::Kernel::TParameterHandler<double> op_fCompression;
			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64SamplingRate;

			OpenViBE::Kernel::TParameterHandler<bool> ip_bStatisticMeanActive;
			OpenViBE::Kernel::TParameterHandler<bool> ip_bStatisticVarianceActive;
			OpenViBE::Kernel::TParameterHandler<bool> ip_bStatisticRangeActive;
			OpenViBE::Kernel::TParameterHandler<bool> ip_bStatisticMedianActive;
			OpenViBE::Kernel::TParameterHandler<bool> ip_bStatisticIQRActive;
			OpenViBE::Kernel::TParameterHandler<bool> ip_bStatisticPercentileActive;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64StatisticParameterValue;

			OpenViBE::CIdentifier m_oInputTypeIdentifier;
		};

		class CBoxUnivariateStatisticDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Univariate Statistics"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Matthieu Goyat"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Gipsa-lab"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Mean, Variance, Median, etc. on the incoming Signal"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Statistics"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-missing-image"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_UnivariateStatistic; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxUnivariateStatistic(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input Signals", OV_TypeId_Signal);
				rPrototype.addOutput("Mean", OV_TypeId_Signal);
				rPrototype.addOutput("Variance", OV_TypeId_Signal);
				rPrototype.addOutput("Range", OV_TypeId_Signal);
				rPrototype.addOutput("Median", OV_TypeId_Signal);
				rPrototype.addOutput("IQR", OV_TypeId_Signal);
				rPrototype.addOutput("Percentile", OV_TypeId_Signal);
				rPrototype.addSetting("Mean", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Variance", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Range", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Median", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("IQR", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Percentile", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Percentile value", OV_TypeId_Float, "30");
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_UnivariateStatisticDesc)
		};
	};
};

#endif // __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_CUnivariateStatistic_H__
