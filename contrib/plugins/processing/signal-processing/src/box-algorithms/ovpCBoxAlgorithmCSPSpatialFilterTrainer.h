// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#ifndef __OpenViBEPlugins_BoxAlgorithm_CSPSpatialFilterTrainer_H__
#define __OpenViBEPlugins_BoxAlgorithm_CSPSpatialFilterTrainer_H__

#if defined TARGET_HAS_ThirdPartyITPP

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_CSPSpatialFilterTrainer     OpenViBE::CIdentifier(0x51DB0D64, 0x2109714E)
#define OVP_ClassId_BoxAlgorithm_CSPSpatialFilterTrainerDesc OpenViBE::CIdentifier(0x05120978, 0x14E061CD)


namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmCSPSpatialFilterTrainer : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CSPSpatialFilterTrainer);

		protected:

			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmCSPSpatialFilterTrainer>* m_pStimulationDecoder;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmCSPSpatialFilterTrainer>* m_pSignalDecoderCondition1;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmCSPSpatialFilterTrainer>* m_pSignalDecoderCondition2;


			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmCSPSpatialFilterTrainer> m_oStimulationEncoder;

			uint64_t m_ui64StimulationIdentifier;
			OpenViBE::CString m_sSpatialFilterConfigurationFilename;
			uint64_t m_ui64FilterDimension;
			bool m_bSaveAsBoxConfig;
		};

		class CBoxAlgorithmCSPSpatialFilterTrainerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("CSP Spatial Filter Trainer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Dieter Devlaminck"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Ghent University"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Computes spatial filter coeffcients according to the Common Spatial Pattern algorithm."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("The CSP algortihm increases the signal variance for one condition while minimizing the variance for the other condition."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Filtering"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString(""); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CSPSpatialFilterTrainer; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmCSPSpatialFilterTrainer; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput("Signal condition 1", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Signal condition 2", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addSetting("Train Trigger", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Session");
				rBoxAlgorithmPrototype.addSetting("Spatial filter configuration", OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Filter dimension", OV_TypeId_Integer, "2");
				rBoxAlgorithmPrototype.addSetting("Save as box config", OV_TypeId_Boolean, "true");

				rBoxAlgorithmPrototype.addOutput("Train-completed Flag", OV_TypeId_Stimulations);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CSPSpatialFilterTrainerDesc);
		};
	};
};

#endif // TARGET_HAS_ThirdPartyITPP

#endif // __OpenViBEPlugins_BoxAlgorithm_XDAWNSpatialFilterTrainer_H__
