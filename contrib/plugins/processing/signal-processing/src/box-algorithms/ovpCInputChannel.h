#ifndef __OpenViBEPlugins_InputChannel_H__
#define __OpenViBEPlugins_InputChannel_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define SET_BIT(bit)	(1 << bit)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CInputChannel
		{
		private:
			typedef enum
			{
				NOT_STARTED = 0,
				SIGNAL_HEADER_DETECTED = SET_BIT(0),
				STIMULATION_SYNCHRO_DETECTED = SET_BIT(1),
				SIGNAL_SYNCHRO_DETECTED = SET_BIT(2),
				IN_WORK = SET_BIT(3),
			} status_t;

			typedef enum
			{
				SIGNAL_CHANNEL,
				STIMULATION_CHANNEL,
			} channel_t;

		public:

			CInputChannel();
			~CInputChannel();
			bool initialize(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm);
			bool uninitialize();

			bool hasHeader() { return (m_ui32Status & SIGNAL_HEADER_DETECTED) != 0; }
			bool hasSynchro() { return hasSynchroStimulation() && hasSynchroSignal(); }
			bool isWorking() { return (m_ui32Status & IN_WORK) != 0; }
			bool waitForSignalHeader();
			void waitForSynchro();
			void startWorking() { m_ui32Status |= IN_WORK; }
			uint64_t getStimulationPosition() { return m_ui64TimeStimulationPosition; }
			uint64_t getSignalPosition() { return m_ui64TimeSignalPosition; }
			uint32_t getNbOfStimulationBuffers();
			uint32_t getNbOfSignalBuffers();
			OpenViBE::IStimulationSet* getStimulation(OpenViBE::uint64& startTimestamp, uint64_t& endTimestamp, const uint32_t stimulationIndex);
			OpenViBE::CMatrix* getSignal(OpenViBE::uint64& startTimestamp, uint64_t& endTimestamp, const uint32_t signalIndex);
			OpenViBE::CMatrix* getMatrixPtr();
			uint64_t getSamplingRate() { return op_ui64SamplingRateSignal; }

		private:
			bool hasSynchroStimulation() { return (m_ui32Status & STIMULATION_SYNCHRO_DETECTED) != 0; }
			bool hasSynchroSignal() { return (m_ui32Status & SIGNAL_SYNCHRO_DETECTED) != 0; }
			void waitForSynchroStimulation();
			void waitForSynchroSignal();
			void processSynchroSignal();
			OpenViBE::CMatrix* getMatrix();
			void copyData(const bool copyFirstBlock, uint64_t matrixIndex);

		protected:
			uint16_t m_ui32Status;
			OpenViBE::CMatrix* m_oMatrixBuffer[2];
			uint64_t m_ui64PtrMatrixIndex;
			uint64_t m_ui64SynchroStimulation;

			uint64_t m_ui64TimeStimulationPosition;
			uint64_t m_ui64TimeStampStartStimulation;
			uint64_t m_ui64TimeStampEndStimulation;
			bool m_bFirstStimulation;


			uint64_t m_ui64TimeSignalPosition;
			uint64_t m_ui64TimeStampStartSignal;
			uint64_t m_ui64TimeStampEndSignal;

			uint64_t m_ui64FirstBlock;
			uint64_t m_ui64SecondBlock;
			uint64_t m_ui64NbSamples;
			uint64_t m_ui64NbChannels;
			bool m_bFirstChunk;

			OpenViBE::IStimulationSet* m_oIStimulationSet;

			// parent memory
			OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>* m_pTBoxAlgorithm;
			
			// signal section
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoderSignal;

			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pMemoryBufferSignal;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> op_pMatrixSignal;
			OpenViBE::Kernel::TParameterHandler<uint64_t> op_ui64SamplingRateSignal;
			
			
			// stimulation section
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoderStimulation;

			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pMemoryBufferStimulation;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pStimulationSetStimulation;
		};
	};
};

#endif // __OpenViBEPlugins_InputChannel_H__
