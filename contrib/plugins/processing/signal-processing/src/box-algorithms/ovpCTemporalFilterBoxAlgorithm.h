// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#ifndef __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CTemporalFilterBoxAlgorithm_H__
#define __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CTemporalFilterBoxAlgorithm_H__

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CTemporalFilterBoxAlgorithm : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_Box_TemporalFilterBoxAlgorithm)

		protected:

			OpenViBEToolkit::TSignalDecoder<CTemporalFilterBoxAlgorithm>* m_pStreamDecoder;
			OpenViBEToolkit::TSignalEncoder<CTemporalFilterBoxAlgorithm>* m_pStreamEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pComputeTemporalFilterCoefficients;
			OpenViBE::Kernel::IAlgorithmProxy* m_pApplyTemporalFilter;

			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pMemoryBufferToDecode;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pEncodedMemoryBuffer;
			//			OpenViBE::uint64 m_ui64LastStartTime;
			uint64_t m_ui64LastEndTime;
		};

		class CTemporalFilterBoxAlgorithmDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Temporal Filter (INSERM contrib)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Applies temporal filtering on time signal"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("The user can choose among a variety of filter types to process the signal"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Temporal Filtering"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString(""); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Box_TemporalFilterBoxAlgorithm; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CTemporalFilterBoxAlgorithm(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input signal", OV_TypeId_Signal);
				rPrototype.addOutput("Filtered signal", OV_TypeId_Signal);
				rPrototype.addSetting("Filter method", OVP_TypeId_FilterMethod, OVP_TypeId_FilterMethod_Butterworth.toString());
				rPrototype.addSetting("Filter type", OVP_TypeId_FilterType, OVP_TypeId_FilterType_BandPass.toString());
				rPrototype.addSetting("Filter order", OV_TypeId_Integer, "4");
				rPrototype.addSetting("Low cut frequency (Hz)", OV_TypeId_Float, "29");
				rPrototype.addSetting("High cut frequency (Hz)", OV_TypeId_Float, "40");
				rPrototype.addSetting("Pass band ripple (dB)", OV_TypeId_Float, "0.5");
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_Box_TemporalFilterBoxAlgorithmDesc)
		};
	};
};

#endif // __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CTemporalFilterBoxAlgorithm_H__
