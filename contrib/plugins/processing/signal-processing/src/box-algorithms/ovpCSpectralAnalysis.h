// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#if defined TARGET_HAS_ThirdPartyITPP

#ifndef __OpenViBEPlugins_SignalProcessing_CSpectralAnalysis_H__
#define __OpenViBEPlugins_SignalProcessing_CSpectralAnalysis_H__

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

#include <vector>
#include <map>
#include <string>

#ifndef  CString2Boolean
#define CString2Boolean(string) (strcmp(string,"true"))?0:1
#endif

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/**
		* The Spectral Anlaysis plugin's main class.
		*/
		class CSpectralAnalysis : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CSpectralAnalysis();

			void release() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_SpectralAnalysis)


		public:

			//start time and end time of the last arrived chunk
			uint64_t m_ui64LastChunkStartTime;
			uint64_t m_ui64LastChunkEndTime;

			//codecs
			OpenViBEToolkit::TSignalDecoder<CSpectralAnalysis> m_oSignalDecoder;
			OpenViBEToolkit::TSpectrumEncoder<CSpectralAnalysis> m_vSpectrumEncoder[4];

			///number of channels
			uint32_t m_ui32ChannelCount;
			uint32_t m_ui32SamplingRate;
			uint32_t m_ui32FrequencyBandCount;
			uint32_t m_ui32SampleCount;

			uint32_t m_ui32HalfFFTSize;			// m_ui32SampleCount / 2 + 1;

			bool m_bAmplitudeSpectrum;
			bool m_bPhaseSpectrum;
			bool m_bRealPartSpectrum;
			bool m_bImagPartSpectrum;
		};

		class CSpectralAnalysisDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Spectral Analysis (FFT)(INSERM contrib)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Compute spectral analysis using Fast Fourier Transform"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Spectral Analysis"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_SpectralAnalysis; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CSpectralAnalysis(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input signal", OV_TypeId_Signal);

				rPrototype.addOutput("Amplitude", OV_TypeId_Spectrum);
				rPrototype.addOutput("Phase", OV_TypeId_Spectrum);
				rPrototype.addOutput("Real Part", OV_TypeId_Spectrum);
				rPrototype.addOutput("Imag Part", OV_TypeId_Spectrum);

				rPrototype.addSetting("Spectral components", OVP_TypeId_SpectralComponent, "Amplitude");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_SpectralAnalysisDesc)
		};
	}
}

#endif // __SamplePlugin_CSpectralAnalysis_H__

#endif // TARGET_HAS_ThirdPartyITPP
