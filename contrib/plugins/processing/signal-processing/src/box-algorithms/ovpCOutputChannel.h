#ifndef __OpenViBEPlugins_OutputChannel_H__
#define __OpenViBEPlugins_OutputChannel_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class COutputChannel
		{
		private:
			typedef enum
			{
				SIGNAL_CHANNEL,
				STIMULATION_CHANNEL,
			} channel_t;

		public:

			bool initialize(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm);
			bool uninitialize();

			void sendStimulation(OpenViBE::IStimulationSet* stimset, OpenViBE::uint64 startTimestamp, uint64_t endTimestamp);
			void sendSignal(OpenViBE::CMatrix* pMatrix, OpenViBE::uint64 startTimestamp, uint64_t endTimestamp);

			void sendHeader(uint64_t samplingRate, OpenViBE::CMatrix* pMatrix);
			void processSynchroSignal(OpenViBE::uint64 stimulationPosition, uint64_t signalPosition);

		protected:

			OpenViBE::CMatrix* m_oMatrixBuffer;

			uint64_t m_ui64TimeStimulationPosition;
			uint64_t m_ui64TimeSignalPosition;

			uint64_t m_ui64SamplingRate;

			// parent memory
			OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>* m_pTBoxAlgorithm;
			
			// signal section
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderSignal;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pMemoryBufferSignal;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> ip_pMatrixSignal;
			OpenViBE::Kernel::TParameterHandler<uint64_t> ip_ui64SamplingRateSignal;			
			
			// stimulation section
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoderStimulation;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pMemoryBufferStimulation;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> ip_pStimulationSetStimulation;
		};
	};
};

#endif // __OpenViBEPlugins_OutputChannel_H__
