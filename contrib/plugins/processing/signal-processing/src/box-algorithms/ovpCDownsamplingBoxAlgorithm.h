// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#ifndef __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CDownsamplingBoxAlgorithm_H__
#define __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CDownsamplingBoxAlgorithm_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CDownsamplingBoxAlgorithm : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_Box_DownsamplingBoxAlgorithm)

		protected:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pComputeTemporalFilterCoefficients;
			OpenViBE::Kernel::IAlgorithmProxy* m_pApplyTemporalFilter;
			OpenViBE::Kernel::IAlgorithmProxy* m_pDownsampling;

			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pMemoryBufferToDecode;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pEncodedMemoryBuffer;

			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> m_pInputSignal;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*> m_pOutputSignal;
			OpenViBE::IMatrix* m_pOutputSignalDescription;
			uint64_t m_ui64NewSamplingRate;
			OpenViBE::Kernel::TParameterHandler<uint64_t> m_ui64SamplingRate;

			uint64_t m_ui64LastEndTime;
			bool m_bFlagFirstTime;
			bool m_bWarned;
			uint64_t m_ui64LastBufferSize;
			uint64_t m_ui64CurrentBufferSize;
			uint64_t m_ui64SignalType;
		};

		class CDownsamplingBoxAlgorithmDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Downsampling"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("G. Gibert - E. Maby - P.E. Aguera"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM/U821"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Filters and downsamples input buffer."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("First, applies a low-pass (Butterworth or Chebychev) filter (frequency cut is 1/4, 1/3 or 1/2 of the new sampling rate) to input buffers of signal for anti-aliasing. Then, the input buffers of signal is downsampled."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Basic"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.01"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString(""); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_Box_DownsamplingBoxAlgorithm; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CDownsamplingBoxAlgorithm(); }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input signal", OV_TypeId_Signal);
				rPrototype.addOutput("Output signal", OV_TypeId_Signal);
				rPrototype.addSetting("New sampling rate (Hz)", OV_TypeId_Integer, "32");
				rPrototype.addSetting("Frequency cutoff ratio", OVP_TypeId_FrequencyCutOffRatio, "1/4");
				rPrototype.addSetting("Name of filter", OVP_TypeId_FilterMethod, "Butterworth");
				rPrototype.addSetting("Filter order", OV_TypeId_Integer, "4");
				rPrototype.addSetting("Pass band ripple (dB)", OV_TypeId_Float, "0.5");
				rPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				rPrototype.addFlag(OpenViBE::Kernel::BoxFlag_IsDeprecated);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_Box_DownsamplingBoxAlgorithmDesc)
		};
	};
};

#endif // __OpenViBEPlugins_SignalProcessing_BoxAlgorithms_Filter_CDownsamplingBoxAlgorithm_H__
