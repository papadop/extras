// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#ifndef __OpenViBEPlugins_SignalProcessing_CWindowingFunctions_H__
#define __OpenViBEPlugins_SignalProcessing_CWindowingFunctions_H__

#if defined TARGET_HAS_ThirdPartyITPP

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

#include <vector>
#include <map>
#include <string>

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		namespace WindowingFunctions
		{
			// Used to store information about the signal stream
			class CSignalDescription
			{
			public:

				CSignalDescription()
					: m_ui32StreamVersion(1)
					  , m_ui32SamplingRate(0)
					  , m_ui32ChannelCount(0)
					  , m_ui32SampleCount(0)
					  , m_ui32CurrentChannel(0)
					  , m_bReadyToSend(false) { }

			public:

				uint32_t m_ui32StreamVersion;
				uint32_t m_ui32SamplingRate;
				uint32_t m_ui32ChannelCount;
				uint32_t m_ui32SampleCount;
				std::vector<std::string> m_pChannelName;
				uint32_t m_ui32CurrentChannel;

				bool m_bReadyToSend;
			};
		}

		/**
		* The Window Anlaysis plugin's main class.
		*/
		class CWindowingFunctions : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CWindowingFunctions();

			void release() override;

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_WindowingFunctions)

		public:

			void setSampleBuffer(const double* pBuffer);

		public:

			//start time and end time of the last arrived chunk
			uint64_t m_ui64LastChunkStartTime;
			uint64_t m_ui64LastChunkEndTime;
			uint64_t m_ui64SamplesPerBuffer;
			uint64_t m_ui64ChannelCount;

			// Needed to write on the plugin output
			OpenViBEToolkit::TSignalDecoder<CWindowingFunctions>* m_pSignalDecoder;
			OpenViBEToolkit::TSignalEncoder<CWindowingFunctions>* m_pSignalEncoder;

			//! Structure containing information about the signal stream
			WindowingFunctions::CSignalDescription* m_pSignalDescription;

			//! Size of the matrix buffer (output signal)
			uint64_t m_ui64MatrixBufferSize;
			//! Output signal's matrix buffer
			double* m_pMatrixBuffer;

			uint64_t m_ui64WindowMethod;
		};

		class CWindowingFunctionsDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Windowing (INSERM contrib)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Apply a window to the signal buffer"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Windowing"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_WindowingFunctions; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CWindowingFunctions(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input signal", OV_TypeId_Signal);

				rPrototype.addOutput("Output signal", OV_TypeId_Signal);

				rPrototype.addSetting("Window method", OVP_TypeId_WindowMethod, "Hamming");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_WindowingFunctionsDesc)
		};
	}
}
#endif // TARGET_HAS_ThirdPartyITPP

#endif // __SamplePlugin_CWindowingFunctions_H__
