// @copyright notice: Possibly due to dependencies, this box used to be GPL before upgrade to AGPL3

#ifndef __OpenViBEPlugins_SignalProcessing_CFastICA_H__
#define __OpenViBEPlugins_SignalProcessing_CFastICA_H__

#if defined TARGET_HAS_ThirdPartyITPP

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>
#include <map>
#include <string>
 
// TODO create a member function to get rid of this
#ifndef  CString2Boolean
#define CString2Boolean(string) (strcmp(string,"true"))?0:1
#endif

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		/**
		* The FastICA plugin's main class.
		*/
		class CFastICA : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CFastICA();

			void release() override;

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(uint32_t inputIndex) override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithm, OVP_ClassId_FastICA)

		protected:

			virtual void computeICA();

			OpenViBEToolkit::TSignalDecoder<CFastICA> m_oDecoder;
			OpenViBEToolkit::TSignalEncoder<CFastICA> m_oEncoder;

		protected:

			double* m_pFifoBuffer;
			OpenViBE::CMatrix m_oDemixer;		// The estimated matrix W

			bool m_bTrained;
			bool m_bFileSaved;

			uint32_t m_ui32Buff_Size;
			uint32_t m_ui32Samp_Nb;
			uint32_t m_ui32Nb_ICs;
			uint32_t m_ui32Duration;
			uint32_t m_ui32NbRep_max;
			uint32_t m_ui32NbTune_max;
			OpenViBE::CString m_sSpatialFilterFilename;
			bool m_bSaveAsFile;
			bool m_bSetFineTune;
			double m_ui64Set_Mu;
			double m_ui64Epsilon;
			uint64_t m_ui64Non_Lin;
			uint64_t m_ui64Type;
			uint64_t m_ui64Mode;
		};

		class CFastICADesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }
			OpenViBE::CString getName() const override { return OpenViBE::CString("Independent Component Analysis (FastICA)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert / Jeff B."); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM / Independent"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Computes fast independent component analysis"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Signal processing/Independent component analysis"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.2"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_FastICA; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CFastICA(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Input signal", OV_TypeId_Signal);
				rPrototype.addOutput("Output signal", OV_TypeId_Signal);

				rPrototype.addSetting("Number of components to extract", OV_TypeId_Integer, "4");
				rPrototype.addSetting("Operating mode", OVP_TypeId_FastICA_OperatingMode, OVP_TypeId_FastICA_OperatingMode_ICA.toString());
				rPrototype.addSetting("Sample size (seconds) for estimation", OV_TypeId_Integer, "120");
				rPrototype.addSetting("Decomposition type", OVP_TypeId_FastICA_DecompositionType, OVP_TypeId_FastICA_DecompositionType_Symmetric.toString());
				rPrototype.addSetting("Max number of reps for the ICA convergence", OV_TypeId_Integer, "100000");
				rPrototype.addSetting("Fine tuning", OV_TypeId_Boolean, "true");
				rPrototype.addSetting("Max number of reps for the fine tuning", OV_TypeId_Integer, "100");
				rPrototype.addSetting("Used nonlinearity", OVP_TypeId_FastICA_Nonlinearity, OVP_TypeId_FastICA_Nonlinearity_TANH.toString());
				rPrototype.addSetting("Internal Mu parameter for FastICA", OV_TypeId_Float, "1.0");
				rPrototype.addSetting("Internal Epsilon parameter for FastICA", OV_TypeId_Float, "0.0001");
				rPrototype.addSetting("Spatial filter filename", OV_TypeId_Filename, "");
				rPrototype.addSetting("Save the spatial filter/demixing matrix", OV_TypeId_Boolean, "false");

				rPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_FastICADesc)
		};
	}
}
#endif // TARGET_HAS_ThirdPartyITPP

#endif // __SamplePlugin_CFastICA_H__
