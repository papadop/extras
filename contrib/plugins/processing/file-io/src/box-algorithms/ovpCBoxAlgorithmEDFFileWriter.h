#ifndef __OpenViBEPlugins_BoxAlgorithm_EDFFileWriter_H__
#define __OpenViBEPlugins_BoxAlgorithm_EDFFileWriter_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "edf/edflib.h"
#include <queue>
#include <deque>
#include <vector>


namespace OpenViBEPlugins
{
	namespace FileIO
	{
		typedef struct
		{
			double m_f64MinValue;
			double m_f64MaxValue;
		} SChannelInfo;


		class CBoxAlgorithmEDFFileWriter : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			//CBoxAlgorithmEDFFileWriter();
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_EDFFileWriter);

		protected:

			OpenViBEToolkit::TExperimentInformationDecoder<CBoxAlgorithmEDFFileWriter> m_ExperimentInformationDecoder;
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmEDFFileWriter> m_SignalDecoder;
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmEDFFileWriter> m_StimulationDecoder;

			OpenViBE::CString m_sFilename;
			bool m_bIsFileOpened;
			int m_iFileHandle;
			int m_iSampleFrequency;
			int m_iNumberOfChannels;
			int m_iNumberOfSamplesPerChunk;
			std::queue<double, std::deque<double>> m_vBuffer;
			//int * m_pTemporyBuffer;
			//int * m_pTemporyBufferToWrite;

			std::vector<SChannelInfo> m_oChannelInformation;
		};

		class CBoxAlgorithmEDFFileWriterDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("EDF File Writer"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Aurelien Van Langhenhove"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("CICIT Garches"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Writes experiment information, signal and stimulations in a EDF file"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString(""); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/EDF"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-save"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_EDFFileWriter; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmEDFFileWriter; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Experiment information", OV_TypeId_ExperimentInformation);
				rBoxAlgorithmPrototype.addInput("Signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Stimulations", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Filename", OV_TypeId_Filename, "record-[$core{date}-$core{time}].edf");

				//rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_IsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_EDFFileWriterDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_EDFFileWriter_H__
