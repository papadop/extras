/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu,

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriterGipsa_H__
#define __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriterGipsa_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <cstdlib>
#include <cstdio>
#include <fstream>

#include <boost/date_time/posix_time/posix_time.hpp>

#define OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsa     OpenViBE::CIdentifier(0x0C7E0BDE, 0x4EC90F95)
#define OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsaDesc OpenViBE::CIdentifier(0x0A77142C, 0x316B6E47)

namespace OpenViBEPlugins
{
	namespace FileIO
	{
		class CBoxAlgorithmBrainampFileWriterGipsa : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CBoxAlgorithmBrainampFileWriterGipsa();
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			bool writeHeaderFile(); //write the .vhdr file

			std::string getShortName(std::string fullpath); //of a file
			std::string FormatTime(boost::posix_time::ptime now);


			template <class type>
			bool saveBuffer(const type myDummy)
			{
				std::vector<type> output(m_pMatrix->getBufferElementCount());

				if (output.size() != m_pMatrix->getBufferElementCount())
					return false;

				uint32_t l_uint32_tChannelCount = m_pMatrix->getDimensionSize(0);
				uint32_t l_uint32_tSamplesPerChunk = m_pMatrix->getDimensionSize(1);
				double* input = m_pMatrix->getBuffer();

				for (uint32_t k = 0; k < l_uint32_tChannelCount; k++)
				{
					for (uint32_t j = 0; j < l_uint32_tSamplesPerChunk; j++)
					{
						uint32_t index = (k * l_uint32_tSamplesPerChunk) + j;
						output[j * l_uint32_tChannelCount + k] = type(input[index]);
					}
				}

				m_oDataFile.write((char*)&output[0], m_pMatrix->getBufferElementCount() * sizeof(type));

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsa);

		protected:

			enum EBinaryFormat
			{
				BinaryFormat_Integer16,
				BinaryFormat_UnsignedInteger16,
				BinaryFormat_Float32,
			};

			//input signal 1
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmBrainampFileWriterGipsa>* m_pStreamDecoder;
			OpenViBE::IMatrix* m_pMatrix;
			uint64_t m_ui64SamplingFrequency;
			
			//input stimulation 1 
			OpenViBEToolkit::TStimulationDecoder<CBoxAlgorithmBrainampFileWriterGipsa>* m_pStimulationDecoderTrigger;
			//OpenViBE::Kernel::TParameterHandler <const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			//OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulationSetTrigger;

			std::string m_sHeaderFilename;
			std::string m_sDataFilename;
			std::string m_sMarkerFilename;

			std::ofstream m_oHeaderFile;
			std::ofstream m_oDataFile;
			std::ofstream m_oMarkerFile;

			uint32_t m_ui32BinaryFormat;

			uint32_t m_uint32_tStimulationCounter;

			bool m_bIsVmrkHeaderFileWritten; //Is the beginning of the .vmrk (file with stimulations is written)
		};

		class CBoxAlgorithmBrainampFileWriterGipsaListener : public OpenViBEToolkit::TBoxListener<OpenViBE::Plugins::IBoxListener>
		{
		public:

			bool onInputTypeChanged(OpenViBE::Kernel::IBox& rBox, const uint32_t ui32Index) override
			{
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmBrainampFileWriterGipsaDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("BrainVision Format file writer (Gipsa)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Anton Andreev"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Gipsa-lab"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Writes signal in the Brainamp file format."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("You must select the location of the output header file .vhdr. The .eeg and .vmrk files will be created with the same name and in the same folder. Integer codes of OpenVibe stimulations are saved in the .vmrk file. OV Stimulation with code 233 will be saved as S233 on the .vmrk file."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("File reading and writing/BrainVision Format"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-save"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsa; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmBrainampFileWriterGipsa; }
			OpenViBE::Plugins::IBoxListener* createBoxListener() const override { return new CBoxAlgorithmBrainampFileWriterGipsaListener; }
			void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const override { delete pBoxListener; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Streamed matrix", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Input stimulation channel", OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Header filename", OV_TypeId_Filename, "record-[$core{date}-$core{time}].vhdr");
				rBoxAlgorithmPrototype.addSetting("Binary format", OVP_TypeId_BinaryFormat, "INT_16");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_BrainampFileWriterGipsaDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_BrainampFileWriterGipsa_H__
