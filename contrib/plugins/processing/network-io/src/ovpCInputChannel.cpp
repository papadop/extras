#include "ovpCInputChannel.h"

#include <iostream>
#include <system/ovCMemory.h>

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace SignalProcessing;

CInputChannel::CInputChannel(const uint16_t ui16InputIndex /*= 0*/)
	: m_ui32SignalChannel(ui16InputIndex * NB_CHANNELS + SIGNAL_CHANNEL)
	  , m_ui32StimulationChannel(ui16InputIndex * NB_CHANNELS + STIMULATION_CHANNEL) {}

CInputChannel::~CInputChannel() { }

bool CInputChannel::initialize(OpenViBEToolkit::TBoxAlgorithm<IBoxAlgorithm>* pTBoxAlgorithm)
{
	m_bIsWorking = false;

	m_ui64StartTimestamp = 0;
	m_ui64EndTimestamp = 0;

	m_oIStimulationSet = nullptr;
	m_pTBoxAlgorithm = pTBoxAlgorithm;

	m_pStreamDecoderSignal = new OpenViBEToolkit::TSignalDecoder<OpenViBEToolkit::TBoxAlgorithm<IBoxAlgorithm>>();
	m_pStreamDecoderSignal->initialize(*m_pTBoxAlgorithm, 0);

	m_pStreamDecoderStimulation = new OpenViBEToolkit::TStimulationDecoder<OpenViBEToolkit::TBoxAlgorithm<IBoxAlgorithm>>();
	m_pStreamDecoderStimulation->initialize(*m_pTBoxAlgorithm, 1);

	return true;
}

bool CInputChannel::uninitialize()
{
	m_pStreamDecoderStimulation->uninitialize();
	delete m_pStreamDecoderStimulation;

	m_pStreamDecoderSignal->uninitialize();
	delete m_pStreamDecoderSignal;

	return true;
}

bool CInputChannel::waitForSignalHeader()
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();

	if (l_rDynamicBoxContext.getInputChunkCount(m_ui32SignalChannel))
	{
		m_pStreamDecoderSignal->decode(0);

		if (m_pStreamDecoderSignal->isHeaderReceived())
		{
			m_bIsWorking = true;

			m_ui64StartTimestamp = l_rDynamicBoxContext.getInputChunkStartTime(m_ui32SignalChannel, 0);
			m_ui64EndTimestamp = l_rDynamicBoxContext.getInputChunkEndTime(m_ui32SignalChannel, 0);

			l_rDynamicBoxContext.markInputAsDeprecated(m_ui32SignalChannel, 0);

			return true;
		}
	}

	return false;
}

uint32_t CInputChannel::getNbOfStimulationBuffers()
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();

	return l_rDynamicBoxContext.getInputChunkCount(m_ui32StimulationChannel);
}

uint32_t CInputChannel::getNbOfSignalBuffers()
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();

	return l_rDynamicBoxContext.getInputChunkCount(m_ui32SignalChannel);
}

IStimulationSet* CInputChannel::getStimulation(uint64& startTimestamp, uint64_t& endTimestamp, const uint32_t stimulationIndex)
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();

	m_pStreamDecoderStimulation->decode(stimulationIndex);
	m_oIStimulationSet = m_pStreamDecoderStimulation->getOutputStimulationSet();

	startTimestamp = l_rDynamicBoxContext.getInputChunkStartTime(m_ui32StimulationChannel, stimulationIndex);
	endTimestamp = l_rDynamicBoxContext.getInputChunkEndTime(m_ui32StimulationChannel, stimulationIndex);

	l_rDynamicBoxContext.markInputAsDeprecated(m_ui32StimulationChannel, stimulationIndex);

	return m_oIStimulationSet;
}

IStimulationSet* CInputChannel::discardStimulation(const uint32_t stimulationIndex)
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();

	m_pStreamDecoderStimulation->decode(stimulationIndex);
	m_oIStimulationSet = m_pStreamDecoderStimulation->getOutputStimulationSet();

	l_rDynamicBoxContext.markInputAsDeprecated(m_ui32StimulationChannel, stimulationIndex);

	return m_oIStimulationSet;
}


double* CInputChannel::getSignal(uint64& startTimestamp, uint64_t& endTimestamp, const uint32_t signalIndex)
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();
	m_pStreamDecoderSignal->decode(signalIndex);
	if (!m_pStreamDecoderSignal->isBufferReceived())
		return nullptr;

	startTimestamp = l_rDynamicBoxContext.getInputChunkStartTime(m_ui32SignalChannel, signalIndex);
	endTimestamp = l_rDynamicBoxContext.getInputChunkEndTime(m_ui32SignalChannel, signalIndex);

	l_rDynamicBoxContext.markInputAsDeprecated(m_ui32SignalChannel, signalIndex);

	return m_pStreamDecoderSignal->getOutputMatrix()->getBuffer();
}


double* CInputChannel::discardSignal(const uint32_t signalIndex)
{
	IBoxIO& l_rDynamicBoxContext = m_pTBoxAlgorithm->getDynamicBoxContext();
	m_pStreamDecoderSignal->decode(signalIndex);
	if (!m_pStreamDecoderSignal->isBufferReceived())
		return nullptr;

	l_rDynamicBoxContext.markInputAsDeprecated(m_ui32SignalChannel, signalIndex);

	return m_pStreamDecoderSignal->getOutputMatrix()->getBuffer();
}

#if 0
void CInputChannel::copyData(const bool copyFirstBlock, uint64_t matrixIndex)
{
	OpenViBE::CMatrix*&    l_pMatrixBuffer = m_oMatrixBuffer[matrixIndex & 1];

	double*     l_pSrcData = m_pStreamDecoderSignal->getOutputMatrix()->getBuffer() + (copyFirstBlock ? 0 : m_ui64FirstBlock);
	double*     l_pDstData = l_pMatrixBuffer->getBuffer()  + (copyFirstBlock ? m_ui64SecondBlock : 0);
	uint64_t       l_ui64Size = (copyFirstBlock ? m_ui64FirstBlock : m_ui64SecondBlock)*sizeof(double);

	for(uint64_t i=0; i < m_ui64NbChannels; i++, l_pSrcData+=m_ui64NbSamples, l_pDstData+=m_ui64NbSamples)
	{
		System::Memory::copy(l_pDstData, l_pSrcData, size_t (l_ui64Size));
	}
}
#endif
