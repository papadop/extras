#ifndef __OpenViBEPlugins_BoxAlgorithm_OSCController_H__
#define __OpenViBEPlugins_BoxAlgorithm_OSCController_H__

#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "oscpkt.h"
#include "oscpkt_udp.h"

#define OVP_ClassId_BoxAlgorithm_OSCController OpenViBE::CIdentifier(0xC66F2F0C, 0x3BA5B424)
#define OVP_ClassId_BoxAlgorithm_OSCControllerDesc OpenViBE::CIdentifier(0xF7A35BD7, 0x6331C7D9)

namespace OpenViBEPlugins
{
	namespace NetworkIO
	{
		/**
		 * \class CBoxAlgorithmOSCController
		 * \author Ozan Caglayan (Galatasaray University)
		 * \date Thu May  8 20:57:24 2014
		 * \brief The class CBoxAlgorithmOSCController describes the box OSC Controller.
		 *
		 */
		class CBoxAlgorithmOSCController : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
				
			//Here is the different process callbacks possible
			// - On new input received (the most common behaviour for signal processing) :
			bool processInput(uint32_t inputIndex) override;

			bool process() override;
			
			// As we do with any class in openvibe, we use the macro below 
			// to associate this box to an unique identifier. 
			// The inheritance information is also made available, 
			// as we provide the superclass OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_OSCController);

		private:

			// Decodes the stream
			OpenViBE::Kernel::IAlgorithmProxy* m_pStreamDecoder;

			// UDP Socket (oscpkt_udp.h)
			oscpkt::UdpSocket m_oUdpSocket;
			
			// OSC Address to some device
			OpenViBE::CString m_sOSCAddress;
		};

		/**
		 * \class CBoxAlgorithmOSCControllerDesc
		 * \author Ozan Caglayan (Galatasaray University)
		 * \date Thu May  8 20:57:24 2014
		 * \brief Descriptor of the box OSC Controller.
		 *
		 */
		class CBoxAlgorithmOSCControllerDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("OSC Controller"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Ozan Caglayan"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Galatasaray University"); }
			// + Stimulation support & some code refactoring in v1.1 by Jussi T. Lindgren / Inria
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Sends OSC messages to an OSC controller"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("This box allows OpenViBE to send OSC (Open Sound Control) messages to an OSC server. See http://www.opensoundcontrol.org to learn about the OSC protocol and its use cases."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-network"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_OSCController; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmOSCController; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input",OV_TypeId_Signal);

				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addInputSupport(OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_CanModifyInput);

				rBoxAlgorithmPrototype.addSetting("OSC Server IP",OV_TypeId_String, "127.0.0.1");
				rBoxAlgorithmPrototype.addSetting("OSC Server Port",OV_TypeId_Integer, "9001");
				rBoxAlgorithmPrototype.addSetting("OSC Address",OV_TypeId_String, "/a/b/c");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_OSCControllerDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_OSCController_H__
