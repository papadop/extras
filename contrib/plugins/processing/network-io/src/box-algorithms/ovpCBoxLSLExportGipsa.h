#ifndef __OpenViBEPlugins_BoxAlgorithm_LSLExportGipsa_H__
#define __OpenViBEPlugins_BoxAlgorithm_LSLExportGipsa_H__

#if defined TARGET_HAS_ThirdPartyLSL

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <vector>

#include "../ovpCInputChannel.h"
#include <lsl_cpp.h>

#define OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsa 	 	OpenViBE::CIdentifier(0x591D2E94, 0x221C23AD)
#define OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsaDesc 	OpenViBE::CIdentifier(0x22AF11F5, 0x58F2787D)

namespace OpenViBEPlugins
{
	namespace NetworkIO
	{
		class CBoxAlgorithmLSLExportGipsa : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CBoxAlgorithmLSLExportGipsa();

			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsa);

		protected:

			int64_t m_i64DecimationFactor;
			uint64_t m_ui64OutputSamplingFrequency;
			OpenViBE::CString m_sStreamName;
			OpenViBE::CString m_sStreamType;

			SignalProcessing::CInputChannel m_oCInputChannel1;

			lsl::stream_outlet* m_outlet;
			std::vector<std::pair<float, uint64_t>> m_stims;//identifier,time
		};

		class CBoxAlgorithmLSLExportGipsaDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("LSL Export (Gipsa)"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Anton Andreev"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Gipsa-lab"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Streams signal outside OpenVibe using Lab Streaming Layer library"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("More on how to read the signal in your application: https://code.google.com/p/labstreaminglayer/"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Acquisition and network IO"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("1.0"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-connect"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsa; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmLSLExportGipsa; }

			bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addInput("Input stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addSetting("Stream name", OV_TypeId_String, "OpenViBE Stream");
				rBoxAlgorithmPrototype.addSetting("Stream type", OV_TypeId_String, "EEG");
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CBoxAlgorithmLSLExportGipsaDesc);
		};
	};
};

#endif

#endif // __OpenViBEPlugins_BoxAlgorithm_LSLExportGipsa_H__
