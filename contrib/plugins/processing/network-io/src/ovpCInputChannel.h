#ifndef __OpenViBEPlugins_Streaming_InputChannel_H__
#define __OpenViBEPlugins_Streaming_InputChannel_H__

// @author Gipsa-lab

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

/**
	Use this class to receive send and stimulations channels
*/

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CInputChannel
		{
		private:
			typedef enum
			{
				SIGNAL_CHANNEL,
				STIMULATION_CHANNEL,
				NB_CHANNELS,
			} channel_t;

		public:

			CInputChannel(const uint16_t ui16InputIndex = 0);
			~CInputChannel();
			bool initialize(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm);
			bool uninitialize();

			bool isLastChannel(const uint32_t inputIndex) { return inputIndex == m_ui32StimulationChannel; }
			bool isWorking() { return m_bIsWorking; }
			bool waitForSignalHeader();
			uint32_t getNbOfStimulationBuffers();
			uint32_t getNbOfSignalBuffers();
			OpenViBE::IStimulationSet* getStimulation(OpenViBE::uint64& startTimestamp, uint64_t& endTimestamp, const uint32_t stimulationIndex);
			OpenViBE::IStimulationSet* discardStimulation(const uint32_t stimulationIndex);
			double* getSignal(OpenViBE::uint64& startTimestamp, uint64_t& endTimestamp, const uint32_t signalIndex);
			double* discardSignal(const uint32_t signalIndex);
			uint64_t getSamplingRate() const { return m_pStreamDecoderSignal->getOutputSamplingRate(); }
			uint64_t getNbOfChannels() const { return m_pStreamDecoderSignal->getOutputMatrix()->getDimensionSize(0); }
			uint64_t getNbOfSamples() const { return m_pStreamDecoderSignal->getOutputMatrix()->getDimensionSize(1); }
			uint64_t getStartTimestamp() const { return m_ui64StartTimestamp; }
			uint64_t getEndTimestamp() const { return m_ui64EndTimestamp; }
			const char* getChannelName(uint32_t index) { return m_pStreamDecoderSignal->getOutputMatrix()->getDimensionLabel(0, index); }
			const OpenViBE::Kernel::TParameterHandler<OpenViBE::IMatrix*>& getOpMatrix() const { return m_pStreamDecoderSignal->getOutputMatrix(); }

		protected:
			uint32_t m_ui32SignalChannel;
			uint32_t m_ui32StimulationChannel;
			bool m_bIsWorking;

			uint64_t m_ui64StartTimestamp;
			uint64_t m_ui64EndTimestamp;

			OpenViBE::IStimulationSet* m_oIStimulationSet;

			// parent memory
			OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>* m_pTBoxAlgorithm;
			
			// signal section
			//OpenViBE::Kernel::IAlgorithmProxy*                                      m_pStreamDecoderSignal;
			OpenViBEToolkit::TSignalDecoder<OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>>* m_pStreamDecoderSignal;

			// stimulation section
			OpenViBEToolkit::TStimulationDecoder<OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>>* m_pStreamDecoderStimulation;
		};
	};
};

#endif // __OpenViBEPlugins_InputChannel_H__
