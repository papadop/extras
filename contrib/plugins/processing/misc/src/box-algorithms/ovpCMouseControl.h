#ifndef __OpenViBEPlugins_Tools_CMouseControl_H__
#define __OpenViBEPlugins_Tools_CMouseControl_H__

#include "../ovp_defines.h"

#include <toolkit/ovtk_all.h>

#include <vector>
#include <string>
#include <map>

#if defined TARGET_OS_Linux
	#include <X11/X.h>
	#include <X11/Xlib.h>
	#include <X11/Xutil.h>
#endif

namespace OpenViBEPlugins
{
	namespace Tools
	{
		class CMouseControl : public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:

			CMouseControl();

			void release() override;
			bool initialize() override;
			bool uninitialize() override;
			bool processInput(uint32_t inputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>, OVP_ClassId_MouseControl)

		protected:

			//codec
			OpenViBEToolkit::TStreamedMatrixDecoder<CMouseControl>* m_pStreamedMatrixDecoder;

#if defined TARGET_OS_Linux
			::Display* m_pMainDisplay;
			::Window m_oRootWindow;
#endif
		};

		class CMouseControlDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			OpenViBE::CString getName() const override { return OpenViBE::CString("Mouse Control"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Guillaume Gibert"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("INSERM"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Mouse Control for Feedback"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Experimental box to move the mouse in x direction with respect to the input value. Only implemented on Linux."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Tools"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			void release() override { }
			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_MouseControl; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CMouseControl(); }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rPrototype) const override
			{
				rPrototype.addInput("Amplitude", OV_TypeId_StreamedMatrix);
				rPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				rPrototype.addInputSupport(OV_TypeId_StreamedMatrix);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_MouseControlDesc)
		};
	};
};

#endif
