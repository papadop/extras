#pragma once
/*
#include "openeeg-modulareeg/src/ovasCDriverOpenEEGModularEEG.h"
#include "field-trip-protocol/src/ovasCDriverFieldtrip.h"
#include "brainproducts-brainvisionrecorder/src/ovasCDriverBrainProductsBrainVisionRecorder.h"
*/
#include "ovasCPluginExternalStimulations.h"
#include "ovasCPluginTCPTagging.h"

#include "ovasCDriverBrainmasterDiscovery.h"
#include "ovasCDriverBrainProductsBrainVisionRecorder.h"
#include "ovasCDriverCognionics.h"
#include "ovasCDriverCtfVsmMeg.h"
#include "ovasCDriverEncephalan.h"
#include "ovasCDriverGTecGUSBamp.h"
#include "ovasCDriverGTecGUSBampLegacy.h"
#include "ovasCDriverGTecGUSBampLinux.h"
#include "ovasCDriverGTecGMobiLabPlus.h"
#include "ovasCDrivergNautilusInterface.h"
#include "ovasCDriverMBTSmarting.h"
#include "ovasCDriverMitsarEEG202A.h"
#include "ovasCDriverOpenALAudioCapture.h"
#include "ovasCDriverOpenEEGModularEEG.h"
#include "ovasCDriverOpenBCI.h"
#include "ovasCDriverEEGO.h"

#if !defined(WIN32) || defined(TARGET_PLATFORM_i386)
#include "ovasCDriverFieldtrip.h"
#endif

namespace OpenViBEContributions
{
	inline void InitiateContributions(OpenViBEAcquisitionServer::CAcquisitionServerGUI* pGUI, OpenViBEAcquisitionServer::CAcquisitionServer* pAS, const OpenViBE::Kernel::IKernelContext& context, std::vector<OpenViBEAcquisitionServer::IDriver*>* drivers)
	{
		//No Limitations
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverBrainProductsBrainVisionRecorder(pAS->getDriverContext()));
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverCtfVsmMeg(pAS->getDriverContext()));
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverMBTSmarting(pAS->getDriverContext()));
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverOpenEEGModularEEG(pAS->getDriverContext()));
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverOpenBCI(pAS->getDriverContext()));

		//OS Limitations
#if defined WIN32
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverCognionics(pAS->getDriverContext()));
#endif
#if defined TARGET_OS_Windows
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverEncephalan(pAS->getDriverContext()));
#endif
#if defined TARGET_HAS_PThread
#if !defined(WIN32) || defined(TARGET_PLATFORM_i386)
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverFieldtrip(pAS->getDriverContext()));
#endif
#endif
		//Commercial Limitations
#if defined TARGET_HAS_ThirdPartyGUSBampCAPI
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverGTecGUSBamp(pAS->getDriverContext()));
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverGTecGUSBampLegacy(pAS->getDriverContext()));
#endif
#if defined TARGET_HAS_ThirdPartyGUSBampCAPI_Linux
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverGTecGUSBampLinux(pAS->getDriverContext()));
#endif
#if defined TARGET_HAS_ThirdPartyGMobiLabPlusAPI
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverGTecGMobiLabPlus(pAS->getDriverContext()));
#endif
#if defined TARGET_HAS_ThirdPartyGNEEDaccessAPI
		drivers->push_back(new OpenViBEAcquisitionServer::CDrivergNautilusInterface(pAS->getDriverContext()));
#endif
#if defined TARGET_HAS_ThirdPartyBrainmasterCodeMakerAPI
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverBrainmasterDiscovery(pAS->getDriverContext()));
#endif
#if defined(TARGET_HAS_ThirdPartyMitsar)
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverMitsarEEG202A(pAS->getDriverContext()));
#endif
#if defined TARGET_HAS_ThirdPartyOpenAL
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverOpenALAudioCapture(pAS->getDriverContext()));
#endif
#if defined(TARGET_HAS_ThirdPartyEEGOAPI)
		drivers->push_back(new OpenViBEAcquisitionServer::CDriverEEGO(pAS->getDriverContext()));
#endif

		pGUI->registerPlugin(new OpenViBEAcquisitionServer::OpenViBEAcquisitionServerPlugins::CPluginExternalStimulations(context));
		pGUI->registerPlugin(new OpenViBEAcquisitionServer::OpenViBEAcquisitionServerPlugins::CPluginTCPTagging(context));
	}
}
