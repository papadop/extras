build/windows/boost_1.58_x64_vc120_dev.zip;boost;1.58
build/windows/cegui_0.8.4_x64_vc120_dev.zip;cegui;0.8.4
build/windows/cegui_0.8.4_x64_vc120_runtime.zip;cegui;0.8.4
build/windows/cmake-3.4.0.zip;cmake;3.4.0
build/windows/eigen-3.2.0.zip;eigen;3.2.0
build/windows/freealut_1.1.0_x64_vc120.zip;freealut;1.1.0
build/windows/gtk_2.24.31_x64_vc120_dev.zip;gtk;2.24.31
build/windows/gtk_2.24.31_x64_vc120_runtime.zip;gtk;2.24.31
build/windows/itpp_4.3.1_x64_vc120.zip;itpp;4.3.1
build/windows/liblsl_1.12_x64.zip;liblsl;1.12
build/windows/libogg_1.2.1_x64_vc120.zip;libogg;1.2.1
build/windows/libvorbis_1.3.2_x64_vc120.zip;libvorbis;1.3.2
build/windows/lua_5.1.4_x64_vc110.zip;lua;5.1.4
build/windows/ninja-1.4.0.zip;ninja;1.4.0
build/windows/nsis_log_zip_access_2.51.zip;nsis_log_zip_access;2.51
build/windows/ogre_1.9.0_x64_vc120_dev.zip;ogre;1.9.0
build/windows/ogre_1.9.0_x64_vc120_runtime.zip;ogre;1.9.0
build/windows/openal_1.1_x64.zip;openal;1.1
build/windows/pthread_2.9.1_x64_vc120_dev.zip;pthread;2.9.1
build/windows/sdk_brainproducts_actichamp_x64.zip;sdk_brainproducts_actichamp;0.0.0
build/windows/sdk_brainproducts_liveamp_x64.zip;sdk_brainproducts_liveamp;0.0.0
build/windows/sdk_eemagine_eego_1.3.19.40453_x64.zip;sdk_eemagine_eego;1.3.19.40453
build/windows/sdk_micromed_x64_vc120.zip;sdk_micromed;0.0.0
build/windows/sdk_tmsi_x64.zip;sdk_tmsi;0.0.0
build/windows/vcredist_x64_vc120.zip;vcredist;1.0
build/windows/vcredist_x64_vc100.zip;vcredist_100;1.0
build/windows/vcredist_x64_vc110.zip;vcredist_110;1.0
build/windows/vrpn_7.31_x64_vc120_dev.zip;vrpn;7.31
build/windows/vrpn_7.31_x64_vc120_runtime.zip;vrpn;7.31
